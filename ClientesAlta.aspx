<%@ Page Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="ClientesAlta.aspx.cs" Inherits="ClientesAlta" %>

<asp:Content ID="TicketPage" ContentPlaceHolderID="cphTicket" runat="server">
    <asp:Panel ID="panelTicket" runat="server">
        <div style="text-align: center">
            <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 100%; border-bottom: black 1px solid">
                <tr>
                    <td align="center">
                            <table cellpadding="2" cellspacing="0" style="width: 100%" border="0" >
                                <tr>
                                    <td align="right" style="width: 5%">
                                        C�digo</td>
                                    <td align="left" style="width: 44%">
                                        <asp:TextBox ID="txtCodigo" readonly="true" runat="server" CssClass="TextoTextBoxReadonly"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Raz�n Social</td>
                                    <td align="left" style="width: 44%; font-size: medium;">
                                        <asp:TextBox ID="txtRazonSocial" runat="server" Width="74%" MaxLength="60"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvRazonSocial" runat="server" 
                                            ControlToValidate="txtRazonSocial" ErrorMessage="Debe ingresar la Raz�n Social" 
                                            Font-Size="Medium" Visible="False">*</asp:RequiredFieldValidator>
                                        <asp:Label ID="lblV3" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Fantas�a</td>
                                    <td align="left" style="width: 44%">
                                        <asp:TextBox ID="txtFantasia" runat="server" Width="74%" MaxLength="40"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Categor�a IVA</td>
                                    <td align="left" style="width: 44%">
                                        <telerik:RadComboBox ZIndex=10000 ID="ddlCategoriaIva" Runat="server" AutoPostBack="True" 
                                            Culture="es-ES" DataSourceID="odsCategoriaIva" DataTextField="Nombre" 
                                            DataValueField="Codigo" HighlightTemplatedItems="True" 
                                            onselectedindexchanged="ddLocalidad_SelectedIndexChanged" Skin="Windows7" 
                                            Width="200px">
                                        </telerik:RadComboBox>
                                        <asp:ObjectDataSource ID="odsCategoriaIva" Runat="server" 
                                        TypeName="App_Code.TicketsLib.TablasCombo"
                                        SelectMethod="getComboCategoriaIVA" />
                                        
                                        <td align="right" style="width: 5%">
                                        Documento</td>
                                    <td align="left" style="width: 21%">
                                        <asp:TextBox ID="txtDocumento" runat="server" MaxLength="15" Width="169px"></asp:TextBox>
                                        </td>

                                    </td>
                                </tr>
                            </table>
                            <asp:Panel runat=server ID=panelDatosSugeridos Visible=false>
                             <table width="100%" style="border-style: none; border-collapse: collapse; border-spacing: 0px; background-color: #D3E1F1;">
                                <tr><td colspan=4 align="center" 
                                        style="font-weight: bold; background-color: #395271; color: #FFFFFF;">Datos 
                                    sugeridos por el sistema</td></tr>
                                <tr><td width="10%">&nbsp;</td>
                                    <td style="width: 336px">
                                        &nbsp;<asp:Label ID="lblDomicilio0" runat="server" Font-Bold="True" 
                                            ForeColor="#333333">Domicilio:</asp:Label>
                                        &nbsp;<asp:Label ID="lblDomicilio" runat="server" Font-Bold="True" 
                                            ForeColor="#395271"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCP0" runat="server" Font-Bold="True" ForeColor="#333333">C�digo Postal</asp:Label>
&nbsp;<asp:Label runat=server ID=lblCP Font-Bold="True" ForeColor="#395271"></asp:Label></td>
                                    <td width="10%">
                                        &nbsp;</td>
                                 </tr>
                                <tr><td width="10%">
                                    &nbsp;</td>
                                    <td style="width: 336px">
                                        <asp:Label ID="lblProvincia0" runat="server" Font-Bold="True" 
                                            ForeColor="#333333">Provincia:</asp:Label>
                                        &nbsp;<asp:Label ID="lblProvincia" runat="server" Font-Bold="True" 
                                            ForeColor="#395271"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLocalidad0" runat="server" Font-Bold="True" 
                                            ForeColor="#333333">Localidad:</asp:Label>
                                        <asp:Label runat=server ID=lblLocalidad Font-Bold="True" ForeColor="#395271"></asp:Label></td>
                                    <td width="10%">
                                        &nbsp;</td>
                                 </tr>
                                <tr><td colspan=4 align="center" 
                                        style="font-weight: bold; color: #FFFFFF; background-color: #993333;">
                                    La informaci�n sugerida puede contener errores. Por favor, verifique los datos 
                                    con el cliente.</td></tr>
                                 <tr>
                                     <td align="center">
                                         &nbsp;</td>
                                     <td align="center" colspan="2">
                                         <asp:Button ID="btInfoTelOk" runat="server" Height="23px" 
                                             onclick="btInfoTelOk_Click" Text="Utilizar estos datos" 
                                             CssClass="btnSubmit" />
                                         &nbsp;
                                         <asp:Button ID="btInfoTelOk0" runat="server" Height="23px" 
                                             onclick="btInfoTelOk0_Click" Text="Cancelar" />
                                     </td>
                                     <td align="center">
                                         &nbsp;</td>
                                 </tr>
                             </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlNoHayDatos" runat="server" Visible="False">
                                <table style="border-style: none; border-collapse: collapse; border-spacing: 0px; background-color: #D3E1F1;" 
                                    width="100%">
                                    <tr>
                                        <td align="center" colspan="2" 
                                            style="font-weight: bold; background-color: #395271; color: #FFFFFF;">
                                            Datos sugeridos por el sistema</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #293747; background-color: #D3E1F1">
                                            No se han encontrado datos sobre el tel�fono ingresado.</td>
                                    </tr>
                                </table>
                            </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" >
                                <tr>
                                    <td align="right" width="15%">
                                        Domicilio</td>
                                    <td align="left"colspan="6">
                                        <asp:TextBox ID="txtDomicilio" runat="server" Width="60%" MaxLength="40"></asp:TextBox>
                                    </td>
                                </tr>                             
                                <tr>
                                    <td align="right" width="15%">
                                        Provincia</td>
                                    <td align="left" colspan="2">
                                        <telerik:RadComboBox ZIndex=10000 ID="ddProvincia" Runat="server" AutoPostBack="True" 
                                            Culture="es-ES" DataSourceID="odsProvincia" DataTextField="Nombre" 
                                            DataValueField="Codigo" Filter="Contains" HighlightTemplatedItems="True" 
                                            onselectedindexchanged="ddProvincia_SelectedIndexChanged" Skin="Windows7" 
                                            Width="250px">
                                        </telerik:RadComboBox>
                                        <asp:Label ID="lblV0" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*"></asp:Label>
                                        <asp:ObjectDataSource ID="odsProvincia" Runat="server" 
                                            SelectMethod="getComboProvincia" TypeName="App_Code.TicketsLib.TablasCombo" />
                                    </td>
                                    <td align="right" colspan="2">
                                        <asp:Label ID="lblPartido" runat="server" Text="Partido" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" colspan="2">
                                        <telerik:RadComboBox ZIndex=10000 ID="ddPartido" Runat="server" AutoPostBack="True" 
                                            Filter="Contains" HighlightTemplatedItems="True" 
                                            onselectedindexchanged="ddPartido_SelectedIndexChanged" Skin="Windows7" 
                                            Visible="False" Width="250px">
                                        </telerik:RadComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="15%">
                                        Localidad</td>
                                    <td align="left" nowrap="nowrap">
                                        <telerik:RadComboBox ZIndex=10000 ID="ddLocalidad" Runat="server" AutoPostBack="True" 
                                            Filter="Contains" HighlightTemplatedItems="True" 
                                            onselectedindexchanged="ddLocalidad_SelectedIndexChanged" Skin="Windows7" 
                                            Width="250px">
                                        </telerik:RadComboBox>
                                        <asp:Label ID="lblV4" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtLocalidad" runat="server" MaxLength="40" Visible="False"></asp:TextBox>
                                    </td>
                                    <td align="left" colspan="2">
                                        Cod. Postal</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCodPostal" runat="server" MaxLength="8"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr>
                                    <td align="right">
                                        Tel�fono</td>
                                    <td align="left" colspan="2" style="width: 33%" nowrap="nowrap">
                                        <telerik:RadTextBox ID="txtTelefono" runat="server" 
                                            EmptyMessage="Ingrese el n�mero y presione [Enter]"
                                            OnTextChanged="txtTelefono_TextChanged1" Width="210px" AutoPostBack="True" 
                                            CssClass="onChangeSubmit">
                                            <EmptyMessageStyle BackColor="#D5EAFF" BorderColor="#005995" 
                                                BorderStyle="Solid" Font-Bold="True" ForeColor="#1D4583" />
                                            <FocusedStyle BorderColor="#6986A3" BorderStyle="Solid" />
                                            <HoveredStyle BorderColor="#6986A3" BorderStyle="Solid" ForeColor="#666666" />
                                            <EnabledStyle BorderColor="#6986A3" BorderStyle="Solid" />
                                        </telerik:RadTextBox>

                                        <asp:Label ID="lblV2" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblErrTel" runat="server" ForeColor="Maroon"></asp:Label>
                                        &nbsp;
                                        
                                        <asp:LinkButton ID="lnkCliente" runat="server" 
                                            ToolTip="Click aqu� para acceder al cliente."></asp:LinkButton>
                                    </td>
                                    <td align="right" colspan="2" style="width: 5%">
                                        Celular</td>
                                    <td align="left" width="25%" colspan="2">
                                        <asp:TextBox ID="txtFax" runat="server" MaxLength="30" Width="70%"></asp:TextBox>
                                        <asp:Label ID="lblV1" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                </tr>  
                                <tr>
                                    <td align="right" width="15%">
                                        e-Mail</td>
                                    <td align="left"colspan="6">
                                        <asp:TextBox ID="txtEmail" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                                        
                                        </td>
                                </tr>                                    
                                <tr>
                                    <td align="right" width="15%">
                                        Observaciones</td>
                                    <td align="left"colspan="6">
                                        <asp:TextBox ID="txtObservaciones" runat="server" Width="80%" MaxLength="100" Rows="5" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>                                                                                              
                            </table>
                    </td>
                </tr>
                 
            
                <tr>
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" 
                            Text="Ingresar" CssClass="btnSubmit" />
                        <br />
                        <asp:LinkButton ID="lnkClienteExistente" runat="server" 
                            onclick="lnkClienteExistente_Click"></asp:LinkButton>
                        <asp:Label ID="idClienteViejo" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="nomClienteViejo" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
                 <tr>
                    <td align="left" >
                        <asp:Label ID="lblError" runat="server" CssClass="labelError"></asp:Label>
                        <asp:ValidationSummary ID="validSummary" runat="server" />
                    </td>
                        
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelMensaje" runat="server">
        <table>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblMensajePanel" runat="server" CssClass="labelError"></asp:Label>
                </td>        
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
