﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="cargacoaching.aspx.vb" Inherits="cargacoaching" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
 
  <div style="width: 100%" align="center"> 
 <h2 align="center">Monitoreo</h2>

      <table runat="server" id="t1" align="center" style="width: 684px" 
          caption="Monitoreo" title="Monitoreo" >
      <tr>
      <td align="right">Operador: 
      </td>
      <td align="left" colspan="3"><asp:Label ID="lbloper" runat="server" Text=""></asp:Label>
          <asp:Label ID="lbloperName" runat="server"></asp:Label></td>
      </tr>
      <tr>
      <td align="right">Fecha</td>
       <td align="left"> <asp:Label ID="lblfecha" runat="server" Text=""></asp:Label></td>
        <td align="left">TKT</td>
       <td align="left">  
           <telerik:RadTextBox ID="txttkt" runat="server" 
               EmptyMessage="Ingrese el nro. TKT" AutoPostBack="True">
          </telerik:RadTextBox> <asp:CheckBox ID="Chkexperiencia" Text="Experiencia" 
               runat="server" AutoPostBack="True" /></td>
      </tr>
      <tr>
      <td align="right">
      Modalidad:&nbsp;
      </td>
      <td align="left">
          <telerik:RadComboBox ZIndex=10000 ID="ddlmodo" runat="server">
              <Items>
                  <telerik:RadComboBoxItem runat="server" Text="Grabación" Value="Grabacion" />
                  <telerik:RadComboBoxItem runat="server" Text="Auditoria" Value="Auditoria" />
                  <telerik:RadComboBoxItem runat="server" Text="Al lado del operador" 
                      Value="Al lado del operador" />

              </Items>
          </telerik:RadComboBox>
      </td>
      <td align="left">ANI</td>
      <td align="left">
          <telerik:RadTextBox ID="txtani" ReadOnly="true" runat="server">
          </telerik:RadTextBox> </td>
      </tr>
      <tr>
      <td align="right">Tipo de problema</td>
      <td align="left">
          <asp:Label ID="lbltipoproblema" runat="server" Text=""></asp:Label></td>

          <td align="left">&nbsp;</td>
          <td align="left">
              
              &nbsp;</td>
      </tr>
      </table>
      <br />
            <br />

    <asp:Table ID="Table1" runat="server" Height="59px" Width="841px" 
          Caption="Errores Criticos de Negocio" CaptionAlign="Top" 
        BorderColor="#3366FF" BorderStyle="Solid">
        <asp:TableRow runat="server" TableSection="TableHeader">
            <asp:TableCell runat="server" Width="40%" HorizontalAlign="Left">1. Evita desmerecer el funcionamiento de otras áreas/operadores</asp:TableCell>
            <asp:TableCell runat="server" Width="10%">
                <telerik:RadComboBox ZIndex=10000 ID="ddlecn1" runat="server" Width="45" AutoPostBack="True">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
            <asp:TableCell runat="server" HorizontalAlign="Left">
            2. Gestiona el caso en los sistemas  de forma correcta	
            </asp:TableCell>
            <asp:TableCell runat="server">
                  <telerik:RadComboBox ZIndex=10000 ID="ddlecn2" runat="server" Width="45" AutoPostBack="True" >
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
        <asp:TableCell ID="TableCell1" runat="server" HorizontalAlign="Left" AutoPostBack="True" >3. Realiza una correcta indagación u ofrecimiento	</asp:TableCell>
            <asp:TableCell ID="TableCell2" runat="server">
                <telerik:RadComboBox ZIndex=10000 ID="ddlecn3" runat="server" Width="45">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server" HorizontalAlign="Left">4. Utiliza la herramienta de acceso remoto aprobada por la compañía	</asp:TableCell>
            <asp:TableCell ID="TableCell4" runat="server">
                  <telerik:RadComboBox ZIndex=10000 ID="ddlecn4" runat="server" Width="45" AutoPostBack="True">
          <Items>
 <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />             
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
        <asp:TableCell ID="TableCell5" runat="server" HorizontalAlign="Left">5. Ingresa correctamente reclamo en sistema de gestión	</asp:TableCell>
            <asp:TableCell ID="TableCell6" runat="server">
                <telerik:RadComboBox ZIndex=10000 ID="ddlecn5" runat="server" Width="45" AutoPostBack="True" >
          <Items>
 <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />               
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
        <asp:TableCell>Total: 
            <asp:Label ID="ecn" ForeColor="Red" runat="server" Text=""></asp:Label>
        </asp:TableCell>  
        </asp:TableRow>
     


    </asp:Table>
          <br />      <br />
   

    <asp:Table ID="Table2" runat="server" Caption="Errores Criticos del Usuario" 
        Width="841px" BorderColor="#3366FF" BorderStyle="Solid" >

        <asp:TableRow ID="TableRow1" runat="server" TableSection="TableHeader">
            <asp:TableCell ID="TableCell7" runat="server" Width="40%" HorizontalAlign="Left">1. Evita cortar/cerrar la llamada de manera injustificada</asp:TableCell>
            <asp:TableCell ID="TableCell8" runat="server" Width="10%">
                <telerik:RadComboBox ZIndex=10000 ID="ddlecc1" runat="server" Width="45" AutoPostBack="True">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
            <asp:TableCell ID="TableCell9" runat="server" HorizontalAlign="Left">
            2. Informa plazos correctamente		
            </asp:TableCell>
            <asp:TableCell ID="TableCell10" runat="server">
                  <telerik:RadComboBox ZIndex=10000 ID="ddlecc2" runat="server" Width="45" AutoPostBack="True">
          <Items>
 <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />               
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell11" runat="server" HorizontalAlign="Left">3. Brinda información correcta, completa y actualizada		</asp:TableCell>
            <asp:TableCell ID="TableCell12" runat="server">
                <telerik:RadComboBox ZIndex=10000 ID="ddlecc3" runat="server" Width="45" AutoPostBack="True">
          <Items>
 <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />               
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
            <asp:TableCell ID="TableCell13" runat="server" HorizontalAlign="Left">4. Evita confrontar/ironizar con el cliente		</asp:TableCell>
            <asp:TableCell ID="TableCell14" runat="server">
                  <telerik:RadComboBox ZIndex=10000 ID="ddlecc4" runat="server" Width="45" AutoPostBack="True">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow3" runat="server">
        <asp:TableCell ID="TableCell15" runat="server" HorizontalAlign="Left">5. Evita divulgar/modificar información confidencial del cliente sin tomar los recaudos estipulados		</asp:TableCell>
            <asp:TableCell ID="TableCell16" runat="server">
                <telerik:RadComboBox ZIndex=10000 ID="ddlecc5" runat="server" Width="45" AutoPostBack="True">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
            
</asp:TableCell>
          <asp:TableCell HorizontalAlign="Left">6. Cumplimiento de procedimientos para la resolución	</asp:TableCell>
          <asp:TableCell>
                          <telerik:RadComboBox ZIndex=10000 ID="ddlecc6" runat="server" Width="45" AutoPostBack="True">
          <Items>
 <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />               
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">7. Evita dejar asuntos pendientes	</asp:TableCell>
         <asp:TableCell>
         <telerik:RadComboBox ZIndex=10000 ID="ddlecc7" runat="server" Width="45" AutoPostBack="True">
          <Items>
 <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />               
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
         </asp:TableCell>
         <asp:TableCell>
         <asp:Label id="ecc" runat="server" ForeColor="Red" ></asp:Label>
         </asp:TableCell>
        </asp:TableRow>
       
    </asp:Table>
    <br />
          <br />

    <asp:Table ID="Table3" runat="server" Caption="Errores No criticos"   
        Width="841px" BorderColor="#3366FF" BorderStyle="Solid" >

            
        <asp:TableRow>
        <asp:TableCell Width="40%" HorizontalAlign="Left">1. Demuestra control de tiempos		</asp:TableCell>
         <asp:TableCell Width="10%">
         <telerik:RadComboBox ZIndex=10000 ID="ddlenc1" runat="server" Width="45" AutoPostBack="True">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
         </asp:TableCell>
                 <asp:TableCell HorizontalAlign="Left">2. Presentación – Personalización			</asp:TableCell>
         <asp:TableCell>
         <telerik:RadComboBox ZIndex=10000 ID="ddlenc2" runat="server" Width="45" AutoPostBack="True">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
         </asp:TableCell>
        </asp:TableRow>



                <asp:TableRow>
        <asp:TableCell Width="40%" HorizontalAlign="Left">3. Buen manejo de baches de la llamada		</asp:TableCell>
         <asp:TableCell Width="10%">
         <telerik:RadComboBox ZIndex=10000 ID="ddlenc3" runat="server" Width="45" AutoPostBack="True">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
         </asp:TableCell>
                 <asp:TableCell HorizontalAlign="Left">4. Demuestra habilidades suficientes de comunicación		</asp:TableCell>
         <asp:TableCell>
         <telerik:RadComboBox ZIndex=10000 ID="ddlenc4" runat="server" Width="45" AutoPostBack="True">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
         </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow >
        	
        <asp:TableCell HorizontalAlign="Left" >5. Deja registro escrito suficiente e inteligible
        </asp:TableCell>
                                   
         <asp:TableCell>
         <telerik:RadComboBox ZIndex=10000 ID="ddlenc5" runat="server" Width="45" AutoPostBack="True">
          <Items>
              
<telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
              
<telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
          
</Items>
                
                
</telerik:RadComboBox>
         </asp:TableCell>
         <asp:TableCell>
         <asp:Label ID="ENC" runat="server" ForeColor="Red"></asp:Label>
         </asp:TableCell>
        </asp:TableRow>


    </asp:Table>
          <br />      <br />
          <p> Justificación: </p>
      <telerik:RadTextBox ID="txtjustificacion" runat="server" TextMode="MultiLine" 
          Width="90%" Rows="10" >
      </telerik:RadTextBox>
                <br /> <br />          
      <p >Escuchas:</p>
      <asp:Label ID="lblerror" runat="server" Text=""></asp:Label>
      <asp:FileUpload ID="escuchasfile" runat="server" />
      <asp:RegularExpressionValidator ID="REGEXFileUploadwav" runat="server" ErrorMessage="Solo Wavs/PCM" ControlToValidate="escuchasfile" 
      ValidationExpression= "(.*).(.wav|.WAV)$" />
      <br />
      
      <br />
      
            <br /><telerik:RadButton ID="btnguardar" runat="server" Text="Guardar">
      </telerik:RadButton>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

