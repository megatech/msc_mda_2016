﻿Imports System.Data.SqlClient
Imports App_Code.TicketsLib

Partial Class cargamasiva
    Inherits System.Web.UI.Page

    Protected Sub btnact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnact.Click

        Dim SQL As New SqlConnection
        Dim SQL2 As New SqlConnection
        Dim query As New SqlCommand
        Dim rs As SqlDataReader
        Dim ret(,) As Long
        Dim cantidadTkt As Long

        Dim tbl As New Table
        Dim rows() As TableRow
        Dim cells(,) As TableCell

        SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
        SQL.Open()
        query.Connection = SQL
        query.CommandText = "select ticketid,informeinterno from BULKTICKET "
        rs = query.ExecuteReader()

        Dim update As New SqlCommand, i As Long = 0
        SQL2.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
        SQL2.Open()
        update.Connection = SQL2
        update.CommandText = "SP_ACTUALIZAR_TICKET"
        update.CommandType = Data.CommandType.StoredProcedure
        Dim usuario As Usuario
        usuario = Session("usuario")




        While rs.Read
            ReDim Preserve ret(1, i)
            ret(0, i) = New Long
            ret(1, i) = New Long

            Try
                update.Parameters.AddWithValue("@ticketid", rs(0))
                update.Parameters.AddWithValue("@InformeInterno", rs(1))
                update.Parameters.AddWithValue("@Usuarioid", usuario.UsuarioId)
                update.Parameters.AddWithValue("@estadoid", 0)
                update.Parameters.AddWithValue("@InformeCliente", DBNull.Value)
                update.Parameters.Add("fglerror", Data.SqlDbType.SmallInt)
                update.Parameters("fglerror").Direction = Data.ParameterDirection.Output
                update.Parameters.Add("DescError", Data.SqlDbType.VarChar, 200)
                update.Parameters("DescError").Direction = Data.ParameterDirection.Output
                update.Parameters.Add("retorno", Data.SqlDbType.Int)
                update.Parameters("retorno").Direction = Data.ParameterDirection.ReturnValue

                update.ExecuteNonQuery()
                ret(0, i) = rs(0)

                ret(1, i) = CLng(update.Parameters("retorno").SqlValue.ToString)

                update.Parameters.Clear()
            Catch ex As SqlException
                ret(0, i) = rs(0)
                ret(1, i) = 1
            End Try
            i = i + 1
        End While

        GridView1.Visible = False

        cantidadTkt = i
        i = 0


        For i = 0 To cantidadTkt - 1

            If ret(1, i) = 1 Then

                ReDim Preserve rows(i)
                ReDim Preserve cells(1, i)

                rows(i) = New TableRow
                cells(0, i) = New TableCell
                cells(1, i) = New TableCell

                cells(0, i).Text = ret(0, i)
                cells(1, i).Text = ret(1, i)

                rows(i).Cells.Add(cells(0, i))
                rows(i).Cells.Add(cells(1, i))

                tblResultado.Rows.Add(rows(i))
            End If

        Next
        If tblResultado.Rows.Count <= 0 Then
            Dim cell As New TableCell
            Dim row As New TableRow
            cell.Text = "Se actualizaron correctamente " & cantidadTkt & " Tickets"
            row.Cells.Add(cell)
            tblResultado.Rows.Add(row)
        End If

        btnact.Visible = False
        rs.Close()
        SQL.Close()
        SQL2.Close()
    End Sub

End Class
