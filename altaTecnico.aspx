﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="altaTecnico.aspx.cs" Inherits="Default4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
    <table style="border: thin solid #000000; width:31%;" cellpadding="3">
        <tr>
            <td style="height: 25px; font-weight: bold;" align="left" colspan="2">
                Alta de Tecnico/CAS</td>
        </tr>
        <tr>
            <td style="height: 23px; " align="right">
                Area:</td>
            <td style="height: 23px; width: 213px;">
                <asp:DropDownList ID="ddArea" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DropDownList1_SelectedIndexChanged">
                    <asp:ListItem Value="57">Técnico</asp:ListItem>
                    <asp:ListItem Value="58">CAS</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                Nombre:</td>
            <td style="height: 23px; width: 213px;">
                <asp:TextBox ID="txtNombre" runat="server" Width="170px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Label ID="lblApellido" runat="server" Text="Apellido:"></asp:Label>
            </td>
            <td style="width: 213px">
                <asp:TextBox ID="txtApellido" runat="server" Width="170px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                E-Mail:</td>
            <td style="width: 213px">
                <asp:TextBox ID="txtEmail" runat="server" Width="170px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; height: 12px;" align="center" colspan="2">
                <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="#990000"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 53px; height: 57px;">
                </td>
            <td style="width: 213px; text-align: right; height: 57px;">
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Guardar" 
                    Width="147px" />
            </td>
        </tr>
    </table>
</asp:Content>

