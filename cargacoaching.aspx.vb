﻿Imports System.Data.SqlClient
Imports System.Data
Imports App_Code.TicketsLib

Partial Class cargacoaching
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cant = getScalar("SELECT count(*) from coaching where userId=" & Request.QueryString("usuarioid") & " AND id in (select id from viewCoachingsCriticos) And month(fecha)=month(getDate())")
        'If cant > 2 Then Response.Redirect("regCoaching.aspx")
        lblfecha.Text = DateTime.Now
        lbloper.Text = Request.QueryString("usuarioid")
        lbloperName.Text = getScalar("SELECT ' - ' + RTRIM(nombreCompleto) as nombre FROM usuario WHERE usuarioId=" & lbloper.Text)

        If (ddlecn1.SelectedValue = "0" Or ddlecn2.SelectedValue = "0" Or ddlecn3.SelectedValue = "0" Or ddlecn4.SelectedValue = "0" Or ddlecn5.SelectedValue = "0") Then
            ecn.Text = "0%"
            ecn.ForeColor = Drawing.Color.Red
        Else
            ecn.Text = "100%"
            ecn.ForeColor = Drawing.Color.Green
        End If

        If (ddlecc1.SelectedValue = "0" Or ddlecc2.SelectedValue = "0" Or ddlecc3.SelectedValue = "0" Or ddlecc4.SelectedValue = "0" Or ddlecc5.SelectedValue = "0" Or ddlecc6.SelectedValue = "0" Or ddlecc7.SelectedValue = "0") Then
            ecc.Text = "0%"
            ecc.ForeColor = Drawing.Color.Red
        Else
            ecc.Text = "100%"
            ecc.ForeColor = Drawing.Color.Green
        End If

        ENC.Text = CalculaPorcentaje().ToString() & "%"
        If CalculaPorcentaje() > 60 Then
            ENC.ForeColor = Drawing.Color.Green
        Else
            ENC.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Private Function CalculaPorcentaje() As Long
        Dim lPercent As Long = 0
        If ddlenc1.SelectedValue <> "0" Then lPercent += 20
        If ddlenc2.SelectedValue <> "0" Then lPercent += 20
        If ddlenc3.SelectedValue <> "0" Then lPercent += 20
        If ddlenc4.SelectedValue <> "0" Then lPercent += 20
        If ddlenc5.SelectedValue <> "0" Then lPercent += 20
        Return lPercent
    End Function

    Protected Sub txttkt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txttkt.TextChanged

        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim telefono As String


        If txttkt.Text = "" Then Exit Sub

        If Not IsNumeric(txttkt.Text) Then Exit Sub


        If Chkexperiencia.Checked = False Then
            sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("TicketDB").ToString()
            sql.Open()
            query.Connection = sql



            query.CommandText = " select top 1 telefono1 " & _
                                " from viewcliente " & _
                                " where clienteid = ( select clienteid from ticket where ticketid = " & txttkt.Text & " ) "

            telefono = query.ExecuteScalar()

            txtani.Text = telefono
            txtani.ReadOnly = True
            If txtani.Text = "" Then

                txttkt.BorderColor = Drawing.Color.Red
                txttkt.ForeColor = Drawing.Color.Red
                lbltipoproblema.Text = "No existe el TKT"
                lbltipoproblema.ForeColor = Drawing.Color.Red

            Else
                txttkt.BorderColor = Drawing.Color.Black
                txttkt.ForeColor = Drawing.Color.Black
                lbltipoproblema.ForeColor = Drawing.Color.Black

            End If

            query.CommandText = " select tipoproblema.nombre from ticket inner join tipoproblema on tipoproblema.idtipoproblema = ticket.idtipoproblema where ticketid = " & txttkt.Text & ""

            lbltipoproblema.Text = query.ExecuteScalar()

        Else
            sql.ConnectionString = "Data Source=10.1.0.12;Initial Catalog=experienciaspd;Persist Security Info=True;User ID=tickets;Password=tickets"
            sql.Open()
            query.Connection = sql
            query.CommandText = "SELECT telefono FROM ticket WHERE id = " & txttkt.Text
            txtani.Text = query.ExecuteScalar

            query.CommandText = "SELECT nombre from infotipogestion inner join ticket on ticket.idtipogestion = infotipogestion.id"
            lbltipoproblema.Text = query.ExecuteScalar

        End If


        query.Dispose()
        sql.Close()
        sql.Dispose()


    End Sub

    Protected Sub btnguardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        Dim usuarioCarga As Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
        Dim escucha As String
        Dim fecha As String = Year(Date.Now) & "-" & Month(Date.Now) & "-" & Day(Date.Now) & " 00:00:00 "

        'Guardar DB

        Dim nombrefile As String
        nombrefile = Now.ToString("yyyyMMdd_HHmmss") & IIf(txtani.Text.Trim <> "", "_ani" & txtani.Text & "-", "") & "_oper" & lbloper.Text & ".wav"
        If (escuchasfile.HasFile = True) Then
            escucha = 1
        Else
            escucha = 0
        End If


        If escuchasfile.HasFile = True Then
            escuchasfile.PostedFile.SaveAs("C:\inetpub\wwwroot\Webs\escuchas_speedy\" & nombrefile.ToString())
        Else
            lblerror.Text = "Debe seleccionar una grabación"
            lblerror.ForeColor = Drawing.Color.Red
            Exit Sub
        End If


        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand

        sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("TicketDB").ToString()
        sql.Open()
        query.Connection = sql
        query.Parameters.Add(New SqlParameter("@usuarioCarga", usuarioCarga.UsuarioId))
        query.Parameters.Add(New SqlParameter("@userid", lbloper.Text))
        query.Parameters.Add(New SqlParameter("@ani", txtani.Text))
        query.Parameters.Add(New SqlParameter("@mmon", ddlmodo.SelectedValue))
        Dim fecha1 As New SqlParameter
        fecha1.DbType = Data.DbType.Date
        fecha1.Value = fecha
        fecha1.ParameterName = "@fecha"
        query.Parameters.Add(fecha1)

        query.Parameters.Add(New SqlParameter("@escucha", escucha))
        query.Parameters.Add(New SqlParameter("@devolucion", 0))
        query.Parameters.Add(New SqlParameter("@pec1", ddlecn1.SelectedValue))
        query.Parameters.Add(New SqlParameter("@pec2", ddlecn2.SelectedValue))
        query.Parameters.Add(New SqlParameter("@pec3", ddlecn3.SelectedValue))

        If ddlecn4.SelectedValue = "NULL" Then
            query.Parameters.Add(New SqlParameter("@pec4", DBNull.Value))
        Else
            query.Parameters.Add(New SqlParameter("@pec4", ddlecn4.SelectedValue))
        End If

        If ddlecn5.SelectedValue = "NULL" Then

            query.Parameters.Add(New SqlParameter("@pec5", DBNull.Value))
        Else
            query.Parameters.Add(New SqlParameter("@pec5", ddlecn5.SelectedValue))
        End If


        query.Parameters.Add(New SqlParameter("@pec6", ddlecc1.SelectedValue))

        If ddlecc2.SelectedValue = "NULL" Then
            query.Parameters.Add(New SqlParameter("@pec7", DBNull.Value))
        Else

            query.Parameters.Add(New SqlParameter("@pec7", ddlecc2.SelectedValue))
        End If


        If ddlecc3.SelectedValue = "NULL" Then

            query.Parameters.Add(New SqlParameter("@pec8", DBNull.Value))

        Else

            query.Parameters.Add(New SqlParameter("@pec8", ddlecc3.SelectedValue))

        End If

        query.Parameters.Add(New SqlParameter("@pec9", ddlecc4.SelectedValue))
        query.Parameters.Add(New SqlParameter("@pec10", ddlecc5.SelectedValue))

        If ddlecc6.SelectedValue = "NULL" Then

            query.Parameters.Add(New SqlParameter("@pec11", DBNull.Value))
        Else
            query.Parameters.Add(New SqlParameter("@pec11", ddlecc6.SelectedValue))
        End If

        If ddlecc7.SelectedValue = "NULL" Then
            query.Parameters.Add(New SqlParameter("@pec12", DBNull.Value))
        Else
            query.Parameters.Add(New SqlParameter("@pec12", ddlecc7.SelectedValue))
        End If

        query.Parameters.Add(New SqlParameter("@penc1", ddlenc1.SelectedValue))
        query.Parameters.Add(New SqlParameter("@penc2", ddlenc2.SelectedValue))
        query.Parameters.Add(New SqlParameter("@penc3", ddlenc3.SelectedValue))
        query.Parameters.Add(New SqlParameter("@penc4", ddlenc4.SelectedValue))
        query.Parameters.Add(New SqlParameter("@penc5", ddlenc5.SelectedValue))
        If Not (Chkexperiencia.Checked And txttkt.Text.Trim = "") Then query.Parameters.Add(New SqlParameter("@tkt", txttkt.Text))
        query.Parameters.Add(New SqlParameter("@justificacion", txtjustificacion.Text))
        query.Parameters.Add(New SqlParameter("@validado", DBNull.Value))
        query.Parameters.Add(New SqlParameter("@tipoproblema", lbltipoproblema.Text))
        query.Parameters.Add(New SqlParameter("@hash", "falta"))
        query.Parameters.Add(New SqlParameter("@archivo", nombrefile.ToString()))
        query.Parameters.Add(New SqlParameter("@monitoreo", usuarioCarga.Perfil = 17))

        query.CommandType = Data.CommandType.StoredProcedure
        query.CommandText = "SP_INSERT_COACHING"

        query.ExecuteNonQuery()


        query.Dispose()
        sql.Close()
        sql.Dispose()

        'Subir Archivo



        If usuarioCarga.Perfil <> 17 Then notificarCoaching()
        Response.Redirect("regcoaching.aspx")

    End Sub

    Protected Sub Chkexperiencia_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chkexperiencia.CheckedChanged
        If txttkt.Text <> "" Then
            txttkt_TextChanged(txttkt, New System.EventArgs())
        End If
    End Sub

    Private Function getScalar(ByVal query As String) As Object
        Dim SQL As New SqlConnection
        Dim CMD As New SqlCommand
        Dim RES As Object
        Try
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ToString
            CMD.Connection = SQL
            CMD.CommandText = query
            SQL.Open()
            RES = CMD.ExecuteScalar()
            SQL.Close()
            Return RES
        Catch ex As Exception
            If SQL.State <> ConnectionState.Closed Then
                SQL.Close()
            End If
            Throw ex
        End Try
    End Function

    Private Sub notificarCoaching()
        Dim destinatario, subject, cuerpo, nombreUsuario As String
        nombreUsuario = getScalar("SELECT nombreCompleto FROM usuario WHERE usuarioId=" & Request.QueryString("usuarioid"))
        destinatario = getScalar("SELECT Email FROM Usuario WHERE usuarioId=" & Request.QueryString("usuarioid"))

        subject = "Devolución de Monitoreo [" & nombreUsuario & " - " & lblfecha.Text.Substring(0, 10) & "]"
        cuerpo = "<div style=""text-align:center;""><table style=""width:700px; margin:auto; font-weight:bold;"">" &
                "<tr><th style=""text-align:left; background-color:#333333; color:white;"">Devoluci&oacuten de monitoreo [" & nombreUsuario & " - " & lblfecha.Text.Substring(0, 10) & "]</th></tr>" &
                "<tr><td>Se ha cargado un nuevo monitoreo a su nombre en el sistema.</td></tr>" &
                "<tr><td>Por favor acceda al link que figura a continuaci&oacuten, para visualizar el detalle del mismo.</td>" &
                "<tr><td>Debajo del detalle, tendr&aacute un campo para enviar una replica y confirmar que ha sido notificado de esta devoluci&oacuten.</td></tr>" &
                "<tr><td style=""text-align:center""><a href=""" & generateLink() & """>" & generateLink() & "</a></td></tr>" &
                "</table> <div>"
        FUNCIONES_MOD.mailmega2.enviamail("megarobot@megatech.la", destinatario, "megarobot@megatech.la", subject, cuerpo)
    End Sub

    Public Function generateLink() As String
        Dim coachId = getScalar("Select top 1 id from coaching where userId=" & Request.QueryString("usuarioid") & " Order by id desc")
        Return "http://mscmda.mega.com.ar/vercoach.aspx" & "?coachingid=" & coachId
    End Function
End Class
