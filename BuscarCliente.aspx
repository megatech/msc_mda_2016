﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BuscarCliente.aspx.vb" Inherits="BuscarCliente" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BUSQUEDA DE CLIENTE</title>
        <link href="css/overlay.css" rel="stylesheet" />
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/overlay.js"></script>
    <script src="js/PopupMgr.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div style="min-width:700px;">
    <div style="height:60px;">
        <table style="margin:auto;"><tr>
            <td>
               <telerik:RadTextBox ID="txtBusqueda" runat="server" EmptyMessage="Buscar..." Font-Bold="False" Font-Size="14px" Height="24px" Width="250px" AutoPostBack="True" LabelWidth="81px"></telerik:RadTextBox>
            </td>
            <td style="text-align:center;"><asp:RadioButtonList ID="tipo" runat="server" RepeatDirection="Horizontal" Font-Bold="True" Font-Size="14px" ForeColor="#333333">
            <asp:ListItem Value="todo" Selected="True">Todo</asp:ListItem>
            <asp:ListItem Value="clienteId">Código</asp:ListItem>
            <asp:ListItem Value="CUIT">CUIT</asp:ListItem>
            <asp:ListItem Value="razonSocial">Razón Social</asp:ListItem>
            <asp:ListItem Value="domicilio">Domicilio</asp:ListItem>
        </asp:RadioButtonList>
            </td></tr></table>
    </div>
    <div style="overflow-y:auto;">
    
        <telerik:RadGrid ID="gridResultados" runat="server" CellSpacing="0" DataSourceID="dsClientes" GridLines="None" Culture="es-ES">
            <ExportSettings>
                <Pdf PageWidth="" />
            </ExportSettings>
<MasterTableView AutoGenerateColumns="False" DataSourceID="dsClientes">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="select" FilterControlAltText="Filter column column" Text="-" UniqueName="column" HeaderStyle-Width="40px">
<HeaderStyle Width="40px"></HeaderStyle>
        </telerik:GridButtonColumn>
        <telerik:GridBoundColumn DataField="ClienteId" FilterControlAltText="Filter ClienteId column" HeaderText="Código" SortExpression="ClienteId" UniqueName="ClienteId" HeaderStyle-Width="60px">
<HeaderStyle Width="60px"></HeaderStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="RazonSocial" FilterControlAltText="Filter RazonSocial column" HeaderText="Razón Social" SortExpression="RazonSocial" UniqueName="RazonSocial">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Domicilio" FilterControlAltText="Filter Domicilio column" HeaderText="Domicilio" SortExpression="Domicilio" UniqueName="Domicilio">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Localidad" FilterControlAltText="Filter Localidad column" HeaderText="Localidad" SortExpression="Localidad" UniqueName="Localidad">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CUIT" FilterControlAltText="Filter CUIT column" HeaderText="CUIT" SortExpression="CUIT" UniqueName="CUIT">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Contacto" FilterControlAltText="Filter Contacto column" SortExpression="Contacto" UniqueName="Contacto">
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
</MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsClientes" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" ></asp:SqlDataSource>
    </div>
    </div>
    </form>
</body>
</html>
