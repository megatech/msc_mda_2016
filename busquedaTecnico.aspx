﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="busquedaTecnico.aspx.cs" Inherits="busquedaTecnico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
    <table style="width: 43%">
        <tr>
            <td style="height: 23px; font-weight: bold;" colspan="2">
                Busqueda</td>
        </tr>
        <tr>
            <td style="height: 23px; " align="right">
                Area:</td>
            <td style="height: 23px; width: 177px;">
                <asp:DropDownList ID="ddArea" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DropDownList1_SelectedIndexChanged">
                    <asp:ListItem Value="57">Técnico</asp:ListItem>
                    <asp:ListItem Value="58">CAS</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                Nombre:</td>
            <td style="height: 23px; width: 177px;">
                <asp:TextBox ID="txtNombre" runat="server" Width="170px" 
                    ontextchanged="txtNombre0_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Label ID="lblApellido0" runat="server" Text="Apellido:"></asp:Label>
            </td>
            <td style="width: 177px">
                <asp:TextBox ID="txtApellido" runat="server" Width="170px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; height: 26px;">
                E-Mail:</td>
            <td style="width: 177px; height: 26px;">
                <asp:TextBox ID="txtEmail" runat="server" Width="170px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; height: 29px; width: 221px;" align="center">
                &nbsp;</td>
            <td style="text-align: right; height: 29px; width: 177px;" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: right; height: 29px; width: 221px;" align="center">
                &nbsp;</td>
            <td style="text-align: right; height: 29px; width: 177px;" align="left">
                <asp:Button ID="btBuscar" runat="server" onclick="Button1_Click" Text="Buscar" 
                    Width="147px" />
            </td>
        </tr>
        <tr>
            <td style="text-align: right; height: 29px; width: 221px;" align="center">
                &nbsp;</td>
            <td style="text-align: right; height: 29px; width: 177px;" align="left">
                &nbsp;</td>
        </tr>
    </table>
    <hr />
    <asp:GridView ID="gvResultados" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" 
        GridLines="None" onselectedindexchanged="gvResultados_SelectedIndexChanged" 
        Width="324px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" 
                ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="nombre" HeaderText="nombre" 
                SortExpression="nombre" />
            <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        SelectCommand="SELECT id,nombre, email FROM tecnico WHERE"></asp:SqlDataSource>
</asp:Content>

