using System;
using App_Code.TicketsLib;

public partial class Tickets : System.Web.UI.MasterPage

{

    protected void form1_Load(object sender, EventArgs e)
    {
        Usuario usr = (Usuario)Session["Usuario"];

        if (usr == null)
        {
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString);
        }
    }

               
    protected void Button1_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("Default.aspx");
    }
}
