<%@ Page Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="Tickets.aspx.cs" Inherits="Tickets" %>

<asp:Content ID="TicketPage" ContentPlaceHolderID="cphTicket" runat="server">
    <script src="js/jquery-1.8.2.js" type="text/javascript"></script>
    
   <asp:Panel ID="panelTicket" runat="server">
        <div style="text-align: center">
            <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 100%; border-bottom: black 1px solid">
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="right">
                                        Fecha</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFecha" runat="server" ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox></td>
                                    <td align="right">
                                        F.Inicio</td>
                                    <td align="left">
                                        <telerik:RadDatePicker ID="RadDatePicker1" Runat="server" Culture="es-ES" 
                                            Width="100px" onselecteddatechanged="RadDatePicker1_SelectedDateChanged" 
                                            Skin="WebBlue">
                                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" 
                                                ViewSelectorText="x" Skin="WebBlue">
                                            </Calendar>
                                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                            </DateInput>
                                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                        </telerik:RadDatePicker>
                                        <asp:TextBox ID="txtFechaInicio" runat="server" Visible="False" Width="16px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvFechaInicio" runat="server" ControlToValidate="txtFechaInicio" ErrorMessage="Debe ingresar la fecha de Inicio">*</asp:RequiredFieldValidator>
                                        </td>
                                    <td align="right">
                                        F. Fin</td>
                                    <td align="left">
                                        <telerik:RadDatePicker ID="RadDatePicker2" Runat="server" Culture="es-ES" 
                                            onselecteddatechanged="RadDatePicker2_SelectedDateChanged" Skin="WebBlue" 
                                            Width="100px">
                                            <Calendar Skin="WebBlue" UseColumnHeadersAsSelectors="False" 
                                                UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                            </Calendar>
                                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                            </DateInput>
                                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                        </telerik:RadDatePicker>
                                        <asp:TextBox ID="txtFechaFin" runat="server" Width="16px" Visible="False"></asp:TextBox>
                                         
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Nro.Claim</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNroClaim" runat="server" ReadOnly="true"></asp:TextBox></td>
                                    <td align="right">
                                        Ticket</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTicket" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%;">
                                <tr>
                                    <td align="right" width="15%">
                                        Negocio</td>
                                    <td align="left" width="35%">
                                        <asp:DropDownList ID="ddlNegocio" runat="server" 
                                        DataSourceID="odsNegocio" DataTextField="Nombre" DataValueField="Codigo" 
                                            onselectedindexchanged="ddlNegocio_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsNegocio" Runat="server" 
                                        TypeName="App_Code.TicketsLib.TablasCombo"
                                        SelectMethod="getComboNegocio" />
                                        
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="lnvercliente" runat="server" 
                                            ToolTip="Ver/Editar Cliente">Cliente</asp:LinkButton>
&nbsp;<asp:TextBox ID="txtClienteID" runat="server" Visible="false" Width="16px"></asp:TextBox>
                                    </td>
                                    <td align="left">
                                        <table style="width: 92%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 24px" align="center">
                                                    <asp:ImageButton ID="nuevocliente" runat="server" alt="Nuevo Cliente" 
                                                        ImageUrl="Images/nuevo_cliente.jpg"  onclick="nuevocliente_Click" OnClientClick="AbrirPopup('ClientesAltaPop.aspx','popup'); return false;"
                                                        style="width: 21px; height: 21px" ToolTip="Nuevo Cliente" />
                                                </td>
                                                <td style="width: 54px; text-indent: 2px;">
                                                    <asp:TextBox ID="txtCodCliente" runat="server" AutoPostBack="true" 
                                                        OnTextChanged="txtCodCliente_TextChanged" Width="50px"></asp:TextBox>
                                                </td>
                                                <td style="width: 18px">
                                                    <asp:ImageButton ID="imgBuscar" runat="server" CausesValidation="false" 
                                                        ImageUrl="~/Images/search.png"  ToolTip="Buscar Cliente" OnClientClick="AbrirPopup('buscarCliente.aspx','buscarCliente'); return false;"  />
                                                </td>
                                                <td style="width: 173px" nowrap="nowrap">
                                                    <asp:TextBox ID="txtNomCliente" runat="server" ReadOnly="true" Width="156px"></asp:TextBox>
                                                </td>
                                                <td align="left" nowrap="nowrap">
                                                    <asp:ImageButton ID="btHistorial" runat="server" 
                                                        ImageUrl="~/Images/historial.png" onclick="btHistorial_Click" 
                                                        ToolTip="Ver Historial" Visible="False" Width="22px" />
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Ubicaci�n</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlUbicacion" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUbicacion_SelectedIndexChanged" >
                                        </asp:DropDownList>
                                        
                                    </td>
                                    <td align="right" style="width: 14%">
                                        Contacto </td>
                                    <td align="left">
                                        &nbsp;<asp:DropDownList ID="ddlContacto" runat="server" >
                                        </asp:DropDownList>
                                     </td>
                                </tr>                                
                                <tr>
                                    <td align="right" colspan="4">
                                        &nbsp;</td>
                                </tr>
                            </table>
                    </td>
                </tr>
                 
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="right" style="height: 28px">
                                        Equipo</td>
                                    <td align="left" style="height: 28px" valign="middle">
                                        <table cellpadding="0" cellspacing="0" style="width: 91%;">
                                            <tr>
                                                <td style="width: 50px">
                                                    <asp:TextBox ID="txtCodEquipo" runat="server" AutoPostBack="true" 
                                                        OnTextChanged="txtCodEquipo_TextChanged" Width="50px"></asp:TextBox>
                                                </td>
                                                <td style="width: 24px">
                                               <asp:ImageButton ID="imgBuscarEquipo" runat="server" CausesValidation="false" 
                                            ImageUrl="~/Images/search.png" OnClientClick="AbrirPopup('buscarEquipo.aspx','popup'); return false;" />
                                                      </td>
                                                <td style="width: 190px">
                                                    <asp:TextBox ID="txtNombreEquipo" runat="server" ReadOnly="true" Width="190px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                        <asp:TextBox ID="txtEquipoId" runat="server" Visible="false" Width="16px"></asp:TextBox>
                                    </td>
                                    <td align="right" style="height: 28px;" nowrap="nowrap" width="100px">
                                        Nro. Serie</td>
                                    <td align="left" style="width: 0%; height: 28px;">
                                        <asp:TextBox ID="txtSerie" runat="server" Width="70px" ReadOnly="true"></asp:TextBox>
                                    </td>
                                    <td align="left">
                                        &nbsp;</td>
                                    <td align="left" rowspan="2" style="width: 17%" width="35%">
                                        <table style="width: 100%; vertical-align: top;">
                                            <tr>
                                                <td style="height: 23px; width: 74px">
                                                    &nbsp;</td>
                                                <td style="height: 23px">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Especialista</td>
                                    <td align="left">
                                            <asp:DropDownList ID="ddlEspecialista" runat="server">
                                            </asp:DropDownList>
                                        
                                    </td>
                                    <td align="right" style="width: 17%">
                                        <asp:Label ID="lbltxtVendedor" runat="server" Text="Vendedor" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" colspan="2">
                                        &nbsp;<asp:DropDownList ID="ddlVendedor" runat="server" 
                                        DataSourceID="odsVendedor" DataTextField="Nombre" DataValueField="Codigo" 
                                            Visible="False">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsVendedor" Runat="server" 
                                        TypeName="App_Code.TicketsLib.TablasCombo"
                                        SelectMethod="getComboVendedor" />
                                        
                                    </td>
                                </tr>                                
                            </table>
                    </td>
                </tr> 
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="right" width="15%" class="auto-style2">
                                        <asp:Label ID="Label1" runat="server" Text="T.Problema"></asp:Label>
                                    </td>
                                    <td align="left" width="35%" class="auto-style2">
                                        &nbsp;<asp:DropDownList ID="ddlTipoProblema" runat="server" Enabled="false" 
                                        DataSourceID="dsTipoProblema" DataTextField="nombre" DataValueField="id">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="dsTipoProblema" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="select idTipoProblema as id, nombre from dbo.tiposDeProblema(@perfilId,NULL)">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="perfilId" SessionField="perfil" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                    <td align="right" width="15%" class="auto-style2">
                                        Prioridad</td>
                                    <td align="left" width="35%" class="auto-style2">
                                        <asp:DropDownList ID="ddlPrioridad" runat="server" >
                                        </asp:DropDownList>
                                        
                                    </td>

                                </tr>
                                <tr>
                                    <td align="right" width="15%">
                                        <asp:Label ID="Label2" runat="server" Text="Estado"></asp:Label>
                                    </td>
                                    <td align="left" width="35%">
                                        &nbsp;<asp:DropDownList ID="ddlEstado" runat="server" 
                                        DataSourceID="odsEstado" DataTextField="Nombre" DataValueField="Codigo">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsEstado" Runat="server" 
                                        TypeName="App_Code.TicketsLib.TablasCombo"
                                        SelectMethod="getComboEstados" onselecting="odsEstado_Selecting" />                                        
                                    </td>
                                    <td align="right">
                                        F. Estimada soluc.</td>
                                    <td align="left">
                                        <telerik:RadDatePicker ID="RadDatePicker3" Runat="server" Culture="es-ES" 
                                            onselecteddatechanged="RadDatePicker3_SelectedDateChanged" Skin="Silk" 
                                            Width="100px">
                                            <Calendar Skin="Silk" UseColumnHeadersAsSelectors="False" 
                                                UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                <FastNavigationSettings CancelButtonCaption="Cancelar" 
                                                EnableTodayButtonSelection="True" TodayButtonCaption="Hoy">
                                             </FastNavigationSettings>
                                            </Calendar>
                                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                            </DateInput>
                                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                        </telerik:RadDatePicker>
                                        <asp:TextBox ID="txtFechaSol" runat="server" Width="16px" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td align="right" width="10%" valign="top">
                                        Sistema Operativo</td>
                                    <td align="left" colspan="3">
                                        <telerik:RadComboBox ZIndex=10000 ID="ddSistemaOperativo" Runat="server" Culture="es-ES" DataSourceID="dsSistOperativo" DataTextField="nombre" DataValueField="id" Filter="Contains" HighlightTemplatedItems="True" Width="230px" AutoPostBack="True" OnSelectedIndexChanged="RadComboBox1_SelectedIndexChanged">
                                        </telerik:RadComboBox>
                                        <br />
                                        <asp:SqlDataSource ID="dsServicePack" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [servicePackId], [nombre] FROM [servicePackSO]"></asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsArquitectura" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [arquitecturaId], [nombre] FROM [arquitecturaSO]"></asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsSistOperativo" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT -1 as id, 'Seleccione un valor' as nombre UNION 
SELECT [id], [nombre] FROM [sistemaOperativo] WHERE ([fechaBaja] IS NULL)"></asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle" width="10%">
                                        <asp:Label ID="lblArquitecturaSO" runat="server" Text="Arquitectura - SP" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" colspan="3" nowrap="nowrap">
                                        <asp:RadioButtonList ID="arquitectura" runat="server" CssClass="radioAsButton" DataSourceID="dsArquitectura" DataTextField="nombre" DataValueField="arquitecturaId" Font-Bold="False" RepeatDirection="Horizontal" Visible="False">
                                        </asp:RadioButtonList>
                                        &nbsp;<asp:RadioButtonList ID="servicePack" runat="server" CssClass="radioAsButton" DataSourceID="dsServicePack" DataTextField="nombre" DataValueField="servicePackId" RepeatDirection="Horizontal" Visible="False">
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" width="10%">&nbsp;</td>
                                    <td align="left" colspan="3">
                                        <asp:CheckBox ID="chkSOModificado" runat="server" Text="Versi�n modificada" CssClass="customCheck" Font-Bold="True" ForeColor="#666666" Visible="False" />
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="right" valign="top" width="10%">Detalle</td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtDetalle" runat="server" Rows="5" TextMode="MultiLine" Width="98%" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>              
                <tr>
                    <td>
                        <asp:Panel ID="pTicketAbierto" runat="server" HorizontalAlign="Center" 
                            Visible="False">
                            <asp:LinkButton ID="lnkTicketAbierto" runat="server" Font-Bold="True" 
                                Font-Size="18px" Font-Underline="True" ForeColor="#0066FF">A�n existe un ticket abierto para este cliente. Haga click en este enlace.</asp:LinkButton>
                        </asp:Panel>
                        <asp:Button ID="btnSubmit" runat="server" Text="Ingresar" OnClientClick="deshabilitar();" UseSubmitBehavior="false" OnClick="btnSubmit_Click" /></td>
                </tr>
                 <tr>
                    <td align="left" >
                        <asp:Label ID="lblError" runat="server" CssClass="labelError"></asp:Label>
                    </td>
                        
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelMensaje" runat="server">
        <table>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblMensajePanel" runat="server" CssClass="labelError"></asp:Label>
                </td>        
            </tr>
        </table>
    </asp:Panel>
    <telerik:radwindowmanager id="WinMgr" runat="server" skin="Metro" AutoSize="true" Modal="true" OnClientClose="SavePopupValues" ShowContentDuringLoad="false" KeepInScreenBounds="true">
            <Windows>
                <telerik:RadWindow ID="popEncuesta" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true"></telerik:RadWindow>
             <telerik:RadWindow ID="popup" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true"></telerik:RadWindow>
                <telerik:RadWindow ID="popup2" runat="server" OnClientClose="SavePopupValues" MinWidth="700" MinHeight="800" AutoSize="true"  Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true"></telerik:RadWindow>
           
                 </Windows>
    </telerik:radwindowmanager>
   
    <asp:HiddenField runat="server" ID="retCliente" ClientIDMode="Static"/>    
    <asp:HiddenField runat="server" ID="retEquipo" ClientIDMode="Static"/>   
       
        <telerik:radwindowmanager id="Radwindowmanager1" runat="server" skin="Metro" AutoSize="true">
            <Windows>
               <telerik:RadWindow ID="buscarCliente" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true">
               </telerik:RadWindow>
               <telerik:RadWindow ID="buscarEquipo" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true">
               </telerik:RadWindow>
            </Windows>
    </telerik:radwindowmanager>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="cphHead">
    <style type="text/css">
        .auto-style2 {
            height: 64px;
        }
    </style>
</asp:Content>

