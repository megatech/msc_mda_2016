﻿Imports App_Code.TicketsLib.SqlHelper
Imports App_Code.TicketsLib.Funciones
Imports System.Data.SqlClient
Imports App_Code.TicketsLib
Imports System.Data
Imports App_Code


Partial Class Viaticos
    Inherits System.Web.UI.Page

    Protected Sub cbtipo_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles cbtipo.SelectedIndexChanged

        If e.Value = "auto" Then
            pnauto.Visible = True
            pnpubli.Visible = False
            btnagregar.Visible = True

        ElseIf e.Value = "pub" Then
            pnauto.Visible = False
            pnpubli.Visible = True
            btnagregar.Visible = True
        End If


    End Sub


    Protected Sub btnagregar_Click(sender As Object, e As EventArgs) Handles btnagregar.Click
        Dim usr As Usuario = DirectCast(Session("Usuario"), Usuario)


        If cbtipo.SelectedValue = "auto" Then

            Dim totala As SqlParameter = New SqlParameter("@total", SqlDbType.Decimal, 13)
            totala.Value = CDec(txtcankm.value) * CDec(txtvalkm.Value)

            Try

           
            ExecuteNonQuery(getConnString(), "SP_CREAR_VIATICOS", New SqlParameter("@Ticketid", Request.QueryString("id")), New SqlParameter("@km", txtcankm.Text), New SqlParameter("@costokm", txtvalkm.Text), totala, New SqlParameter("@transpub", "Auto"), New SqlParameter("@tecnicoid", Request.QueryString("t")), New SqlParameter("@usuarioid", usr.UsuarioId))
                cerrar()
                RGVerviaticos.DataBind()
            Catch ex As Exception

                lblerr.Text = ex.Message
            End Try

        ElseIf cbtipo.SelectedValue = "pub" Then

            Dim totalp As SqlParameter = New SqlParameter("@total", SqlDbType.Decimal, 13)
            totalp.Value = CDec(txtpubprecio.Value)
            Try
                ExecuteScalar(getConnString(), "SP_CREAR_VIATICOS", New SqlParameter("@Ticketid", Request.QueryString("id")), New SqlParameter("@km", DBNull.Value), New SqlParameter("@costokm", DBNull.Value), totalp, New SqlParameter("@transpub", cbtipopublic.SelectedValue), New SqlParameter("@tecnicoid", Request.QueryString("t")), New SqlParameter("@usuarioid", usr.UsuarioId))
                cerrar()
                RGVerviaticos.DataBind()

            Catch ex As Exception

                lblerr.Text = ex.Message
            End Try
        End If


    End Sub
    Private Sub cerrar()

        txtcankm.Text = ""
        txtpubprecio.Value = 0
        txtvalkm.Text = ""



    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load


    End Sub
End Class
