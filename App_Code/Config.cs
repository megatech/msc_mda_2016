using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;

/// <summary>
/// Summary description for Config
/// </summary>
public class Config
{
    public Config()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static string getValue(string _key)
    {
        NameValueCollection appSettings = WebConfigurationManager.AppSettings;
        string value = appSettings[_key];
        return value;
    }

}
