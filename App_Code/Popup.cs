﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;


namespace Popups
{
    public class PopupMgr2
    {
        public static void Cerrar(System.Web.UI.Page page, string idHiddenField, Dictionary<string, string> valores)
		{
			string functions = "<script>" + File.ReadAllText(page.Server.MapPath("~/js/PopupMgr.js")) + "</script>";
			string strVals = "";
			foreach (String k in valores.Keys) {
				if (strVals.Length > 0) {
					strVals += "[|]";
				}
				strVals += k + "[>]" + valores[k];
			}
			strVals.Replace("'", "\\'").Replace("\"", "\\\"");
			page.Response.Write(functions + "<script> CloseAndReturn('" + idHiddenField + "','" + strVals + "');</script>");
		}
        public static string LeerDato(HiddenField ctrl, string key, bool bClean = false)
        {
            if (!string.IsNullOrEmpty(ctrl.Value))
            {

                foreach (string v in Split(ctrl.Value, "[|]"))
                {
                    if (Split(v, "[>]")[0] == key)
                    {
                        if (bClean)
                            ctrl.Value = "";
                        return Split(v, "[>]")[1];
                    }

                }
                return null;
            }
            return null;
        }
        public static string[] Split(String text, String del)
        {
            string[] arr= {del};
            return text.Split(arr,StringSplitOptions.None);
        }
    }
}