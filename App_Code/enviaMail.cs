﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
//

//<summary>
// envia mail desde megatech "megarobot"
//</summary>

namespace FUNCIONES_MOD
{

    public class mailmega2
    {
        public ArrayList newArrLst = new ArrayList();

        public static bool enviamail(string @from, string para, string respuesta, string subjet, string cuerpo, Attachment logo, Attachment att1, Attachment att2)
        {
            System.Net.Mail.SmtpClient clie = new System.Net.Mail.SmtpClient();
            System.Net.NetworkCredential cred = new System.Net.NetworkCredential();

            cred.UserName = "megarobot";
            cred.Domain = "mega";
            cred.Password = "Tobo2012";
            clie.UseDefaultCredentials = false;

            clie.Credentials = cred;
            MailAddress tempfrom = new MailAddress(@from, "Megatech S.A.");
            MailMessage message = new MailMessage();
            if (para.Contains(","))
            {
                foreach (String dir in para.Split(",".ToCharArray())) { message.To.Add(dir); }
            }
            else { message.To.Add(para); }

            message.IsBodyHtml = true;
            if (!string.IsNullOrEmpty(respuesta))
            {
                message.ReplyToList.Add(new MailAddress(respuesta));

            }
            else
            {
                message.ReplyToList.Add(tempfrom);

            }

            message.Attachments.Add(att1);
            message.Attachments.Add(att2);
            message.Body = cuerpo;
            logo.ContentId = "logo";
            message.Attachments.Add(logo);
            message.Subject = subjet;
            message.Sender = tempfrom;
            message.From = tempfrom;


            clie.DeliveryMethod = SmtpDeliveryMethod.Network;
            clie.Send(message);
            return true;

        }
        public static bool enviamail(string @from, string para, string respuesta, string subjet, string cuerpo, ArrayList adjuntos = null)
        {

            System.Net.Mail.SmtpClient clie = new System.Net.Mail.SmtpClient();
            System.Net.NetworkCredential cred = new System.Net.NetworkCredential();

            cred.UserName = "megarobot";
            cred.Domain = "mega";
            cred.Password = "Tobo2012";
            clie.UseDefaultCredentials = false;
            clie.Credentials = cred;
            MailAddress tempfrom = new MailAddress(@from, "Megatech S.A.");
            MailMessage message = new MailMessage();

            if (adjuntos != null)
            {
                foreach (Attachment att in adjuntos) { message.Attachments.Add(att); }
            }
            message.Body = cuerpo;
            message.Subject = subjet;
            message.Sender = tempfrom;
            message.From = tempfrom;
            message.IsBodyHtml = true;
            message.To.Add(para);

            if (!string.IsNullOrEmpty(respuesta))
            {
                message.ReplyToList.Add(new MailAddress(respuesta));

            }
            else
            {
                message.ReplyToList.Add(tempfrom);

            }
            clie.DeliveryMethod = SmtpDeliveryMethod.Network;

            if (para.Contains(","))
            {
                foreach (String dir in para.Split(",".ToCharArray()))
                {
                    message.To.Clear();
                    message.To.Add(dir);
                    clie.Send(message);
                }
            }
            else
            {
                message.To.Add(para);
                clie.Send(message);
            }
            return true;
        }
        public static bool enviamail(string from, string to, string respuesta, string subjet, string cuerpo)
        {
            System.Net.Mail.SmtpClient clie = new System.Net.Mail.SmtpClient();
            System.Net.NetworkCredential cred = new System.Net.NetworkCredential();

            cred.UserName = "alertas@megatech.la";
            cred.Domain = "smtp.office365.com"; // Verificar si parametro Dominio es correcto
            cred.Password = "Al3rt4s2016";

            clie.UseDefaultCredentials = false;

            clie.Credentials = cred;

            MailAddress tempfrom = new MailAddress(from, "Megatech");
            MailMessage message = new MailMessage();

            message.IsBodyHtml = true;

            if (respuesta != "")
            {
                message.ReplyToList.Add(new MailAddress(respuesta));
            }
            else
            {
                message.ReplyToList.Add(tempfrom);

            }


            message.To.Add(to);
            message.Body = cuerpo;
            message.Subject = subjet;
            message.Sender = tempfrom;
            clie.DeliveryMethod = SmtpDeliveryMethod.Network;
            clie.Send(message);
            return true;

        }

    }


    public class validamail
    {

        public static bool validar_Mail(string sMail)
        {
            // retorna true o false   
            return Regex.IsMatch(sMail, "^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*)@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        }



    }

    public class leehtml
    {
        public static string readhtm(string path)
        {

            StreamReader objReader = new StreamReader(path);
            string sLine = "";
            ArrayList arrText = new ArrayList();
            string output = "<BR />";
            do
            {
                sLine = objReader.ReadLine();
                if ((sLine != null))
                {
                    arrText.Add(sLine);
                }
            } while (!(sLine == null));
            objReader.Close();


            foreach (String sLin in arrText)
            {
                output = output + sLin;



            }

            return output;

        }

    }

}