using System;
using System.Data;

namespace App_Code.TicketsLib
{
    public class BusquedaTicketAgenda
    {
        public static DataSet getResultado(BusquedaTicketAgendaFiltro bf)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = GetQueryBase();

                if (bf.FechaInicio != null)
                    sSql+= " And convert(char(19) ,Ticket.Fecha,120) >= '" +bf.FechaInicio.ToString("yyyy-MM-dd") + " 00:00:00' ";

                if (bf.FechaFin != null)
                    sSql+= " And convert(char(19) ,Ticket.Fecha,120) < '" + bf.FechaFin.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00' ";

                if (!bf.NroClaim.Equals(""))
                    sSql += " And Equipo.ClainNro like '%" + bf.NroClaim + "%' ";
                
                if (!bf.NroTicket.Equals(""))
                    sSql += " And Ticket.TicketId = " + bf.NroTicket + " ";

                if (!bf.Negocio.Equals(""))
                    sSql += " And	Ticket.EmpresaPartnerId In (Select EmpresaPartnerId From EmpresaPartner Where Upper(Nombre) like '%" + bf.Negocio.ToUpper() + "%') ";

                if (!bf.CodigoCliente.Equals(""))
                    sSql += " And ViewCliente.Codigo = '" + bf.CodigoCliente + "' ";
                else
                {
                    if (!bf.RazonSocial.Equals(""))
                        sSql += " And	UPPER(ViewCliente.RazonSocial) like '%" + bf.RazonSocial.ToUpper() + "%' ";
                }

                if (!bf.CodigoEquipo.Equals(""))
                    sSql += " And Equipo.Codigo = '" + bf.CodigoEquipo + "' ";
                else
                {
                    if (!bf.RazonSocial.Equals(""))
                        sSql += " And	UPPER(Equipo.Nombre) like '%" + bf.NombreEquipo.ToUpper() + "%' ";
                }

                if (!bf.NroSerie.Equals(""))
                    sSql += " And	UPPER(Ticket.NroSerie) like '%" + bf.NroSerie.ToUpper() + "%' ";

                if (!bf.Especialista.Equals(""))
                    sSql += " And	UPPER(Especialista.Nombre) like '%" + bf.Especialista.ToUpper() + "%' ";

                if (!bf.Estado.Equals(""))
                    sSql += " And Ticket.EstadoId In (Select ee.EstadoId From Estado ee Where UPPER(ee.Nombre) like '%" + bf.Estado.ToUpper() + "%') ";

                if (!bf.Especialista.Equals(""))
                    sSql += " And	UPPER(TipoProblema.Nombre) like '%" + bf.TipoProblema.ToUpper() + "%' ";
                if (bf.Usuario != "")
                {
                    sSql += "AND Ticket.EspecialistaId IN "
                    + "( "  +
                    "SELECT EU.EspecialistaId FROM Usuario U (nolock) INNER JOIN Perfil P (nolock) ON U.PerfilId = P.PerfilId " +
                    "AND P.ModifSoloSusTickets != 0 INNER JOIN EspecialistaUsuario EU (nolock) ON U.UsuarioId = EU.UsuarioId " +
                    "AND U.Nombre = '" + bf.Usuario +
                    "'  UNION SELECT DISTINCT E.EspecialistaId FROM Especialista E (nolock) , Usuario U (nolock) " +
                    "INNER JOIN Perfil P (nolock) ON U.PerfilId = P.PerfilId AND P.ModifSoloSusTickets = 0 AND U.Nombre = '" +
                    bf.Usuario + "' " +
                    " ) ";
                }
                sSql += " order by Ticket.TicketId";

                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                return ds;

            }
            catch
            {
                return null;
            }
        }
        private static string GetQueryBase() 
        {
            return "Select Ticket.TicketId as TicketNro,  " +
                    "	LEFT(ViewCliente.RazonSocial,15) as Razon_Social,   " +
                    "	LEFT(Ubicacion.Nombre,15) As Ubicacion,   " +
                    "	LEFT(UbicacionContacto.Nombre,15) As Contacto,   " +
                    "	LEFT(Especialista.Nombre,15) as Especialista,   " +
                    "	LEFT(Equipo.Nombre,25) as Nombre_Equipo,   " +
                    "	Ticket.EstadoId as EstadoId,   " +
                    "   Estado.Nombre as Estado, " +
                    "	Ticket.NroSerie as Numero_Serie,   " +
                    "	LEFT(TipoProblema.Nombre,15) as Tipo_Problema,   " +
                    "	Ticket.FechaUltimaModif as UltimaModif  " +
                    "FROM Equipo (nolock)   " +
                    "	INNER JOIN (Especialista (nolock)   " +
                    "	INNER JOIN  (Ubicacion (nolock)   " +
                    "	INNER JOIN  (UbicacionContacto (nolock)   " +
                    "	INNER JOIN (ViewCliente (nolock)   " +
                    "	INNER JOIN  Ticket (nolock)  " +
                    "	INNER JOIN TipoProblema (nolock)   " +
                    "	ON Ticket.IdTipoProblema = TipoProblema.IdTipoProblema   " +
                    "	ON ViewCliente.ClienteId = Ticket.ClienteId  " +
                    "	AND  ViewCliente.EmpresaId = Ticket.EmpresaId)  " +
                    "	ON  UbicacionContacto.UbicacionContactoId = Ticket.UbicacionContactoId)  " +
                    "	ON  Ubicacion.UbicacionId = UbicacionContacto.UbicacionId)  " +
                    "	ON  Especialista.EspecialistaId = Ticket.EspecialistaId)  " +
                    "	ON  Equipo.EquipoId = Ticket.ArticuloId   " +
                    "	AND Equipo.EmpresaId = Ticket.EmpresaId   " +
                    "   Inner Join Estado (nolock) On Estado.EstadoId = Ticket.EstadoId " +
                    "Where Ticket.EmpresaId = 1   " +
                    "And (Ticket.FechaBaja Is Null)  ";
        }

    }
    public class BusquedaTicketAgendaFiltro
    {
        public DateTime FechaInicio;
        public DateTime FechaFin;
        public string NroClaim;
        public string NroTicket;
        public string Negocio;
        public string CodigoCliente;
        public string RazonSocial;
        public string CodigoEquipo;
        public string NombreEquipo;
        public string NroSerie;
        public string Especialista;
        public string Estado;
        public string TipoProblema;
        public string Usuario;
    }
}
