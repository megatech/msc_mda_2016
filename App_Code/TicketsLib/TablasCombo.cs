using System;
using System.Data;
using System.Data.SqlClient;

namespace App_Code.TicketsLib
{
    public enum TablasTipoCombo {
        Negocio = 1,
        Especialista = 2,
        Vendedor = 3,
        TipoProblema = 4,
        Estado = 5
    }
    public class TablasCombo
    {
        public static DataTable getComboNegocio() 
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "SELECT EmpresaPartnerId as Codigo, Nombre as Nombre " +
                                "FROM EmpresaPartner (nolock) " +
                                "WHERE FechaBaja IS NULL AND EmpresaPartnerdefault = 1" +
                                "ORDER BY Nombre";
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataSet getComboEspecialista(Usuario usr)
        {
            try
            {
                string connString = Funciones.getConnString();

                string sSql = "SELECT EspecialistaId As Codigo, '[' + CAST(EspecialistaId As VARCHAR) + '] ' + Nombre As Nombre " +
                              "From Especialista (nolock) " +
                              "Where FechaBaja Is Null ";
                if (!(usr.EspecialistaID.Equals(DBNull.Value) || usr.EspecialistaID == ""))
                {
                    sSql += " AND EspecialistaId = " + usr.EspecialistaID + " ";
                }
                sSql += " ORDER BY EspecialistaId";
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                return ds;
            }
            catch 
            {
                return null;
            }
        }
        public static DataTable getComboVendedor()
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "Select '' as Codigo, '[Ninguno]' as Nombre Union "+
                                "SELECT VendedorId As Codigo , Nombre " +
                                "FROM ViewVendedor  (nolock) " +
                                "WHERE FechaBaja IS NULL " +
                                "ORDER BY Nombre";
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataTable getComboNuevoEstado(int EstadoActual )
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "Select 0 as Codigo, '[Ninguno]' as Nombre Union " +
                                "SELECT Estado.EstadoId Codigo, Estado.Nombre " +
                                "FROM Estado (nolock) " +
                                "WHERE Estado.FechaBaja IS NULL " +
                                "AND Estado.EstadoId IN (SELECT EstadoIdFin " +
                                "FROM Accion (nolock) " +
                                "WHERE Convert(int,EstadoIdIni) = @EstadoActual ) ";
                SqlParameter p = new SqlParameter("EstadoActual", SqlDbType.Decimal, 13);
                p.Value = EstadoActual;
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql,p);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataTable getComboNuevoEstado(int EstadoActual,int perfil)
        {
            try
            {
                string connString = Funciones.getConnString();
                string sSql = "SELECT 0 AS Codigo, '[Ninguno]' AS Nombre " +
                              "UNION " +
                              "SELECT e.EstadoId Codigo, e.Nombre FROM " +
                                "Estado (nolock) e	INNER JOIN permisoEstado pe ON pe.estadoId = e.estadoId " +
                                "AND pe.perfilId = @perfil AND pe.fechabaja IS NULL	WHERE " +
                                "e.FechaBaja IS NULL	AND e.EstadoId IN ( SELECT " +
                                            "EstadoIdFin	FROM Accion (nolock) WHERE " +
                                            "CONVERT (INT, EstadoIdIni) = @EstadoActual)";

                //string sSql = "Select 0 as Codigo, '[Ninguno]' as Nombre Union " +
                //                "SELECT e.EstadoId Codigo, e.Nombre " +
                //                "FROM Estado (nolock) e INNER JOIN permisoEstado pe on pe.estadoId=e.estadoId " +
                //                "AND pe.perfilId = @perfil AND pe.fechabaja is null " +
                //                "WHERE e.FechaBaja IS NULL " +
                //                "AND e.EstadoId IN (SELECT EstadoIdFin " +
                //                "FROM Accion (nolock) " +
                //                "WHERE Convert(int,EstadoIdIni) = @EstadoActual ) ";
                SqlParameter[] par = new SqlParameter[2];
                par[0] = new SqlParameter("EstadoActual", SqlDbType.Decimal, 13);
                par[1] = new SqlParameter("Perfil", SqlDbType.Int, 13);
                par[0].Value = perfil;
                par[1].Value = EstadoActual;
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql, par);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataTable getComboTipoProblema()
        {
            try
            {
                string connString = Funciones.getConnString();

                string sSql = "Select 0 as Codigo, '[Ninguno]' as Nombre Union "+
                              "SELECT IdTipoProblema As Codigo , Nombre FROM TipoProblema (nolock)" +
                              " WHERE FechaBaja IS NULL " +
                              "ORDER BY Nombre";
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataTable getComboEstados()
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "SELECT EstadoId  As Codigo , Nombre " +
                                "FROM Estado (nolock) " +
                                "WHERE FechaBaja IS NULL And EstadoInicio != 0 " +
                                "ORDER BY Nombre";
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataTable getComboCategoriaIVA()
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "SELECT CategoriaIvaId  As Codigo , Nombre " +
                                "FROM viewcategoriaiva " +
                                "WHERE CategoriaIvaId = 2"+
                                "ORDER BY Nombre";
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataTable getComboProvincia()
        {
            try
            {
                string connString = Funciones.getConnString();

                string sSql = "Select '-1' as Codigo , '[Seleccione una opci�n]' as Nombre Union " +
                                "SELECT ProvinciaId  As Codigo , ltrim(rtrim(UPPER(Nombre))) " +
                                "FROM ViewProvincia " +
                                "ORDER BY Codigo";
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataTable getComboMarca(int EmpresaId, bool esObligatorio)
        {
            try
            {
                string connString = Funciones.getConnString();

                string sSql = "";
                if (!esObligatorio)
                    sSql += "Select -1 as Codigo , '[Sin selecci�n]' as Nombre Union ";

                sSql += "Select MarcaId as Codigo, Nombre " +
                              "  From ViewMarca (nolock) " +
                              "  Where FechaBaja Is Null " +
                              " AND (MarcaId = 1 OR MarcaId = 30 OR MarcaId = 5 OR MarcaId = 33 OR MarcaId = 49 OR MarcaId = 19 OR MarcaId = 10)" + 
                              "  And EmpresaId = " + EmpresaId.ToString()+
                              "Order by Nombre";    

                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }        
        public static DataTable getComboRubro(int EmpresaId, bool esObligatorio)
        {
            try
            {
                string connString = Funciones.getConnString();

                string sSql = "";
                if (!esObligatorio)
                    sSql += "Select -1 as Codigo , '[Sin selecci�n]' as Nombre Union ";

                sSql += "Select RubroId as Codigo, Nombre " +
                              "  From ViewRubro (nolock) " +
                              "  Where FechaBaja Is Null" +
                              "  AND RubroId != 2" +
                              "  And EmpresaId = " + EmpresaId.ToString();

                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataTable dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
