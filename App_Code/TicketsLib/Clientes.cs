using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace App_Code.TicketsLib
{
    public class Clientes
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger("File");

        public static DataSet getClientes(FiltroCliente fc)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                string sSql = "SELECT ClienteId , Codigo , RazonSocial , CUIT, Domicilio, Localidad  " +
                                "FROM ViewCliente (nolock) " +
                                "WHERE FechaBaja IS NULL ";
                if (fc.Codigo != "")
                {
                    sSql += " AND Codigo LIKE '%" + fc.Codigo.Trim() + "%' ";
                }
                if (fc.RazonSocial != "")
                {
                    sSql += " AND RazonSocial LIKE '%" + fc.RazonSocial.Trim() + "%' ";
                }
                if (fc.Cuit != "")
                {
                    sSql += " AND CUIT LIKE '%" + fc.Cuit.Trim() + "%' ";
                }
                if (fc.Telefono1  != "")
                {
                    sSql += " AND telefono1 LIKE '%" + fc.Telefono1.Trim() + "%' ";
                }
                sSql += "ORDER BY RazonSocial ";
                
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                return ds;

            }
            catch
            {
                return null;
            }
        }
        public static FiltroCliente getUnCliente(FiltroCliente fc)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "SELECT Top 1 ClienteId , Codigo , RazonSocial , CUIT, Telefono1  " +
                            "FROM   ViewCliente (nolock) " +
                            "WHERE FechaBaja IS NULL ";
                if (fc.ClienteId != null)
                {
                    sSql += "AND ClienteId = '" + fc.ClienteId + "' ";
                }
                else if (fc.Codigo!=null) {
                    sSql += "And   Codigo ='" + fc.Codigo + "' ";
                }
                else if (fc.Cuit != null)
                {
                    sSql += "And  Cuit ='" + fc.Cuit + "'";
                }
                else if (fc.Telefono1 != null)
                {
                    sSql += "And  Telefono1 ='" + fc.Telefono1 + "' ";
                }
                sSql += "ORDER BY CONVERT(INTEGER,codigo) DESC ";
                DataSet ds= SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataRow row = ds.Tables[0].Rows[0];
                fc.ClienteId = row["ClienteId"].ToString();
                fc.RazonSocial = row["RazonSocial"].ToString();
                fc.Codigo = row["Codigo"].ToString();
                fc.Cuit = row["CUIT"].ToString();
                fc.Telefono1 = row["Telefono1"].ToString();
                return fc;

            }
            catch
            {
                return null;
            }
        }
        public static DataSet getUbicacionesCliente(string ClienteId)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "SELECT u.UbicacionId as Id, rtrim(u.Nombre) + ' - [' + rtrim(u.Domicilio) + '] - [' + rtrim(u.Telefono1) + ']' NOMBRE " +
                            "FROM Ubicacion u (nolock) " +
                            "WHERE u.FechaBaja IS NULL " +
                            "And u.ClienteId ='" + ClienteId + "'";

                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                return ds;

            }
            catch
            {
                return null;
            }
        }
        public static DataSet getContactosUbicacion(string UbicacionId)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "SELECT UbicacionContactoId as Id, rtrim(Nombre) + ' - [' + rtrim(Telefono1) + ']' +  " +
                            "		Case When Sector Is Null Then ''  " +
                            "		Else ' - [' + rtrim(Sector) + ']'  " +
                            "	End as NOMBRE  " +
                            "FROM UbicacionContacto (nolock)  " +
                            "WHERE FechaBaja IS NULL  " +
                            "AND UbicacionId = " + UbicacionId + " ";

                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                return ds;

            }
            catch
            {
                return null;
            }
        }
        public static recCliente insertCliente(recCliente rc)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string spQry = "SP_GENERO_NUEVO_CLIENTE";

                bool bOk = true;

                SqlParameter[] par = new SqlParameter[19];

                SqlParameter p = new SqlParameter("EmpresaId", SqlDbType.Decimal, 13);
                p.Value = rc.EmpresaId;
                par[0] = p;

                p = new SqlParameter("Nombre", rc.RazonSocial);
                par[1] = p;

                p = new SqlParameter("Fantasia", rc.Fantasia);
                par[2] = p;

                p = new SqlParameter("Domicilio", rc.Domicilio);
                par[3] = p;

                p = new SqlParameter("Localidad", rc.Localidad);
                par[4] = p;

                p = new SqlParameter("CodPos", rc.CodPostal);
                par[5] = p;

                p = new SqlParameter("CodPrv", rc.Provincia);
                par[6] = p;

                p = new SqlParameter("CodPais ", "");
                par[7] = p;

                p = new SqlParameter("Telefono", rc.Telefono);
                par[8] = p;

                p = new SqlParameter("Fax", rc.Fax);
                par[9] = p;

                p = new SqlParameter("Email", rc.Email);
                par[10] = p;

                p = new SqlParameter("CodTrat", rc.CategoriaIVA);
                par[11] = p;

                p = new SqlParameter("CodDoc", "");
                par[12] = p;

                p = new SqlParameter("NroDoc1", rc.CUIT);
                par[13] = p;

                p = new SqlParameter("NroDoc2", rc.Documento);
                par[14] = p;

                p = new SqlParameter("CodCondi", "");
                par[15] = p;

                p = new SqlParameter("tr", SqlDbType.SmallInt);
                p.Value = 1;
                par[16] = p;

                p = new SqlParameter("ClienteId", SqlDbType.Char, 8);
                p.Direction = ParameterDirection.Output;
                par[17] = p;

                p = new SqlParameter("DescError", SqlDbType.VarChar, 200);
                p.Direction = ParameterDirection.Output;
                par[18] = p;

                foreach (SqlParameter itm in par)
                { if (itm.Value == "") { itm.Value = null; } }

                try
                {
                    int ret = SqlHelper.ExecuteNonQuery(connString, CommandType.StoredProcedure, spQry, par);
                    rc.ClienteID = par[17].Value.ToString();
                    rc.MensajeError = par[18].Value.ToString();
                    if (!(rc.ClienteID.Trim() == ""))
                    {
                        SqlConnection sql = new SqlConnection(ConfigurationManager.ConnectionStrings["TicketDB"].ToString());
                        SqlCommand cmd = new SqlCommand();
                        sql.Open();
                        cmd.Connection = sql;
                        cmd.CommandText = "update viewCliente set observaciones = '" + rc.Observaciones + "' where ClienteId = '" + rc.ClienteID + "'";
                        cmd.ExecuteNonQuery();
                        sql.Close();
                        cmd.Dispose();
                        sql.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    rc.MensajeError = ex.Message;
                }
                return rc;
            }
            catch (Exception ex)
            {
                rc.MensajeError = "Error en InsertCliente: " + ex.Message;
                logger.Error(rc.MensajeError, ex);
                return rc;
            }
        }
    }
    public class FiltroCliente
    {
        public string ClienteId;
        public string Codigo;
        public string RazonSocial;
        public string Cuit;
        public string Telefono1;
    }

    /*
    public class recCliente
    {
        public string ClienteID;
        public string RazonSocial="";
        public int EmpresaId=0;
        public string Fantasia="";
        public string CategoriaIVA;
        public string CUIT="";
        public string Documento="";
        public string Domicilio="";
        public string Localidad = "";
        public string Provincia;
        public string Pais = "1";
        public string CodPostal = "";
        public string Telefono = "";
        public string Fax = "";
        public string Email = "";
        public string Observaciones = "";
        public string MensajeError;
    }*/

    public class recCliente
    {
        public string ClienteID;
        public string RazonSocial;
        public int EmpresaId = 0;
        public string Fantasia;
        public string CategoriaIVA;
        public string CUIT;
        public string Documento;
        public string Domicilio;
        public string Localidad;
        public string Provincia;
        public string Pais = "1";
        public string CodPostal;
        public string Telefono;
        public string Fax;
        public string Email;
        public string Observaciones;
        public string MensajeError;
    }
}
