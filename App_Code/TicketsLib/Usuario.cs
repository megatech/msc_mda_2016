using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
namespace App_Code.TicketsLib {
    [Serializable]
    public class Usuario
    {

        private string mNick;
        private string mNombre;
        private string mClave;
        private string mUsuarioID;
        private string mEspecialistaID;
        private string mConnString;
        private int nCodePage;
        private string mPerfilId;
        private string mEquipo;
        private string mEmpresaPartner;
        public List<Pantallas> Pantalla;



        public Usuario(string connString, int codePage) {
            mConnString = connString;
            nCodePage = codePage;
            

        }//CONSTRUCTOR

        public bool BuscarPantalla(int p)
        {
            bool ok = false;


            foreach (Pantallas Pa in Pantalla)
            {
                if (Pa.IdPantalla == p)
                {
                    ok = true;
                    return ok;
                }
            }


            return ok;
        }// Retorna true, si el int suministrado como IdPantalla se encuentra en la lista
        public bool BuscarPantalla(string descripcion) {
            bool ok = false;

            foreach (Pantallas Pa in Pantalla)
            {
                if (String.Compare(Pa.Descripcion.Trim(), descripcion, true) == 0) { ok = true; return ok; }
            
            }

            return ok;
        }// Retorna true, si el int suministrado como Descripcion se encuentra en la lista
        public string Nick
        {
            get
            {
                return mNick;
            }
            set
            {
                mNick = value.Trim();
            }
        } 
        public string EmpresaPartner {
            get { return mEmpresaPartner.Trim(); }
            set { mEmpresaPartner = value.Trim(); }
        } // Area en el Relleno del Ticket si es NULL Probablemente sea Supervisor
        public string Equipo
        {
            get
            {
                return mEquipo.Trim();
            }

            set

            {
                mEquipo = value.Trim();
            }
        } // Equipo en el Relleno del Ticket si es NULL Probablemente sea Supervisor
        public string Perfil
        {
            get
            {
                return mPerfilId.Trim();
            }
            set
            {
                mPerfilId = value.Trim();
            }
        }
        public string Nombre
        {
            get
            {
                return mNombre.Trim();
            }
            set
            {
                mNombre = value.Trim();
            }
        }
        public string Clave
        {
            get
            {
                return mClave;
            }
            set
            {
                mClave = value.Trim();
            }
        }
        public string UsuarioId
        {
            get
            {
                return mUsuarioID;
            }
            set
            {
                mUsuarioID = value.Trim();
            }
        }
        public string EspecialistaID
        {
            get
            {
                return mEspecialistaID;
            }
            set
            {
                mEspecialistaID = value.Trim();
            }
        }
        public Boolean cambioClave(string newClave)
        {
            String msg = "";
            try
            {
                bool ok = loadUsuario();
                if (ok) {
                    mClave = getClaveEncriptada(newClave);
                    ok = saveUsuario();
                };

                return ok;

            }
            catch (Exception e)
            {

                throw new CustomExceptions.UserLoginException(e.Message);
            }
        }
        public bool checkNick()
        {
            String msg = "";
            try
            {
                String currClave = mClave;
                bool ok = loadUsuario();
                if (ok)
                {
                    ok = true;
                    if (getClaveEncriptada(currClave.Trim()).Equals(mClave.Trim()))
                    {
                        ok = true;
                    }
                    else { ok = false; }
                    /////////////////////////////////////
                    //ok = true;
                    /////////////////////////////////////

                }
                mClave = currClave;
                return ok;

            }
            catch (Exception e)
            {

                throw new CustomExceptions.UserLoginException(e.Message);
            }
        }
        public void loadPantallas()
        {
            SqlConnection SQL = new SqlConnection();
            SqlCommand CMD = new SqlCommand();
            SqlDataAdapter DA;
            DataTable DT = new DataTable();
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings["ticketDB"].ToString();
            string q = "SELECT p.PantallaId, p.PantNomReal, p.PantNomFantasia " +
                              " FROM pantalla p left join permiso pe ON pe.pantallaId = p.PantallaId" +
                              " WHERE pe.PerfilId =" + Convert.ToString(mPerfilId);
            DA = new SqlDataAdapter(q, SQL);
            DA.Fill(DT);
            if (DT.Rows.Count > 0)
                Pantalla = new List<Pantallas>();
                try
                {
                    foreach (DataRow r in DT.Rows)
                    {
                        string a = r["PantallaId"].ToString();
                        string b = r["PantNomFantasia"].ToString();
                        Pantallas c = new Pantallas(Convert.ToInt32(a), b);
                        Pantalla.Add(c);
                       

                    }
                
                

                }
                catch (Exception ex)
                {

                    //throw new CustomExceptions.UserLoginException(e.Message);
                    throw ex;
                }
        
           
        }// Carga en la lista, las pantallas y las descripciones pertinentes
        private bool loadUsuario()
        {
            String msg = "";
            bool ok = true;
            try
            {
                String sSql = "Select top 1 u.Nombre as Codigo, u.Nombre as Nombre, u.password as clave  " +
                               "	,UsuarioId,(SELECT e.EspecialistaId " +
		                       "            FROM EspecialistaUsuario e (nolock) " +
                               "                ,Especialista ee " +
                               "            WHERE ee.EspecialistaId = e.EspecialistaId " +
                               "            AND ee.FechaBaja Is Null " +
                               "            AND e.UsuarioId = u.UsuarioId) EspecialistaId, perfilid as perfilid, " +
                               " (SELECT CodEquipo FROM NIVELES WHERE perfilID = u.PerfilID ) Equipo , " +
                               " (SELECT n.EmpresaPartnerID FROM NIVELES n left join empresapartner e ON n.EmpresaPartnerID = e.EmpresaPartnerId WHERE perfilID = u.PerfilID ) EmpresaPartner " +
                               " FROM Usuario u Where u.Nombre = @nick and u.fechabaja is null";
                System.Data.SqlClient.SqlParameter p = new System.Data.SqlClient.SqlParameter("nick", mNick.ToUpper());
                DataSet ds = SqlHelper.ExecuteDataset(mConnString, System.Data.CommandType.Text, sSql, p);
                if (ds.Tables[0].Rows.Count != 1)
                {
                    ok = false;
                }
                else {
                    DataRow r = ds.Tables[0].Rows[0];
                    mNick = r["Codigo"].ToString();
                    mNombre =r["Nombre"].ToString();
                    mClave = r["clave"].ToString();
                    mEspecialistaID = r["EspecialistaId"].ToString();
                    mUsuarioID = r["UsuarioId"].ToString();
                    mPerfilId = r["perfilid"].ToString();
                    mEquipo = r["Equipo"].ToString();
                    mEmpresaPartner = r["EmpresaPartner"].ToString();
                    loadPantallas();
                }                

                return ok;

            }
            catch (Exception e)
            {

                throw new CustomExceptions.UserLoginException(e.Message);
            }
        }
        private string getClaveEncriptada(string sClave)
        {
            try
            {

               

                string res = "";
                char[] chr = sClave.ToCharArray();
                char[] chrOut = sClave.ToCharArray();
                
                Encoding ec = Encoding.GetEncoding(nCodePage);
                byte[] bbc = ec.GetBytes(chr);
                byte[] bbcOut = ec.GetBytes(chr);
                for (int i = 0; i < sClave.Length; ++i)
                {
                    bbcOut[i] = (byte)((255 - bbc[i]) + 7);
                }

                
                byte[] bb = ec.GetBytes(chrOut);
                chrOut = ec.GetChars(bbcOut);
                for (int i = 0; i < chrOut.Length; ++i)
                {
                    res += chrOut[i].ToString();
                }
                return res;
            }
            catch
            {
                return null;
            }
        }
        private bool saveUsuario()
        {   
            String msg = "";
            bool ok = true;
            try
            {

                String sSql = "Update Usuario Set password = @clave Where UsuarioID = @usuarioid";

                System.Data.SqlClient.SqlParameter[] pCol = new System.Data.SqlClient.SqlParameter[2];
                System.Data.SqlClient.SqlParameter p1 = new System.Data.SqlClient.SqlParameter("clave", mClave);
                System.Data.SqlClient.SqlParameter p2 = new System.Data.SqlClient.SqlParameter("UsuarioID", mUsuarioID);
                pCol[0] = p1;
                pCol[1] = p2;
                int i = SqlHelper.ExecuteNonQuery(mConnString, System.Data.CommandType.Text, sSql, pCol);
                if (i!=1)
                {
                    ok = false;
                }

                return ok;

            }
            catch (Exception e)
            {

                throw new CustomExceptions.UserLoginException(e.Message);
            }
        }
    }
}