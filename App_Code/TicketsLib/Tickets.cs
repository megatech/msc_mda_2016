using System;
using System.Data;
using System.Data.SqlClient;

namespace App_Code.TicketsLib
{
    public class Tickets
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger("File");

        public static TicketData InsertTicket(TicketData unTicket)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string spQry = "SP_GENERO_NUEVO_TICKET";
                SqlParameter[] par = new SqlParameter[24];

                SqlParameter p = new SqlParameter("EmpresaId", SqlDbType.Decimal,13);

                p.Value=unTicket.idEmpresa;
                par[0] = p;
                p = new SqlParameter("ClienteId", unTicket.idCliente);
                //p.Value = unTicket.idCliente;
                par[1] = p;
                p = new SqlParameter("EspecialistaId", SqlDbType.Decimal, 13);
                p.Value = unTicket.idEspecialista;
                par[2] = p;
                p = new SqlParameter("ArticuloId", SqlDbType.Int);
                p.Value = unTicket.idArticulo;
                par[3] = p;
                p = new SqlParameter("EstadoId", SqlDbType.Decimal, 13);
                p.Value = unTicket.idEstado;
                par[4] = p;
                p = new SqlParameter("EmpresaPartnerId", SqlDbType.Decimal, 13);
                p.Value = unTicket.idEmpresaPartner;
                par[5] = p;
                p = new SqlParameter("Prioridad", unTicket.Prioridad);
                par[6] = p;
                p = new SqlParameter("NroSerie", unTicket.NroSerie);
                par[7] = p;
                p = new SqlParameter("Problema", unTicket.Problema);
                par[8] = p;
                p = new SqlParameter("Fecha", SqlDbType.DateTime, 13);
                p.Value = unTicket.fecha;
                par[9] = p;
                p = new SqlParameter("ClainNro", unTicket.nroClaim);
                par[10] = p;
                p = new SqlParameter("FechaEstimada", SqlDbType.DateTime, 13);
                if (Funciones.isDateTimeNull(unTicket.FechaEstimada))
                    p.Value = Convert.DBNull;
                else
                    p.Value = unTicket.FechaEstimada;

                par[11] = p;
                p = new SqlParameter("UsuarioId", SqlDbType.Decimal, 13);
                p.Value = unTicket.idUsuario;
                par[12] = p;
                p = new SqlParameter("UbicacionContactoId", SqlDbType.Decimal, 13);
                p.Value = unTicket.idUbicacionContacto;
                par[13] = p;
                p = new SqlParameter("IdTipoProblema", SqlDbType.Decimal, 13);
                p.Value = unTicket.idTipoProblema;
                par[14] = p;
                p = new SqlParameter("VendedorId", unTicket.idVendedor);
                if (unTicket.idVendedor.Equals("0"))
                    p.Value = Convert.DBNull; ;
                par[15] = p;
                p = new SqlParameter("FechaInicio", SqlDbType.DateTime, 13);
                p.Value = unTicket.fechaInicio;
                par[16] = p;
                p = new SqlParameter("FechaFin", SqlDbType.DateTime, 13);
                if (!Funciones.isDateTimeNull(unTicket.fechaFin))
                    p.Value = unTicket.fechaFin;
                else
                    p.Value=Convert.DBNull;
                par[17] = p;

                //p = new SqlParameter("sistemaOperativoId", 7);
                //par[18] = p;

                //p = new SqlParameter("arquitecturaId", 1);
                //par[19] = p;

                //p = new SqlParameter("servicePackId", 1);
                //par[20] = p;

                //p = new SqlParameter("modificado", 0);
                //par[21] = p;

                p = new SqlParameter("sistemaOperativoId", 3);
                par[18] = p;

                p = new SqlParameter("arquitecturaId", 2);
                par[19] = p;

                p = new SqlParameter("servicePackId", 3);
                par[20] = p;

                p = new SqlParameter("modificado", 1);
                par[21] = p;

                p = new SqlParameter("TicketId", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                par[22] = p;

                p = new SqlParameter("DescError", SqlDbType.VarChar,200);
                p.Direction = ParameterDirection.Output;
                par[23] = p;

                try
                {
                    int ret = SqlHelper.ExecuteNonQuery(connString, CommandType.StoredProcedure, spQry, par);
                    unTicket.idTicket = int.Parse(par[22].Value.ToString());
                    unTicket.mensajeError = par[23].Value.ToString();
                }
                catch (Exception ex)
                {
                    unTicket.mensajeError = ex.Message;
                }
                return unTicket;
            }
            catch (Exception ex)
            {
                logger.Error("Error en InsertTicket: " + ex.Message,ex);
                return null;
            }
        }

        public static TicketData EditTicket(TicketData unTicket)
        {
            try
            {
                string connString = Funciones.getConnString();
                string spQry = "SP_ACTUALIZAR_TICKET_mscmda";

                
                SqlParameter[] par = new SqlParameter[30];

                SqlParameter p = new SqlParameter("TicketId", SqlDbType.Decimal, 13);
                p.Value = unTicket.idTicket;
                par[0] = p;

                p = new SqlParameter("EstadoId", SqlDbType.Int);
                p.Value = unTicket.idEstado;
                par[1] = p;

                p = new SqlParameter("NegocioId", unTicket.idEmpresaPartner);
                par[2] = p;

                p = new SqlParameter("InformeCliente", unTicket.InformeCliente);
                par[3] = p;

                p = new SqlParameter("InformeInterno", unTicket.InformeInterno);
                par[4] = p;


                p = new SqlParameter("UsuarioId", SqlDbType.Decimal, 13);
                p.Value = unTicket.idUsuario;
                par[5] = p;


                p = new SqlParameter("fglError", SqlDbType.SmallInt);
                p.Direction = ParameterDirection.Output;
                par[6] = p;
                p = new SqlParameter("DescError", SqlDbType.VarChar, 200);
                p.Direction = ParameterDirection.Output;
                par[7] = p;

                p = new SqlParameter("ClienteId", unTicket.idCliente);
                par[8] = p;

                p = new SqlParameter("UbicacionContactoId", unTicket.idUbicacionContacto);
                par[9] = p;

                p = new SqlParameter("ClainNro", unTicket.nroClaim);
                par[10] = p;

                p = new SqlParameter("EspecialistaId", unTicket.idEspecialista);
                par[11] = p;

                p = new SqlParameter("NroSerie", unTicket.NroSerie);
                par[12] = p;

                p = new SqlParameter("Prioridad", unTicket.Prioridad);
                par[13] = p;

                p = new SqlParameter("Problema", unTicket.Problema);
                par[14] = p;

                p = new SqlParameter("ArticuloId", unTicket.idArticulo);
                par[15] = p;

                p = new SqlParameter("IdTipoProblema", unTicket.idTipoProblema);
                par[16] = p;

                //p = new SqlParameter("sistemaOperativoId", unTicket.sistemaOperativoId);
                //par[17] = p;

                //p = new SqlParameter("arquitecturaId", unTicket.arquitecturaId);
                //par[18] = p;

                //p = new SqlParameter("servicePackId", unTicket.servicePackId);
                //par[19] = p;
                
                //p = new SqlParameter("modificado", unTicket.modificado);
                //par[20] = p;
                
                p = new SqlParameter("recontactarDesde", unTicket.recontactarDesde);
                par[17] = p;

                p = new SqlParameter("popContacto", unTicket.popContacto);
                par[18] = p;

                p = new SqlParameter("popSo", unTicket.popSO);
                par[19] = p;

                p = new SqlParameter("poplicenciaSO", unTicket.poplicenciaSO);
                par[20] = p;

                p = new SqlParameter("popPC", unTicket.popPC);
                par[21] = p;

                p = new SqlParameter("popAntiguedad", unTicket.popAntiguedad);
                par[22] = p;

                p = new SqlParameter("popProcesador", unTicket.popProcesador);
                par[23] = p;

                p = new SqlParameter("popRam", unTicket.popRam);
                par[24] = p;

                p = new SqlParameter("popTipoconexion", unTicket.popTipoconexion);
                par[25] = p;

                p = new SqlParameter("popmodem", unTicket.popmodem);
                par[26] = p;

                p = new SqlParameter("popDetalle", unTicket.popDetalle);
                par[27] = p;

                p = new SqlParameter("idMotivoRecontacto", unTicket.idMotivoRecontacto);
                par[28] = p;

                p = new SqlParameter("idMotivoEnv_Tec", unTicket.popMotivo_Tec);
                par[29] = p;
                try
                {
                    int ret = SqlHelper.ExecuteNonQuery(connString, CommandType.StoredProcedure, spQry, par);
                    unTicket.mensajeError = par[7].Value.ToString();
                }
                catch (Exception ex)
                {
                    unTicket.mensajeError = ex.Message;
                }
                return unTicket;
            }
            catch (Exception ex)
            {
                logger.Error("Error en EditTicket: " + ex.Message, ex);
                return null;
            }
        }

        public static TicketData getTicket(int idTicket)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                string sql = "SELECT     dbo.ticket.TicketId " +
                        "		,Convert(varchar,dbo.ticket.Fecha,103) + ' ' + Left(Convert(varchar,dbo.ticket.Fecha,108),5) as Fecha " +
                        "		,Convert(varchar,dbo.ticket.FechaInicio,103) FechaInicio " +
                        "		,IsNull(Convert(varchar,dbo.ticket.FechaFin,103),'') as FechaFin " +
                        "		,IsNull(dbo.ticket.ClainNro,'') as ClainNro " +
                        "		,EmpresaPartner.Nombre as Negocio " +
                        "		,dbo.ViewCliente.Codigo as ClienteCodigo " +
                        "		,LEFT(dbo.ViewCliente.RazonSocial, 15) AS Razon_Social " +
                        "		,LEFT(dbo.Ubicacion.Nombre, 15) AS Ubicacion " +
                        "		,LEFT(dbo.UbicacionContacto.Nombre, 15) AS Contacto " +
                        "		,LEFT(dbo.especialista.Nombre, 15) AS Especialista " +
                        "		,dbo.equipo.Codigo as Codigo_Equipo " +
                        "		,LEFT(dbo.equipo.Nombre, 25) AS Nombre_Equipo " +
                        "		,dbo.Estado.Nombre AS Estado " +
                        "		,ticket.EstadoId " +
                        "		,dbo.ticket.NroSerie AS Numero_Serie " +
                        "		, LEFT(dbo.tipoproblema.Nombre, 15) AS Tipo_Problema " +
                        "		,Convert(varchar,dbo.ticket.FechaUltimaModif,103)  AS UltimaModif " +
                        "		,IsNull(ViewVendedor.Nombre,'') as Vendedor " +
                        "		,dbo.Ticket.Prioridad " +
                        "		,Problema as Detalle " +
		                "       ,Ticket.EmpresaId, Ticket.ClienteId, Ticket.EspecialistaId, Ticket.ArticuloId, Ticket.EmpresaPartnerID, Ticket.Fecha as FechaDt " +
                        "       ,IsNull(Ticket.UsuarioId,0) as UsuarioId, Ticket.UbicacionContactoId, Ticket.IdTipoProblema, Ticket.VendedorId, Ticket.FechaInicio as FechaInicioDt, Ticket.FechaFin as FechaFinDt " +
                        "       ,dbo.Ticket.FechaEstimada, IsNull(dbo.Ticket.InformeInterno,'') as InformeInterno, " +
                        "       IsNull(dbo.Ticket.InformeCliente,'') as InformeCliente, " +
                        "       sistemaOperativoId,licenciaSo, "  +
                        "       tir.desde recontactarDesde, tir.motivoId idMotivoRecontacto," +
                        "       tpro.contacto contactopop,tpro.pc,tpro.tipoconexion,tpro.antiguedad,tpro.procesador,tpro.ram,tpro.detalle detallepop ,tpro.modem" +
                        " FROM    dbo.ticket (nolock)  " +
                        "		INNER JOIN dbo.equipo (nolock)  " +
                        "				ON dbo.equipo.EquipoId = dbo.ticket.ArticuloId AND  " +
                        "				dbo.equipo.EmpresaId = dbo.ticket.EmpresaId " +
                        "		INNER JOIN dbo.Estado (nolock)  " +
                        "				ON dbo.ticket.EstadoId = dbo.Estado.EstadoId " +
                        "		INNER JOIN dbo.especialista (nolock) " +
                        "				ON dbo.especialista.EspecialistaId = dbo.ticket.EspecialistaId  " +
                        "		INNER JOIN dbo.ViewCliente (nolock) " +
                        "				ON dbo.ViewCliente.ClienteId = dbo.ticket.ClienteId  " +
                        "						AND dbo.ViewCliente.EmpresaId = dbo.ticket.EmpresaId  " +
                        "		INNER JOIN dbo.tipoproblema (nolock)  " +
                        "			ON dbo.tipoproblema.IdTipoProblema = dbo.ticket.IdTipoProblema  " +
                        "		INNER JOIN dbo.EmpresaPartner (nolock) " +
                        "				ON dbo.EmpresaPartner.EmpresaPartnerId = dbo.ticket.EmpresaPartnerId " +
                        "		INNER JOIN dbo.UbicacionContacto (nolock) " +
                        "				ON dbo.UbicacionContacto.UbicacionContactoId = dbo.ticket.UbicacionContactoId  " +
                        "				INNER JOIN dbo.Ubicacion (nolock)  " +
                        "					ON dbo.Ubicacion.UbicacionId = dbo.UbicacionContacto.UbicacionId  " +
                        "		LEFT JOIN ViewVendedor (nolock) " +
                        "					ON dbo.ticket.VendedorId = dbo.ViewVendedor.VendedorId " +
                        "       LEFT JOIN dbo.itemSO (nolock) ON dbo.ticket.ticketId=dbo.itemSO.ticketId " +
                        "       LEFT JOIN dbo.ticketInfoRecontacto tir (nolock) ON dbo.ticket.ticketId=tir.ticketId " +
                        "       LEFT JOIN dbo.ticketProblema tpro ON dbo.ticket.ticketId = tpro.ticketId" +        
                        "       Where Ticket.TicketId = @TicketId ";
                SqlParameter p = new SqlParameter("TicketId", SqlDbType.Int);
                p.Value = idTicket;

                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sql,p);
                TicketData tkt = new TicketData();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow row = ds.Tables[0].Rows[0];
                        tkt.idTicket = idTicket;
                        tkt.idEmpresa = int.Parse(row["EmpresaId"].ToString());
                        tkt.idCliente = row["ClienteId"].ToString();
                        tkt.idEspecialista = int.Parse(row["EspecialistaId"].ToString());
                        tkt.idArticulo = int.Parse(row["ArticuloId"].ToString());
                        tkt.idEstado = int.Parse(row["EstadoId"].ToString());
                        tkt.idEmpresaPartner = int.Parse(row["EmpresaPartnerID"].ToString());
                        tkt.Prioridad = row["Prioridad"].ToString();
                        tkt.NroSerie = row["Numero_Serie"].ToString();
                        tkt.Problema = row["Detalle"].ToString();
                        tkt.fecha = DateTime.Parse(row["FechaDt"].ToString());
                        tkt.nroClaim = row["ClainNro"].ToString();
                        tkt.idUsuario = int.Parse(row["UsuarioId"].ToString());
                        tkt.idUbicacionContacto = int.Parse(row["UbicacionContactoId"].ToString());
                        tkt.idTipoProblema = int.Parse(row["IdTipoProblema"].ToString());
                        tkt.idVendedor = row["VendedorId"].ToString();
                        tkt.fechaInicio = DateTime.Parse(row["FechaInicioDt"].ToString());
                        if (row["FechaFinDt"] != Convert.DBNull)
                            tkt.fechaFin = DateTime.Parse( row["FechaFinDt"].ToString());

                        tkt.FechaP = row["Fecha"].ToString();
                        tkt.FechaInicioScreen = row["FechaInicio"].ToString();
                        tkt.FechaFinScreen = row["FechaFin"].ToString();
                        tkt.NombreCliente = row["Razon_Social"].ToString();
                        tkt.Ubicacion = row["Ubicacion"].ToString();
                        tkt.Contacto = row["Contacto"].ToString();
                        tkt.Especialista = row["Especialista"].ToString();
                        tkt.CodigoEquipo = row["Codigo_Equipo"].ToString();
                        tkt.NombreEquipo = row["Nombre_Equipo"].ToString();
                        tkt.Estado = row["Estado"].ToString();
                        tkt.TipoProblema = row["Tipo_Problema"].ToString();
                        tkt.UltimaModifScreen = row["UltimaModif"].ToString();
                        tkt.Vendedor = row["Vendedor"].ToString();
                        tkt.Negocio = row["Negocio"].ToString();
                        tkt.CodigoCliente = row["ClienteCodigo"].ToString();
                        tkt.InformeCliente = row["InformeCliente"].ToString(); 
                        tkt.InformeInterno = row["InformeInterno"].ToString();
                        //if (row["sistemaOperativoId"] == Convert.DBNull) { tkt.sistemaOperativoId = -1; }
                        //else
                        //{
                        //    tkt.sistemaOperativoId = int.Parse(row["sistemaOperativoId"].ToString());
                        //    tkt.arquitecturaId = int.Parse(row["arquitecturaId"].ToString());
                        //    tkt.servicePackId = int.Parse(row["servicePackId"].ToString());
                        //    tkt.modificado = (Boolean)row["modificado"];


                           
                        //}
                        
                        tkt.popContacto = row["contactopop"].ToString();
                        tkt.popSO = (int)row["sistemaOperativoId"];
                        tkt.poplicenciaSO = row["licenciaSo"].ToString();
                        tkt.popPC = row["pc"].ToString();
                        tkt.popAntiguedad = row["antiguedad"].ToString();
                        tkt.popProcesador = row["procesador"].ToString();
                        tkt.popRam = row["ram"].ToString();
                        tkt.popTipoconexion = row["tipoconexion"].ToString();
                        tkt.popmodem = row["modem"].ToString();
                        tkt.popDetalle = row["detallepop"].ToString();




                        if (row["recontactarDesde"] != Convert.DBNull)
                        {
                            tkt.recontactarDesde = DateTime.Parse(row["recontactarDesde"].ToString());
                            tkt.idMotivoRecontacto = int.Parse(row["idMotivoRecontacto"].ToString());
                        }
                        if (row["FechaEstimada"] != Convert.DBNull)
                            tkt.FechaEstimada = DateTime.Parse( row["FechaEstimada"].ToString());
                    }
                }
                 
                return tkt;
            }
            catch (Exception ex)
            {
                logger.Error("Error en getTicket: " + ex.Message, ex);
                return null;
            }
        }
    }
    public class TicketData 
    {
        private int _idTicket;
        private string _mensajeError;

        public int idEmpresa;
        public string idCliente;
        public int idEspecialista;
        public int idArticulo;
        public int idEstado;
        public int idEmpresaPartner;
        public string Prioridad;
        public string NroSerie;
        public string Problema;
        public DateTime fecha;
        public string nroClaim;
        public int idUsuario;
        public int idUbicacionContacto;
        public int idTipoProblema;
        public string idVendedor;
        public DateTime fechaInicio;
        public DateTime fechaFin;
        public string FechaP;
        public string FechaInicioScreen;
        public string FechaFinScreen;
        public string NombreCliente;
        public string CodigoCliente;
        public string Ubicacion;
        public string Contacto;
        public string Especialista;
        public string CodigoEquipo;
        public string NombreEquipo;
        public string Estado;
        public string TipoProblema;
        public string UltimaModifScreen;
        public string Vendedor;
        public string Negocio;
        public DateTime FechaEstimada;
        public string InformeInterno;
        public string InformeCliente;
        public int sistemaOperativoId;
        public int arquitecturaId;
        public int servicePackId;
        public Boolean modificado;
        public DateTime recontactarDesde;
        public int idMotivoRecontacto;
        
        public  string popContacto;
        public int popSO;
        public int popMotivo_Tec; //Agregado para guardar del popup Motivo
        public string poplicenciaSO;
        public string popPC;
        public string popAntiguedad;
        public string popProcesador;
        public string popRam;
        public string popTipoconexion;
        public string popmodem;
        public string popDetalle;

        public string mensajeError
        {
            get
            {
                return _mensajeError;
            }
            set
            {
                _mensajeError = value;
            }
        }
        public int idTicket
        {
            get
            {
                return _idTicket;
            }
            set
            {
                _idTicket = value;
            }
        }

    }
}
  
                   
                    
                  
                   