using System;
using System.Data;

namespace App_Code.TicketsLib
{
    public class BusquedaTicket
    {
        public static String getStrResultado(BusquedaTicketFiltro bf)
        {
            try
            {
                string connString = Funciones.getConnString();

                string sSql = GetQueryBase2();

                if (bf.FechaInicio != null)
                    sSql += " And convert(char(19) ,Ticket.Fecha,120) >= '" + bf.FechaInicio.ToString("yyyy-MM-dd") + " 00:00:00' ";

                if (bf.FechaFin != null)
                    sSql += " And convert(char(19) ,Ticket.Fecha,120) < '" + bf.FechaFin.ToString("yyyy-MM-dd") + " 23:59:59' ";

                if (!bf.TipoProblema.Equals(""))
                    sSql += " And RTRIM(Ticket.idTipoProblema) = '" + bf.TipoProblema.TrimEnd() + "' ";

                if (!bf.NroClaim.Equals(""))
                    sSql += " And Ticket.ClainNro like '%" + bf.NroClaim + "%' ";

                if (!bf.NroTicket.Equals(""))
                    sSql += " And Ticket.TicketId = " + bf.NroTicket + " ";

                if (!bf.Negocio.Equals(""))
                    sSql += " And	Ticket.EmpresaPartnerId In (Select EmpresaPartnerId From EmpresaPartner Where Upper(Nombre) like '%" + bf.Negocio.ToUpper() + "%') ";

                if (!bf.CodigoCliente.Equals(""))
                    sSql += " And ViewCliente.Codigo = '" + bf.CodigoCliente + "' ";
                else
                {
                    if (!bf.RazonSocial.Equals(""))
                        sSql += " And	UPPER(ViewCliente.RazonSocial) like '%" + bf.RazonSocial.ToUpper() + "%' ";
                }

                if (!bf.CodigoEquipo.Equals(""))
                    sSql += " And Equipo.Codigo = '" + bf.CodigoEquipo + "' ";

                if (!bf.NroSerie.Equals(""))
                    sSql += " And	UPPER(Ticket.NroSerie) like '%" + bf.NroSerie.ToUpper() + "%' ";

                if (!bf.Lider.Equals(""))
                    sSql += " And	vl.liderId =" + bf.Lider + " ";

                if (!bf.Especialista.Equals(""))
                    sSql += " And	UPPER(Especialista.EspecialistaId) like '%" + bf.Especialista.ToUpper() + "%' ";

                if (!bf.Estado.Equals(""))
                    sSql += " And Ticket.EstadoId In (Select ee.EstadoId From Estado ee Where UPPER(ee.Nombre) like '%" + bf.Estado.ToUpper() + "%') ";

                if (!bf.Especialista.Equals(""))
                    sSql += " And	UPPER(TipoProblema.Nombre) like '%" + bf.TipoProblema.ToUpper() + "%' ";
                if (!bf.Tecnicoid.Equals(""))
                    sSql += " And tt.tecnicoid=" + bf.Tecnicoid + " ";
                if (bf.idMotivoRecontacto != null)
                    sSql += " And tir.motivoid=" + bf.idMotivoRecontacto + " ";
                if (bf.fechaRecontactar != null)
                    sSql += " And dateDiff(dd,tir.desde,'" + ((DateTime)bf.fechaRecontactar).ToString("yyyyMMdd HH:mm:ss") + "')=0 ";
                if (bf.horaRecontactar != null)
                    sSql += " And datePart(hour,tir.desde)= datepart(hour,'" + ((DateTime)bf.horaRecontactar).ToString("yyyyMMdd HH:mm:ss") + "') ";
                if (bf.Usuario != "")
                {
                    sSql += "AND Ticket.EspecialistaId IN "
                    + "( " +
                    "SELECT EU.EspecialistaId FROM Usuario U (nolock) INNER JOIN Perfil P (nolock) ON U.PerfilId = P.PerfilId " +
                    "AND P.ModifSoloSusTickets != 0 INNER JOIN EspecialistaUsuario EU (nolock) ON U.UsuarioId = EU.UsuarioId " +
                    "AND U.Nombre = '" + bf.Usuario +
                    "'  UNION SELECT DISTINCT E.EspecialistaId FROM Especialista E (nolock) , Usuario U (nolock) " +
                    "INNER JOIN Perfil P (nolock) ON U.PerfilId = P.PerfilId AND P.ModifSoloSusTickets = 0 AND U.Nombre = '" +
                    bf.Usuario + "' " +
                    " ) ";
                    sSql += " AND (Ticket.empresapartnerId not in (58,57) and 23 = (SELECT top 1 P.PerfilID FROM Perfil P (nolock) inner Join Usuario U (nolock)  on p.perfilid = u.perfilid where U.Nombre = '" + bf.Usuario + "') or 23 != (SELECT top 1 P.PerfilID FROM Perfil P (nolock) inner Join Usuario U (nolock)  on p.perfilid = u.perfilid where U.Nombre = '" + bf.Usuario + "') )";
    
                }
                sSql += " order by Ticket.TicketId DESC";

                return sSql;

            }
            catch
            {
                return null;
            }
        }
        public static DataSet getResultado(BusquedaTicketFiltro bf)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = GetQueryBase();

                if (bf.FechaInicio != null)
                    sSql+= " And convert(char(19) ,Ticket.Fecha,120) >= '" +bf.FechaInicio.ToString("yyyy-MM-dd") + " 00:00:00' ";

                if (bf.FechaFin != null)
                    sSql+= " And convert(char(19) ,Ticket.Fecha,120) < '" + bf.FechaFin.AddDays(1).ToString("yyyy-MM-dd") + " 22:00:00' ";

                if (!bf.TipoProblema.Equals(""))
                    sSql += " And RTRIM(Ticket.idTipoProblema) = '" + bf.TipoProblema.TrimEnd() + "%' ";

                if (!bf.NroClaim.Equals(""))
                    sSql += " And Ticket.ClainNro like '%" + bf.NroClaim + "%' ";
                
                if (!bf.NroTicket.Equals(""))
                    sSql += " And Ticket.TicketId = " + bf.NroTicket + " ";

                if (!bf.Negocio.Equals(""))
                    sSql += " And	Ticket.EmpresaPartnerId In (Select EmpresaPartnerId From EmpresaPartner Where Upper(Nombre) like '%" + bf.Negocio.ToUpper() + "%') ";

                if (!bf.CodigoCliente.Equals(""))
                    sSql += " And ViewCliente.Codigo = '" + bf.CodigoCliente + "' ";
                else
                {
                    if (!bf.RazonSocial.Equals(""))
                        sSql += " And	UPPER(ViewCliente.RazonSocial) like '%" + bf.RazonSocial.ToUpper() + "%' ";
                }

                if (!bf.CodigoEquipo.Equals(""))
                    sSql += " And Equipo.Codigo = '" + bf.CodigoEquipo + "' ";

                if (!bf.NroSerie.Equals(""))
                    sSql += " And	UPPER(Ticket.NroSerie) like '%" + bf.NroSerie.ToUpper() + "%' ";

                if (!bf.Lider.Equals(""))
                    sSql += " And	vl.liderId =" + bf.Lider + " ";

                if (!bf.Especialista.Equals(""))
                    sSql += " And	UPPER(Especialista.EspecialistaId) = '" + bf.Especialista.ToUpper() + "' ";

                if (!bf.Estado.Equals(""))
                    sSql += " And Ticket.EstadoId In (Select ee.EstadoId From Estado ee Where UPPER(ee.Nombre) like '%" + bf.Estado.ToUpper() + "%') ";

                if (!bf.Especialista.Equals(""))
                    sSql += " And	UPPER(TipoProblema.Nombre) like '%" + bf.TipoProblema.ToUpper() + "%' ";
                if (!bf.Tecnicoid.Equals(""))
                    sSql += " And tt.tecnicoid=" + bf.Tecnicoid+" ";
                if (bf.fechaRecontactar != null)
                    sSql += " And dateDiff(dd,tir.desde,'" + ((DateTime)bf.fechaRecontactar).ToString("yyyyMMdd HH:mm:ss") + "')=0 ";
                if (bf.horaRecontactar != null)
                    sSql += " And datePart(hour,tir.desde)= datepart(hour,'" + ((DateTime)bf.horaRecontactar).ToString("yyyyMMdd HH:mm:ss") + "') ";
                if (bf.Usuario != "")
                {
                    sSql += "AND Ticket.EspecialistaId IN "
                    + "( "  +
                    "SELECT EU.EspecialistaId FROM Usuario U (nolock) INNER JOIN Perfil P (nolock) ON U.PerfilId = P.PerfilId " +
                    "AND P.ModifSoloSusTickets != 0 INNER JOIN EspecialistaUsuario EU (nolock) ON U.UsuarioId = EU.UsuarioId " +
                    "AND U.Nombre = '" + bf.Usuario +
                    "'  UNION SELECT DISTINCT E.EspecialistaId FROM Especialista E (nolock) , Usuario U (nolock) " +
                    "INNER JOIN Perfil P (nolock) ON U.PerfilId = P.PerfilId AND P.ModifSoloSusTickets = 0 AND U.Nombre = '" +
                    bf.Usuario + "' " +
                    " ) ";
                    sSql += " AND (Ticket.empresapartnerId not in (58,57) and 23 = (SELECT top 1 P.PerfilID FROM Perfil P (nolock) inner Join Usuario U (nolock)  on p.perfilid = u.perfilid where U.Nombre = '" + bf.Usuario + "') or 23 != (SELECT top 1 P.PerfilID FROM Perfil P (nolock) inner Join Usuario U (nolock)  on p.perfilid = u.perfilid where U.Nombre = '" + bf.Usuario + "') )";
    
                }
                sSql += " order by Ticket.Fecha DESC";

                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                return ds;

            }
            catch
            {
                return null;
            }
        }
        private static string GetQueryBase() 
        {
            return "Select Ticket.TicketId as TicketNro,  " +
                    "	LEFT(ViewCliente.RazonSocial,15) as Razon_Social,   " +
                    "	LEFT(Ubicacion.Nombre,15) As Ubicacion,   " +
                    "	LEFT(UbicacionContacto.Nombre,15) As Contacto,   " +
                    "	LEFT(Especialista.Nombre,15) AS Especialista,   " +
                    "	Negocio.nombre as Nombre_Negocio,   " +
                    "	LEFT(Equipo.Nombre,25) as Nombre_Equipo,   " +
                    "	Ticket.EstadoId as EstadoId,   " +
                    "   Estado.Nombre as Estado, " +
                    "	Ticket.NroSerie as NroSerie,   " +
                    "	Ticket.Fecha as Fecha,   " +
                    "	Ticket.FechaInicio as FechaInicio,   " +
                    "	Ticket.FechaFin as FechaFin,   " +
                    "	Ticket.ClainNro as ClainNro,   " +
                    "	Ticket.Problema as Detalle,   " +
                    "	LEFT(TipoProblema.Nombre,15) as Tipo_Problema,   " +
                    "	Ticket.FechaUltimaModif as UltimaModif,  " +
                    "	vl.nombre as Lider, " +
                    "	mr.nombre as MotivoRecontacto, " +
                    "	tir.desde as FechaRecontacto " +
                    "   FROM Equipo (nolock)   " +
                    "	INNER JOIN (Especialista (nolock)   " +
                    "	INNER JOIN  (Ubicacion (nolock)   " +
                    "	INNER JOIN  (UbicacionContacto (nolock)   " +
                    "	INNER JOIN (ViewCliente (nolock)   " +
                    "	INNER JOIN  Ticket (nolock)  " +
                    "	INNER JOIN TipoProblema (nolock)   " +
                    "	ON Ticket.IdTipoProblema = TipoProblema.IdTipoProblema   " +
                    "	ON ViewCliente.ClienteId = Ticket.ClienteId  " +
                    "	AND  ViewCliente.EmpresaId = Ticket.EmpresaId)  " +
                    "	ON  UbicacionContacto.UbicacionContactoId = Ticket.UbicacionContactoId)  " +
                    "	ON  Ubicacion.UbicacionId = UbicacionContacto.UbicacionId)  " +
                    "	ON  Especialista.EspecialistaId = Ticket.EspecialistaId)  " +
                    "	ON  Equipo.EquipoId = Ticket.ArticuloId   " +
                    "	AND Equipo.EmpresaId = Ticket.EmpresaId   " +
                    "   Inner Join Estado (nolock) On Estado.EstadoId = Ticket.EstadoId " +
                    "   LEFT JOIN ticketTecnico tt on Ticket.TicketId=tt.ticketid " +
                    "   LEFT JOIN empresaPartner as negocio on Ticket.empresaPartnerId=negocio.empresaPartnerId " +
                    "	LEFT JOIN viewLider vl ON ticket.EspecialistaId=vl.EspecialistaId " +
                    "	LEFT JOIN ticketInforecontacto tir ON ticket.TicketId=tir.ticketId " +
                    "	LEFT JOIN motivoRecontacto mr ON tir.motivoId=mr.id " +
                    "Where Ticket.EmpresaId = 1   " +
                    "And (Ticket.FechaBaja Is Null)  ";
        }
        private static string GetQueryBase2()
        {
            return "Select Ticket.TicketId as TicketNro,  " +
                    "	LEFT(ViewCliente.RazonSocial,15) as Razon_Social,   " +
                    "	LEFT(Ubicacion.Nombre,15) As Ubicacion,   " +
                    "	LEFT(UbicacionContacto.Nombre,15) As Contacto,   " +
                    "	Especialista.Nombre AS Especialista,   " +
                    "	Negocio.Nombre as Nombre_Negocio,   " +
                    "	LEFT(Equipo.Nombre,25) as Nombre_Equipo,   " +
                    "	Ticket.EstadoId as EstadoId,   " +
                    "   Estado.Nombre as Estado, " +
                    "	Ticket.NroSerie as NroSerie,   " +
                    "	Ticket.Fecha as Fecha,   " +
                    "	Ticket.FechaInicio as FechaInicio,   " +
                    "	Ticket.FechaFin as FechaFin,   " +
                    "	Ticket.ClainNro as ClainNro,   " +
                    "	Ticket.Problema as Detalle,   " +
                    "	LEFT(TipoProblema.Nombre,15) as Tipo_Problema,   " +
                    "	Ticket.FechaUltimaModif as UltimaModif,  " +
                    "	vl.nombre as Lider, " +
                    "	mr.nombre as MotivoRecontacto, " +
                    "	tir.desde as FechaRecontacto " +
                    "   FROM Equipo (nolock)   " +
                    "	INNER JOIN (Especialista (nolock)   " +
                    "	INNER JOIN  (Ubicacion (nolock)   " +
                    "	INNER JOIN  (UbicacionContacto (nolock)   " +
                    "	INNER JOIN (ViewCliente (nolock)   " +
                    "	INNER JOIN  Ticket (nolock)  " +
                    "	INNER JOIN TipoProblema (nolock)   " +
                    "	ON Ticket.IdTipoProblema = TipoProblema.IdTipoProblema   " +
                    "	ON ViewCliente.ClienteId = Ticket.ClienteId  " +
                    "	AND  ViewCliente.EmpresaId = Ticket.EmpresaId)  " +
                    "	ON  UbicacionContacto.UbicacionContactoId = Ticket.UbicacionContactoId)  " +
                    "	ON  Ubicacion.UbicacionId = UbicacionContacto.UbicacionId)  " +
                    "	ON  Especialista.EspecialistaId = Ticket.EspecialistaId)  " +
                    "	ON  Equipo.EquipoId = Ticket.ArticuloId   " +
                    "	AND Equipo.EmpresaId = Ticket.EmpresaId   " +
                    "   Inner Join Estado (nolock) On Estado.EstadoId = Ticket.EstadoId " +
                    "   LEFT JOIN ticketTecnico tt on Ticket.TicketId=tt.ticketid " +
                    "   LEFT JOIN empresaPartner as negocio on Ticket.empresaPartnerId=negocio.empresaPartnerId " +
                    "	LEFT JOIN viewLider vl ON ticket.EspecialistaId=vl.EspecialistaId " +
                    "	LEFT JOIN ticketInforecontacto tir ON ticket.TicketId=tir.ticketId  " +
                    "	LEFT JOIN motivoRecontacto mr ON tir.motivoId=mr.id  " +
                    "Where Ticket.EmpresaId = 1   " +
                    "And (Ticket.FechaBaja Is Null)  ";
        }
    }

     
    public class BusquedaTicketFiltro
    {
        public DateTime FechaInicio;
        public DateTime FechaFin;
        public string NroClaim;
        public string NroTicket;
        public string Negocio;
        public string CodigoCliente;
        public string RazonSocial;
        public string CodigoEquipo;
        public string NroSerie;
        public string Especialista;
        public string Estado;
        public string TipoProblema;
        public string Usuario;
        public string Tecnicoid;
        public string Lider;
        public object fechaRecontactar=null;
        public object horaRecontactar=null;
        public string idMotivoRecontacto;

    }
}
