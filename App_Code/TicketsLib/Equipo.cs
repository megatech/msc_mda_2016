using System;
using System.Data;
using System.Data.SqlClient;

namespace App_Code.TicketsLib
{
    public class Equipo
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger("File");

        public static DataSet getEquipo(FiltroEquipo fc)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "SELECT EquipoId, Codigo, Nombre  " +
                                "FROM Equipo (nolock) " +
                                "WHERE FechaBaja IS NULL ";
                if (fc.Codigo != null)
                {
                    sSql += " AND Codigo LIKE '%" + fc.Codigo.Trim() + "%' ";
                }
                if (fc.Nombre != null)
                {
                    sSql += " AND Nombre LIKE '%" + fc.Nombre.Trim() + "%' ";
                }

                sSql += "ORDER BY Nombre ";
                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                return ds;

            }
            catch
            {
                return null;
            }
        }
        public static FiltroEquipo getUnEquipo(FiltroEquipo fc)
        {
            try
            {
                string connString = Funciones.getConnString(); 
                
                string sSql = "SELECT Top 1 EquipoId, Codigo, Nombre  " +
                            "FROM   Equipo (nolock) " +
                            "WHERE FechaBaja IS NULL ";
                if (fc.EquipoId != null)
                {
                    sSql += "AND EquipoId = '" + fc.EquipoId + "' ";
                }
                else if (fc.Codigo != null)
                {
                    sSql += "And   Codigo ='" + fc.Codigo + "'";
                }

                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, sSql);
                DataRow row = ds.Tables[0].Rows[0];
                fc.EquipoId = row["EquipoId"].ToString();
                fc.Nombre = row["Nombre"].ToString();
                fc.Codigo = row["Codigo"].ToString();
                return fc;

            }
            catch
            {
                return null;
            }
        }
        public static RecEquipo insertEquipo(RecEquipo rc)
        {
            try
            {
                string connString = Funciones.getConnString();

                string spQry = "SP_GENERO_NUEVO_EQUIPO_CAS";

                

                SqlParameter[] par = new SqlParameter[19];

                SqlParameter p = new SqlParameter("EmpresaId", SqlDbType.Decimal, 13);
                p.Value = rc.EmpresaId;
                par[0] = p;

                p = new SqlParameter("Codigo", rc.Codigo);
                par[1] = p;

                p = new SqlParameter("Nombre", rc.Nombre);
                par[2] = p;

                p = new SqlParameter("RubroId1", rc.RubroId1);
                par[3] = p;

                
                p = new SqlParameter("MarcaId", rc.MarcaId);
                par[6] = p;
             
                                           
                p = new SqlParameter("tr", SqlDbType.SmallInt);
                p.Value = 1;
                par[16] = p;

                p = new SqlParameter("EquipoId", SqlDbType.Char, 8);
                p.Direction = ParameterDirection.Output;
                par[17] = p;

                p = new SqlParameter("DescError", SqlDbType.VarChar, 200);
                p.Direction = ParameterDirection.Output;
                par[18] = p;

                try
                {
                    int ret = SqlHelper.ExecuteNonQuery(connString, CommandType.StoredProcedure, spQry, par);
                    rc.EquipoId = par[17].Value.ToString();
                    rc.MensajeError = par[18].Value.ToString();
                }
                catch (Exception ex)
                {
                    rc.MensajeError = ex.Message;
                }
                return rc;
            }
            catch (Exception ex)
            {
                rc.MensajeError = "Error en InsertEquipo: " + ex.Message;
                logger.Error(rc.MensajeError, ex);
                return rc;
            }
        }
        public static bool ExisteCodigo(string sCodigo)
        {
            try
            {
                string connString = Funciones.getConnString();

                string spQry = "select count(1) as existe from Equipo where Codigo = '" + sCodigo + "' ";

                DataSet ds = SqlHelper.ExecuteDataset(connString, CommandType.Text, spQry);
                int cant = int.Parse(ds.Tables[0].Rows[0]["existe"].ToString());
                return (cant > 0);

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return true;
            }
        }
    }
    public class FiltroEquipo
    {
        public string EquipoId;
        public string Codigo;
        public string Nombre;
    }
    public class RecEquipo
    {
        public string EquipoId;
        public string Codigo;
        public string Nombre;
        public int EmpresaId;
        public string RubroId1;
        public string RubroId2;
        public string RubroId3;
        public string MarcaId;
        public string Presentacion;
        public string CodBarras;
        public int GarantiaCompra;
        public int GarantiaVenta;
        public int Vigencia;
        public string Observaciones;
        public float Medida1;
        public float Medida2;
        public float Medida3;
        public string MensajeError;
    }
}
