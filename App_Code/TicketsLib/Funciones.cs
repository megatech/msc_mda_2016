using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;

namespace App_Code.TicketsLib
{
    public static class MyStringExtensions
    {
        public static bool Like(this string toSearch, string toFind)
        {
            String a = toSearch.ToUpper().Trim().Replace("�","A").Replace("�","E").Replace("�","I").Replace("�","O").Replace("�","U");
            String b = toFind.ToUpper().Trim().Replace("�","A").Replace("�","E").Replace("�","I").Replace("�","O").Replace("�","U");
            return a == b;
        }
    }
    public class Funciones
    {
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex, int length)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = param.Substring(startIndex);
            //return the result of the operation
            return result;
        }
        public static DateTime DateParse(string s)
        {
            System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CreateSpecificCulture("es-AR");
            ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            return DateTime.Parse(s, ci.DateTimeFormat);
        }
        public static DateTime DateParse(string s, string formatString)
        {
            System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CreateSpecificCulture("es-AR");
            ci.DateTimeFormat.ShortDatePattern = formatString;
            return DateTime.Parse(s, ci.DateTimeFormat);
        }
        public static float FloatParse(string s)
        {
            System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CreateSpecificCulture("es-AR");
            ci.NumberFormat.NumberDecimalSeparator = ".";
            ci.NumberFormat.NumberGroupSeparator = ",";

            return float.Parse(s, ci.NumberFormat);
        }
        public static DateTime FechaMaxima()
        {
            return new DateTime(2999, 12, 31);
        }
        public static DateTime rowDateTimeParse(Object dcFecha)
        {
            try
            {
                if (dcFecha.GetType().ToString().Equals("System.DateTime"))
                    return (DateTime)dcFecha;

                if (dcFecha.GetType().ToString().Equals("System.DBNull"))
                    dcFecha = DateTime.MinValue;

                return (DateTime)dcFecha;
            }
            catch
            {
                return DateTime.MinValue;
            }
        }
        public static bool isDateTimeNull(DateTime dt)
        {
            return dt.ToString("yyyyMMdd").Equals("00010101");
        }
        public static DateTime dateNull()
        {
            return new DateTime();
        }
        public static string getConnString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["TicketDB"].ToString();
        }
        public static DateTime fechaHoraToDateTime(string fecha, string hora)
        {
            try
            {
                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("es-AR");
                ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy HH:mm";

                return DateTime.Parse(fecha + " " + hora, ci.DateTimeFormat);

            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        public static object getScalar(string query)
        {
            SqlConnection SQL = new SqlConnection();
            SqlCommand CMD = new SqlCommand();
            object RES = null;
            try
            {
                SQL.ConnectionString = ConfigurationManager.ConnectionStrings["TicketDB"].ToString();
                CMD.Connection = SQL;
                CMD.CommandText = query;
                SQL.Open();
                RES = CMD.ExecuteScalar();
                SQL.Close();
                return RES;
            }
            catch (Exception ex)
            {
                if (SQL.State != ConnectionState.Closed)
                {
                    SQL.Close();
                }
                throw ex;
            }
        }

        public static int obtenerEstadoSLA(String tktId)
        {
            SqlConnection SQL = new SqlConnection(getConnString());
            SqlCommand CMD = new SqlCommand("", SQL);
            SQL.Open();

            //TRAYENDO LOS DATOS DEL TICKET
            CMD.CommandText = "SELECT idContrato,fecha,DtRespuesta,DtSolucion FROM ticket WHERE ticketid=" + tktId;
            SqlDataReader RS = CMD.ExecuteReader();
            RS.Read();
            DateTime fechaRespuesta, fechaSolucion, fechaCreacion;
            String contratoId = RS["idContrato"].ToString();
            fechaCreacion = (DateTime)RS["fecha"];
            fechaRespuesta = (DateTime)RS["DtRespuesta"];
            fechaSolucion = (DateTime)RS["DtSolucion"];
            Boolean tieneFechaRespuesta = !RS.IsDBNull(2);
            Boolean tieneFechaSolucion = !RS.IsDBNull(3);
            if (tieneFechaRespuesta) { fechaRespuesta = (DateTime)RS["dtRespuesta"]; }
            if (tieneFechaSolucion) { fechaSolucion = (DateTime)RS["dtSolucion"]; }
            RS.Close();

            //TRAYENDO LOS DATOS DEL CONTRATO
            CMD.CommandText = "SELECT CASE WHEN horarioSLALunes =1 THEN '2' ELSE'' END + " +
                                            "CASE WHEN horarioSLAMartes =1 THEN '3' ELSE'' END + " +
                                            "CASE WHEN horarioSLAMiercoles =1 THEN '4' ELSE'' END + " +
                                            "CASE WHEN horarioSLAJueves =1 THEN '5' ELSE'' END + " +
                                            "CASE WHEN horarioSLAViernes =1 THEN '6' ELSE'' END + " +
                                            "CASE WHEN horarioSLASabado =1 THEN '7' ELSE'' END + " +
                                            "CASE WHEN horarioSLADomingo =1 THEN '1' ELSE'' END AS diasSLA, " +
                                            "tpoSolRestanteRango1,tpoSolRestanteRango2,tpoRptaRestanteRango1," +
                                      "tpoRptaRestanteRango2,horarioSLADesde,horarioSLAHasta, tiempoRespuestaCantidad, tiempoSolucionCantidad " +
                                "FROM contrato WHERE rtrim(idContrato)=" + contratoId;
            RS = CMD.ExecuteReader();
            RS.Read();
            String dias = RS["diasSLA"].ToString();
            DateTime tsAux = fechaCreacion.AddSeconds(-fechaCreacion.Second);
            DateTime tsNow = DateTime.Now;
            tsNow.AddSeconds(-tsNow.Second);
            TimeSpan umbralRespuesta1 = new TimeSpan();
            TimeSpan umbralRespuesta2 = new TimeSpan();
            TimeSpan umbralSolucion1 = new TimeSpan();
            TimeSpan umbralSolucion2 = new TimeSpan();
            TimeSpanMinutesConverter tsmc = new TimeSpanMinutesConverter();
            TimeSpan tsMaxRespuesta = new TimeSpan();
            TimeSpan tsMaxSolucion = new TimeSpan();
            tsMaxSolucion = (TimeSpan)(tsmc.ConvertFrom(RS["tiempoSolucionCantidad"].ToString()));
            tsMaxRespuesta = (TimeSpan)(tsmc.ConvertFrom(RS["tiempoRespuestaCantidad"].ToString()));
            umbralRespuesta1 = (TimeSpan)(tsmc.ConvertFrom(RS["tpoRptaRestanteRango1"].ToString()));
            umbralRespuesta2 = (TimeSpan)(tsmc.ConvertFrom(RS["tpoRptaRestanteRango2"].ToString()));
            umbralSolucion1 = (TimeSpan)(tsmc.ConvertFrom(RS["tpoSolRestanteRango1"].ToString()));
            umbralSolucion2 = (TimeSpan)(tsmc.ConvertFrom(RS["tpoSolRestanteRango2"].ToString()));
            TimeSpan desde = TimeSpan.Parse(RS["horarioSLADesde"].ToString());
            TimeSpan hasta = TimeSpan.Parse(RS["horarioSLAHasta"].ToString());
            SQL.Close();

            //Este es el intervalo de tiempo transcurrido (contemplando solo horarios y dias habiles del contrato)
            TimeSpan intervaloAplicable = new TimeSpan();
            TimeSpan tsMax = new TimeSpan();
            TimeSpan umbral1 = new TimeSpan();
            TimeSpan umbral2 = new TimeSpan();

            //Se calcula el intervalo habil transcurrido
            if (!tieneFechaRespuesta) { tsMax = tsMaxRespuesta; umbral1 = umbralRespuesta1; umbral2 = umbralRespuesta2; }
            else { tsMax = tsMaxSolucion; umbral1 = umbralRespuesta1; umbral2 = umbralRespuesta2; }
            if (tieneFechaSolucion) { tsNow = fechaSolucion; tsNow.AddSeconds(-tsNow.Second); }


            while (tsAux != tsNow)
            {
                tsAux.AddMinutes(1);
                Boolean esDiaAplicable = dias.Contains(tsNow.DayOfWeek.ToString());
                Boolean esHorarioAplicable = tsAux.TimeOfDay > desde && tsAux.TimeOfDay <= hasta;
                if (esDiaAplicable && esHorarioAplicable) { intervaloAplicable.Add(TimeSpan.FromMinutes(1)); }


                if (intervaloAplicable <= umbral1) return 1;
                if (intervaloAplicable > umbral1 && intervaloAplicable <= umbral2) return 2;
                if (intervaloAplicable > umbral2) return 3;
                if (intervaloAplicable > tsMax) return 4;
            }
            return 0;
        }
    }
}


