using System;
namespace App_Code.TicketsLib
{
    public class CustomExceptions
    {
        // Base exception which all custom exceptions should inherit from
        public class BaseStepException : ApplicationException
        {
            private string _customMessage;

            // Gets or Sets the custom error message detailing the error
            public string CustomMessage
            {
                get { return this._customMessage; }
                set { this._customMessage = value; }
            }
        }

        // Exception resulting from a failure in the FTP cleanup process
        public class UserLoginException : BaseStepException
        {
            // Constructor (default error message)
            public UserLoginException()
            {
                this.CustomMessage = "Error en el login del Usuario.";
            }

            /// Overloaded constructor (initialize custom error message)
            public UserLoginException(string strCustomMessage)
            {
                this.CustomMessage = strCustomMessage;
            }
        }
    }

}