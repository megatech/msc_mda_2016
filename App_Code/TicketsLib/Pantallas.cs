﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Descripción breve de Pantallas
/// </summary>
public class Pantallas
{
    private int nPantalla;
    private string mDescripcion;


    public int IdPantalla {
        get { return nPantalla; }
        set { nPantalla = value; }
    }

    public string Descripcion {
        get { return mDescripcion; }
        set { mDescripcion = value; }
    }

    public Pantallas( int a , string b)
    {
        IdPantalla = a;
        Descripcion =b;
    }
}