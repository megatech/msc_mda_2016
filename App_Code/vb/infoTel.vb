﻿Imports System.Net
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration
Imports System
Imports System.Text.RegularExpressions
Imports System.Linq






Public Class InfoTelData
    Private _nombre As String
    Private _tel As String
    Private _direccion As String
    Private _prov As String
    Private _localidad As String
    Private _cp As String

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
    Public Property Tel As String
        Get
            Return _tel
        End Get
        Set(ByVal value As String)
            _tel = value
        End Set
    End Property
    Public Property Direccion As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property
    Public Property Provincia As String
        Get
            Return _prov
        End Get
        Set(ByVal value As String)
            _prov = value
        End Set
    End Property
    Public Property Localidad As String
        Get
            Return _localidad
        End Get
        Set(ByVal value As String)
            _localidad = value
        End Set
    End Property
    Public Property CP As String
        Get
            Return _cp
        End Get
        Set(ByVal value As String)
            _cp = value
        End Set
    End Property

End Class

Public Class InfoTel
    Private Data1 As InfoTelData
    Private Data2 As InfoTelData

    Public Shared Function buscar(ByVal ANI As String, ByRef data As InfoTelData) As Boolean
        Try
            Dim itAux = New InfoTel
            Dim it = buscar(itAux.getNumbers(ANI))
            data = it.seleccionarFuente(it.Data1, it.Data2)
            Return it.hayDatos()
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function seleccionarFuente(ByVal data1 As InfoTelData, ByVal data2 As InfoTelData) As InfoTelData
        Dim d1 = 0
        Dim d2 = 0
        If Not data1 Is Nothing Then
            If data1.CP.Trim <> "" Then d1 += 1
            If data1.Localidad.Trim <> "" Then d1 += 1
            If data1.Provincia.Trim <> "" Then d1 += 1
            If data1.Direccion.Trim <> "" Then d1 += 1
        End If
        If Not data2 Is Nothing Then
            If data2.CP.Trim <> "" Then d2 += 1
            If data2.Localidad.Trim <> "" Then d2 += 1
            If data2.Provincia.Trim <> "" Then d2 += 1
            If data2.Direccion.Trim <> "" Then d2 += 1
        End If
        If d1 > d2 Then Return data1 Else Return data2
    End Function

    Public Shared Function buscar(ByVal ANI As String) As InfoTel
        Dim it = New InfoTel

        Dim webCli = New WebClient
        Dim RES2 As String = ""
        Dim RES1 As String = ""
        Dim bData1_Ok As Boolean
        Dim bData2_Ok As Boolean

        Try
            RES1 = webCli.DownloadString("http://buscador.telexplorer.com/ans.asp?q=" & ANI)
            bData1_Ok = RES1.Contains("<p class=""resultado_titulo"">")
            RES2 = webCli.DownloadString("http://www.paginasblancas.com.ar/Telefono/" & ANI)
            bData2_Ok = RES2.Contains("<h2 class=""advertise-name"">")
        Catch ex As Exception
        Finally


            If bData1_Ok Then 'Si hay datos en TELEXPLORER, genera DATA1
                it.Data1 = New InfoTelData
                it.Data1.Nombre = it.StripTags(it.SeleccionarEntre(RES1, "<p class=""resultado_titulo"">", "</p>")).ToUpper
                it.Data1.Tel = it.getNumbers(it.StripTags(it.SeleccionarEntre(RES1, "<p class=""resultado_telefono"">", "</p>")).Replace("(0", ""))
                Dim TE_Domicilio = it.SeleccionarEntre(RES1, "<p class=""resultado_domicilio"">", "</p>")
                Dim TE_Ubicacion = it.SeleccionarEntre(TE_Domicilio, "<br />", "</p>")
                it.Data1.Direccion = it.StripTags(it.SeleccionarEntre(TE_Domicilio, "<a", "</a>")).Trim().ToUpper
                it.Data1.Provincia = it.StripTags(TE_Ubicacion).Split("(")(0).Split("-")(0).Trim.ToUpper
                Dim arrSplit = it.StripTags(TE_Ubicacion).Split("(")(0).Split("-")
                it.Data1.Localidad = arrSplit(arrSplit.Count - 1).Trim.ToUpper
                If it.StripTags(TE_Ubicacion).Contains("(") Then 'si hay CP, lo extraigo
                    it.Data1.CP = it.getNumbers(it.StripTags(TE_Ubicacion).Split("(")(1))
                Else : it.Data1.CP = ""
                End If



                it.Data1.Provincia = it.corregirProvincia(it.Data1.Provincia)
                it.Data1.Localidad = it.validarLocalidad(it.Data1.Localidad, it.Data1.Provincia)
            End If

            If bData2_Ok Then 'Si hay datos en PAGINAS BLANCAS, genera DATA2
                it.Data2 = New InfoTelData
                it.Data2.Nombre = it.StripTags(it.SeleccionarEntre(RES2, "<h2 class=""advertise-name"">", "</h2>")).Trim.ToUpper
                Dim PB_Datos = it.SeleccionarEntre(RES2, "<div class=""advertiseBlockContent"">", "</div>")
                it.Data2.Tel = it.getNumbers(PB_Datos.Split(">")(4))
                it.Data2.Direccion = it.StripTags(PB_Datos).Split(",")(0).Trim.ToUpper
                If PB_Datos.Contains("CIUDAD DE BUENOS AIRES") Then
                    it.Data2.Provincia = it.SeleccionarEntre(PB_Datos, "-", "</p>").Replace("-", "").Replace("</p>", "").Trim.ToUpper
                    it.Data2.CP = PB_Datos.Split("-")(0).Split(":")(1).Trim()
                    it.Data2.Localidad = ""
                Else
                    it.Data2.Provincia = it.StripTags(PB_Datos.Split("-")(1)).Trim.ToUpper
                    it.Data2.Localidad = it.StripTags(PB_Datos.Split("-")(2).Remove(PB_Datos.Split("-")(2).IndexOf("<"), PB_Datos.Split("-")(2).Count - PB_Datos.Split("-")(2).IndexOf("<"))).Trim.ToUpper
                    If PB_Datos.Contains("CP:") Then 'si hay CP, lo extraigo
                        it.Data2.CP = PB_Datos.Split("-")(0).Split(":")(1).Trim()
                    Else : it.Data2.CP = ""
                    End If


                End If

                it.Data2.Nombre = it.HtmlDecode(it.Data2.Nombre)
                it.Data2.Direccion = it.HtmlDecode(it.Data2.Direccion)
                it.Data2.Provincia = it.HtmlDecode(it.Data2.Provincia)
                it.Data2.Localidad = it.HtmlDecode(it.Data2.Localidad)

                it.Data2.Provincia = it.corregirProvincia(it.Data2.Provincia)
                it.Data2.Localidad = it.validarLocalidad(it.Data2.Localidad, it.Data2.Provincia)
                Dim a = 0
            End If

        End Try
        Return it
    End Function

    Public Function hayDatos() As Boolean
        Return Not (Me.Data1 Is Nothing And Me.Data2 Is Nothing)
    End Function

    Public Function StripTags(ByVal html As String) As String
        ' Remove HTML tags.
        Return Regex.Replace(html, "<.*?>", "")

    End Function

    Public Function SeleccionarEntre(ByVal texto As String, ByVal prefijo As String, ByVal sufijo As String) As String
        Dim html = texto
        Dim htmlCount = Convert.ToString((texto))
        Dim preIndex = html.IndexOf(prefijo.ToString())
        Dim suIndex = html.Substring(preIndex).IndexOf(sufijo, 1) + sufijo.Count + preIndex
        Dim cuenta = html.Count - suIndex + 1
        Dim res = html.Remove(suIndex, cuenta - 1)
        res = res.Substring(preIndex)
        Return res
    End Function

    Private Function getNumbers(ByVal texto As String) As String
        Dim ret = ""
        For Each ch As Char In texto
            If "0123456789".Contains(ch) Then ret &= ch
        Next
        Return ret
    End Function

    Private Function HtmlDecode(ByVal html As String) As String
        Return html.Replace("&OACUTE;", "Ó").Replace("&AACUTE;", "Á").Replace("&EACUTE;", "É").Replace("&IACUTE;", "Í").Replace("&UACUTE;", "Ú").Replace("&UUML;", "Ü").Replace("&#39;", "'")
    End Function

    Private Function corregirProvincia(ByVal prov As String) As String
        Dim retVal = prov
        Select retVal
            Case "CDAD. DE BUENOS AIRES", "CIUDAD DE BUENOS AIRES"
                retVal = "CAPITAL FEDERAL"
            Case "BUENOS AIRES"
                retVal = "PROVINCIA DE BS AS"
        End Select
        Return retVal
    End Function

    Private Function validarLocalidad(ByVal localidad As String, ByVal prov As String) As String
        Dim query = "SELECT isNull(Min(l.nombre),'') " & _
                    "FROM localidad l inner join viewProvincia vp On vp.provinciaId = l.provinciaId " & _
                    "WHERE ltrim(rtrim(upper(l.nombre))) like rtrim(ltrim(upper('" & localidad & "'))) " & _
                    "AND ltrim(rtrim(upper(vp.Nombre))) = ltrim(rtrim(upper('" & prov & "')))"
        Return getScalar(query)
    End Function

    Private Function getScalar(ByVal query As String) As Object
        Dim SQL As New SqlConnection
        Dim CMD As New SqlCommand
        Dim RES As Object
        Try
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ToString
            CMD.Connection = SQL
            CMD.CommandText = query
            SQL.Open()
            RES = CMD.ExecuteScalar()
            SQL.Close()
            Return RES
        Catch ex As Exception
            If SQL.State <> ConnectionState.Closed Then
                SQL.Close()
            End If
            Throw ex
        End Try
    End Function

End Class
