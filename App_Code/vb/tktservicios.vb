﻿Imports Microsoft.VisualBasic
Imports System
Imports App_Code.TicketsLibVb.Funciones
Imports System.Data.SqlClient
Imports System.Data
Imports App_Code.TicketsLibvisual
Imports App_Code
Imports App_Code.TicketsLibVb
Imports App_Code.TicketsLibL.SqlHelper
Imports log4net

Public Class tktservicios
    Public Shared Function insertar_tkt(ByVal clienteid As String, ByVal nrotkt As Integer) As String
        Dim nuevotkt As String = ""
        Dim nrocliente = crear_cliente(clienteid)
        Dim ubicacion As String = getScalar(getConnStringservicios, "select top 1 ubicacioncontactoid from ubicacioncontacto where clienteid = '" & nrocliente.Trim & "'")
        nuevotkt = generarTKT(nrotkt, nrocliente, 35, ubicacion)
        Return nuevotkt
    End Function

    Private Shared Function crear_cliente(ByVal clienteid As String) As String

        Dim nuevocliente = ""
        Dim ani = getScalar(getConnString, "select telefono1 from viewcliente where clienteid = '" & clienteid.Trim & "'")
        Dim existe = getScalar(getConnStringservicios, "Select clienteid from viewcliente where telefono1 like '%" & ani & "%'")

        If existe Is DBNull.Value Or existe Is Nothing Then
            'no existe crea cliente
            Dim sSql As String = "SELECT Top 1  RazonSocial , documento, Telefono1, Domicilio, Localidad,zip, provinciaid, civa, telefono2, telefono3 FROM   ViewCliente (nolock) WHERE ClienteId = " & clienteid

            Dim cliente = ExecuteReader(getConnString, CommandType.Text, sSql)
            If cliente.HasRows Then
                cliente.Read()
                Dim par(0 To 20 - 1) As SqlParameter
                Dim p As New SqlParameter("EmpresaId", SqlDbType.Decimal, 13)
                p.Value = 1
                par(0) = p
                p = New SqlParameter("Nombre", cliente(0))
                par(1) = p
                p = New SqlParameter("Fantasia", cliente(0))
                par(2) = p
                p = New SqlParameter("Domicilio", cliente(3))
                par(3) = p
                p = New SqlParameter("Localidad", cliente(4))
                par(4) = p
                p = New SqlParameter("CodPos", cliente(5))
                par(5) = p
                p = New SqlParameter("CodPrv", cliente(6))
                par(6) = p
                p = New SqlParameter("CodPais ", "")
                par(7) = p
                p = New SqlParameter("Telefono", cliente(2))
                par(8) = p
                p = New SqlParameter("Fax", cliente(8))
                par(9) = p
                p = New SqlParameter("Email", cliente(9))
                par(10) = p
                p = New SqlParameter("CodTrat", cliente(7))
                par(11) = p
                p = New SqlParameter("CodDoc", "")
                par(12) = p
                p = New SqlParameter("NroDoc1", "")
                par(13) = p
                p = New SqlParameter("NroDoc2", cliente(2))
                par(14) = p
                p = New SqlParameter("CodCondi", "")
                par(15) = p
                p = New SqlParameter("obs", "")
                par(16) = p
                p = New SqlParameter("tr", SqlDbType.SmallInt)
                p.Value = 1
                par(17) = p
                p = New SqlParameter("ClienteId", SqlDbType.Char, 8)
                p.Direction = ParameterDirection.Output
                par(18) = p
                p = New SqlParameter("DescError", SqlDbType.VarChar, 200)
                p.Direction = ParameterDirection.Output
                par(19) = p
                Dim sql As New SqlConnection(getConnStringservicios)
                Dim cmd As New SqlCommand("SP_GENERO_NUEVO_CLIENTE", sql)
                cmd.CommandType = CommandType.StoredProcedure
                For Each pm As SqlParameter In par
                    cmd.Parameters.Add(pm)
                Next
                sql.Open()
                cmd.ExecuteNonQuery()
                nuevocliente = par(18).Value.ToString.Trim
            End If
        Else
            nuevocliente = existe.trim
        End If
        Return nuevocliente
    End Function

    Private Shared Function generarTKT(ByVal unTkt As String, ByVal clienteid As String, ByVal empresapartner As Decimal, ByVal ubicacioncontactoid As String) As String

        Dim unticket As TicketDatavisual = App_Code.TicketsLibvisual.Ticketsvisual.getTicket(unTkt)

        Try
            Dim connString As String = getConnStringservicios()
            Dim spQry As String = "SP_GENERO_NUEVO_TICKET_DESDE_SPD"

            Dim par(0 To 22 - 1) As SqlParameter
            Dim p As New SqlParameter("EmpresaId", SqlDbType.Decimal, 13)
            p.Value = 1
            par(0) = p
            p = New SqlParameter("ClienteId", clienteid)
            p.Value = clienteid
            par(1) = p
            p = New SqlParameter("EspecialistaId", SqlDbType.Decimal, 13)
            p.Value = 168
            par(2) = p
            p = New SqlParameter("ArticuloId", SqlDbType.Int)
            p.Value = 1
            par(3) = p
            p = New SqlParameter("EstadoId", SqlDbType.Decimal, 13)
            p.Value = 42
            par(4) = p
            p = New SqlParameter("subEstadoId", SqlDbType.Decimal, 13)
            p.Value = Convert.DBNull
            par(5) = p
            p = New SqlParameter("EmpresaPartnerId", SqlDbType.Decimal, 13)
            p.Value = empresapartner
            par(6) = p
            p = New SqlParameter("Prioridad", unticket.Prioridad)
            par(7) = p
            p = New SqlParameter("NroSerie", unticket.NroSerie)
            par(8) = p
            p = New SqlParameter("Problema", unticket.Problema)
            par(9) = p
            p = New SqlParameter("Fecha", SqlDbType.DateTime, 13)
            p.Value = unticket.fecha
            par(10) = p
            p = New SqlParameter("ClainNro", unticket.idTicket)
            par(11) = p
            p = New SqlParameter("FechaEstimada", SqlDbType.DateTime, 13)
            If isDateTimeNull(unticket.FechaEstimada) Then
                p.Value = Convert.DBNull
            Else
                p.Value = unticket.FechaEstimada
            End If
            par(12) = p
            p = New SqlParameter("UsuarioId", SqlDbType.Decimal, 13)
            p.Value = 1
            par(13) = p
            p = New SqlParameter("UbicacionContactoId", SqlDbType.Decimal, 13)
            p.Value = ubicacioncontactoid
            par(14) = p
            p = New SqlParameter("IdTipoProblema", SqlDbType.Decimal, 13)
            p.Value = 5
            par(15) = p
            p = New SqlParameter("VendedorId", DBNull.Value)
            If unticket.idVendedor.Equals("0") Then
                p.Value = Convert.DBNull

            End If
            par(16) = p
            p = New SqlParameter("FechaInicio", SqlDbType.DateTime, 13)
            p.Value = unticket.fechaInicio
            par(17) = p
            p = New SqlParameter("FechaFin", SqlDbType.DateTime, 13)
            If Not isDateTimeNull(unticket.fechaFin) Then
                p.Value = unticket.fechaFin
            Else
                p.Value = Convert.DBNull
            End If

            par(18) = p
            p = New SqlParameter("TicketId", SqlDbType.Decimal)
            p.Direction = ParameterDirection.Output
            par(19) = p
            p = New SqlParameter("DescError", SqlDbType.VarChar, 200)
            p.Direction = ParameterDirection.Output
            par(20) = p
            p = New SqlParameter("TicketIdSpd", SqlDbType.Decimal)
            p.Value = unTkt
            par(21) = p

            Dim nuevotkt As Integer = 0
            Try
                Dim ret As Integer = ExecuteNonQuery(connString, CommandType.StoredProcedure, spQry, par)
                nuevotkt = Integer.Parse(par(19).Value.ToString())
                unticket.mensajeError = par(20).Value.ToString()

            Catch ex As Exception
                unticket.mensajeError = "ERROR:" & ex.Message
                Return unticket.mensajeError
            End Try

            Return nuevotkt

        Catch ex As Exception

            Return "ERROR:" & ex.Message

        End Try



        '  Return unticket.idTicket

    End Function

    Public Shared Sub ActualizarEstadoTicketServicios(ticketIdServ As Integer, ticketIdSPD As Integer, informeEstado As String, informeInterno As String)
        Dim flgError As Integer = 0
        Dim spQry As String = "SP_ACTUALIZAR_TICKET_DESDE_SPD"

        Dim par(0 To 24 - 1) As SqlParameter
        Dim p As New SqlParameter("TicketId", SqlDbType.Decimal, 13)
        p.Value = ticketIdServ
        par(0) = p
        p = New SqlParameter("TicketIdSPD", SqlDbType.Decimal, 13)
        p.Value = ticketIdSPD
        par(1) = p
        p = New SqlParameter("InformeEstado", SqlDbType.VarChar, 2000)
        p.Value = informeEstado
        par(2) = p
        p = New SqlParameter("InformeInterno", SqlDbType.VarChar, 2000)
        p.Value = informeInterno
        par(3) = p
        p = New SqlParameter("UsuarioId", SqlDbType.Decimal, 13)
        p.Value = 1
        par(4) = p
        p = New SqlParameter("fglError", SqlDbType.SmallInt)
        p.Direction = ParameterDirection.Output
        par(5) = p
        Dim ret As Integer = App_Code.TicketsLibL.SqlHelper.ExecuteNonQuery(getConnStringservicios(), CommandType.StoredProcedure, spQry, par)
        flgError = par(4).Value.ToString()
    End Sub

    Public Shared Sub ActualizarDatosMSCServicios(ticketIdSPD As Integer, estadoIdSPD As Integer, informe As String)

        Dim informeEstado = getScalar(getConnString(), "SELECT dbo.mensajeCambioEstado( " & ticketIdSPD & "," & estadoIdSPD & ")")
        Dim ticketIdServ = getScalar(getConnString(), "SELECT ticketIdServicios from ticketsLinkeados where ticketIdSPD=" & ticketIdSPD)
        ActualizarEstadoTicketServicios(ticketIdServ, ticketIdSPD, informeEstado, informe)
    End Sub

End Class
