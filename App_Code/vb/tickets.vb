﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient

Imports App_Code.TicketsLibVb.Funciones
Imports App_Code.TicketsLibL
Imports App_Code.TicketsLibL.SqlHelper


Namespace App_Code.TicketsLibvisual
    Public Class Ticketsvisual
        Private Shared logger As log4net.ILog = log4net.LogManager.GetLogger("File")
        Public Shared Function InsertTicket(unTicket As TicketDatavisual) As TicketDatavisual
            Try
                Dim connString As String = getConnString()

                Dim spQry As String = "SP_GENERO_NUEVO_TICKET"

                Dim par(0 To 24 - 1) As SqlParameter
                Dim p As New SqlParameter("EmpresaId", SqlDbType.Decimal, 13)
                p.Value = unTicket.idEmpresa
                par(0) = p
                p = New SqlParameter("ClienteId", unTicket.idCliente)
                'p.Value = unTicket.idCliente;
                par(1) = p
                p = New SqlParameter("EspecialistaId", SqlDbType.Decimal, 13)
                p.Value = unTicket.idEspecialista
                par(2) = p
                p = New SqlParameter("ArticuloId", SqlDbType.Int)
                p.Value = unTicket.idArticulo
                par(3) = p
                p = New SqlParameter("EstadoId", SqlDbType.Decimal, 13)
                p.Value = unTicket.idEstado
                par(4) = p
                p = New SqlParameter("EmpresaPartnerId", SqlDbType.Decimal, 13)
                p.Value = unTicket.idEmpresaPartner
                par(5) = p
                p = New SqlParameter("Prioridad", unTicket.Prioridad)
                par(6) = p
                p = New SqlParameter("NroSerie", unTicket.NroSerie)
                par(7) = p
                p = New SqlParameter("Problema", unTicket.Problema)
                par(8) = p
                p = New SqlParameter("Fecha", SqlDbType.DateTime, 13)
                p.Value = unTicket.fecha
                par(9) = p
                p = New SqlParameter("ClainNro", unTicket.nroClaim)
                par(10) = p
                p = New SqlParameter("FechaEstimada", SqlDbType.DateTime, 13)
                If isDateTimeNull(unTicket.FechaEstimada) Then
                    p.Value = Convert.DBNull
                Else
                    p.Value = unTicket.FechaEstimada
                End If
                par(11) = p
                p = New SqlParameter("UsuarioId", SqlDbType.Decimal, 13)
                p.Value = unTicket.idUsuario
                par(12) = p
                p = New SqlParameter("UbicacionContactoId", SqlDbType.Decimal, 13)
                p.Value = unTicket.idUbicacionContacto
                par(13) = p
                p = New SqlParameter("IdTipoProblema", SqlDbType.Decimal, 13)
                p.Value = unTicket.idTipoProblema
                par(14) = p
                p = New SqlParameter("VendedorId", unTicket.idVendedor)
                If unTicket.idVendedor.Equals("0") Then
                    p.Value = Convert.DBNull

                End If
                par(15) = p
                p = New SqlParameter("FechaInicio", SqlDbType.DateTime, 13)
                p.Value = unTicket.fechaInicio
                par(16) = p
                p = New SqlParameter("FechaFin", SqlDbType.DateTime, 13)
                If Not Funciones.isDateTimeNull(unTicket.fechaFin) Then
                    p.Value = unTicket.fechaFin
                Else
                    p.Value = Convert.DBNull
                End If
                par(17) = p
                p = New SqlParameter("sistemaOperativoId", unTicket.sistemaOperativoId)
                If unTicket.sistemaOperativoId.Equals("-1") Then
                    p.Value = Convert.DBNull
                End If
                par(18) = p
                p = New SqlParameter("arquitecturaId", unTicket.arquitecturaId)
                If unTicket.arquitecturaId.Equals("-1") Then
                    p.Value = Convert.DBNull
                End If
                par(19) = p
                p = New SqlParameter("servicePackId", unTicket.servicePackId)
                If unTicket.arquitecturaId.Equals("-1") Then
                    p.Value = Convert.DBNull
                End If
                par(20) = p
                p = New SqlParameter("modificado", unTicket.modificado)
                If unTicket.arquitecturaId.Equals("-1") Then
                    p.Value = Convert.DBNull
                End If
                par(21) = p
                p = New SqlParameter("TicketId", SqlDbType.Decimal)
                p.Direction = ParameterDirection.Output
                par(22) = p
                p = New SqlParameter("DescError", SqlDbType.VarChar, 200)
                p.Direction = ParameterDirection.Output
                par(23) = p
                Try
                    Dim ret As Integer = App_Code.TicketsLibL.SqlHelper.ExecuteNonQuery(connString, CommandType.StoredProcedure, spQry, par)
                    unTicket.idTicket = Integer.Parse(par(22).Value.ToString())
                    unTicket.mensajeError = par(23).Value.ToString()

                Catch ex As Exception
                    unTicket.mensajeError = ex.Message

                End Try
                Return unTicket

            Catch ex As Exception
                logger.Error("Error en InsertTicket: " & ex.Message, ex)
                Return Nothing

            End Try

        End Function

        Public Shared Function EditTicket(unTicket As TicketDatavisual) As TicketDatavisual
            Try
                Dim connString As String = Funciones.getConnString()
                Dim spQry As String = "SP_ACTUALIZAR_TICKET"

                Dim par(0 To 23 - 1) As SqlParameter
                Dim p As New SqlParameter("TicketId", SqlDbType.Decimal, 13)
                p.Value = unTicket.idTicket
                par(0) = p
                p = New SqlParameter("EstadoId", SqlDbType.Int)
                p.Value = unTicket.idEstado
                par(1) = p
                p = New SqlParameter("NegocioId", unTicket.idEmpresaPartner)
                par(2) = p
                p = New SqlParameter("InformeCliente", unTicket.InformeCliente)
                par(3) = p
                p = New SqlParameter("InformeInterno", unTicket.InformeInterno)
                par(4) = p

                p = New SqlParameter("UsuarioId", SqlDbType.Decimal, 13)
                p.Value = unTicket.idUsuario
                par(5) = p

                p = New SqlParameter("fglError", SqlDbType.SmallInt)
                p.Direction = ParameterDirection.Output
                par(6) = p
                p = New SqlParameter("DescError", SqlDbType.VarChar, 200)
                p.Direction = ParameterDirection.Output
                par(7) = p
                p = New SqlParameter("ClienteId", unTicket.idCliente)
                par(8) = p
                p = New SqlParameter("UbicacionContactoId", unTicket.idUbicacionContacto)
                par(9) = p
                p = New SqlParameter("ClainNro", unTicket.nroClaim)
                par(10) = p
                p = New SqlParameter("EspecialistaId", unTicket.idEspecialista)
                par(11) = p
                p = New SqlParameter("NroSerie", unTicket.NroSerie)
                par(12) = p
                p = New SqlParameter("Prioridad", unTicket.Prioridad)
                par(13) = p
                p = New SqlParameter("Problema", unTicket.Problema)
                par(14) = p
                p = New SqlParameter("ArticuloId", unTicket.idArticulo)
                par(15) = p
                p = New SqlParameter("IdTipoProblema", unTicket.idTipoProblema)
                par(16) = p
                p = New SqlParameter("sistemaOperativoId", unTicket.sistemaOperativoId)
                par(17) = p
                p = New SqlParameter("arquitecturaId", unTicket.arquitecturaId)
                par(18) = p
                p = New SqlParameter("servicePackId", unTicket.servicePackId)
                par(19) = p

                p = New SqlParameter("modificado", unTicket.modificado)
                par(20) = p

                p = New SqlParameter("recontactarDesde", unTicket.recontactarDesde)
                par(21) = p
                p = New SqlParameter("idMotivoRecontacto", unTicket.idMotivoRecontacto)
                par(22) = p
                Try
                    Dim ret As Integer = App_Code.TicketsLibL.SqlHelper.ExecuteNonQuery(connString, CommandType.StoredProcedure, spQry, par)
                    unTicket.mensajeError = par(7).Value.ToString()

                Catch ex As Exception
                    unTicket.mensajeError = ex.Message

                End Try
                Return unTicket

            Catch ex As Exception
                logger.Error("Error en EditTicket: " & ex.Message, ex)
                Return Nothing

            End Try

        End Function

        Public Shared Function getTicket(idTicket As Integer) As TicketDatavisual

            Try
                Dim connString As String = App_Code.TicketsLibVb.Funciones.getConnString
                Dim sql As String = "SELECT     dbo.ticket.TicketId " & "        ,Convert(varchar,dbo.ticket.Fecha,103) + ' ' + Left(Convert(varchar,dbo.ticket.Fecha,108),5) as Fecha " & "        ,Convert(varchar,dbo.ticket.FechaInicio,103) FechaInicio " & "        ,IsNull(Convert(varchar,dbo.ticket.FechaFin,103),'') as FechaFin " & "        ,IsNull(dbo.ticket.ClainNro,'') as ClainNro " & "        ,EmpresaPartner.Nombre as Negocio " & "        ,dbo.ViewCliente.Codigo as ClienteCodigo " & "        ,LEFT(dbo.ViewCliente.RazonSocial, 15) AS Razon_Social " & "        ,LEFT(dbo.Ubicacion.Nombre, 15) AS Ubicacion " & "        ,LEFT(dbo.UbicacionContacto.Nombre, 15) AS Contacto " & "        ,LEFT(dbo.especialista.Nombre, 15) AS Especialista " & "        ,dbo.equipo.Codigo as Codigo_Equipo " & "        ,LEFT(dbo.equipo.Nombre, 25) AS Nombre_Equipo " & "        ,dbo.Estado.Nombre AS Estado " & "        ,ticket.EstadoId " & "        ,dbo.ticket.NroSerie AS Numero_Serie " & "        , LEFT(dbo.tipoproblema.Nombre, 15) AS Tipo_Problema " & "        ,Convert(varchar,dbo.ticket.FechaUltimaModif,103)  AS UltimaModif " & "        ,IsNull(ViewVendedor.Nombre,'') as Vendedor " & "        ,dbo.Ticket.Prioridad " & "        ,Problema as Detalle " & "       ,Ticket.EmpresaId, Ticket.ClienteId, Ticket.EspecialistaId, Ticket.ArticuloId, Ticket.EmpresaPartnerID, Ticket.Fecha as FechaDt " & "       ,IsNull(Ticket.UsuarioId,0) as UsuarioId, Ticket.UbicacionContactoId, Ticket.IdTipoProblema, Ticket.VendedorId, Ticket.FechaInicio as FechaInicioDt, Ticket.FechaFin as FechaFinDt " & "       ,dbo.Ticket.FechaEstimada, IsNull(dbo.Ticket.InformeInterno,'') as InformeInterno, " & "       IsNull(dbo.Ticket.InformeCliente,'') as InformeCliente, " & "       sistemaOperativoId, arquitecturaId, servicePackId, modificado, " & "       tir.desde recontactarDesde, tir.motivoId idMotivoRecontacto" & " FROM    dbo.ticket (nolock)  " & "        INNER JOIN dbo.equipo (nolock)  " & "                ON dbo.equipo.EquipoId = dbo.ticket.ArticuloId AND  " & "                dbo.equipo.EmpresaId = dbo.ticket.EmpresaId " & "        INNER JOIN dbo.Estado (nolock)  " & "                ON dbo.ticket.EstadoId = dbo.Estado.EstadoId " & "        INNER JOIN dbo.especialista (nolock) " & "                ON dbo.especialista.EspecialistaId = dbo.ticket.EspecialistaId  " & "        INNER JOIN dbo.ViewCliente (nolock) " & "                ON dbo.ViewCliente.ClienteId = dbo.ticket.ClienteId  " & "                        AND dbo.ViewCliente.EmpresaId = dbo.ticket.EmpresaId  " & "        INNER JOIN dbo.tipoproblema (nolock)  " & "            ON dbo.tipoproblema.IdTipoProblema = dbo.ticket.IdTipoProblema  " & "        INNER JOIN dbo.EmpresaPartner (nolock) " & "                ON dbo.EmpresaPartner.EmpresaPartnerId = dbo.ticket.EmpresaPartnerId " & "        INNER JOIN dbo.UbicacionContacto (nolock) " & "                ON dbo.UbicacionContacto.UbicacionContactoId = dbo.ticket.UbicacionContactoId  " & "                INNER JOIN dbo.Ubicacion (nolock)  " & "                    ON dbo.Ubicacion.UbicacionId = dbo.UbicacionContacto.UbicacionId  " & "        LEFT JOIN ViewVendedor (nolock) " & "                    ON dbo.ticket.VendedorId = dbo.ViewVendedor.VendedorId " & "       LEFT JOIN dbo.itemSO (nolock) ON dbo.ticket.ticketId=dbo.itemSO.ticketId " & "       LEFT JOIN dbo.ticketInfoRecontacto tir (nolock) ON dbo.ticket.ticketId=tir.ticketId " & "Where Ticket.TicketId = @TicketId "
                Dim p As New SqlParameter("TicketId", SqlDbType.Int)
                p.Value = idTicket
                Dim ds As DataSet = App_Code.TicketsLibL.SqlHelper.ExecuteDataset(connString, CommandType.Text, sql, p)
                Dim tkt As New TicketDatavisual()
                If ds IsNot Nothing Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim row As DataRow = ds.Tables(0).Rows(0)
                        tkt.idTicket = idTicket
                        tkt.idEmpresa = Integer.Parse(row("EmpresaId").ToString())
                        tkt.idCliente = row("ClienteId").ToString()
                        tkt.idEspecialista = Integer.Parse(row("EspecialistaId").ToString())
                        tkt.idArticulo = Integer.Parse(row("ArticuloId").ToString())
                        tkt.idEstado = Integer.Parse(row("EstadoId").ToString())
                        tkt.idEmpresaPartner = Integer.Parse(row("EmpresaPartnerID").ToString())
                        tkt.Prioridad = row("Prioridad").ToString()
                        tkt.NroSerie = row("Numero_Serie").ToString()
                        tkt.Problema = row("Detalle").ToString()
                        tkt.fecha = DateTime.Parse(row("FechaDt").ToString())
                        tkt.nroClaim = row("ClainNro").ToString()
                        tkt.idUsuario = Integer.Parse(row("UsuarioId").ToString())
                        tkt.idUbicacionContacto = Integer.Parse(row("UbicacionContactoId").ToString())
                        tkt.idTipoProblema = Integer.Parse(row("IdTipoProblema").ToString())
                        tkt.idVendedor = row("VendedorId").ToString()
                        tkt.fechaInicio = DateTime.Parse(row("FechaInicioDt").ToString())
                        If Not (row("FechaFinDt") Is DBNull.Value) Then
                            tkt.fechaFin = DateTime.Parse(row("FechaFinDt").ToString())
                        End If
                        tkt.FechaP = row("Fecha").ToString()
                        'End If
                        tkt.FechaInicioScreen = row("FechaInicio").ToString()
                        'End If
                        tkt.FechaFinScreen = row("FechaFin").ToString()
                        tkt.NombreCliente = row("Razon_Social").ToString()
                        tkt.Ubicacion = row("Ubicacion").ToString()
                        tkt.Contacto = row("Contacto").ToString()
                        tkt.Especialista = row("Especialista").ToString()
                        tkt.CodigoEquipo = row("Codigo_Equipo").ToString()
                        tkt.NombreEquipo = row("Nombre_Equipo").ToString()
                        tkt.Estado = row("Estado").ToString()
                        tkt.TipoProblema = row("Tipo_Problema").ToString()
                        tkt.UltimaModifScreen = row("UltimaModif").ToString()
                        tkt.Vendedor = row("Vendedor").ToString()
                        tkt.Negocio = row("Negocio").ToString()
                        tkt.CodigoCliente = row("ClienteCodigo").ToString()
                        tkt.InformeCliente = row("InformeCliente").ToString()
                        tkt.InformeInterno = row("InformeInterno").ToString()

                        If row("sistemaOperativoId") Is DBNull.Value Then
                            tkt.sistemaOperativoId = -1
                        Else
                            tkt.sistemaOperativoId = Integer.Parse(row("sistemaOperativoId").ToString())
                            tkt.arquitecturaId = Integer.Parse(row("arquitecturaId").ToString())
                            tkt.servicePackId = Integer.Parse(row("servicePackId").ToString())
                            tkt.modificado = CType(row("modificado"), Boolean)

                        End If
                        If Not row("recontactarDesde") Is DBNull.Value Then
                            tkt.recontactarDesde = DateTime.Parse(row("recontactarDesde").ToString())
                            tkt.idMotivoRecontacto = Integer.Parse(row("idMotivoRecontacto").ToString())

                        End If
                        If Not row("FechaEstimada") Is DBNull.Value Then
                            tkt.FechaEstimada = DateTime.Parse(row("FechaEstimada").ToString())
                        End If

                    End If

                End If


                Return tkt



            Catch ex As Exception
                logger.Error("Error en getTicket: " & ex.Message, ex)
                Return Nothing

            End Try


        End Function

    End Class

End Namespace

Public Class TicketDatavisual
    Private _idTicket As Integer
    Private _mensajeError As String
    Public idEmpresa As Integer
    Public idCliente As String
    Public idEspecialista As Integer
    Public idArticulo As Integer
    Public idEstado As Integer
    Public idEmpresaPartner As Integer
    Public Prioridad As String
    Public NroSerie As String
    Public Problema As String
    Public fecha As DateTime
    Public nroClaim As String
    Public idUsuario As Integer
    Public idUbicacionContacto As Integer
    Public idTipoProblema As Integer
    Public idVendedor As String
    Public fechaInicio As DateTime
    Public fechaFin As DateTime
    Public FechaP As String
    Public FechaInicioScreen As String
    Public FechaFinScreen As String
    Public NombreCliente As String
    Public CodigoCliente As String
    Public Ubicacion As String
    Public Contacto As String
    Public Especialista As String
    Public CodigoEquipo As String
    Public NombreEquipo As String
    Public Estado As String
    Public TipoProblema As String
    Public UltimaModifScreen As String
    Public Vendedor As String
    Public Negocio As String
    Public FechaEstimada As DateTime
    Public InformeInterno As String
    Public InformeCliente As String
    Public sistemaOperativoId As Integer
    Public arquitecturaId As Integer
    Public servicePackId As Integer
    Public modificado As Boolean
    Public recontactarDesde As DateTime
    Public idMotivoRecontacto As Integer
    Public Property mensajeError() As String
        Get
            Return _mensajeError

        End Get
        Set(value As String)
            _mensajeError = value

        End Set

    End Property
    Public Property idTicket() As Integer
        Get
            Return _idTicket

        End Get
        Set(value As Integer)
            _idTicket = value

        End Set

    End Property


End Class



