﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Object




Namespace Popups
    Public Class PopupMgr
        Public Shared Sub Cerrar(page As System.Web.UI.Page, idHiddenField As String, valores As Dictionary(Of String, String))
            Dim functions As String = "<script>" & File.ReadAllText(page.Server.MapPath("~/js/PopupMgr.js")) & "</script>"
            Dim strVals = ""
            For Each k As Object In valores.Keys
                If strVals.Length > 0 Then
                    strVals &= "[|]"
                End If
                strVals &= k & "[>]" & valores.Item(k)
            Next
            strVals.Replace("'", "\'").Replace("""", "\""")
            page.Response.Write(functions + "<script> CloseAndReturn('" & idHiddenField & "','" & strVals & "');</script>")
        End Sub

        Public Shared Function LeerDato(ctrl As HiddenField, key As String, Optional bClean As Boolean = False) As String
            If ctrl.Value <> "" Then
                For Each v As String In Split(ctrl.Value, "[|]")
                    If Split(v, "[>]")(0) = key Then
                        If bClean Then ctrl.Value = ""
                        Return Split(v, "[>]")(1)
                    End If

                Next
                Return Nothing
            End If
            Return Nothing
        End Function

    End Class
End Namespace
