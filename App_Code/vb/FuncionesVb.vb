﻿
Imports System
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data
Imports System.Collections.Generic
Imports System.Web

Namespace App_Code.TicketsLibVb
    Public Class Funciones
        Public Shared Function Left(param As String, length As Integer) As String
            'we start at 0 since we want to get the characters starting from the
            'left and with the specified lenght and assign it to a variable
            Dim result As String = param.Substring(0, length)
            'return the result of the operation
            Return result
        End Function
        Public Shared Function Right(param As String, length As Integer) As String
            'start at the index based on the lenght of the sting minus
            'the specified lenght and assign it a variable
            Dim result As String = param.Substring(param.Length - length, length)
            'return the result of the operation
            Return result
        End Function

        Public Shared Function Mid(param As String, startIndex As Integer, length As Integer) As String
            'start at the specified index in the string ang get N number of
            'characters depending on the lenght and assign it to a variable
            Dim result As String = param.Substring(startIndex, length)
            'return the result of the operation
            Return result
        End Function

        Public Shared Function Mid(param As String, startIndex As Integer) As String
            'start at the specified index and return all characters after it
            'and assign it to a variable
            Dim result As String = param.Substring(startIndex)
            'return the result of the operation
            Return result
        End Function
        Public Shared Function DateParse(s As String) As DateTime
            Dim ci As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("es-AR")
            ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
            Return DateTime.Parse(s, ci.DateTimeFormat)
        End Function
        Public Shared Function DateParse(s As String, formatString As String) As DateTime
            Dim ci As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("es-AR")
            ci.DateTimeFormat.ShortDatePattern = formatString
            Return DateTime.Parse(s, ci.DateTimeFormat)
        End Function
        Public Shared Function FloatParse(s As String) As Single
            Dim ci As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("es-AR")
            ci.NumberFormat.NumberDecimalSeparator = "."
            ci.NumberFormat.NumberGroupSeparator = ","

            Return Single.Parse(s, ci.NumberFormat)
        End Function
        Public Shared Function FechaMaxima() As DateTime
            Return New DateTime(2999, 12, 31)
        End Function
        Public Shared Function rowDateTimeParse(dcFecha As [Object]) As DateTime
            Try
                If dcFecha.[GetType]().ToString().Equals("System.DateTime") Then
                    Return DirectCast(dcFecha, DateTime)
                End If

                If dcFecha.[GetType]().ToString().Equals("System.DBNull") Then
                    dcFecha = DateTime.MinValue
                End If

                Return DirectCast(dcFecha, DateTime)
            Catch
                Return DateTime.MinValue
            End Try
        End Function
        Public Shared Function isDateTimeNull(dt As DateTime) As Boolean
            Return dt.ToString("yyyyMMdd").Equals("00010101")
        End Function
        Public Shared Function dateNull() As DateTime
            Return New DateTime()
        End Function
        Public Shared Function getConnString() As String
            Return System.Configuration.ConfigurationManager.ConnectionStrings("TicketDB").ToString()
        End Function
        Public Shared Function getConnStringservicios() As String
            Return System.Configuration.ConfigurationManager.ConnectionStrings("TicketDBServicios").ToString()
        End Function
        Public Shared Function fechaHoraToDateTime(fecha As String, hora As String) As DateTime
            Try
                Dim ci As New System.Globalization.CultureInfo("es-AR")
                ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy HH:mm"


                Return DateTime.Parse(Convert.ToString(fecha & Convert.ToString(" ")) & hora, ci.DateTimeFormat)
            Catch
                Return DateTime.MinValue
            End Try
        End Function
        Public Shared Function getScalar(connstring As String, query As String) As Object
            Dim SQL As New SqlConnection(connstring)
            Dim CMD As New SqlCommand("", SQL)
            CMD.CommandText = query
            Try
                SQL.Open()
                Dim val As [Object] = CMD.ExecuteScalar()
                SQL.Close()
                Return val
            Catch e As Exception
                SQL.Close()
                Throw e
            End Try
        End Function
        Public Shared Function execSql(nonQuery As String) As Object
            Dim SQL As New SqlConnection(getConnString())
            Dim CMD As New SqlCommand(nonQuery, SQL)
            Try
                SQL.Open()
                Dim val As [Object] = CMD.ExecuteNonQuery()
                SQL.Close()
                Return val
            Catch e As Exception
                SQL.Close()
                Throw e
            End Try
        End Function
        Public Shared Function esNumerico(texto As String) As Boolean
            For Each ch As Char In texto
                If Not "0123456789.,".Contains(ch.ToString()) Then
                    Return False
                End If
            Next
            Return True
        End Function
        Public Shared Function ObtenerArchivosComprobantes(TicketId As Integer) As DataTable
            Dim path = HttpContext.Current.Server.MapPath("~/Images/" & TicketId)
            Dim di As New DirectoryInfo(path)
            Dim res As New List(Of String)
            Dim dt As New DataTable("Documentos")
            dt.Columns.Add("NombreArchivo")
            dt.Columns.Add("Tamaño")
            If di.Exists Then
                For Each fi As FileInfo In di.GetFiles()
                    dt.Rows.Add({fi.Name, Math.Round(fi.Length / 1024, 2) & "kb"})
                Next
            End If
            Return dt
        End Function
    End Class
End Namespace