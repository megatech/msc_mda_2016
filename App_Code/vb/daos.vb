﻿Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic


Public Class DAOs
    Public Shared Function getScalar(query As String) As Object
        Dim SQL As New SqlConnection()
        Dim CMD As New SqlCommand()
        Dim RES As Object = Nothing
        Try
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ToString()
            CMD.Connection = SQL
            CMD.CommandText = query
            SQL.Open()
            RES = CMD.ExecuteScalar()
            SQL.Close()
            Return RES
        Catch ex As Exception
            If SQL.State <> ConnectionState.Closed Then
                SQL.Close()
            End If
            Throw ex
        End Try
    End Function

End Class
