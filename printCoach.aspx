﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="printCoach.aspx.vb" Inherits="printCoach" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Imprimir Coaching</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
<asp:scriptmanager ID="Scriptmanager1" runat="server"></asp:scriptmanager>
         <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%">
            <LocalReport ReportPath="coaching.rdlc" >
                  <DataSources>
                    <rsweb:ReportDataSource DataSourceId="dsCoach" Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
      
<asp:SqlDataSource ID="dsCoach" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT
	vtc.id,
	vtc.Operador,
	vtc.usuarioId,
	vtc.Lider,
	vtc.LiderUid,
	vtc.fecha,
	vtc.fechaDevolucion,
	vtc.[PEC(%)] AS PEC__,
	vtc.PEC,
	vtc.[PENC(%)] AS PENC__,
	vtc.PENC,
	vtc.ECN1,
	vtc.ECN2,
	vtc.ECN3,
	vtc.ECN4,
	vtc.ECN5,
	vtc.ECN6,
	vtc.ECU1,
	vtc.ECU2,
	vtc.ECU3,
	vtc.ECU4,
	vtc.ENC1,
	vtc.ENC2,
	vtc.ENC3,
	vtc.ENC4,
	vtc.ENC5,
	vtc.ENC6,
	vtc.justificacion,
	vtc.replica,
	vtc.Autor,
	vtc.autorUid,
	vtc.monitoreo,

	tk.TicketId,
	vc.Telefono1 AS ani,
	tp.Nombre AS tipoProblema,
	c.SSIDQOOM,
	c.escucha,
	c.devolucion,
	CAST (
		dc.PEC_UsuarioFinal_SinRespetoIncorrecto AS VARCHAR (10)
	) + '%' AS PEC_UsuarioFinal_SinRespetoIncorrecto,
	CAST (
		dc.PEC_UsuarioFinal_ConRespetoIncorrecto AS VARCHAR (10)
	) + '%' AS PEC_UsuarioFinal_ConRespetoIncorrecto
FROM
	viewTableroCoaching_Nuevo AS vtc
INNER JOIN coachingNuevo AS c ON vtc.id = c.id
LEFT OUTER JOIN ticket AS tk ON c.tkt = tk.TicketId
LEFT OUTER JOIN ViewCliente AS vc ON tk.ClienteId = vc.ClienteId
LEFT OUTER JOIN tipoproblema AS tp ON tp.IdTipoProblema = tk.IdTipoProblema
LEFT OUTER JOIN viewDetalleCoaching AS dc ON c.id = dc.id
WHERE
	(vtc.id = @id)">

            <SelectParameters>
                <asp:QueryStringParameter QueryStringField="id" Name="id" />
            </SelectParameters>
        </asp:SqlDataSource>
    
    
    </div>
    </form>
</body>
</html>
