﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="partes.aspx.vb" Inherits="Default3" MasterPageFile="TicketsPage.master" %>

<asp:Content ContentPlaceHolderID="cphTicket" runat="server" >

    <div style="border-style: double">    
    <div align="right">
        <asp:Button ID="Button1" runat="server" Text="Volver" 
            /></div>
    <h3 align="center" class="headerStyle"><u>Modulo de Partes</u></h3>

      <h4 align="center">Partes Asignadas</h4>
    <div align="center">
        <asp:ListView ID="ListView1" runat="server" 
            DataSourceID="SqlDataSource1">
            <AlternatingItemTemplate>
                <tr style="background-color:#FFF8DC;">
                    <td>
                        <asp:Label ID="P_NLabel" runat="server" 
                            Text='<%# Eval("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DetalleLabel" runat="server" Text='<%# Eval("Detalle") %>' />
                    </td>
                    <td>
                        <asp:Label ID="depositoLabel" runat="server" 
                            Text='<%# Eval("deposito") %>' />
                    </td>
                    <td>
                        <asp:Label ID="NroSerieLabel" runat="server" 
                            Text='<%# Eval("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:Label ID="RemitoLabel" runat="server" Text='<%# Eval("Remito") %>' />
                    </td>
                    <td>
                        <asp:Label ID="CondicionLabel" runat="server" Text='<%# Eval("Condicion") %>' />
                    </td>
                    <td>
                        <asp:Label ID="PrecioLabel" runat="server" Text='<%# Eval("Precio") %>' />
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <EditItemTemplate>
                <tr style="background-color:#008A8C;color: #FFFFFF;">
                    <td>
                        <asp:Button ID="UpdateButton" runat="server" CommandName="Update" 
                            Text="Actualizar" />
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                            Text="Cancelar" />
                    </td>
                    <td>
                        <asp:TextBox ID="P_NTextBox" runat="server" Text='<%# Bind("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="DetalleTextBox" runat="server" 
                            Text='<%# Bind("Detalle") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="depositoTextBox" runat="server" 
                            Text='<%# Bind("deposito") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="NroSerieTextBox" runat="server" 
                            Text='<%# Bind("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="RemitoTextBox" runat="server" 
                            Text='<%# Bind("Remito") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="CondicionTextBox" runat="server" 
                            Text='<%# Bind("Condicion") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="PrecioTextBox" runat="server" Text='<%# Bind("Precio") %>' />
                    </td>
                </tr>
            </EditItemTemplate>
            <EmptyDataTemplate>
                <table runat="server" 
                    style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                    <tr>
                        <td>
                            No se han devuelto datos.</td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <InsertItemTemplate>
                <tr style="">
                    <td>
                        <asp:Button ID="InsertButton" runat="server" CommandName="Insert" 
                            Text="Insertar" />
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                            Text="Borrar" />
                    </td>
                    <td>
                        <asp:TextBox ID="P_NTextBox" runat="server" Text='<%# Bind("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="DetalleTextBox" runat="server" 
                            Text='<%# Bind("Detalle") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="depositoTextBox" runat="server" 
                            Text='<%# Bind("deposito") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="NroSerieTextBox" runat="server" 
                            Text='<%# Bind("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="RemitoTextBox" runat="server" 
                            Text='<%# Bind("Remito") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="CondicionTextBox" runat="server" 
                            Text='<%# Bind("Condicion") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="PrecioTextBox" runat="server" Text='<%# Bind("Precio") %>' />
                    </td>
                </tr>
            </InsertItemTemplate>
            <ItemTemplate>
                <tr style="background-color:#DCDCDC;color: #000000;">
                    <td>
                        <asp:Label ID="P_NLabel" runat="server" 
                            Text='<%# Eval("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DetalleLabel" runat="server" Text='<%# Eval("Detalle") %>' />
                    </td>
                    <td>
                        <asp:Label ID="depositoLabel" runat="server" 
                            Text='<%# Eval("deposito") %>' />
                    </td>
                    <td>
                        <asp:Label ID="NroSerieLabel" runat="server" 
                            Text='<%# Eval("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:Label ID="RemitoLabel" runat="server" Text='<%# Eval("Remito") %>' />
                    </td>
                    <td>
                        <asp:Label ID="CondicionLabel" runat="server" Text='<%# Eval("Condicion") %>' />
                    </td>
                    <td>
                        <asp:Label ID="PrecioLabel" runat="server" Text='<%# Eval("Precio") %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <LayoutTemplate>
                <table runat="server">
                    <tr runat="server">
                        <td runat="server">
                            <table ID="itemPlaceholderContainer" runat="server" border="1" 
                                style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                                <tr runat="server" style="background-color:#DCDCDC;color: #000000;">
                                    <th runat="server">
                                        P/N</th>
                                    <th runat="server">
                                        Detalle</th>
                                    <th runat="server">
                                        Deposito</th>
                                    <th runat="server">
                                        NroSerie</th>
                                    <th runat="server">
                                        Remito</th>
                                    <th runat="server">
                                        Condición</th>
                                    <th runat="server" >
                                        Precio U$D</th>
                                </tr>
                                <tr ID="itemPlaceholder" runat="server">
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td runat="server" 
                            style="text-align: center;background-color: #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;color: #000000;">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                        ShowLastPageButton="True" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <SelectedItemTemplate>
                <tr style="background-color:#008A8C;font-weight: bold;color: #FFFFFF;">
                    <td>
                        <asp:Label ID="P_NLabel" runat="server" 
                            Text='<%# Eval("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DetalleLabel" runat="server" Text='<%# Eval("Detalle") %>' />
                    </td>
                    <td>
                        <asp:Label ID="depositoLabel" runat="server" 
                            Text='<%# Eval("deposito") %>' />
                    </td>
                    <td>
                        <asp:Label ID="NroSerieLabel" runat="server" 
                            Text='<%# Eval("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:Label ID="RemitoLabel" runat="server" Text='<%# Eval("Remito") %>' />
                    </td>
                    <td>
                        <asp:Label ID="CondicionLabel" runat="server" Text='<%# Eval("Condicion") %>' />
                    </td>
                    <td>
                        <asp:Label ID="PrecioLabel" runat="server" Text='  <%# Eval("Precio") %>' />
                    </td>
                </tr>
            </SelectedItemTemplate>
        </asp:ListView>
        <asp:Button ID="CmdAsignar" runat="server" Text="Asignar Partes" />
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
            SelectCommand="select instanciaarticulo.articuloid as 'P/N', 
viewarticulo.nombre as 'Detalle',viewdeposito.nombre as 'deposito',
 nroserie as 'NroSerie', remitoid as 'Remito',condicionpago as 'Condicion',instanciaarticulo.precio as 'Precio'
from instanciaarticulo 
inner join viewarticulo on viewarticulo.articuloid = instanciaarticulo.articuloid
inner join viewdeposito on viewdeposito.depositoid = instanciaarticulo.depositoid
inner join ticketpresupuesto tp on instanciaarticulo.ticketid = tp.ticketid
where instanciaarticulo.ticketid = 200123 and instanciaarticulo.ticketpresupuestoid is not null
 
 

"></asp:SqlDataSource>
    
    </div>
    <div align="center">
    <h4 align="center">Partes Devueltas</h4>
   
        <asp:ListView ID="ListView2" runat="server" 
            DataSourceID="SqlDataSource2">
            <AlternatingItemTemplate>
                <tr style="background-color:#FFF8DC;">
                    <td>
                        <asp:Label ID="P_NLabel" runat="server" 
                            Text='<%# Eval("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DescripciónLabel" runat="server" 
                            Text='<%# Eval("Descripción") %>' />
                    </td>
                    <td>
                        <asp:Label ID="NroSerieLabel" runat="server" 
                            Text='<%# Eval("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DepositoLabel" runat="server" 
                            Text='<%# Eval("Deposito") %>' />
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <EditItemTemplate>
                <tr style="background-color:#008A8C;color: #FFFFFF;">
                    <td>
                        <asp:Button ID="UpdateButton" runat="server" CommandName="Update" 
                            Text="Actualizar" />
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                            Text="Cancelar" />
                    </td>
                    <td>
                        <asp:TextBox ID="P_NTextBox" runat="server" Text='<%# Bind("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="DescripciónTextBox" runat="server" 
                            Text='<%# Bind("Descripción") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="NroSerieTextBox" runat="server" 
                            Text='<%# Bind("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="DepositoTextBox" runat="server" 
                            Text='<%# Bind("Deposito") %>' />
                    </td>
                </tr>
            </EditItemTemplate>
            <EmptyDataTemplate>
                <table runat="server" 
                    style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                    <tr>
                        <td>
                            No se han devuelto datos.</td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <InsertItemTemplate>
                <tr style="">
                    <td>
                        <asp:Button ID="InsertButton" runat="server" CommandName="Insert" 
                            Text="Insertar" />
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                            Text="Borrar" />
                    </td>
                    <td>
                        <asp:TextBox ID="P_NTextBox" runat="server" Text='<%# Bind("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="DescripciónTextBox" runat="server" 
                            Text='<%# Bind("Descripción") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="NroSerieTextBox" runat="server" 
                            Text='<%# Bind("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="DepositoTextBox" runat="server" 
                            Text='<%# Bind("Deposito") %>' />
                    </td>
                </tr>
            </InsertItemTemplate>
            <ItemTemplate>
                <tr style="background-color:#DCDCDC;color: #000000;">
                    <td>
                        <asp:Label ID="P_NLabel" runat="server" 
                            Text='<%# Eval("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DescripciónLabel" runat="server" Text='<%# Eval("Descripción") %>' />
                    </td>
                    <td>
                        <asp:Label ID="NroSerieLabel" runat="server" 
                            Text='<%# Eval("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DepositoLabel" runat="server" 
                            Text='<%# Eval("Deposito") %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <LayoutTemplate>
                <table runat="server">
                    <tr runat="server">
                        <td runat="server">
                            <table ID="itemPlaceholderContainer" runat="server" border="1" 
                                style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                                <tr runat="server" style="background-color:#DCDCDC;color: #000000;">
                                    <th runat="server">
                                        P/N</th>
                                    <th runat="server">
                                        Descripción</th>
                                    <th runat="server">
                                        NroSerie</th>
                                    <th runat="server">
                                        Deposito</th>
                                </tr>
                                <tr ID="itemPlaceholder" runat="server">
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td runat="server" 
                            style="text-align: center;background-color: #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;color: #000000;">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                        ShowLastPageButton="True" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <SelectedItemTemplate>
                <tr style="background-color:#008A8C;font-weight: bold;color: #FFFFFF;">
                    <td>
                        <asp:Label ID="P_NLabel" runat="server" 
                            Text='<%# Eval("[P/N]") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DescripciónLabel" runat="server" 
                            Text='<%# Eval("Descripción") %>' />
                    </td>
                    <td>
                        <asp:Label ID="NroSerieLabel" runat="server" 
                            Text='<%# Eval("NroSerie") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DepositoLabel" runat="server" 
                            Text='<%# Eval("Deposito") %>' />
                    </td>
                </tr>
            </SelectedItemTemplate>
        </asp:ListView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
            SelectCommand="select ia.Articuloid as 'P/N',va.nombre as 'Descripción',nroserie as 'NroSerie',viewdeposito.nombre as 'Deposito' from ticketparte tp inner join instanciaarticulo ia on ia.instanciaarticuloid = tp.instanciaarticuloid inner join viewarticulo va on va.articuloid = ia.articuloid inner join viewdeposito on viewdeposito.depositoid = ia.depositoid where tp.ticketid = 208443 "></asp:SqlDataSource>

            <br />
            <h4 align="center">Servicios</h4>
     <asp:ListView ID="ListView3" runat="server" DataSourceID="SqlDataSource3">
         <AlternatingItemTemplate>
             <tr style="background-color:#FFF8DC;">
                 <td>
                     <asp:Label ID="Cant_Label" runat="server" Text='<%# Eval("Cant.") %>' />
                 </td>
                 <td>
                     <asp:Label ID="DescripciónLabel" runat="server" 
                         Text='<%# Eval("Descripción") %>' />
                 </td>
                 <td>
                     <asp:Label ID="CondicionLabel" runat="server" Text='<%# Eval("Condicion") %>' />
                 </td>
                 <td>
                     <asp:Label ID="PrecioLabel" runat="server" Text='<%# Eval("Precio") %>' />
                 </td>
             </tr>
         </AlternatingItemTemplate>
         <EditItemTemplate>
             <tr style="background-color:#008A8C;color: #FFFFFF;">
                 <td>
                     <asp:Button ID="UpdateButton" runat="server" CommandName="Update" 
                         Text="Actualizar" />
                     <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                         Text="Cancelar" />
                 </td>
                 <td>
                     <asp:TextBox ID="Cant_TextBox" runat="server" Text='<%# Bind("Cant.") %>' />
                 </td>
                 <td>
                     <asp:TextBox ID="DescripciónTextBox" runat="server" 
                         Text='<%# Bind("Descripción") %>' />
                 </td>
                 <td>
                     <asp:TextBox ID="CondicionTextBox" runat="server" 
                         Text='<%# Bind("Condicion") %>' />
                 </td>
                 <td>
                     <asp:TextBox ID="PrecioTextBox" runat="server" Text='<%# Bind("Precio") %>' />
                 </td>
             </tr>
         </EditItemTemplate>
         <EmptyDataTemplate>
             <table runat="server" 
                 style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                 <tr>
                     <td>
                         No se han devuelto datos.</td>
                 </tr>
             </table>
         </EmptyDataTemplate>
         <InsertItemTemplate>
             <tr style="">
                 <td>
                     <asp:Button ID="InsertButton" runat="server" CommandName="Insert" 
                         Text="Insertar" />
                     <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                         Text="Borrar" />
                 </td>
                 <td>
                     <asp:TextBox ID="Cant_TextBox" runat="server" Text='<%# Bind("Cant") %>' />
                 </td>
                 <td>
                     <asp:TextBox ID="DescripciónTextBox" runat="server" 
                         Text='<%# Bind("Descripción") %>' />
                 </td>
                 <td>
                     <asp:TextBox ID="CondicionTextBox" runat="server" 
                         Text='<%# Bind("Condicion") %>' />
                 </td>
                 <td>
                     <asp:TextBox ID="PrecioTextBox" runat="server" Text='<%# Bind("Precio") %>' />
                 </td>
             </tr>
         </InsertItemTemplate>
         <ItemTemplate>
             <tr style="background-color:#DCDCDC;color: #000000;">
                 <td>
                     <asp:Label ID="Cant_Label" runat="server" Text='<%# Eval("Cant") %>' />
                 </td>
                 <td>
                     <asp:Label ID="DescripciónLabel" runat="server" 
                         Text='<%# Eval("Descripción") %>' />
                 </td>
                 <td>
                     <asp:Label ID="CondicionLabel" runat="server" Text='<%# Eval("Condicion") %>' />
                 </td>
                 <td>
                     <asp:Label ID="PrecioLabel" runat="server" Text='<%# Eval("Precio") %>' />
                 </td>
             </tr>
         </ItemTemplate>
         <LayoutTemplate>
             <table runat="server">
                 <tr runat="server">
                     <td runat="server">
                         <table ID="itemPlaceholderContainer" runat="server" border="1" 
                             style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                             <tr runat="server" style="background-color:#DCDCDC;color: #000000;">
                                 <th runat="server">
                                     Cant</th>
                                 <th runat="server">
                                     Descripción</th>
                                 <th runat="server">
                                     Condicion</th>
                                 <th runat="server">
                                     Precio</th>
                             </tr>
                             <tr ID="itemPlaceholder" runat="server">
                             </tr>
                         </table>
                     </td>
                 </tr>
                 <tr runat="server">
                     <td runat="server" 
                         style="text-align: center;background-color: #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;color: #000000;">
                         <asp:DataPager ID="DataPager1" runat="server">
                             <Fields>
                                 <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                     ShowLastPageButton="True" />
                             </Fields>
                         </asp:DataPager>
                     </td>
                 </tr>
             </table>
         </LayoutTemplate>
         <SelectedItemTemplate>
             <tr style="background-color:#008A8C;font-weight: bold;color: #FFFFFF;">
                 <td>
                     <asp:Label ID="Cant_Label" runat="server" Text='<%# Eval("Cant") %>' />
                 </td>
                 <td>
                     <asp:Label ID="DescripciónLabel" runat="server" 
                         Text='<%# Eval("Descripción") %>' />
                 </td>
                 <td>
                     <asp:Label ID="CondicionLabel" runat="server" Text='<%# Eval("Condicion") %>' />
                 </td>
                 <td>
                     <asp:Label ID="PrecioLabel" runat="server" Text='<%# Eval("Precio") %>' />
                 </td>
             </tr>
         </SelectedItemTemplate>
        </asp:ListView>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TicketDB %>"></asp:SqlDataSource>
    </div>
       
    </div>

    </asp:Content>