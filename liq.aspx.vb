﻿Imports App_Code.TicketsLib
Imports System.Data.SqlClient


Partial Class liq

    Inherits System.Web.UI.Page

    Dim tkt As String
    Dim SQL As New SqlConnection
    Dim CMD As New SqlCommand
    Dim fill As New SqlCommand
    Dim insert As New SqlCommand
    Dim update As New SqlCommand
    Dim cosrep As New SqlCommand
    Dim esp As New SqlCommand
    Dim especialista As String
    Dim query1 As SqlDataReader
    Dim resu As SqlDataReader
    Dim RS As String
    Dim pepe As String
    Dim comprobante As String
    Dim costorep As String
    Dim directorio As String
    Dim estado As String


    Private bExiste As Boolean = False


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session.Item("sessid") Is Nothing Then
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString)
        End If

        Dim ususarioactual As Usuario

        ususarioactual = Session("usuario")

        If ususarioactual.Perfil <> 1 Then
            Response.Redirect("Default2.aspx")
        End If


        tkt = Request.QueryString("id")
        lbtkt.Text = tkt
        lbtotal.Text = "0"


        SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
        SQL.Open()
        CMD.Connection = SQL
        CMD.CommandText = "SELECT count (*) FROM liquidacion_cas where ticketid = " & tkt
        RS = CMD.ExecuteScalar()

        esp.Connection = SQL
        esp.CommandText = " select especialistaid,estadoid from ticket where ticketid = " & tkt & ""
        query1 = esp.ExecuteReader()
        query1.Read()

        especialista = query1(0)
        estado = query1(1)
        query1.Close()
        esp.ExecuteReader.Close()


        If estado = 2 Then
            ddcodrep.Visible = False
            km.ReadOnly = True
            pesos.ReadOnly = True
            Me.lbcerrado.Visible = True
        End If

        directorio = "images"

        If RS = 1 Then

            If IsPostBack = False Then

                fill.Connection = SQL
                fill.CommandText = " SELECT viatico,viaticocant,codrep,costorep,comprobanteok,autorizadook,balanceadook FROM liquidacion_cas where ticketid =" & tkt & ""
                resu = fill.ExecuteReader()
                resu.Read()
                pesos.Text = resu(0)
                km.Text = resu(1)
                lbcodrep.Text = resu(2)
                ddcodrep.SelectedValue = resu(2)
                costo.Text = CDbl(resu(3))
                lbtotal.Text = CDbl(resu(0)) * CDbl(resu(1))
                ok.Checked = resu(4)
                bExiste = True


            Else
                bExiste = False

            End If

            If ok.Checked = True Then
                FileUpload1.Visible = False
                comimg.ImageUrl = ".\" & directorio & "\" & tkt & ".jpg"
                comimg.Visible = True
                ampliar.Visible = True

                ampliar.NavigateUrl = ".\" & directorio & "\" & tkt & ".jpg"

            End If

        End If
        SQL.Close()

    End Sub


    Protected Sub km_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles km.TextChanged
        If (km.Text <> "") And (pesos.Text <> "") Then
            If (IsNumeric(km.Text)) And (IsNumeric(pesos.Text)) Then
                lbtotal.Text = CDbl(km.Text) * CDbl(pesos.Text)
            End If
        End If
        If Not IsNumeric(km.Text) Then
            km.Text = ""
            lbtotal.Text = "0"
        End If
        ddcodrep_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub pesos_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pesos.TextChanged
        If (km.Text <> "") And (pesos.Text <> "") Then
            If (IsNumeric(km.Text)) And (IsNumeric(pesos.Text)) Then
                lbtotal.Text = CDbl(km.Text) * CDbl(pesos.Text)
            End If
        End If
        If Not IsNumeric(pesos.Text) Then
            pesos.Text = ""
            lbtotal.Text = "Inserte Valores"
        End If
        ddcodrep_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        If (FileUpload1.HasFile = True) Then
            Dim filepath As String = FileUpload1.PostedFile.FileName
            Dim pat As String = "\\(?:.+)\\(.+)\.(.+)"
            Dim r As Regex = New Regex(pat)
            'run
            Dim m As Match = r.Match(filepath)
            Dim file_ext As String = m.Groups(2).Captures(0).ToString()
            Dim filename As String = m.Groups(1).Captures(0).ToString()
            Dim file As String = tkt & "." & file_ext

            If Not FileUpload1.PostedFile.ContentLength <= 1048576 Then
                lblStatus.Text = "El tamaño maximo permitido por el servidor es de 1 MB"
                Exit Sub
            End If

            If (file_ext <> "jpg") And (file_ext <> "JPG") Then
                lblStatus.Text = "Debe seleccionar un archivo jpg"
                Exit Sub
            End If

            'Guarda el archivo en el file server. 
            FileUpload1.PostedFile.SaveAs(Server.MapPath(".\" & directorio & "\") & file)
            lblStatus.Text = "El archivo fue guardado: " & file
            ok.Checked = True


        End If

        If (ok.Checked = True) Then
            comprobante = 1

        Else
            comprobante = 0
        End If

        lbtotal.Text = Replace(lbtotal.Text, ",", ".")
        pesos.Text = Replace(pesos.Text, ",", ".")
        km.Text = Replace(km.Text, ",", ".")

        Dim balance As New SqlCommand
        Dim balok As String
        Dim b As String

        balance.Connection = SQL
        balance.CommandText = "SELECT (( SELECT COUNT(*) FROM TicketPresupuesto TPB (nolock) WHERE " & _
                                " TPB.TicketId = TK.TicketId and tp.articuloid != 'DVDGRAB' ) - ( SELECT COUNT(*)FROM TicketParte TPM (nolock) WHERE TK.TicketId = TPM.TicketId " & _
                                " and tp.articuloid != 'DVDGRAB' )) AS balanceo FROM Ticket TK (NOLOCK) LEFT JOIN TicketPresupuesto TP (nolock) ON TK.TicketId = TP.TicketId WHERE tk.ticketid = " & tkt & ""

        balok = balance.ExecuteScalar()

        If balok = 0 Then

            b = 1
        Else
            b = 0

        End If

        If RS = 0 Then
            insert.Connection = SQL
            insert.CommandText = " insert into liquidacion_cas (ticketid,viatico,viaticocant,codrep,costorep,comprobanteok,balanceadook,autorizadook) values (" & tkt & " , " & pesos.Text & " , " & km.Text & " , '" & ddcodrep.SelectedValue & "' , " & costo.Text & " , " & comprobante & ", " & b & ",0 ) "

            pepe = insert.ExecuteNonQuery()

            If pepe <= 0 Then
                Response.Redirect("error.aspx")
            Else


            End If
        End If




        If RS = 1 Then
            update.Connection = SQL
            update.CommandText = " update liquidacion_cas set viatico = " & pesos.Text & " , viaticocant = " & km.Text & " , codrep = '" & ddcodrep.SelectedValue & "' , costorep = " & CLng(costo.Text) & " ,comprobanteok = " & comprobante & ", balanceadook =" & b & " where ticketid = " & tkt & " "
            pepe = update.ExecuteNonQuery()


            If pepe <= 0 Then
                Response.Redirect("error.aspx")
            Else
                Response.Redirect("liq.aspx?id=" & tkt & "")


            End If

        End If

    End Sub

    Protected Sub ddcodrep_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcodrep.DataBound

        If bExiste = False Then

            ddcodrep.Items.Add("Seleccione un Código")
            ddcodrep.Items(ddcodrep.Items.Count - 1).Selected = True

        End If

    End Sub


    Protected Sub ddcodrep_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcodrep.SelectedIndexChanged

        cosrep.Connection = SQL
        cosrep.CommandText = "select valor from tarifario where cod = (select cod from codrep where tipo = '" & ddcodrep.SelectedValue & "') and cas = " & especialista & ""
        costo.Text = CLng(cosrep.ExecuteScalar())

       


        If km.Text <> "" And pesos.Text <> "" Then
            If IsNumeric(km.Text) And IsNumeric(pesos.Text) Then
                lbtotal.Text = CStr(CDbl(km.Text) * CDbl(pesos.Text))
            End If
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click


        Dim tkt As String
        tkt = Request.QueryString("id")
        Response.Redirect("EditaTicket.aspx?id=" & tkt & "")

    End Sub

End Class
