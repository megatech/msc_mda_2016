﻿
Partial Class popupReplica
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim id = Request.QueryString("id")
        Dim replica = DAOs.getScalar("SELECT replica from coachingNuevo where id=" & id)
        If IsDBNull(replica) Then
            lblReplica.Text = "El Operador aún no ha confirmado la devolución del monitoreo."
        Else
            lblReplica.Text = replica
        End If
    End Sub
End Class
