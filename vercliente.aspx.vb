﻿Imports App_Code.TicketsLib
Imports System.Data

Partial Class vercliente
    Inherits System.Web.UI.Page
    Private validacion2 As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Session("Usuario") Is Nothing) Then
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString)
        End If
        validacion2 = detectarTipoValidacion()
    End Sub

    Private Sub AddControls(ByVal page As ControlCollection, ByVal controlList As ArrayList)
        For Each c As Control In page
            If c.ID IsNot Nothing Then
                controlList.Add(c.ID)
            End If

            If c.HasControls() Then
                AddControls(c.Controls, controlList)
            End If
        Next
    End Sub
    Public Function detectarTipoValidacion() As Boolean
        Dim b As Boolean = Not (Request.QueryString("onsite") Is Nothing) Or (Not Request.QueryString("onsite") = "")
        lblV8.Visible = b
        lblV9.Visible = b
        lblV10.Visible = b
        lblV11.Visible = b
        lblV5.Visible = b
        Return b
    End Function

    Private Function SelectInCB(ByVal Cadena As String, ByVal CB As DropDownList) As Boolean
        Dim ret As Boolean = False
        Dim objItem As ListItem
        For Each objItem In CB.Items
            If objItem.Value = Cadena Then
                objItem.Selected = True
                ret = True
            Else : objItem.Selected = False
            End If
        Next
        Return ret
    End Function

    Protected Sub cargarPartidos()
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim rs

        ddlPartido.Items.Clear()
        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql
        query.CommandText = "SELECT id,nombre FROM partido order by nombre"
        ddlPartido.Items.Add("TODOS")
        ddlPartido.Items(0).Value = -1
        rs = query.ExecuteReader()
        While rs.read()
            ddlPartido.Items.Add(rs(1))
            ddlPartido.Items(ddlPartido.Items.Count() - 1).Value = rs(0)
        End While
        rs.Close()
        sql.Close()

        ddlPartido.Visible = ddlProvincia.SelectedValue = "902"
        lblPartido.Visible = ddlProvincia.SelectedValue = "902"
    End Sub

    Protected Sub seleccionarPartido()
        If ddlLocalidad.SelectedValue = "-1" Then
            ddlPartido.SelectedValue = ddlLocalidad.SelectedValue
            ddlProvincia_SelectedIndexChanged(Me, New EventArgs())
            Exit Sub
        End If


        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim rs
        Dim localidadActual = ddlLocalidad.SelectedValue.ToString()
        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql
        query.CommandText = "SELECT partido.id FROM partido right join localidad on localidad.partidoId=partido.id Where localidad.id = " & localidadActual
        rs = query.ExecuteReader()
        rs.read()
        If Not IsDBNull(rs(0)) Then ddlPartido.SelectedValue = rs(0)
        rs.Close()
        sql.Close()
        cargarLocalidades()
        ddlLocalidad.SelectedValue = localidadActual
    End Sub

    Protected Sub cargarLocalidades()
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim rs As Data.SqlClient.SqlDataReader
        Dim condPartido As String = ""

        If ddlPartido.SelectedValue <> "-1" Then condPartido = " AND EXISTS (Select * from partido where partido.id = '" & ddlPartido.SelectedValue & "' AND l.partidoId = partido.id )"
        ddlLocalidad.Items.Clear()
        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql
        query.CommandText = "SELECT l.id, l.nombre FROM localidad l WHERE l.provinciaId = '" & ddlProvincia.SelectedValue & "'" & condPartido & " order by l.nombre"

        rs = query.ExecuteReader()
        ddlLocalidad.Items.Add("Seleccione una opción")
        ddlLocalidad.Items(0).Value = -1
        While rs.Read()
            ddlLocalidad.Items.Add(rs(1).ToString.Split("(")(0))
            ddlLocalidad.Items(ddlLocalidad.Items.Count() - 1).Value = rs(0)
        End While

        rs.Close()
        sql.Close()
    End Sub

    Private Function nulearString(ByVal val As String) As String
        Dim res As String
        If val.ToString().Trim() = "" Then res = "NULL" Else res = "'" & val.Trim & "'"
        Return res
    End Function

    Private Function validar(ByRef msg As String) As Boolean

        If Not validarCampo(txtRazonSocial, txtRazonSocial.Text.Trim() = "") Then
            msg &= "El campo 'Razón Social' está vacío.<br>"
        End If
        If Not validarCampo(txtRazonSocial, Not soloLetras(txtRazonSocial.Text.Trim())) Then
            msg += "El campo 'Razón Social' no puede contener números.<br>"
        End If
        If Not validarCampo(txtcel, Not esTelefonoValido(txtcel.Text.Trim())) Then
            msg &= "El campo 'Celular' contiene caracteres inválidos, o un valor inferior a 10 dígitos.<br>"
        End If
        If Not validarCampo(txtTelefono, txtTelefono.Text.Trim() = "") Then
            msg &= "El campo 'Teléfono' está vacío.<br>"
        End If
        If Not validarCampo(txtcel, txtcel.Text.Trim() = "") Then
            msg &= "El campo 'Celular' está vacío.<br>"
        End If
        If Not validarCampo(txtTelefono, Not esTelefonoValido(txtTelefono.Text.Trim())) Then
            msg &= "El campo 'Teléfono' contiene caracteres inválidos, o un valor inferior a 10 dígitos.<br>"
        End If
        If Not validarCampo(txtLocalidad, Not esLocalidadValida(ddlProvincia.SelectedValue, txtLocalidad.Text)) Then
            If txtLocalidad.Text.Trim = "" Then msg &= "El campo 'Localidad' está vacío.<br>" Else msg &= " La localidad especificada no pertenece a '" & ddlProvincia.SelectedItem.Text.Trim & "'.<br>"
        End If
        'If Not validarCampo(txtTelefono, Not esCaracteristicaTelValida(txtTelefono.Text, ddlProvincia.SelectedValue)) Then
        '    msg &= "La característica del número telefónico corresponde a Buenos Aires y Cap.Fed.(AMBA), verifique la provincia seleccionada.<br>"
        'End If
        'If Not validarCampo(txtTelefono, Not esCaracteristicaTelValida2(txtTelefono.Text, ddlProvincia.SelectedValue)) Then
        '    msg &= "La característica del número telefónico no corresponde a Capital Federal, verifique la provincia seleccionada.<br>"
        'End If
        'If validacion2 Then
        ' If Not validarCampo(txtDomicilio, txtDomicilio.Text.Trim = "") Then Return False
        ' If Not validarCampo(txtCodPostal, txtCodPostal.Text.Trim = "") Then Return False
        ' If Not validarCampo(txtEmail, txtEmail.Text.Trim = "") Then Return False
        ' If Not validarCampo(txtLocalidad, txtEmail.Text.Trim = "") Then Return False
        ' If Not validarCampo(txtDocumento, txtDocumento.Text.Trim = "") Then Return False
        ' End If
        Return msg = ""
    End Function

    Public Function esLocalidadValida(ByVal provId As String, ByVal localidad As String) As [Boolean]
        Return Funciones.getScalar("SELECT count(*) FROM localidad WHERE rtrim(provinciaId)='" + provId.Trim() + "' AND ltrim(rtrim(nombre)) like '" + localidad.Trim() + "' ").ToString() <> "0"
    End Function

    Private Function esCaracteristicaTelValida(ByVal tel As [String], ByVal provid As [String]) As [Boolean]
        Dim telFiltrado As [String] = tel.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "")
        If (telFiltrado.StartsWith("11") OrElse telFiltrado.StartsWith("011")) AndAlso (Not "902.901".Contains(provid.Trim())) Then
            Return False
        End If
        Return True
    End Function
    Private Function esCaracteristicaTelValida2(ByVal tel As [String], ByVal provid As [String]) As [Boolean]
        Dim telFiltrado As [String] = tel.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "")
        If Not (telFiltrado.StartsWith("11") OrElse telFiltrado.StartsWith("011")) AndAlso ("901" = provid.Trim()) Then
            Return False
        End If
        Return True
    End Function

    Private Function esTelefonoValido(ByVal tel As String) As Boolean
        If tel.Trim() = "" Then Return True
        Dim telFiltrado As String = tel.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "")
        If telFiltrado.Length < 10 Then Return False
        For Each c As Char In telFiltrado
            If Not "1234567890".Contains(c.ToString()) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Function validarCampo(ByVal c As [Object], ByVal b As [Boolean]) As [Boolean]
        'resaltar(c, b);
        Return Not b
    End Function

    Protected Sub btnactualiza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnactualiza.Click
        If ddlLocalidad.SelectedValue <> "FALSE" Then
            If ddlLocalidad.SelectedValue <> "-1" Then txtLocalidad.Text = ddlLocalidad.SelectedItem.Text
        End If


        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim datoscli As Long

        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql



        Dim razonSocial, fantasia, domicilio, localidad, provincia, codPostal, telefono, celu, email, obs, documento

        razonSocial = nulearString(txtRazonSocial.Text).Replace("'", "")
        fantasia = nulearString(txtFantasia.Text)
        domicilio = nulearString(txtDomicilio.Text)
        localidad = nulearString(txtLocalidad.Text)
        provincia = nulearString(ddlProvincia.SelectedValue)
        codPostal = nulearString(txtCodPostal.Text)
        telefono = nulearString(txtTelefono.Text)
        celu = nulearString(txtcel.Text)
        email = nulearString(txtEmail.Text)
        obs = nulearString(txtObservaciones.Text)
        documento = nulearString(txtDocumento.Text)
        Dim msg = ""
        If Not validar(msg) Then
            lblMensajePanel.Text = "NO SE PUDO ACTUALIZAR EL CLIENTE: <br>" & msg
            lblError.Text = ""
        Exit Sub

        Else : lblMensajePanel.Text = ""

        End If


        query.CommandText = "UPDATE ViewCliente " & _
                                    " SET RazonSocial ='" & razonSocial & "', " & _
                                    " NombreFantasia =" & fantasia & ", Domicilio =" & domicilio & ", Localidad = " & localidad & "," & _
                                    " provinciaid = " & ddlProvincia.SelectedValue & ", Zip = " & codPostal & " , Telefono1 = " & telefono & " , " & _
                                    " Telefono2 = " & celu & " , telefono3 = " & email & ", observaciones = " & obs & " , documento = " & documento & " " & _
                                    " WHERE clienteid = '" & txtCodigo.Text & "'"

        'query.CommandText = "UPDATE ViewCliente " & _
        '                            " SET RazonSocial ='" & txtRazonSocial.Text & "' , " & _
        '                            " NombreFantasia = '" & txtFantasia.Text & "' , Domicilio = '" & txtDomicilio.Text & "', Localidad = '" & txtLocalidad.Text & "'," & _
        '                            " provinciaid = " & ddlProvincia.SelectedValue & ", Zip = '" & txtCodPostal.Text & "' , Telefono1 = '" & txtTelefono.Text & "' , " & _
        '                            " Telefono2 = '" & txtcel.Text & "' , telefono3 = '" & txtEmail.Text & "', observaciones = '" & txtObservaciones.Text & "' , documento = '" & txtDocumento.Text & "' " & _
        '                            " WHERE clienteid = " & txtCodigo.Text & ""

        datoscli = query.ExecuteNonQuery()
        If datoscli <= 0 Then
            ' hay error}
            lblError.Text = "No se pudo modificar el cliente "

        Else
            ' datos cli = cantidad de registros

            lblError.ForeColor = Drawing.Color.Green


            lblError.Text = "Se actualizo correctamente el cliente "

        End If

        sql.Close()
    End Sub

    Protected Sub ddlProvincia_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProvincia.DataBound
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim datoscli As IDataReader
        ddlCategoriaIva.Enabled = False

        Dim Clienteid As String

        Clienteid = Request.QueryString("codcte").Trim
        txtCodigo.Text = Request.QueryString("codcte").ToString.Trim



        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql

        query.CommandText = "SELECT Top 1 CategoriaIvaId,RazonSocial,NombreFantasia, Domicilio, Localidad, provinciaid," & _
                            " Zip, Telefono1, Telefono2, telefono3, observaciones,documento " & _
                            " FROM ViewCliente(nolock) " & _
                            " WHERE(FechaBaja Is NULL And clienteid = '" & Clienteid & "')"


        datoscli = query.ExecuteReader
        datoscli.Read()


        If Not datoscli.IsDBNull(1) Then txtRazonSocial.Text = datoscli(1)
        If Not datoscli.IsDBNull(2) Then txtFantasia.Text = datoscli(2)
        If Not datoscli.IsDBNull(3) Then txtDomicilio.Text = datoscli(3)
        If Not datoscli.IsDBNull(4) Then txtLocalidad.Text = datoscli(4)
        If Not datoscli.IsDBNull(6) Then txtCodPostal.Text = datoscli(6)
        If Not datoscli.IsDBNull(7) Then txtTelefono.Text = datoscli(7)
        If Not datoscli.IsDBNull(8) Then txtcel.Text = datoscli(8)
        If Not datoscli.IsDBNull(9) Then txtEmail.Text = datoscli(9)
        If Not datoscli.IsDBNull(10) Then txtObservaciones.Text = datoscli(10)
        If Not datoscli.IsDBNull(11) Then txtDocumento.Text = datoscli(11)
        If Not datoscli.IsDBNull(5) Then SelectInCB(datoscli(5), ddlProvincia) 'ddlProvincia.SelectedValue = datoscli(5)
        ''If Not datoscli.IsDBNull(0) Then ddlCategoriaIva.SelectedValue = datoscli(0)
        cargarPartidos()
        cargarLocalidades()
        datoscli.Close()
        sql.Close()
    End Sub

    Protected Sub ddlPartido_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPartido.SelectedIndexChanged
        cargarLocalidades()
    End Sub

    Protected Sub ddlProvincia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProvincia.SelectedIndexChanged
        cargarPartidos()
        cargarLocalidades()
    End Sub

    Protected Sub ddlLocalidad_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocalidad.SelectedIndexChanged
        txtLocalidad.ReadOnly = True
        txtLocalidad.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC4")
        If ddlProvincia.SelectedValue = "902" Then seleccionarPartido()
    End Sub

    Protected Sub btInfoTelOk_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btInfoTelOk.Click
        txtDomicilio.Text = lblDomicilio.Text
        txtCodPostal.Text = lblCP.Text
        txtLocalidad.Text = ""
        For Each i As ListItem In ddlProvincia.Items
            If i.Text.Trim Like lblProvincia.Text.Trim Then ddlProvincia.SelectedValue = i.Value
        Next
        ddlProvincia_SelectedIndexChanged(Me, New EventArgs())
        If lblLocalidad.Text.Trim() <> "" Then
            ddlLocalidad.SelectedValue = valorLocalidad(lblLocalidad.Text)
        End If
        panelDatosSugeridos.Visible = False
    End Sub

    Private Function valorLocalidad(ByVal localidad As String) As String
        For Each itm As ListItem In ddlLocalidad.Items
            If itm.Text.Trim Like localidad.Trim Then
                Return itm.Value
            End If
        Next
        Return "-1"
    End Function

    Protected Sub btInfoTelOk0_Click(ByVal sender As Object, ByVal e As EventArgs)
        panelDatosSugeridos.Visible = False
    End Sub

    Protected Sub txtTelefono_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImageButton1.Click
        If txtTelefono.Text.Replace("(", "").Replace(")", "").Replace("-", "").Trim().Length < 10 Then
            lblErrTel.Text = "El valor debe ser de 10 digitos, incluyendo el código de area."
            panelDatosSugeridos.Visible = False
            Return
        Else
            lblErrTel.Text = ""
        End If
        Dim data As New InfoTelData()
        If InfoTel.buscar(txtTelefono.Text, data) Then
            lblCP.Text = data.CP
            lblDomicilio.Text = data.Direccion
            lblLocalidad.Text = data.Localidad
            lblProvincia.Text = data.Provincia
            panelDatosSugeridos.Visible = True
            pnlNoHayDatos.Visible = False
        Else
            panelDatosSugeridos.Visible = False
            pnlNoHayDatos.Visible = True
        End If
    End Sub

    Private Function soloLetras(ByVal texto As String) As Boolean
        For Each ch As Char In texto
            If "0123456789".Contains(ch.ToString()) Then
                Return False
            End If
        Next
        Return True
    End Function

End Class
