﻿Imports Microsoft.Reporting.WebForms

Partial Class printCoach
    Inherits System.Web.UI.Page
    Private Sub Export(ByRef rviewer As ReportViewer)
        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim extension As String = Nothing
        Dim bytes As Byte()
        bytes = rviewer.LocalReport.Render("PDF", _
          Nothing, mimeType, _
            encoding, extension, streamids, warnings)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Disposition", "inline;filename=[" & Request.QueryString("id") & "]_Informe_Salida_" + DateTime.Now.ToString("dd-MM-yyyy_HH.mm") + ".pdf")
        Response.Charset = "UTF-8"
        Response.BinaryWrite(bytes)
        Response.[End]()
    End Sub

    Protected Sub Page_SaveStateComplete(sender As Object, e As EventArgs) Handles Me.SaveStateComplete
        Export(ReportViewer1)
    End Sub
End Class
