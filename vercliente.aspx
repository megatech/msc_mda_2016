﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="vercliente.aspx.vb" Inherits="vercliente" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target=_self />
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 35%;
        }
        .style2
        {
            width: 15%;
        }
        .style3
        {
            width: 10%;
        }
    </style>
</head>
<body>
<script type="text/javascript">
    $(document).ready(function () {
        $.unblockUI();
        $('.btnSubmit').click(function () {
            $.blockUI({ message: '<h1><img src="Images/busy.gif" /> Procesando...</h1>' });
        });
    }); 
</script> 
    <form id="form1" runat="server">
    <div>
     <div style="text-align: center">
            <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 100%; border-bottom: black 1px solid">
                <tr>
                    <td align="center">
                            <table cellpadding="2" cellspacing="0" style="width: 100%" border="0" >
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Código</td>
                                    <td align="left" class="style1">
                                        <asp:TextBox ID="txtCodigo" runat="server" ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Razón Social</td>
                                    <td align="left" class="style1">
                                        <asp:TextBox ID="txtRazonSocial" runat="server" Width="74%" MaxLength="60"></asp:TextBox>
                                        <asp:Label ID="lblV12" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*"></asp:Label>
                                        <td></td><td> <asp:Label ID="lblError" runat="server" CssClass="labelError"></asp:Label></td>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Fantasía</td>
                                    <td align="left" class="style1">
                                        <asp:TextBox ID="txtFantasia" runat="server" Width="74%" MaxLength="40"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Categoría IVA</td>
                                    <td align="left" class="style1">
                                        <asp:DropDownList ID="ddlCategoriaIva" runat="server" 
                                        DataSourceID="odsCategoriaIva" DataTextField="Nombre" DataValueField="Codigo">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsCategoriaIva" Runat="server" 
                                        TypeName="App_Code.TicketsLib.TablasCombo"
                                        SelectMethod="getComboCategoriaIVA" />
                                        
                                        <td align="right" style="width: 5%">
                                        Documento</td>
                                    <td align="left" style="width: 21%">
                                        <asp:TextBox ID="txtDocumento" runat="server" MaxLength="15" Width="169px"></asp:TextBox>
                                        <asp:Label ID="lblV5" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*" CssClass="val2"></asp:Label>
                                    </td>

                                    </td>
                                </tr>
                            </table>
                            <asp:Panel runat=server ID=panelDatosSugeridos Visible=false>
                             <table width="100%" style="border-style: none; border-collapse: collapse; border-spacing: 0px; background-color: #D3E1F1;">
                                <tr><td colspan=2 align="center" 
                                        style="font-weight: bold; background-color: #395271; color: #FFFFFF;">Datos sugeridos por el sistema</td></tr>
                                <tr><td>&nbsp;<asp:Label runat=server ID=lblDomicilio0 Font-Bold="True" 
                                        ForeColor="#333333">Domicilio:</asp:Label>&nbsp;<asp:Label ID="lblDomicilio" 
                                        runat="server" Font-Bold="True" ForeColor="#395271"></asp:Label>
                                    </td><td>
                                        <asp:Label ID="lblCP0" runat="server" Font-Bold="True" ForeColor="#333333">Código Postal</asp:Label>
&nbsp;<asp:Label runat=server ID=lblCP Font-Bold="True" ForeColor="#395271"></asp:Label></td></tr>
                                <tr><td>
                                    <asp:Label ID="lblProvincia0" runat="server" Font-Bold="True" 
                                        ForeColor="#333333">Provincia:</asp:Label>
                                    &nbsp;<asp:Label runat=server ID=lblProvincia Font-Bold="True" ForeColor="#395271"></asp:Label></td><td>
                                        <asp:Label ID="lblLocalidad0" runat="server" Font-Bold="True" 
                                            ForeColor="#333333">Localidad:</asp:Label>
                                        <asp:Label runat=server ID=lblLocalidad Font-Bold="True" ForeColor="#395271"></asp:Label></td></tr>
                                <tr><td colspan=2 align="center" 
                                        style="color: #FFFFFF; font-weight: bold; background-color: #993333;">
                                    La información sugerida puede contener errores. Por favor, verifique los datos 
                                    con el cliente.<br /> Advertencia: esta acción reemplazará los valores actuales.</td></tr>
                                 <tr>
                                     <td align="center" colspan="2" style="color: #800000; font-weight: bold;">
                                         <asp:Button ID="btInfoTelOk" runat="server" Height="23px" 
                                             onclick="btInfoTelOk_Click" Text="Utilizar estos datos" 
                                             CssClass="btnSubmit" />
                                         &nbsp;
                                         <asp:Button ID="btInfoTelOk0" runat="server" Height="23px" 
                                             onclick="btInfoTelOk0_Click" Text="Cancelar" />
                                         <br />
                                     </td>
                                 </tr>
                             </table>
                            </asp:Panel>
                            <asp:Panel runat=server ID=pnlNoHayDatos Visible="False"> 
                            <table width="100%" style="border-style: none; border-collapse: collapse; border-spacing: 0px; background-color: #D3E1F1;">
                            <tr><td colspan=2 align="center" 
                                        style="font-weight: bold; background-color: #395271; color: #FFFFFF;">Datos sugeridos por el sistema</td></tr>
                            <tr><td style="font-weight: bold; color: #293747; background-color: #D3E1F1">No se han encontrado datos sobre el teléfono ingresado.</td></tr>
                            </table></asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" >
                                <tr>
                                    <td align="right" class="style3">
                                        Provincia</td>
                                    <td align="left" class="style2" nowrap="nowrap">
                                        <asp:DropDownList ID="ddlProvincia" runat="server" 
                                        DataSourceID="odsProvincia" DataTextField="Nombre" DataValueField="Codigo" 
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblV2" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*"></asp:Label>
                                        <asp:ObjectDataSource ID="odsProvincia" Runat="server" 
                                        TypeName="App_Code.TicketsLib.TablasCombo"
                                        SelectMethod="getComboProvincia" />
                                    </td>
                                    <td align="right" width="15%">
                                        <asp:Label ID="lblPartido" runat="server" Text="Partido"></asp:Label>
                                    </td>
                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlPartido" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="style3">
                                        Localidad</td>
                                    <td align="left" class="style2">
                                        <asp:TextBox ID="txtLocalidad" runat="server" Width="60%" MaxLength="40" 
                                            ReadOnly="True" BackColor="#FFFFCC"></asp:TextBox>
                                        <asp:Label ID="lblV8" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*" CssClass="val2"></asp:Label>
                                    </td>
                                    <td align="right">
                                        Nueva Localidad</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLocalidad" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>   
                                <tr>
                                    <td align="right" class="style3">
                                        Domicilio</td>
                                    <td align="left" class="style2" nowrap="nowrap">
                                        <asp:TextBox ID="txtDomicilio" runat="server" Width="60%" MaxLength="40"></asp:TextBox>
                                        <asp:Label ID="lblV9" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*" CssClass="val2"></asp:Label>
                                    </td>
                                    <td align="right" width="15%">
                                        Cod. Postal</td>
                                    <td align="left" width="25%">
                                        <asp:TextBox ID="txtCodPostal" runat="server" MaxLength="8"></asp:TextBox>
                                        <asp:Label ID="lblV10" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*" CssClass="val2"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="style3">
                                        Teléfono</td>
                                    <td align="left" class="style2" valign="middle">
                                        <table width=100%><tr><td width="120px">
                                            <asp:TextBox ID="txtTelefono" runat="server" MaxLength="30" 
                                            Width="120px" ReadOnly="True"                               
                                            ></asp:TextBox><td align="left" width="25px">
                                                <asp:ImageButton ID="ImageButton1" runat="server" 
                                            ImageUrl="~/Images/lupa.jpg" Width="22px" 
                                                    
                                                    ToolTip="OBTENER DATOS DE LA LINEA TELEFONICA" CssClass="btnSubmit" /></td><td><asp:Label ID="lblV0" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*"></asp:Label></td></tr></table> 
                                            
                                        <asp:Label ID="lblErrTel" runat="server" ForeColor="Maroon"></asp:Label>
                                           
                                            </td>
                                    <td align="right" width="15%">
                                        Celular                                     <asp:TextBox ID="txtcel" runat="server" MaxLength="30" Width="70%"></asp:TextBox>
                                        <asp:Label ID="lblV1" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*"></asp:Label>
                                    </td>

                                </tr>  
                                <tr>
                                    <td align="right" class="style3">
                                        e-Mail</td>
                                    <td align="left"colspan="3">
                                        <asp:TextBox ID="txtEmail" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                                        
                                        <asp:Label ID="lblV11" runat="server" Font-Bold="True" Font-Size="15pt" 
                                            ForeColor="Red" Text="*" CssClass="val2"></asp:Label>
                                        
                                        </td>
                                </tr>                                    
                                <tr>
                                    <td align="right" class="style3">
                                        Observaciones</td>
                                    <td align="left"colspan="3">
                                        <asp:TextBox ID="txtObservaciones" runat="server" Width="80%" MaxLength="100" Rows="5" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>                                                                                              
                            </table>
                    </td>
                </tr>
                 <tr>
                 <td>
                 
                     <asp:Button ID="btnactualiza" runat="server" Text="Actualizar" 
                         CssClass="btnSubmit" />
                 
                 </td>
                 </tr>
            
              
                 <tr>
                    <td align="left" >
                    <asp:Label ID="lblMensajePanel" runat="server" CssClass="labelError"></asp:Label>
                     </td>
                        
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelMensaje" runat="server">
        <table>
            <tr>
                <td align="left" >
                    &nbsp;</td>        
            </tr>
        </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
