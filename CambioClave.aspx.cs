using System;
using System.Configuration;
using App_Code.TicketsLib;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMensajeError.Text = "";
        if (!this.IsPostBack)
        {
            lblUsuario.Text = Request.QueryString["usr"];
            if (lblUsuario.Text.Trim().Length == 0) {
                lblMensajeError.Text = "Usuario inválido para cambiar de clave.";
            }
        }

    }
    protected void btnIngresar_Click(object sender, EventArgs e)
    {
        string connStr = ConfigurationManager.ConnectionStrings["TicketDB"].ConnectionString;
        int nCodePage = int.Parse(Config.getValue("Codepage"));
        Usuario u = new Usuario(connStr,nCodePage);

        try
        {
            u.Nick = this.lblUsuario.Text;
            u.Clave = this.txtClave.Text;
            bool ok = u.checkNick();
            if (ok)
            {
                if (txtNuevaClave.Text.Trim().Equals(txtReClave.Text.Trim()))
                {
                    ok = u.cambioClave(this.txtNuevaClave.Text.Trim() );
                    Server.Transfer("Default.aspx?usr=" + u.Nick+ "&msg=Ya puede ingresar con la nueva clave.");
                }
                else
                {
                    lblMensajeError.Text = "No coinciden la nueva clave con la repetición";
                }
                
            }
            else
            {
                lblMensajeError.Text = "La clave o el usuario son erróneos";
            }
        }
        catch (CustomExceptions.UserLoginException ex)
        {
            lblMensajeError.Text = ex.Message;
            
        }
 
    }
    
}
