﻿Imports App_Code.TicketsLib
Imports System.Data.SqlClient

Partial Class verTktsEnProceso
    Inherits System.Web.UI.Page
    Public WithEvents tmr As New Timer
    Dim neg As String = ""
    Dim filtro As String

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridTktsEnProc.RowCommand
        If e.CommandName = "ver" Then
            lblSel.Text = gridTktsEnProc.Rows(e.CommandArgument).Cells(0).Text & "|" & gridTktsEnProc.Rows(e.CommandArgument).Cells(3).Text
            Label1.Text = gridTktsEnProc.Rows(e.CommandArgument).Cells(1).Text
            actualizarTablaEspecialista()
        End If
    End Sub

    Public Sub actualizarTablaEspecialista()
        dsTkts.SelectCommand =
            "SELECT tk.ticketId Ticket, est.nombre as estado, tk.ClainNro 'Clain', tp.nombre 'Tipo de Problema', rtrim(cast (Problema as varchar(MAX))) Detalle, convert(varchar, tk.fecha, 103)+' '+convert(varchar, tk.fecha, 108) 'Creación', datediff(second,tk.fecha, getDate()) minutos " & _
            "FROM Ticket tk inner join estado est on est.estadoId=tk.estadoId inner join tipoproblema tp on tk.IdTipoProblema= tp.IdTipoProblema inner join viewLider vl on vl.especialistaid = tk.especialistaid " & _
            " left join especialista e on tk.especialistaId=e.especialistaId left join especialistaUsuario eu on eu.especialistaId=tk.especialistaid  left join usuario u on eu.usuarioId=u.usuarioId " & _
            "Where tk.estadoId in (115,143) " & filtro & neg & " and e.especialistaId=" & lblSel.Text.Split("|")(0) & "order by tk.estadoId"
        gvDetalleTkts.DataBind()

    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDetalleTkts.RowCommand
        If e.CommandName = "ver" Then
            Response.Redirect("editaTicket.aspx?id=" & gvDetalleTkts.Rows(e.CommandArgument).Cells(1).Text)
        End If
    End Sub

    Protected Sub GridView2_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetalleTkts.DataBound
        For Each r As TableRow In gvDetalleTkts.Rows
            Dim dbl = Double.Parse(r.Cells(7).Text)
            r.Cells(7).Text = TimeSpan.FromSeconds(dbl).ToString()
        Next
        Panel1.Visible = gvDetalleTkts.Rows.Count > 0
    End Sub

    Protected Sub GridView1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridTktsEnProc.DataBound
        txtBusqueda_TextChanged(Me, New EventArgs())
        lblTotal.Text = Funciones.getScalar("SELECT count(*) FROM ticket inner join viewLider vl on vl.especialistaid = ticket.especialistaid left join especialista e on ticket.especialistaId=e.especialistaId left join especialistaUsuario eu on eu.especialistaId=ticket.especialistaid  left join usuario u on eu.usuarioId=u.usuarioId " & _
                                            "WHERE estadoId in (115,143) " & filtro & "")
    End Sub

    Protected Sub txtBusqueda_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        For Each r As TableRow In gridTktsEnProc.Rows
            r.Visible = r.Cells(1).Text.ToUpper Like "*" & txtBusqueda.Text.Trim.ToUpper & "*"
        Next
        If Not (Label1.Text.ToUpper Like "*" & txtBusqueda.Text.Trim.ToUpper & "*") Then Button1_Click(Me, New EventArgs())
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        gridTktsEnProc.DataBind()
        gvDetalleTkts.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If ((ddFiltro.SelectedValue = "") Or (ddFiltro.SelectedValue.Contains("Selec"))) Then

            filtro = ""
        Else
            filtro = " and vl.liderid = " & ddFiltro.SelectedValue

        End If

        If IsPostBack Then



            If (ddlneg.SelectedValue <> "0") Then

                neg = " and articuloid = " & ddlneg.SelectedValue

                aplicarFiltro()



            Else
                neg = ""
            End If





            If lblSel.Text <> "" Then

                actualizarTablaEspecialista()

            End If
        End If


    End Sub

    Private Sub aplicarFiltro()
        dsGridView.SelectCommand =
            "select e.especialistaID ID, max(ArticuloId), min(e.nombre) Especialista, count(*) 'Tickets en proceso',eq.nombre segmento " & _
            "from ticket tk INNER JOIN especialista e ON e.especialistaID=tk.especialistaID " & _
            "inner join equipo eq on eq.equipoId=tk.articuloID inner join viewLider vl on vl.especialistaid = tk.especialistaid " & _
            "left join especialistaUsuario eu on e.especialistaId=eu.especialistaId " & _
            "left join usuario u on eu.usuarioId=u.usuarioId " & _
            "where estadoId in (115,143) " & filtro & neg & _
            " group by eq.nombre,e.especialistaId " & _
            "order by eq.nombre,count(*) DESC"

        gridTktsEnProc.DataBind()
    End Sub

    Protected Sub ddFiltro_DataBound(sender As Object, e As EventArgs) Handles ddFiltro.DataBound
        ddFiltro.Items.Add("Seleccione un valor")
        ddFiltro.SelectedIndex = ddFiltro.Items.Count - 1

    End Sub

    Protected Sub ddFiltro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddFiltro.SelectedIndexChanged
        aplicarFiltro()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        lblSel.Text = "0|0"
        actualizarTablaEspecialista()
    End Sub

    'Private Sub actualizatablaesp()
    '    dsGridView.SelectCommand = "select e.especialistaID ID, max(ArticuloId), min(e.nombre) Especialista, count(*) 'Tickets en proceso'," & _
    '   " eq.nombre segmento from ticket tk INNER JOIN especialista e ON e.especialistaID=tk.especialistaID " & _
    '    " inner join equipo eq on eq.equipoId=articuloId where tk.estadoId = 115 " & neg & _
    '    "group by eq.nombre,e.especialistaId order by eq.nombre,count(*) DESC"""

    '    dsGridView.DataBind()


    'End Sub

End Class
