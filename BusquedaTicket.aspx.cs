using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using App_Code.TicketsLib;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Popups;
public partial class BusquedaTicket : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        
        
        if (retCliente.Value != "")
        {
            txtCodCliente.Text = PopupMgr.LeerDato(retCliente, "clienteId");
            txtNomCliente.Text = PopupMgr.LeerDato(retCliente, "razonSocial", true);
           
        }
      
        lblError.Text = "";
        if (!IsPostBack)
        {
            Usuario usr = (Usuario)Session["Usuario"];
            if (usr == null)
            {
                Response.Redirect("Default.aspx");
            }

            //txtFechaInicio.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
            //txtFechaFin.Text = DateTime.Now.ToString("dd/MM/yyyy");
            Page.Master.Page.Form.DefaultButton = btnbuscar.UniqueID;
            Telerik.Web.UI.RadComboBoxItem itm = new Telerik.Web.UI.RadComboBoxItem();
            itm.Text = " ";
            txtProblema.Items.Add(itm);
        }


        



        //loadScripts();
    }

    #region MetodosPrivados
    private void loadScripts()
    {


        if (!ClientScript.IsClientScriptBlockRegistered("dlgBusqueda"))
        {
            string title = "Busqueda Cliente";
            string campoid = this.txtCodCliente.ClientID;
            string campodescrip = this.txtNomCliente.ClientID;
            string parameters = "&campoid=" + campoid + "&campodescrip=" + campodescrip;
            string height = "420";
            string width = "560";
            string page = "busquedaCliente.aspx";

            // use next line for direct with <base target="_self"> between <Head> and </HEAD> 
            string scrp = "<script>var rc = new Array(0,0);" +
                   "function doIt(){rc=window.showModalDialog('" + page + "?Title=" + title + "&page=" + page +
                   "&campoid=" + campoid + "&campodescrip=" + campodescrip + "','','dialogHeight:" + height +
                   " px;dialogWidth:" + width + " px;');" + "\n";

            scrp += "if(rc[0]!=null){document.getElementById('" + txtCodCliente.ClientID + "').innerText=rc[0];" +
                "document.getElementById('" + txtNomCliente.ClientID + "').innerText=rc[1];" +
                "__doPostBack('','');}}</script>";

            //Page.RegisterStartupScript("dlgBusqueda", scrp);
            ClientScript.RegisterStartupScript(this.GetType(), "dlgBusqueda", scrp);
            this.imgBuscar.Attributes.Add("onClick", "doIt();");
         
        }


    }

    private void ejecutarConsulta()
    {
        BusquedaTicketFiltro bf = (BusquedaTicketFiltro)Session["filtro"];
        if (bf == null)
        {
            lblError.Text = "Debe cargar alg�n filtro.";
        }
        else
        {
            String ds = App_Code.TicketsLib.BusquedaTicket.getStrResultado(bf);
            dsGridResultados.SelectCommand = ds;
            gvBusqueda.DataBind();
        }
    }
    private BusquedaTicketFiltro getFiltro()
    {
        Usuario usr = (Usuario)Session["Usuario"];
        BusquedaTicketFiltro bf = new BusquedaTicketFiltro();
        bool bOk = false;
        if (txtFechaInicio.Text.Trim().Equals(""))
        {
            bf.FechaInicio = DateTime.Today.AddYears(-100);
        }
        else
        {
            try
            {
                
                if (usr.Perfil == "23")
                {
                    bf.FechaInicio = DateTime.Parse(txtFechaInicio.Text);
                    if (bf.FechaInicio < Convert.ToDateTime("29/07/2015"))
                    {
                        bf.FechaInicio = DateTime.Parse("29/07/2015");
                        bOk = true;
        

                    }
                    else
                    {
                        bf.FechaInicio = DateTime.Parse(txtFechaInicio.Text);
                        bOk = true;
                    }

                }
                else
                {
                    bf.FechaInicio = DateTime.Parse(txtFechaInicio.Text);
                    bOk = true;
                }
            }
            catch
            {
                bf.FechaInicio = DateTime.Today.AddYears(-100); //DateTime.MinValue;
            }
        }
        if (txtFechaFin.Text.Trim().Equals(""))
        {
            bf.FechaFin = DateTime.Today.AddYears(100); ;
        }
        else
        {
            try
            {
                if (usr.Perfil == "23")
                {
                    bf.FechaFin = DateTime.Parse(txtFechaFin.Text);
                    if (bf.FechaFin >= Convert.ToDateTime("29/07/2015"))
                    {
                        //bf.FechaFin
                        bOk = true;
                    }
                    else
                    {
                        //bf.FechaFin = DateTime.Parse(txtFechaFin.Text);
                        bf.FechaFin = DateTime.Now.AddYears(-100);
                        bOk = true;
                    }

                }
                else
                {
                    bf.FechaFin = DateTime.Parse(txtFechaFin.Text);
                    bOk = true;
                }
               
               
            }
            catch
            {
                bf.FechaFin = DateTime.Today.AddYears(100);
            }
        }

        bf.NroClaim = txtNroClaim.Text.Trim();
        bf.NroTicket = txtTicket.Text.Trim();
        bf.Negocio = txtNegocio.Text.Trim();
        bf.CodigoCliente = txtCodCliente.Text.Trim();
        bf.RazonSocial = txtNomCliente.Text.Trim();
        if (ddEquipo.SelectedValue != "-1") { bf.CodigoEquipo = ddEquipo.SelectedValue; }
        else { bf.CodigoEquipo = ""; }
        bf.NroSerie = txtNroSerie.Text.Trim();
        bf.Especialista = txtEspecialista.Text.Trim();
        if (rcLider.SelectedValue != "")
        {
            bf.Lider = rcLider.SelectedValue;
        }
        else { bf.Lider = ""; }

        if (ddTecnico.SelectedValue == "-1")
        {
            bf.Tecnicoid = "";
        }
        else
        {
            bf.Tecnicoid = ddTecnico.SelectedValue;
        }

        if (txtEstado.SelectedValue == "-1")
        {
            bf.Estado = "";
        }
        else
        {
            bf.Estado = txtEstado.SelectedValue;
        }


        if (txtProblema.SelectedValue == "-1")
        {
            bf.TipoProblema = "";
        }
        else
        {
            bf.TipoProblema = txtProblema.SelectedValue;
        }

        bf.Usuario = ((Usuario)Session["Usuario"]).Nick;
        if (!dpFechaRecontactar.IsEmpty)
        {
            bf.fechaRecontactar = dpFechaRecontactar.SelectedDate.Value;
            bOk = true;
        }
        if (!tpHoraRecontactar.IsEmpty)
        {
            bf.horaRecontactar = tpHoraRecontactar.SelectedDate.Value;
            bOk = true;
        }
        if (ddMotivoRecontacto.SelectedValue != "0") 
        {
            bf.idMotivoRecontacto = ddMotivoRecontacto.SelectedValue;
            bOk = true;
        }

        if (!bOk)
        {
            int q = bf.NroClaim.Length + bf.NroTicket.Length + bf.Negocio.Length + bf.CodigoCliente.Length + bf.RazonSocial.Length +
                    bf.CodigoEquipo.Length + bf.NroSerie.Length + bf.Especialista.Length + bf.Estado.Length +
                    bf.TipoProblema.Length + bf.Lider.Length ;
            bOk = (q > 0) ;
        }

        return !bOk ? null : bf;
    }
    #endregion
    
    #region EventosControles
    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        if (rcEspecialista.SelectedValue != null) { txtEspecialista.Text = rcEspecialista.SelectedValue; }
        if (rcNegocio.SelectedValue != null) { txtNegocio.Text = rcNegocio.SelectedValue; }
        if (rdpHasta.SelectedDate != null) { txtFechaFin.Text = rdpHasta.SelectedDate.ToString(); }
        if (rdpDesde.SelectedDate != null) { txtFechaInicio.Text = rdpDesde.SelectedDate.ToString(); }
        if (Session["filtro"]== null) {Session.Add("filtro", getFiltro());} else {Session["filtro"] = getFiltro();}
        ejecutarConsulta();
    }

    protected void btnSelect_Click(object sender, EventArgs e)
    {
        ;
    }

    protected void txtProblema_DataBound(object sender, EventArgs e)
    {
        Telerik.Web.UI.RadComboBoxItem vacio = new Telerik.Web.UI.RadComboBoxItem();
        vacio.Text = "Seleccione un valor";
        vacio.Value = "";
        vacio.Selected = true;
        txtProblema.Items.Add(vacio);
    }

    protected void txtestado_DataBound(object sender, EventArgs e)
    {
        Telerik.Web.UI.RadComboBoxItem vacio = new Telerik.Web.UI.RadComboBoxItem();
        vacio.Text = "Seleccione un valor";
        vacio.Value = "";
        vacio.Selected = true;
        txtEstado.Items.Add(vacio);
    }


    protected void gvBusqueda_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblFecha = (Label)e.Row.FindControl("lblUltimaModif");
            //Label lblTicketNro = (Label)e.Row.FindControl("lblTicketNro");
            e.Row.Cells[10].ToolTip = e.Row.Cells[10].Text;
            e.Row.Cells[11].ToolTip = e.Row.Cells[11].Text;
           // e.Row.Cells[12].Text = e.Row.Cells[10].Text.Substring(0, 10);

            ImageButton btnSelect = (ImageButton)e.Row.FindControl("btnSelect");
            btnSelect.PostBackUrl = "ConsultaTicket.aspx?id=" + e.Row.Cells[2].Text.Trim();
            btnSelect.ToolTip = "Consulta";

            //ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
            //btnEdit.PostBackUrl = "EditaTicket.aspx?id=" + e.Row.Cells[2].Text.Trim();
           // btnEdit.ToolTip = "Modificaci�n";
            Usuario usr = (Usuario)Session["Usuario"];
            if (usr.Perfil == "23")
            {
            HyperLink btnEdit = (HyperLink)e.Row.FindControl("btnEdit");
            btnEdit.Visible=false;
            }

            
             }
        
    }
    protected void gvBusqueda_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBusqueda.PageIndex = e.NewPageIndex;
        ejecutarConsulta();
    }



    protected void gvBusqueda_Sorting(object sender, GridViewSortEventArgs e)
    {
        ejecutarConsulta();

    }

    #endregion

    protected void rdpDesde_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
    {
        txtFechaInicio.Text = rdpDesde.SelectedDate.ToString();

    }
    protected void rdpHasta_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
    {
        txtFechaFin.Text = rdpHasta.SelectedDate.ToString();
    }
    protected void rcNegocio_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        txtNegocio.Text = rcNegocio.SelectedValue;
    }
    protected void rcEspecialista_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        txtEspecialista.Text = rcEspecialista.SelectedValue;
    }

    protected void rcEspecialista_DataBound(object sender, EventArgs e)
    {
        Telerik.Web.UI.RadComboBoxItem vacio = new Telerik.Web.UI.RadComboBoxItem();
        vacio.Text = "Seleccione un valor";
        vacio.Value = "";
        vacio.Selected = true;
        rcEspecialista.Items.Add(vacio);
    }
    protected void rcNegocio_DataBound(object sender, EventArgs e)
    {
        
            Usuario usu = (Usuario)Session["Usuario"];
            if (usu.Perfil == "23")
            {
                
                dsNegocio.SelectCommand = "SELECT [EmpresaPartnerId], [Nombre] FROM [empresapartner] WHERE ([FechaBaja] IS NULL) and ([EmpresaPartnerId] in (54,55,56,59,60))  ORDER BY Nombre";
                DSEstados.SelectCommand = "select '[' + CAST(EstadoId as varchar) + '] '+ Estado.Nombre As CodNombre, Estado.Nombre from Estado  where Estado.EstadoId not in(135,117,123,125,126,144,137,138,139,140,142,147) ORDER BY Estado.Nombre ";
                DSEstados.DataBind();
                 
                
                
            }
            if (rcNegocio.Items.Contains(rcNegocio.FindItemByValue("")))
            { 
            
            }
            else
            { 
            Telerik.Web.UI.RadComboBoxItem vacio = new Telerik.Web.UI.RadComboBoxItem();
            vacio.Text = "Seleccione un valor";
            vacio.Value = "";
            vacio.Selected = true;
            rcNegocio.Items.Add(vacio);
            }
        
       

    }

    private void exportarAExcel(string titulo, GridView gv) {

        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Page pagina = new Page();
        //        pagina.StyleSheetTheme = "~/css/gv.css"
        dynamic form = new HtmlForm();

        gv.AllowSorting = false;
        gv.AllowPaging = false;
        ejecutarConsulta();
        GridView grid = gv;
        gv.Columns[0].Visible = false;
        gv.Columns[1].Visible = false;
        HtmlTable t = new HtmlTable();
        Label lblT = new Label();
        lblT.Text = titulo;
        lblT.Font.Bold = true;
        lblT.Font.Size = 14;
        t.Attributes.Add("class", "tableContent");
        t.Rows.Add(new HtmlTableRow());
        t.Rows.Add(new HtmlTableRow());
        t.Rows[0].Cells.Add(new HtmlTableCell());
        t.Rows[1].Cells.Add(new HtmlTableCell());
        t.Rows[0].Cells[0].Controls.Add(lblT);
        t.Rows[1].Cells[0].Controls.Add(grid);

        grid.HeaderStyle.Font.Underline = false;
        grid.HeaderStyle.ForeColor = System.Drawing.Color.White;
        grid.EnableSortingAndPagingCallbacks = false;

        pagina.EnableEventValidation = false;
        pagina.DesignerInitialize();
        pagina.Controls.Add(form);
        form.Controls.Add(t);
        form.RenderControl(htw);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=ResultadosDeBusqueda_" + DateTime.Now.ToString("dd-MM-yyyy_HH.mm") + ".xls");
        Response.Charset = "UTF-8";
        Response.ContentEncoding = Encoding.Default;
        Response.Write("<p><br><p>");
        Response.Write(sb.ToString());
        Response.End();
    }

    protected void btExportar_Click(object sender, EventArgs e)
    {
        if (gvBusqueda.Rows.Count > 0)
        {
            gvBusqueda.Columns[14].Visible = true;
            gvBusqueda.Columns[16].Visible = true;
            gvBusqueda.DataBind();
            exportarAExcel("Resultados de la busqueda [" + DateTime.Now.ToString() + "]", gvBusqueda);
            gvBusqueda.Columns[14].Visible = false;
            gvBusqueda.Columns[16].Visible = false;
        }
    }
    protected void ddTecnico_DataBound(object sender, EventArgs e)
    {
        RadComboBoxItem vacio = new RadComboBoxItem();
        vacio.Text = "Seleccione un valor";
        vacio.Value = "-1";
        vacio.Selected = true;
        ddTecnico.Items.Add(vacio);
    }
    protected void rdpDesde_Load(object sender, EventArgs e)
    {
        txtFechaInicio.Text = rdpDesde.SelectedDate.ToString();
    }


    protected void rdpHasta_Load(object sender, EventArgs e)
    {
        txtFechaFin.Text = rdpHasta.SelectedDate.ToString();
    }
    protected void gvBusqueda_DataBound(object sender, EventArgs e)
    {
        btExportar.Visible = gvBusqueda.Rows.Count > 0;
    }
    protected void rcLider_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (rcLider.SelectedValue == "") {
            dsEspecialista.SelectCommand = "Select especialistaId as soloid, '['+CAST(especialistaid AS VARCHAR)+'] '+nombre as nombre FROM especialista WHERE fechabaja is null";
        }
        else{
            dsEspecialista.SelectCommand = "Select especialistaId as soloid, '['+CAST(especialistaid AS VARCHAR)+'] '+nombre as nombre FROM especialista WHERE fechabaja is null" +
                                           " AND especialistaId IN (SELECT especialistaId FROM viewLider vl  WHERE vl.liderId="+ rcLider.SelectedValue +")";
        }
        rcEspecialista.DataBind();
    }

    protected void rcLider_DataBound(object sender, EventArgs e)
    {
        Telerik.Web.UI.RadComboBoxItem vacio = new Telerik.Web.UI.RadComboBoxItem();
        vacio.Text = "Seleccione un valor";
        vacio.Value = "";
        vacio.Selected = true;
        rcLider.Items.Add(vacio);
    }
    protected void dsGridResultados_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        lblCount.Text = String.Format("Resultados: {0}", e.AffectedRows);
    }
    
    protected void txtNroClaim_TextChanged(object sender, EventArgs e)
    {
        FiltroCliente fc = new FiltroCliente();
        fc.Telefono1 = txtNroClaim.Text;
        FiltroCliente fco = Clientes.getUnCliente(fc);


        if (fco != null)
        {
            txtCodCliente.Text = fco.ClienteId;
            txtNomCliente.Text = fco.RazonSocial;
            
        }

        else
        {
            //NO EXISTE EL CLIENTE
            lblError.Text = "El cliente especificado no existe";
            lblError.Visible = true;
            txtCodCliente.Text = "";
            txtNomCliente.Text = "";
            return;
        }

    }
   
}
