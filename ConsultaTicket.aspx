<%@ Page Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="ConsultaTicket.aspx.cs" Inherits="ConsultaTicket" %>

<asp:Content ID="TicketPage" ContentPlaceHolderID="cphTicket" runat="server">
    <asp:Panel ID="panelTicket" runat="server">
        <div style="text-align: center">
            <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 100%; border-bottom: black 1px solid">
                <caption align="bottom"><u><b>Visualización de Ticket</b></u></caption>
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" bordercolor="white" bordercolordark="#ffffff" bordercolorlight="#ffffff">
                                <tr>
                                    <td align="right">
                                        Fecha</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFecha" runat="server" ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox></td>
                                        
                                    <td align="right">
                                        F.Inicio</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFechaInicio" runat="server" Width="70px" ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox>&nbsp;
                                        </td>
                                    <td align="right">
                                        F. Fin</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFechaFin" runat="server" Width="70px" ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox>&nbsp;
                                   </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Nro.Claim</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNroClaim" runat="server" ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox></td>
                                    <td align="right">
                                        Ticket</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTicket" runat="server" ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" bordercolor="white" bordercolordark="#ffffff" bordercolorlight="#ffffff">
                                <tr>
                                    <td align="right" width="15%">
                                        Negocio</td>
                                    <td align="left" width="35%">
                                        <asp:Label runat="server" ID="lblNegocio" CssClass="TextoTextBoxReadonly" />                                        
                                    </td>
                                    <td align="right" width="15%">
                                        Cliente</td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="txtCodCliente" runat="server" Width="50px"  ReadOnly="true" CssClass="TextoTextBoxReadonly" ></asp:TextBox>
                                        &nbsp;
                                        <asp:TextBox ID="txtNomCliente" runat="server" Width="220px"   ReadOnly="true" CssClass="TextoTextBoxReadonly"     ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Ubicación</td>
                                    <td align="left">
                                        <asp:Label runat="server" ID="lblUbicacion" CssClass="TextoTextBoxReadonly" />
                                        
                                    </td>
                                    <td align="right">
                                        Contacto</td>
                                    <td align="left">
                                        <asp:Label runat="server" ID="lblContacto" CssClass="TextoTextBoxReadonly" />
                                     </td>
                                </tr>                                
                            </table>
                    </td>
                </tr>
                 
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" bordercolor="white" bordercolordark="#ffffff" bordercolorlight="#ffffff">
                                <tr>
                                    <td align="right" width="15%">
                                        Equipo</td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="txtCodEquipo" runat="server" Width="50px"  ReadOnly="true" CssClass="TextoTextBoxReadonly" ></asp:TextBox>
                                        <asp:TextBox ID="txtNombreEquipo" runat="server" Width="220px"  ReadOnly="true" CssClass="TextoTextBoxReadonly" ></asp:TextBox>
                                    </td>
                                    <td align="right" width="15%">
                                        Nro. Serie</td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="txtSerie" runat="server" Width="70px"   ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Especialista</td>
                                    <td align="left">
                                            <asp:Label runat="server" ID="lblEspecialista" CssClass="TextoTextBoxReadonly" />
                                        
                                    </td>
                                    <td align="right">
                                        Vendedor</td>
                                    <td align="left" width="35%">
                                        <asp:Label runat="server" ID="lblVendedor" CssClass="TextoTextBoxReadonly" 
                                            Enabled="False" EnableTheming="False" EnableViewState="False" Visible="False" />
                                        
                                    </td>
                                </tr>                                
                            </table>
                    </td>
                </tr> 
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" bordercolor="white" bordercolordark="#ffffff" bordercolorlight="#ffffff">
                                <tr>
                                    <td align="right" width="15%">
                                        T.Problema</td>
                                    <td align="left" width="35%">
                                        <asp:Label runat="server" ID="lblTipoProblema" CssClass="TextoTextBoxReadonly" />                                       
                                    </td>
                                    <td align="right" width="15%">
                                        Prioridad</td>
                                    <td align="left" width="35%">
                                        <asp:Label runat="server" ID="lblPrioridad" CssClass="TextoTextBoxReadonly" />
                                        
                                    </td>

                                </tr>
                                <tr>
                                    <td align="right" width="15%">
                                        Estado</td>
                                    <td align="left" width="35%">
                                        <asp:Label runat="server" ID="lblEstado" CssClass="TextoTextBoxReadonly" />
                                        <asp:Label runat="server" ID="lblIdEstadoActual" Visible="false" CssClass="TextoTextBoxReadonly" />
                                    </td>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="10%">
                                        Detalle</td>
                                    <td align="left" colspan="5">
                                        <asp:TextBox ID="txtDetalle" runat="server" Rows="5" TextMode="MultiLine" Width="98%"
                                            ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox>                                    
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>   
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" bordercolor="white" bordercolordark="#ffffff" bordercolorlight="#ffffff">
                                <tr>
                                    <td width="15%" valign="top" align="right">Informe Cliente</td>
                                    <td align="left" valign="top"><asp:TextBox ID="txtInformeCliente" runat="server" Rows="5" TextMode="MultiLine" Width="90%"
                                            ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox>
                                        
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>                           
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" bordercolor="white" bordercolordark="#ffffff" bordercolorlight="#ffffff">
                                <tr>
                                    <td width="15%" valign="top" align="right"><asp:label id="lblInformeInterno" Text=" Informe Interno" runat="server"></asp:label> </td>
                                    <td align="left" valign="top"><asp:TextBox ID="txtInformeInterno" runat="server" Rows="5" TextMode="MultiLine" Width="90%"
                                            ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox>
                                        
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>                           

                 <tr>
                    <td align="left" >
                        <asp:Label ID="lblError" runat="server" CssClass="labelError"></asp:Label>
                        <asp:ValidationSummary ID="validSummary" runat="server" />
                    </td>
                        
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelMensaje" runat="server">
        <table>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblMensajePanel" runat="server" CssClass="labelError"></asp:Label>
                </td>        
            </tr>
        </table>
        <asp:Button ID="Button1" runat="server" Text="Button" />
    </asp:Panel>
</asp:Content>
