﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="cargacoachingNuevo.aspx.vb" Inherits="cargacoaching" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
 
  <div style="width: 100%" align="center"> 
 <h2 align="center">Monitoreo</h2>

     <table runat="server" id="t1" align="center" style="width: 684px" 
     caption="Monitoreo" title="Monitoreo" >
          <tr>
           
                 <td align="right">Operador:
                 </td>
                 <td align="left" colspan="3">
                 <asp:Label ID="lbloper" runat="server" Text=""></asp:Label>
                 <asp:Label ID="lbloperName" runat="server"></asp:Label>
                </td>
          </tr>
          <tr>
                <td align="right">Fecha</td>
                <td align="left"> <asp:Label ID="lblfecha" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">TKT</td>
                <td align="left">  
                   <telerik:RadTextBox ID="txttkt" runat="server" 
                       EmptyMessage="Ingrese el nro. TKT" AutoPostBack="True">
                  </telerik:RadTextBox> <asp:CheckBox ID="Chkexperiencia" Text="Experiencia" 
                       runat="server" AutoPostBack="True" />
                </td>
           </tr>
           <tr>
                <td align="right">
                 SID de Qoom:&nbsp;
                </td>
                <td align="left">
               <%--   <telerik:RadComboBox ZIndex=10000 ID="ddlmodo" runat="server">
                      <Items>
                          <telerik:RadComboBoxItem runat="server" Text="Grabación" Value="Grabacion" />
                          <telerik:RadComboBoxItem runat="server" Text="Auditoria" Value="Auditoria" />
                          <telerik:RadComboBoxItem runat="server" Text="Al lado del operador" Value="Al lado del operador" />
                      </Items>
                  </telerik:RadComboBox>--%>
                    <telerik:RadTextBox ID="SSIDQOOM" runat="server"></telerik:RadTextBox>
               </td>
               <td align="left">ANI</td>
               <td align="left">
                  <telerik:RadTextBox ID="txtani" ReadOnly="true" runat="server">
                  </telerik:RadTextBox> </td>
           </tr>
           <tr>
              <td align="right">Tipo de problema</td>
              <td align="left">
                  <asp:Label ID="lbltipoproblema" runat="server" Text=""></asp:Label>
              </td>
              <td align="left">&nbsp;
              </td>
              <td align="left">
               &nbsp;
              </td>
          </tr>
          </table>
      <br />
      <br />
    <asp:Table ID="Table1" runat="server" Height="59px" Width="841px" 
        Caption="Usuario Final (Sin respeto incorrecto)" CaptionAlign="Top" 
        BorderColor="#3366FF" BorderStyle="Solid">

        <asp:TableRow runat="server" TableSection="TableHeader">
            <asp:TableCell runat="server" Width="40%" HorizontalAlign="Left">1.Gestiona Incorrectamente:Deja asuntos pendientes.
            </asp:TableCell>
            <asp:TableCell runat="server" Width="10%">
                <telerik:RadComboBox ZIndex=10000 ID="pec1" runat="server" Width="45" AutoPostBack="True">
                    <Items>
                       <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                       <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                       <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                    </Items>                    
                </telerik:RadComboBox>
            </asp:TableCell>
            
            <asp:TableCell runat="server" HorizontalAlign="Left">2. Gestiona Incorrectamente:No cumple los procedimientos para la resolución.
            </asp:TableCell>
            <asp:TableCell runat="server">
               <telerik:RadComboBox ZIndex=10000 ID="pec2" runat="server" Width="45" AutoPostBack="True" >
                    <Items>
                       <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                       <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                       <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                    </Items>
             </telerik:RadComboBox>
            </asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow runat="server">
        <asp:TableCell ID="TableCell1" runat="server" HorizontalAlign="Left"  >3. Gestiona Incorrectamente:No toma la llamada de primera linea.</asp:TableCell>
            <asp:TableCell ID="TableCell2" runat="server">
                <telerik:RadComboBox ZIndex=10000 ID="pec3" runat="server" Width="45" AutoPostBack="True">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                        <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                        <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                    </Items>
                </telerik:RadComboBox>
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server" HorizontalAlign="Left">4.Gestiona Incorrectamente:Red sondeo árbol de diagnóstico	</asp:TableCell>
            <asp:TableCell ID="TableCell4" runat="server">
                <telerik:RadComboBox ZIndex=10000 ID="pec4" runat="server" Width="45" AutoPostBack="True">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />             
                        <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                        <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                    </Items>
                </telerik:RadComboBox>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow runat="server">
        <asp:TableCell ID="TableCell5" runat="server" HorizontalAlign="Left">5. Informa Incorrectamente:Información desactualizada.
        </asp:TableCell>
        <asp:TableCell ID="TableCell6" runat="server">
               <telerik:RadComboBox ZIndex=10000 ID="pec5" runat="server" Width="45" AutoPostBack="True" >
                   <Items>
                        <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />               
                        <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                        <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                    </Items>
                </telerik:RadComboBox>
        </asp:TableCell>
        <asp:TableCell ID="TableCell17" runat="server" HorizontalAlign="Left">6.Informa Incorrectamente:Plazos de solución.
        </asp:TableCell>
        <asp:TableCell ID="TableCell18" runat="server">
               <telerik:RadComboBox ZIndex=10000 ID="pec6" runat="server" Width="45" AutoPostBack="True" >
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />               
                        <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                        <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                    </Items>
                </telerik:RadComboBox>
        </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell>Total: 
            <asp:Label ID="ecc" ForeColor="Red" runat="server" Text=""></asp:Label>
        </asp:TableCell>   
        </asp:TableRow>
     


    </asp:Table>
      <br />
      <br />
    <asp:Table ID="Table2" runat="server" Caption="Usuario Final (Con respeto incorrecto)" 
        Width="841px" BorderColor="#3366FF" BorderStyle="Solid" >
        <asp:TableRow ID="TableRow1" runat="server" TableSection="TableHeader">
            <asp:TableCell ID="TableCell7" runat="server" Width="40%" HorizontalAlign="Left">1. Corte de llamada.
            </asp:TableCell>
            <asp:TableCell ID="TableCell8" runat="server" Width="10%">
                <telerik:RadComboBox ZIndex=10000 ID="pec7" runat="server" Width="45" AutoPostBack="True">
                   <Items>
                        <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                        <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                        <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                    </Items>
               </telerik:RadComboBox>
            </asp:TableCell>
            <asp:TableCell ID="TableCell9" runat="server" HorizontalAlign="Left">2. Falta actitud de servicio.		
            </asp:TableCell>
            <asp:TableCell ID="TableCell10" runat="server">
                 <telerik:RadComboBox ZIndex=10000 ID="pec8" runat="server" Width="45" AutoPostBack="True">
                    <Items>
                       <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />                       
                       <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                       <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                    </Items>
                </telerik:RadComboBox>
            </asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell11" runat="server" HorizontalAlign="Left">3. Maltrato a clientes.
        </asp:TableCell>
        <asp:TableCell ID="TableCell12" runat="server">
             <telerik:RadComboBox ZIndex=10000 ID="pec9" runat="server" Width="45" AutoPostBack="True">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />               
                    <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                    <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                </Items>
             </telerik:RadComboBox>
        </asp:TableCell>
        <asp:TableCell ID="TableCell13" runat="server" HorizontalAlign="Left">4. Se abandona al cliente en hold.
        </asp:TableCell>
        <asp:TableCell ID="TableCell14" runat="server">
               <telerik:RadComboBox ZIndex=10000 ID="pec10" runat="server" Width="45" AutoPostBack="True">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                        <telerik:RadComboBoxItem runat="server" Text="Si" Value="0" />
                        <telerik:RadComboBoxItem runat="server" Text="No" Value="1" />
                    </Items>
                </telerik:RadComboBox>
        </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell>Total: 
             <asp:Label id="ecc1" runat="server" ForeColor="Red" ></asp:Label>
         </asp:TableCell>
        </asp:TableRow>
  </asp:Table>
      <br />
      <br />
    <asp:Table ID="Table3" runat="server" Caption="Habilidades Blandas"   
     Width="841px" BorderColor="#3366FF" BorderStyle="Solid" >
       <asp:TableRow>
          <asp:TableCell Width="40%" HorizontalAlign="Left">1. Trato brindado.
          </asp:TableCell>
           <asp:TableCell Width="10%">
               <telerik:RadComboBox ZIndex=10000 ID="penc11" runat="server" Width="45" AutoPostBack="True">
                 <Items>
                    <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                    <telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
                    <telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
                </Items>
             </telerik:RadComboBox>
          </asp:TableCell>
          <asp:TableCell HorizontalAlign="Left">2. Voluntad para resolver.	
          </asp:TableCell>
          <asp:TableCell>
            <telerik:RadComboBox ZIndex=10000 ID="penc12" runat="server" Width="45" AutoPostBack="True">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                    <telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
                    <telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
                </Items>
            </telerik:RadComboBox>
         </asp:TableCell>
         </asp:TableRow>

        <asp:TableRow>
        <asp:TableCell Width="40%" HorizontalAlign="Left">3. Claridad en la explicación.
        </asp:TableCell>
        <asp:TableCell Width="10%">
            <telerik:RadComboBox ZIndex=10000 ID="penc13" runat="server" Width="45" AutoPostBack="True">
                <Items>
                   <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                   <telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
                   <telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
                </Items>
            </telerik:RadComboBox>
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Left">4. Manejo de llamada.	
        </asp:TableCell>
        <asp:TableCell>
           <telerik:RadComboBox ZIndex=10000 ID="penc14" runat="server" Width="45" AutoPostBack="True">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                    <telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
                    <telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
                </Items>                
            </telerik:RadComboBox>
         </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow >
        <asp:TableCell HorizontalAlign="Left" >5. Información no crítica.
        </asp:TableCell>
        <asp:TableCell>
             <telerik:RadComboBox ZIndex=10000 ID="penc15" runat="server" Width="45" AutoPostBack="True">
               <Items>
                   <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                   <telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
                   <telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
                </Items>
             </telerik:RadComboBox>
         </asp:TableCell>
         <asp:TableCell HorizontalAlign="Left" >6.Esperas y transferencias.
         </asp:TableCell>
         <asp:TableCell>
             <telerik:RadComboBox ZIndex=10000 ID="penc16" runat="server" Width="45" AutoPostBack="True">
               <Items>
                  <telerik:RadComboBoxItem runat="server" Text="N/A" Value="NULL" />  
                  <telerik:RadComboBoxItem runat="server" Text="Si" Value="1" />
                  <telerik:RadComboBoxItem runat="server"  Text="No" Value="0" />
                </Items>
             </telerik:RadComboBox>
         </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
         <asp:TableCell>Total:
         <asp:Label ID="ecc2" runat="server" ForeColor="Red"></asp:Label>
         </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
          <br />
          <br />
          <p> Justificación: </p>
    <telerik:RadTextBox ID="txtjustificacion" runat="server" TextMode="MultiLine" 
                  Width="90%" Rows="10" >
              </telerik:RadTextBox>
           <br />
           <br />          
            <p >Escuchas:</p>
            <asp:Label ID="lblerror" runat="server" Text=""></asp:Label>
            <asp:FileUpload ID="escuchasfile" runat="server" />
            <asp:RegularExpressionValidator ID="REGEXFileUploadwav" runat="server"
                 ErrorMessage="Solo Wavs/PCM" ControlToValidate="escuchasfile" 
            ValidationExpression= "(.*).(.wav|.WAV)$" />
     
           <br />
           <br />
           <br />
        <telerik:RadButton ID="btnguardar" runat="server" Text="Guardar"></telerik:RadButton>
   </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

