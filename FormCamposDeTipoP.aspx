﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FormCamposDeTipoP.aspx.cs" Inherits="FormCamposDeTipoP" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="css/overlay.css" rel="stylesheet" />
    <link href="Images/loading.gif" rel="icon" />
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/overlay.js" type="text/javascript"></script>
     <script type="text/javascript">

         $(function () {
             $(".rcbSlide").css("z-index", "998000");
             $("body").append('<img id="loaderImage" style="display:none;right:15px;top:57px;" src="images/loading.gif" />');
         });
    </script>
      
   
</head>
<body>
    
    <form id="form1" runat="server">
     <input type="hidden" id="blockUI" value="1" />
     <h3 align="center" class="headerStyle" runat="server" id="Header">Formulario de Equipo</h3>
     <div id="error" style="padding-left:33%;">
         <asp:Label ID="errorM" CssClass="labelError" runat="server"></asp:Label>
     </div>
    <div id="ComboProblema" style="padding-top:8%;padding-left:30%">
            <telerik:RadComboBox ID="Radtipo" EmptyMessage="Seleccione un Problema"  ClientIDMode="Static" Skin="Silk"  DataTextField="Nombre" Width="300px" DataValueField="idtipoproblema" Filter="Contains" runat="server" OnSelectedIndexChanged="Radtipo_SelectedIndexChanged" AutoPostBack="true">
                                <%-- <HeaderTemplate>
                                     <table style="width:100%;">
                                         <tr>
                                             <td>Codigo</td>
                                             <td>Nombre</td>
                                         </tr>
                                     </table>
                                 </HeaderTemplate>
                                 <ItemTemplate>
                                     <table style="width:100%;">
                                         <tr>
                                             <td> <%# DataBinder.Eval(Container.DataItem, "idtipoproblema").ToString().Trim()%> </td>
                                             <td style="text-align:right; padding-right:15px;"><%# DataBinder.Eval(Container.DataItem, "Nombre").ToString().Trim()%></td>
                                         </tr>
                                     </table>
                                 </ItemTemplate>--%>
                                 
           </telerik:RadComboBox>
           
    </div>
    <div id="tbleprob"style="padding-top:9%;padding-left:9%">
        <table id="tipoProblema" runat="server"  >
             <tr>
                <td>Nombre :</td><td><telerik:RadTextBox id="txtn" runat="server"></telerik:RadTextBox></td>
                <td>Windows :</td><td><telerik:RadCombobox ID="radSistemaOP" runat="server" DataValueField="id" DataSourceID="sqltipowindows" DataTextField="Nombre" Skin="Silk" EmptyMessage="seleccionar SO"></telerik:RadCombobox></td>
             </tr>
            <tr>
                <td>TipoWindows</td><td><telerik:RadComboBox ID="RadTipoWin" runat="server" Skin="Silk" EmptyMessage="Tipo Windows" ClientIDMode="Static">
                                        <Items>
                                        <telerik:RadComboBoxItem text="Original" Value="1"/>
                                        <telerik:RadComboBoxItem text="Licencia" Value="2"/>
                                        </Items>
                                        </telerik:RadComboBox></td>
                <td>PC:</td><td><telerik:RadComboBox ID="radTipoPC" runat="server" EmptyMessage="Seleccionar PC" Skin="Silk" ClientIDMode="Static">
                                    <Items>
                                        <telerik:RadComboBoxItem text="PC escritorio" Value="1"/>
                                        <telerik:RadComboBoxItem text="Notebook" Value="2"/>
                                        <telerik:RadComboBoxItem text="AIO" Value="3"/>
                                        <telerik:RadComboBoxItem text="Netbook" Value="4"/>
                                        <telerik:RadComboBoxItem Text="Netbook GOB" Value="5" />
                                     </Items>
                                </telerik:RadComboBox></td>
            </tr>
            <tr>
                <td>Antiguedad.Equipo</td><td><telerik:RadTextBox ID="txtAntiguedad" runat="server"></telerik:RadTextBox></td>
                <td>Procesador</td><td><telerik:RadTextBox ID="txtprocesador" runat="server"></telerik:RadTextBox></td>
            </tr>
            <tr>
                <td>RAM</td><td><telerik:RadTextBox ID="txtram" runat="server"></telerik:RadTextBox></td>
                <td>Tipo.Conexión</td><td><telerik:RadComboBox ID="radTipoConexion" runat="server" EmptyMessage="Seleccionar tipo de conexion" Skin="Silk">
                                            <Items>
                                                <telerik:RadComboBoxItem Text="Cable" Value="1" />
                                                <telerik:RadComboBoxItem Text="Wifi" Value="2" />
                                            </Items>
                                          </telerik:RadComboBox></td>
            </tr>
            <tr>
                <td>Modelos del Modem</td><td><telerik:RadTextBox ID="txtModeloMDM" ClientIDMode="Static" runat="server"></telerik:RadTextBox></td>
            </tr>

            <tr>
                <td colspan="5" style="padding-top:50px"><telerik:RadTextBox ID="txtDetalle" runat="server" Rows="3" TextMode="MultiLine" Width="100%" EmptyMessage="Ingresar Detalle" ></telerik:RadTextBox> </td>
            </tr>
            <tr>
                <td colspan="5" align="center"><telerik:RadButton ID="btnaceptar" runat="server" Text="Ingresar" OnClick="btnaceptar_Click" AutoPostBack="true"></telerik:RadButton></td>
            </tr>
       </table>
    </div>
    <asp:HiddenField ID="hidenbloqueoControles" runat="server" />
           
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    </form>
<asp:SqlDataSource ID="sqlprob" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>"></asp:SqlDataSource>
<asp:SqlDataSource ID="sqlTicketState" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>"></asp:SqlDataSource>
<asp:SqlDataSource ID="sqltipowindows" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT id,nombre from SistemaOperativo where id!=7"></asp:SqlDataSource>
    
</body>
</html>
