﻿Imports System.Data.SqlClient
Imports System.IO

Partial Class Default2
    Inherits System.Web.UI.Page
    Dim sql As New Data.SqlClient.SqlConnection
    Dim query As New Data.SqlClient.SqlCommand
    Dim uname As App_Code.TicketsLib.Usuario
    '    Dim querypb As String




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        configurarSemaforos()
        Dim preguntas() As String
        Dim userid As String


        userid = "and ticket.ticketid = 0"
        uname = Session("Usuario")

        If Not IsPostBack Then
            VerificarPerfil(uname.Perfil, 4)  'Subrutina que realiza la consulta sobre el alcance del perfil
        End If


        If RadTabStrip1.SelectedIndex = 0 Then  'Se verifica el Index del tab = 0 "Bandeja"
            query.CommandText = "select condicion from usuariocondicionagenda where usuarioid = " & uname.UsuarioId
        ElseIf RadTabStrip1.SelectedIndex = 1   'Se verifica el Index del tab = 1 "Bandeja2" o "Remoto Caido"    

            query.CommandText = "select condicion2 from usuariocondicionagenda where usuarioid = " & uname.UsuarioId
        End If


        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        Dim i As Long = 0, j As Long = 0

        sql.Open()
        query.Connection = sql

        Dim rs As System.Data.SqlClient.SqlDataReader = query.ExecuteReader
        While rs.Read
            ReDim Preserve preguntas(0 To i)
            preguntas(i) = rs(0)
            i = i + 1
        End While


        If rs.HasRows Then
            rs.Close()
            userid = preguntas(0)
            userid = "and " & userid & ""


        End If

        sql.Close()

        SqlDataSource1.SelectCommand = "SELECT Ticket.TICKETID as 'Ticket' , Ticket.CLAINNRO as 'Ani' , " _
& "Ticket.FECHA as 'Fecha creacion', " _
& "'(' + rtrim(Ticket.CLIENTEID) + ') ' + VC.RazonSocial as 'Cliente' , " _
& "Especialista.Nombre as 'Especialista' ," _
& "vl.Nombre as 'Lider' ," _
& "Ticket.EstadoId, Estado.Nombre as 'Estado', Ticket.FechaUltimaModif, dbo.ultimoCambioEstado(ticket.ticketId) as FechaEstado, tir.desde fechaReContacto,mr.nombre as motivo " _
& "FROM Ticket (NOLOCK) " _
& "INNER JOIN Equipo (nolock) ON Ticket.ArticuloId = Equipo.EquipoId " _
& "INNER JOIN Especialista (nolock) ON Ticket.EspecialistaId = Especialista.EspecialistaId " _
& "INNER JOIN viewLider vl ON Ticket.EspecialistaId = vl.EspecialistaId " _
& "INNER JOIN TipoProblema (nolock) ON Ticket.IdTipoProblema = TipoProblema.IdTipoProblema " _
& "INNER JOIN EmpresaPartner (nolock) ON Ticket.EmpresaPartnerId = EmpresaPartner.EmpresaPartnerId " _
& "INNER JOIN Estado (nolock) ON Ticket.EstadoId = Estado.EstadoId " _
& "INNER JOIN ViewCliente VC (nolock) ON Ticket.ClienteId = VC.ClienteId " _
& "INNER JOIN UbicacionContacto (nolock) ON Ticket.UbicacionContactoId = UbicacionContacto.UbicacionContactoId " _
& "LEFT JOIN ticketInfoRecontacto tir (nolock) ON Ticket.ticketID = tir.ticketId " _
& "LEFT JOIN motivoRecontacto mr (nolock) ON tir.motivoId = mr.id " _
& "WHERE 1 = 1 and Ticket.estadoid not in (2,10) " _
& "" & userid & ""



    End Sub


    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        query.Dispose()
        sql.Dispose()
    End Sub

    Protected Sub GridView1_DataBound(sender As Object, e As EventArgs) Handles GridView1.DataBound
        For Each r As GridViewRow In GridView1.Rows
            Dim tkt = r.Cells(1).Text
            Dim fecha = r.Cells(9).Text
            r.Cells(9).Text = fecha & "<span style='display:none;' id='fechatkt_" & tkt & "'>" & DateTime.Parse(fecha).ToString("MM-dd-yyyy HH:mm:ss") & "</span>"
            r.Cells(10).Attributes.Add("id", "duraciontkt_" & tkt)
            r.Cells(14).Attributes.Add("id", "semaforo_" & tkt)
            r.Cells(15).Attributes.Add("id", "estadoId_" & tkt)
            r.Cells(15).Style.Add("display", "none")

            r.Cells(16).Attributes.Add("id", "fEstado_" & tkt)
            r.Cells(16).Text = DateTime.Parse(r.Cells(16).Text).ToString("MM-dd-yyyy HH:mm:ss")
            r.Cells(16).Style.Add("display", "none")

            '            If r.Cells(12).Text.Trim() <> "&nbsp;" Then r.Cells(12).ToolTip = r.Cells(17).Text
            tkts.Value &= tkt & ","
        Next
        If GridView1.AllowPaging Then
            lblPagBottom.Text = GridView1.PageIndex + 1 & " de " & GridView1.PageCount
            lblPagTop.Text = GridView1.PageIndex + 1 & " de " & GridView1.PageCount
        Else
            lblPagBottom.Text = GridView1.PageIndex + 1 & " de "
            lblPagTop.Text = GridView1.PageIndex + 1 & " de " & GridView1.PageCount
        End If

    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        'If e.CommandName = "Select" Then
        '    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        '    Dim selectedRow As GridViewRow = GridView1.Rows(index)
        '    Dim tktCell As TableCell = selectedRow.Cells(1)
        '    Dim tkt As String = tktCell.Text

        '    Response.Redirect("EditaTicket.aspx?id=" & tkt & "")

        'End If

        '    Overloads Sub post(ByVal custName As String, ByVal amount As Single)
        ' Insert code to access customer record by customer name.
        '    End Sub
        '   Overloads Sub post(ByVal custAcct As Integer, ByVal amount As Single)
        ' Insert code to access customer record by account number.
        '   End Sub






    End Sub



    Public Sub configurarSemaforos()
        Dim Sql As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("SELECT * FROM  viewSemaforos", Sql)
        Dim Rs As SqlDataReader = Nothing
        Try
            Sql.Open()
            Rs = CMD.ExecuteReader()
            semaforos.Value = ""
            While Rs.Read
                If semaforos.Value.Length > 0 Then semaforos.Value &= "|"
                semaforos.Value &= Rs(0).ToString
            End While
            Sql.Close()
        Catch ex As Exception
            If Sql.State <> Data.ConnectionState.Closed Then Sql.Close()
            Throw ex
        End Try

    End Sub

    Protected Sub lnkBtTop10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtTop10.Click, lnkBtBottom10.Click
        GridView1.AllowPaging = True
        GridView1.PageSize = 10
        GridView1.DataBind()
    End Sub

    Protected Sub lnkBtTop20_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtTop20.Click, lnkBtBottom20.Click
        GridView1.AllowPaging = True
        GridView1.PageSize = 20
        GridView1.DataBind()
    End Sub

    Protected Sub lnkBtTop30_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtTop30.Click, lnkBtBottom30.Click
        GridView1.AllowPaging = True
        GridView1.PageSize = 30
        GridView1.DataBind()
    End Sub

    Protected Sub lnkBtTop50_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtTop50.Click, lnkBtBottom50.Click
        GridView1.AllowPaging = True
        GridView1.PageSize = 50
        GridView1.DataBind()
    End Sub

    Protected Sub lnkBtTopTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtTopTodos.Click, lnkBtBottomTodos.Click
        GridView1.AllowPaging = False
        GridView1.DataBind()
    End Sub

    Protected Sub SqlDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSource1.Selected
        Dim textoAMostrar As String
        If Not GridView1.AllowPaging OrElse (e.AffectedRows < GridView1.PageSize) Then
            textoAMostrar = e.AffectedRows
        Else
            textoAMostrar = GridView1.PageSize & " de " & e.AffectedRows
        End If
        lblCantResTop.Text = textoAMostrar
        lblCantResBottom.Text = textoAMostrar
        deshabilitarBarra(e.AffectedRows > 0)
    End Sub

    Private Sub deshabilitarBarra(ByVal b As Boolean)
        lblCantResTop.Visible = b
        lblResPorPagTopTxt.Visible = b
        lblResPorPagTopTxt0.Visible = b
        lblCantResBottom.Visible = b
        lblCantResTxt.Visible = b
        lblCantResTxt0.Visible = b
        btExportarTop.Visible = b
        btExportarBottom.Visible = b
        lnkBtBottom10.Visible = b
        lnkBtBottom20.Visible = b
        lnkBtBottom30.Visible = b
        lnkBtBottom50.Visible = b
        lnkBtBottomTodos.Visible = b
        lnkBtTop10.Visible = b
        lnkBtTop20.Visible = b
        lnkBtTop30.Visible = b
        lnkBtTop50.Visible = b
        lnkBtTopTodos.Visible = b
        lblPagBottom.Visible = b
        lblPagBottomTxt.Visible = b
        lblPagTop.Visible = b
        lblPagTopTxt.Visible = b
    End Sub

    Private Sub exportarAExcel(ByVal titulo As String, ByVal gv As GridView)

        Dim sb As New StringBuilder()
        Dim sw As New StringWriter(sb)
        Dim htw As New HtmlTextWriter(sw)
        Dim pagina As New Page()
        Dim form = New HtmlForm()

        prepararParaExportacion(gv)
        Dim t As New HtmlTable()
        Dim lblT As New Label()
        lblT.Text = titulo
        lblT.Font.Bold = True
        lblT.Font.Size = 14
        t.Rows.Add(New HtmlTableRow())
        t.Rows.Add(New HtmlTableRow())
        t.Rows(0).Cells.Add(New HtmlTableCell())
        t.Rows(1).Cells.Add(New HtmlTableCell())
        t.Rows(0).Cells(0).Controls.Add(lblT)
        t.Rows(1).Cells(0).Controls.Add(gv)


        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(t)
        form.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=BandejaDeTickets_" + DateTime.Now.ToString("dd-MM-yyyy_HH.mm") + ".xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.[Default]
        Response.Write("<p><br><p>")
        Response.Write(sb.ToString())
        Response.[End]()
    End Sub
    Private Sub prepararParaExportacion(ByRef gv As GridView)
        gv.AllowSorting = False
        gv.AllowPaging = False
        gv.HeaderStyle.Font.Underline = False
        gv.HeaderStyle.ForeColor = System.Drawing.Color.White
        gv.EnableSortingAndPagingCallbacks = False
        gv.Columns(17).Visible = True
        gv.DataBind()
        gv.Columns(0).Visible = False
        gv.Columns(6).Visible = False
        For Each r As GridViewRow In gv.Rows
            Dim dur = Now().Subtract(DateTime.Parse(r.Cells(17).Text))
            Dim dias = ""
            If dur.Days > 0 Then dias = dur.ToString("%d") & "d, "
            r.Cells(10).Text = dias & dur.ToString("hh\:mm\:ss")
            r.Cells(9).Text = r.Cells(17).Text
        Next
        'gv.Columns(12).Visible = False
        'gv.Columns(13).Visible = False
        gv.Columns(14).Visible = False
        gv.Columns(15).Visible = False
        gv.Columns(16).Visible = False
        gv.Columns(17).Visible = False
    End Sub
    Protected Sub btExportar_Click(ByVal sender As Object, ByVal e As EventArgs)
        If GridView1.Rows.Count > 0 Then
            exportarAExcel("Bandeja de tickets [" + DateTime.Now.ToString() + "]", GridView1)
        End If
    End Sub
    Protected Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        configurarSemaforos()
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged

    End Sub

    ' Verifica si el ID del usuario tiene alcance para ver RadTab1
    Public Sub VerificarPerfil(Perfil As String, PantallaID As Integer)

        Dim CMD As New Data.SqlClient.SqlCommand
        Dim sql As New Data.SqlClient.SqlConnection
        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        ' Consulta sobre Tabla "AlcancePerfil"         'perfil    'pantalla    'Variable de Salida
        CMD.CommandText = "EXECUTE dbo.VerificarPerfil @PerfilID, @PantallaID, @Respuesta = @Respuesta OUTPUT"
        CMD.Parameters.Add("@PerfilID", Data.SqlDbType.Int)
        CMD.Parameters("@PerfilID").Value = Perfil
        CMD.Parameters.Add("@PantallaID", Data.SqlDbType.Int)
        CMD.Parameters("@PantallaID").Value = PantallaID
        Dim p As New SqlParameter("@Respuesta", Data.SqlDbType.Bit)
        p.Direction = Data.ParameterDirection.Output
        CMD.Parameters.Add(p)


        Try
            Sql.Open()
            CMD.Connection = sql
            CMD.ExecuteNonQuery()

            If CMD.Parameters("@Respuesta").Value() = True Then
                RadTabStrip1.Visible = True  'Puede ver RadTab1
            Else
                RadTabStrip1.Visible = False 'No Puede ver RadTab1
            End If

            sql.Close()

        Catch ex As Exception
            If Sql.State <> Data.ConnectionState.Closed Then Sql.Close()
            Throw ex
        End Try

    End Sub


    Protected Sub RadTabStrip1_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles RadTabStrip1.TabClick

        GridView1.DataBind()

    End Sub
End Class

