﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="tarifas.aspx.vb" Inherits="tarifas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3 align="center">
    Tarifas</h3>
<p>
    <asp:Table ID="Table1" runat="server" HorizontalAlign="Center" 
        BorderStyle="Double" GridLines="Both">
        <asp:TableRow runat="server" HorizontalAlign="Left" VerticalAlign="Middle">
            <asp:TableCell runat="server">CAS</asp:TableCell>
            <asp:TableCell runat="server">
                <asp:DropDownList ID="ddespecialista" runat="server" 
        DataSourceID="Especialistas" DataTextField="Nombre" 
        DataValueField="EspecialistaId">
        <asp:ListItem Value="0" Selected="True">Selecione un especialista</asp:ListItem>
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">Código Reparación</asp:TableCell>
            <asp:TableCell runat="server"><asp:DropDownList ID="ddcodigo" runat="server" 
        DataSourceID="Codigos" DataTextField="tipo" 
        DataValueField="cod">
        <asp:ListItem Value="0" Selected="True">Seleccione un código</asp:ListItem>
                </asp:DropDownList></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">Valor</asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox ID="txtvalor" runat="server">$</asp:TextBox></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ErrorMessage="Debe ingresar un CAS" ControlToValidate="ddespecialista" 
        ForeColor="Red" InitialValue="0" style="text-align: center"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
        ErrorMessage="Debe ingresar un Código" ControlToValidate="ddcodigo" 
        ForeColor="Red" InitialValue="0" style="text-align: center"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
        ErrorMessage="Debe ingresar un Valor" ControlToValidate="txtvalor" 
        ForeColor="Red" InitialValue="$" style="text-align: center"></asp:RequiredFieldValidator>
</p>
    <p align="center">
        <asp:Button ID="guardar" runat="server" Text="Guardar" />
</p>
    <asp:SqlDataSource ID="Codigos" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        SelectCommand="SELECT [cod], [tipo] FROM [codrep]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Especialistas" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        
        SelectCommand="SELECT [EspecialistaId], [Nombre] FROM [especialista] WHERE (([Nombre] LIKE '%' + @Nombre + '%') AND ([Nombre] NOT LIKE '%' + @Nombre2 + '%') AND ([Nombre] NOT LIKE '%' + @Nombre3 + '%')) ORDER BY [Nombre]">
        <SelectParameters>
            <asp:Parameter DefaultValue="cas" Name="Nombre" Type="String" />
            <asp:Parameter DefaultValue="USAR" Name="Nombre2" Type="String" />
            <asp:Parameter DefaultValue="CASARINI" Name="Nombre3" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="insertar" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" >
    </asp:SqlDataSource>
</asp:Content>

