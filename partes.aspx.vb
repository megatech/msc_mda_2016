﻿
Partial Class Default3
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim tkt As String
        If Session.Item("sessid") Is Nothing Then
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString)
        End If

        tkt = Request.QueryString("id")
        'tkt = Session.Item("ticket")

        SqlDataSource1.SelectCommand = "Select tp.articuloid as 'P/N',viewarticulo.nombre as 'Detalle', nroserie as 'NroSerie', remitoid as 'Remito',viewdeposito.nombre as 'deposito',condicionpago as 'Condicion',tp.PRECIO as 'Precio' from ticketpresupuesto tp inner join viewarticulo on viewarticulo.articuloid = tp.articuloid inner join instanciaarticulo  on tp.ticketpresupuestoid = instanciaarticulo.ticketpresupuestoid inner join viewdeposito on instanciaarticulo.depositoid = viewdeposito.depositoid  where instanciaarticulo.ticketid = " & tkt & " "

        SqlDataSource2.SelectCommand = "select ia.Articuloid as 'P/N',va.nombre as 'Descripción',nroserie as 'NroSerie',viewdeposito.nombre as 'Deposito' from ticketparte tp inner join instanciaarticulo ia on ia.instanciaarticuloid = tp.instanciaarticuloid inner join viewarticulo va on va.articuloid = ia.articuloid inner join viewdeposito on viewdeposito.depositoid = ia.depositoid where tp.ticketid = " & tkt & ""

        SqlDataSource3.SelectCommand = "select cantidad as 'Cant', vs.descripcion as 'Descripción' ,condicionpago as 'Condicion', si.precio as 'Precio' from instanciaservicio si inner join viewservicio vs on si.servicioid = vs.servicioid where ticketid = " & tkt & " "
        Session.Remove("ticket")

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim tkt As String
        tkt = Request.QueryString("id")
        Response.Redirect("EditaTicket.aspx?id=" & tkt & "")

    End Sub

    Protected Sub CmdAsignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdAsignar.Click
        If Request.QueryString("id") Is Nothing Then
            Response.Redirect("default.aspx")
        End If

        Response.Redirect("asignaparte.aspx?id=" & Request.QueryString("id"))

    End Sub
End Class
