﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="repcalle.aspx.vb" Inherits="reportes" %>


<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
<div style="width: 700px" align="center" >    
<rsweb:ReportViewer ID="ReportViewer1" runat="server" 
        Width="800px" Font-Names="Verdana" Font-Size="8pt" 
        InteractiveDeviceInfos="(Colección)" WaitMessageFont-Names="Verdana" 
        WaitMessageFont-Size="14pt" Height="758px">
    <LocalReport ReportPath="Report2.rdlc">
        <DataSources>
            <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="DataSet1" />
        </DataSources>
    </LocalReport>
    </rsweb:ReportViewer>

    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="SC_MEGATECHDataSetrepoTableAdapters.ViewrepingTableAdapter">
    </asp:ObjectDataSource>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT     TOP (1) tk.TicketId, RTRIM(tk.ClienteId + ' ' + vc.RazonSocial) AS cliente, ep.Nombre, tipoproblema.Nombre AS tipoproblema, 
                      vc.Domicilio + ' -  ' + vc.Localidad + ' - ' + CAST(vc.Observaciones as VARCHAR) AS domicilio, vc.Telefono1, tk.Problema, estado.Nombre AS estado, t.Nombre AS tecnico, 
                      CASE WHEN tt.horario1 = 1 THEN '10 a 12' ELSE '' END AS h1, CASE WHEN tt.horario2 = 1 THEN '13 a 15' ELSE '' END AS h2, 
                      CASE WHEN tt.horario3 = 1 THEN '15 a 17' ELSE '' END AS h3, equipo.Nombre AS Equipo, vc.Telefono2
FROM         ticket AS tk INNER JOIN
                      ViewCliente AS vc ON vc.ClienteId = tk.ClienteId INNER JOIN
                      empresapartner AS ep ON ep.EmpresaPartnerId = tk.EmpresaPartnerId INNER JOIN
                      tipoproblema ON tipoproblema.IdTipoProblema = tk.IdTipoProblema INNER JOIN
                      estado ON estado.EstadoId = tk.EstadoId INNER JOIN
                      equipo ON tk.ArticuloId = equipo.EquipoId LEFT OUTER JOIN
                      ticketTecnico AS tt ON tk.TicketId = tt.ticketId LEFT OUTER JOIN
                      tecnico AS t ON tt.tecnicoId = t.id
WHERE     (1 = 1)"></asp:SqlDataSource>

    </div>
    
</asp:Content>

