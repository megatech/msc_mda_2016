﻿using System;
using App_Code.TicketsLib;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using Popups;
using System.Collections.Generic;

public partial class clientesAltaPop : System.Web.UI.Page
{
    log4net.ILog logger = log4net.LogManager.GetLogger("File");
    protected void Page_Load(object sender, EventArgs e)
    {
        Usuario usr = (Usuario)Session["Usuario"];
        if (usr == null)
        {
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString);
        }
        if (!IsPostBack)
        {
            cargarLocalidades();
            panelMensaje.Visible = false;
            panelTicket.Visible = true;
            habilitarTodo(false);
            //initCliente();
        }
    }

    private bool clienteExiste()
    {
        SqlConnection SQL = new SqlConnection();
        SqlCommand CMD = new SqlCommand();
        SqlDataReader RS;
        SQL.ConnectionString = ConfigurationManager.ConnectionStrings["ticketDB"].ToString();
        CMD.Connection = SQL;
        SQL.Open();
        CMD.CommandText = "SELECT top 1 ClienteId, RTRIM(ISNULL(RazonSocial, ' '))+' | '+RTRIM(ISNULL(Telefono1, ' '))+' | '+RTRIM(ISNULL(Domicilio, ' '))+' | '+RTRIM(ISNULL(Localidad, ' '))+' [ '+RTRIM(ISNULL(ViewProvincia.Nombre, ' '))+' ]' AS info,RazonSocial FROM viewCliente LEFT JOIN ViewProvincia ON ViewProvincia.ProvinciaId = ViewCliente.ProvinciaId WHERE ltrim(rtrim(Telefono1)) = '" + txtTelefono.Text.Trim() + "'";
        RS = CMD.ExecuteReader();
        RS.Read();
        String info = RS[1].ToString();
        String clienteId = RS[0].ToString();
        String razonSocial = RS[2].ToString();
        idClienteViejo.Text = clienteId;
        nomClienteViejo.Text = razonSocial;
        lnkClienteExistente.Visible = RS.HasRows;
        btSelCliente.Visible = RS.HasRows;
        if (RS.HasRows) { mostrarAdvertencia(clienteId, info); SQL.Close(); RS.Close(); idClienteViejo.Text = clienteId; return true; }
        else { SQL.Close(); RS.Close(); return false; }
    }

    private void mostrarAdvertencia(String clienteId, String info)
    {
        
        string verbo = "seleccionar dicho";
        if (Request.QueryString["modal"] == null) { verbo = "acceder a dicho"; }
        lnkClienteExistente.Text = "El Telefono ingresado ya existe para el siguiente cliente: </br>" +
            info + ".</br>" + "Para " + verbo + " cliente, haga click en este mensaje.";
        lnkClienteExistente.PostBackUrl = "vercliente.aspx?codcte=" + idClienteViejo.Text;


    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //if (clienteExiste()) { return; }
        lblError.Text = "";
        if (txtTelefono.Text.Trim() == "1144325716") { lblError.Text = "CLIENTE NO GENERADO: El Telefono especificado no puede ser utilizado para la creación de nuevos clientes."; return; }
        string msg = "";
        if (validar(ref msg))
        {
            try
            {
                generoCliente();
                if (Request.QueryString["modal"] != null)
                {
                    String Script = "<Script> var ret=new Array( '" + txtCodigo.Text + "','" + txtRazonSocial.Text + "') ; " +
                                    " window.returnValue=ret; " +
                                    " window.close(); </Script>";
                    Response.Write(Script);
                }
            }

            catch (Exception ex)
            {
                logger.Error("Error en Ingresar_Click: Original: " + ex.Message, ex);
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = "NO SE GENERÓ EL CLIENTE: <br>" + msg;
        }
    }

    private bool validar(ref string msg)
    {
        if (!validarCampo(txtRazonSocial, txtRazonSocial.Text.Trim() == ""))
        {
            msg += "El campo 'Razón Social' está vacío.<br>";
        }
        if (!validarCampo(txtRazonSocial, !soloLetras(txtRazonSocial.Text.Trim())))
        {
            msg += "El campo 'Razón Social' no puede contener números.<br>";
        }
        if (!validarCampo(txtTelefono, txtTelefono.Text == ""))
        {
            msg += "El campo 'Telefono' está vacío.<br>";
        }
        string cliId = "";

        if (!validarCampo(txtTelefono, existeCliente(txtTelefono.Text, ref cliId)))
        { msg += "El número de Telefono ingresado ya existe para el cliente: '" + cliId + "'<br>"; }

        if (!validarCampo(txtFax, txtFax.Text == ""))
        {
            msg += "El campo 'Celular' está vacío.<br>";
        }
        if (!validarCampo(txtTelefono, !esTelefonoValido(txtTelefono.Text)))
        {
            msg += "El campo 'Telefono' posee caracteres inválidos o es inferior a 10 digitos.<br>";
        }
        if (!validarCampo(txtFax, !esTelefonoValido(txtFax.Text)))
        {
            msg += "El campo 'Celular' posee caracteres inválidos o es inferior a 10 digitos.<br>";
        }
        if (!validarCampo(ddProvincia, ddProvincia.SelectedValue == "-1"))
        {
            msg += "El campo 'Provincia' está vacío.<br>";
        }
        if (!validarCampo(ddLocalidad, ddLocalidad.SelectedValue == "-1"))
        {
            msg += "El campo 'Localidad' está vacío.<br>";
        }
        //if (!validarCampo(txtTelefono, !esCaracteristicaTelValida(txtTelefono.Text, ddProvincia.SelectedValue)))
        //{
        //    msg += "La característica del número telefónico corresponde a Buenos Aires y Cap.Fed.(AMBA), verifique la provincia seleccionada.<br>";
        //}
        //if (!validarCampo(txtTelefono, !esCaracteristicaTelValida2(txtTelefono.Text, ddProvincia.SelectedValue)))
        //{
        //    msg += "La característica del número telefónico no corresponde a Capital Federal, verifique la provincia seleccionada.<br>";
        //}

        return msg == "";
    }

    private bool existeCliente(string ani, ref string clienteId)
    {
        String res = Funciones.getScalar("SELECT top 1 CASE WHEN count(*)>0 THEN rtrim(min(clienteId)) ELSE '' END FROM viewCliente WHERE rtrim(telefono1)='" + ani.Trim() + "'").ToString();
        clienteId = res;
        return res.Trim() != "";
    }

    private bool soloLetras(string p)
    {
        foreach (char ch in p)
        {
            if ("0123456789".Contains(ch.ToString())) { return false; }
        }
        return true;
    }

    public Boolean esLocalidadValida(string provId, string localidad)
    {
        string query = "SELECT count(*) FROM localidad WHERE rtrim(provinciaId)='" +
            provId.Trim() + "' AND ltrim(rtrim(upper(nombre))) like '" +
            localidad.ToUpper().Trim() + "' ".ToString();
        return Funciones.getScalar(query).ToString() != "0";
    }
    private Boolean esCaracteristicaTelValida(String tel, String provid)
    {
        String telFiltrado = tel.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
        if ((telFiltrado.StartsWith("11") || telFiltrado.StartsWith("011")) && (!"902.901".Contains(provid.Trim()))) { return false; }
        return true;
    }
    private Boolean esCaracteristicaTelValida2(String tel, String provid)
    {
        String telFiltrado = tel.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
        if (!(telFiltrado.StartsWith("11") || telFiltrado.StartsWith("011")) && ("901" == provid.Trim())) { return false; }
        return true;
    }

    public bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;

        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        return isNum;
    }
    private Boolean esTelefonoValido(String tel)
    {
        if (tel.Trim() == "") return true;
        String telFiltrado = tel.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
        if (telFiltrado.Length < 10) { return false; }
        foreach (char c in telFiltrado) { if (!"1234567890".Contains(c.ToString())) { return false; } }
        return true;
    }

    private Boolean validarCampo(Object c, Boolean b)
    {
        //resaltar(c, b);
        return !b;
    }

    private void resaltar(Object c, Boolean b)
    {
        Control ctrl = (System.Web.UI.Control)c;
        String t = ctrl.GetType().Name;
        if (t == "TextBox")
        {
            TextBox txt = (TextBox)ctrl;
            if (b) { txt.ForeColor = System.Drawing.Color.Red; }
            else { txt.ForeColor = System.Drawing.Color.DimGray; }
        }
        if (t == "DropDownList")
        {
            DropDownList txt = (DropDownList)ctrl;
            if (b) { txt.ForeColor = System.Drawing.Color.Red; }
            else { txt.ForeColor = System.Drawing.Color.DimGray; }
        }

    }

    private void initCliente()
    {
        //recCliente rc = new recCliente();

    }
    private void generoCliente()
    {
        try
        {
            recCliente rc = getDataFromPanatalla();
            rc = Clientes.insertCliente(rc);
            lblError.Text = rc.MensajeError;
            txtCodigo.Text = rc.ClienteID;
            if (txtCodigo.Text.Trim().Length > 0)
            {
                lblError.Text = "Cliente generado correctamente.";
                btnSubmit.Visible = false;
            }


        }
        catch (Exception ex)
        {
            logger.Error("Error en generoTicket: " + ex.Message, ex);
            lblError.Text = "Error en generoTicket: " + ex.Message;
        }
    }
    private recCliente getDataFromPanatalla()
    {
        recCliente rc = new recCliente();
        try
        {
            rc.EmpresaId = int.Parse(Config.getValue("EmpresaId"));
            rc.RazonSocial = txtRazonSocial.Text.Replace("'","");
            rc.Fantasia = txtFantasia.Text.Replace("'", ""); ;
            rc.CategoriaIVA = ddlCategoriaIva.SelectedValue;
            rc.Documento = txtDocumento.Text;
            rc.Domicilio = txtDomicilio.Text.Replace("'", ""); ;
            rc.Localidad = txtLocalidad.Text;
            rc.Provincia = ddProvincia.SelectedValue;
            rc.Telefono = txtTelefono.Text;
            rc.Fax = txtFax.Text;
            rc.Email = txtEmail.Text;
            rc.Observaciones = txtObservaciones.Text.Replace("'", ""); ;
            rc.CodPostal = txtCodPostal.Text;
        }
        catch (Exception ex)
        {
            lblError.Text = "[Error de conversión] Original: " + ex.Message;
            logger.Error("Error al tomar de pantalla", ex);

        }
        return rc;
    }

    protected void txtTelefono_TextChanged(object sender, EventArgs e)
    {
        string cliId = "";
        if ((txtTelefono.Text.Replace("(", "").Replace(")", "").Replace("-", "").Trim().Length) > 10)
        { lblErrTel.Text = "El valor debe ser de 10 dígitos, incluyendo el código de area."; panelDatosSugeridos.Visible = false; return; }
        else { lblErrTel.Text = ""; }
        if (!validarCampo(txtTelefono, existeCliente(txtTelefono.Text, ref cliId)))
        {
            habilitarTodo(false);
            lblErrTel.Text = "El teléfono ingresado ya existe para el cliente:";
            lnkCliente.Attributes.Add("OnClick", "function VerCliente() {window.open('vercliente.aspx?codcte= " + cliId + "');} VerCliente();");
            lnkCliente.Text = cliId; lnkCliente.Visible = true; panelDatosSugeridos.Visible = false;
            btSelCliente.Visible = true;
            txtCodEncontrado.Text = cliId;
            txtRSEncontrada.Text = Funciones.getScalar("SELECT rtrim(razonSocial) FROM viewCliente WHERE clienteId='" + cliId + "'").ToString();
            btSelCliente.Enabled = true;
            return;
        }
        else { lblErrTel.Text = ""; lnkCliente.Visible = false; btSelCliente.Visible = false; }
        habilitarTodo(true);
        InfoTelData data = new InfoTelData();
        if (InfoTel.buscar(txtTelefono.Text, ref data))
        {
            lblCP.Text = data.CP;
            lblDomicilio.Text = data.Direccion;
            lblLocalidad.Text = data.Localidad;
            lblProvincia.Text = data.Provincia;
            panelDatosSugeridos.Visible = true;
            pnlNoHayDatos.Visible = false;
        }
        else { panelDatosSugeridos.Visible = false; pnlNoHayDatos.Visible = true; }
    }

    protected void cargarPartidos()
    {
        System.Data.SqlClient.SqlConnection sql = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand query = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader rs;

        ddPartido.Items.Clear();
        sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketDB"].ToString();
        sql.Open();
        query.Connection = sql;
        query.CommandText = "SELECT id,nombre FROM partido order by nombre";
        ddPartido.Items.Add(new RadComboBoxItem("TODOS", "-1"));
        ddPartido.Items[0].Selected = true;
        rs = query.ExecuteReader();

        while (rs.Read())
        {
            ddPartido.Items.Add(new RadComboBoxItem(rs[1].ToString(), rs[0].ToString()));
        }
        rs.Close();
        sql.Close();


        ddPartido.Visible = ddProvincia.SelectedValue == "902";
        lblPartido.Visible = ddProvincia.SelectedValue == "902";
    }
    protected void cargarLocalidades()
    {
        System.Data.SqlClient.SqlConnection sql = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand query = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader rs;
        String condPartido = "";

        if (ddPartido.SelectedValue != "-1") { condPartido = " AND EXISTS (Select * from partido where partido.id = '" + ddPartido.SelectedValue + "' AND l.partidoId = partido.id )"; }
        ddLocalidad.Items.Clear();
        sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketDB"].ToString();
        sql.Open();
        query.Connection = sql;
        query.CommandText = "SELECT l.id, l.nombre FROM localidad l WHERE l.provinciaId = '" + ddProvincia.SelectedValue + "'" + condPartido + " order by l.nombre";

        rs = query.ExecuteReader();
        ddLocalidad.Items.Add(new RadComboBoxItem("[Seleccione una opción]", "-1"));
        ddLocalidad.Items[0].Selected = true;

        while (rs.Read())
        {
            ddLocalidad.Items.Add(new RadComboBoxItem(rs[1].ToString().Split('(')[0], rs[0].ToString()));
        }
        rs.Close();
        sql.Close();

    }

    protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        cargarPartidos();
        cargarLocalidades();
    }
    protected void ddlPartido_SelectedIndexChanged(object sender, EventArgs e)
    {
        cargarLocalidades();
    }
    protected void ddlLocalidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddLocalidad.SelectedValue != "-1")
        { txtLocalidad.Text = ddLocalidad.SelectedItem.Text; }
    }

    protected void lnkClienteExistente_Click(object sender, EventArgs e)
    {
        Response.Redirect("vercliente.aspx?codcte=" + lnkCliente.Text);
    }
    protected void btInfoTelOk_Click(object sender, EventArgs e)
    {
        txtDomicilio.Text = lblDomicilio.Text;
        txtCodPostal.Text = lblCP.Text;
        foreach (RadComboBoxItem i in ddProvincia.Items)
        {
            if (i.Text.Trim().Like(lblProvincia.Text.Trim())) { ddProvincia.SelectedValue = i.Value; }
        }
        ddlProvincia_SelectedIndexChanged(this, new EventArgs());
        if (lblLocalidad.Text.Trim() != "")
        {
            ddLocalidad.SelectedValue = valorLocalidad(lblLocalidad.Text);
        }
        panelDatosSugeridos.Visible = false;
    }

    private string valorLocalidad(string localidad)
    {
        foreach (RadComboBoxItem itm in ddLocalidad.Items)
        {
            if (itm.Text.ToUpper().Trim() == localidad.ToUpper().Trim())
            {
                return itm.Value;
            }
        }
        return "-1";
    }

    protected void btInfoTelOk0_Click(object sender, EventArgs e)
    {
        panelDatosSugeridos.Visible = false;
    }

    private void habilitarTodo(bool b)
    {
        foreach (Control ctrl in panelTicket.Controls)
        {
            if (ctrl.GetType() == txtFax.GetType()) { TextBox tb = (TextBox)ctrl; tb.Enabled = b; }
            if (ctrl.GetType() == ddLocalidad.GetType()) { RadComboBox tb = (RadComboBox)ctrl; tb.Enabled = b; }
            if (ctrl.GetType() == btnSubmit.GetType()) { Button tb = (Button)ctrl; tb.Enabled = b; }
        }
    }
    protected void lnkCliente_Click(object sender, EventArgs e)
    {
        Response.Redirect("vercliente.aspx?codcte=" + lnkCliente.Text);
    }
    protected void ddPartido_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        cargarLocalidades();
    }
    protected void ddLocalidad_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (ddLocalidad.SelectedValue != "-1")
        { txtLocalidad.Text = ddLocalidad.SelectedItem.Text; }
        else { txtLocalidad.Text = ""; }
    }
    protected void ddProvincia_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        cargarPartidos();
        cargarLocalidades();
    }

    //protected void btSelCliente_Click(object sender, EventArgs e)
    //{
    //    if (Request.QueryString["modal"] != null)
    //    {
    //        String Script = "<Script> var ret=new Array( '" + txtCodEncontrado.Text + "','" + txtRSEncontrada.Text + "') ; " +
    //                        " window.returnValue=ret; " +
    //                        " window.close(); </Script>";
    //        Response.Write(Script);
    //    }
        




    //}
    protected void btSelCliente_Click(object sender, EventArgs e)
    {
        txtCodigo.Text = txtCodEncontrado.Text;
        txtRazonSocial.Text = txtRSEncontrada.Text;
        CerrarPopup();
    }
    private void CerrarPopup()
    {
        Dictionary<String, String> valores = new Dictionary<string, string>();
        valores["clienteId"] = txtCodigo.Text;
        valores["razonSocial"] = txtRazonSocial.Text;
        valores["Ani"] = txtTelefono.Text;
        PopupMgr2.Cerrar(this, "retCliente", valores);
    }

}