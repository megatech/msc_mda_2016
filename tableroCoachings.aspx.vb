﻿Imports Telerik.Web.UI
Imports DAOs
Imports System.IO

Partial Class tableroCoachings
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
        If u IsNot Nothing Then
            If u.Perfil <> "20" And u.Perfil <> "1" And u.Perfil <> "17" Then
                Response.Redirect("default2.aspx")
            End If
        End If
    End Sub

    Protected Sub gridTablero_DataBound(sender As Object, e As EventArgs) Handles gridTablero.DataBound
        For Each i As GridDataItem In gridTablero.Items
            Dim btVerCoach = TryCast(i.Cells(2).Controls(0), ImageButton)
            Dim btJustificacion = TryCast(i.Cells(27).Controls(0), ImageButton)
            Dim btReplica = TryCast(i.Cells(28).Controls(0), ImageButton)
            Dim key = i.KeyValues.Replace("{", "").Replace("}", "").Split(":")(1).Replace("""", "")
            btJustificacion.OnClientClick = "window.radopen(""popupJustificacion.aspx?id=" & key & """,""justificacion""); return false;"
            btReplica.OnClientClick = "window.radopen(""popupReplica.aspx?id=" & key & """,""replica""); return false;"
            btVerCoach.OnClientClick = "window.radopen(""verCoachPopNuevo.aspx?coachingid=" & key & """,""monitoreo""); return false;"
        Next
        mostrarEstadisticas()
    End Sub

    Private Sub mostrarEstadisticas()
        Dim condLider = "", condOper = "", condDesde = "", condHasta = "", condAutor = ""
        If Not ddLider.SelectedValue = "-1" Then condLider = " AND LiderUid=" & ddLider.SelectedValue
        If Not ddOperador.SelectedValue = "-1" Then condOper = " AND Usuarioid=" & ddOperador.SelectedValue
        If dpDesde.SelectedDate IsNot Nothing Then condDesde = " AND convert(datetime,fecha,103) >= cast('" & dpDesde.SelectedDate.Value.Date.ToString("yyyyMMdd") & " 00:00:00' as datetime)"
        If dpDesde.SelectedDate IsNot Nothing Then condHasta = " AND convert(datetime,fecha,103) <= cast('" & dpHasta.SelectedDate.Value.Date.ToString("yyyyMMdd") & " 23:59:59' as datetime)"
        If ddAutor.SelectedValue <> "todos" Then
            If ddAutor.SelectedValue = "mon" Then
                condAutor = " AND monitoreo=1"
            ElseIf ddAutor.SelectedValue = "lid" Then
                condAutor = " AND monitoreo=0"
            End If
        End If
        dsTablero.SelectCommand = " SELECT [id], usuarioId,[Operador],[Lider], [fecha], [fechaDevolucion], pec,penc, " & _
         "  [PEC(%)] AS column1, [PENC(%)] AS column2, [ECN1], [ECN2], [ECN3], [ECN4], [ECN5],[ECN6]," & _
     " [ECU1], [ECU2], [ECU3], [ECU4], [ENC1], [ENC2], [ENC3], [ENC4], [ENC5],[ENC6], [Autor] FROM [viewTableroCoaching_Nuevo] WHERE 1=1 " & condLider & condOper & condDesde & condHasta & condAutor

        lblValConCoaching.Text = getScalar("SELECT count(Distinct usuarioId) FROM (" & dsTablero.SelectCommand & ") a")
        lblValSinCoachings.Text = getScalar("SELECT count(Distinct usuarioId) FROM viewOperadores vo where 1=1 " & condLider & " AND vo.usuarioId not in (SELECT usuarioId From viewTableroCoaching_Nuevo where 1=1" & condLider & condOper & condDesde & condHasta & condAutor & ")")
        Dim sinC As Integer, conC As Integer
        sinC = lblValSinCoachings.Text
        conC = lblValConCoaching.Text
        lblValCumplimiento.Text = (Math.Round(conC / (sinC + conC), 2) * 100).ToString & "%"
        lblValPEC.Text = getScalar("SELECT avg(pec) FROM (" & dsTablero.SelectCommand & ") a") & "%"

        Dim P = getScalar("SELECT avg(penc) FROM (" & dsTablero.SelectCommand & ") a")

        If P Is DBNull.Value Then
            lblValPENC.Text = "%"
        Else
            lblValPENC.Text = P.ToString().Substring(0, P.ToString().IndexOf(",")) & "%"
        End If
      

        estadisticas.Visible = condOper = ""
        winMgr.Windows(3).NavigateUrl = "popupOperSinCoach.aspx?l=" & condLider & "&q=" & condLider & condDesde & condHasta & condAutor
    End Sub

    Protected Sub RadGrid1_PageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles gridTablero.PageIndexChanged
        actualizarGrid()
    End Sub

    Protected Sub RadGrid1_PageSizeChanged(sender As Object, e As GridPageSizeChangedEventArgs) Handles gridTablero.PageSizeChanged
        actualizarGrid()
    End Sub

    Protected Sub ddLider_DataBound(sender As Object, e As EventArgs) Handles ddLider.DataBound
        Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
        Dim val = buscarItemEnCombo(ddLider, u.UsuarioId)
        If val IsNot Nothing Then
            'ddLider.Enabled = False
            ddLider.SelectedValue = val
            actualizarOperadores()
        End If

    End Sub

    Protected Sub ddLider_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddLider.SelectedIndexChanged
        actualizarOperadores()
        actualizarGrid()
    End Sub

    Public Sub actualizarGrid()
        Dim condLider = "", condOper = "", condDesde = "", condHasta = "", condAutor = ""
        If Not ddLider.SelectedValue = "-1" Then condLider = " AND LiderUid=" & ddLider.SelectedValue
        If Not ddOperador.SelectedValue = "-1" Then condOper = " AND Usuarioid=" & ddOperador.SelectedValue
        If dpDesde.SelectedDate IsNot Nothing Then condDesde = " AND convert(datetime,fecha,103) >= cast('" & dpDesde.SelectedDate.Value.Date.ToString("yyyyMMdd") & " 00:00:00' as datetime)"
        If dpDesde.SelectedDate IsNot Nothing Then condHasta = " AND convert(datetime,fecha,103) <= cast('" & dpHasta.SelectedDate.Value.Date.ToString("yyyyMMdd") & " 23:59:59' as datetime)"
        If ddAutor.SelectedValue <> "todos" Then
            If ddAutor.SelectedValue = "mon" Then
                condAutor = " AND monitoreo=1"
            ElseIf ddAutor.SelectedValue = "lid" Then
                condAutor = " AND monitoreo=0"
            End If
        End If
        dsTablero.SelectCommand = " SELECT [id], usuarioId,[Operador],[Lider], [fecha], [fechaDevolucion], pec,penc," & _
       " [PEC(%)] AS column1, [PENC(%)] AS column2, [ECN1], [ECN2], [ECN3], [ECN4], [ECN5],[ECN6], [ECU1], [ECU2], [ECU3], [ECU4], [ENC1], [ENC2], [ENC3], [ENC4], [ENC5],[ENC6], [Autor] FROM [viewTableroCoaching_Nuevo] WHERE 1=1" & condLider & condOper & condDesde & condHasta & condAutor

        'SELECT [id], [UsuarioId],[Operador],[Lider], [fecha], [fechaDevolucion], [PEC(%)] AS column1, [PENC(%)] AS column2, [pec], [penc], [ECN1], [ECN2], [ECN3], [ECN4], [ECN5], [ECU1], [ECU2], [ECU3], [ECU4], [ECU5], [ECU6], [ECU7], [ENC1], [ENC2], [ENC3], [ENC4], [ENC5], [Autor] FROM [viewTableroCoaching] WHERE 1=1" & condLider & condOper & condDesde & condHasta & condAutor
        gridTablero.DataBind()
    End Sub

    Public Sub actualizarOperadores()
        Dim condLider = ""
        If Not ddLider.SelectedValue = "" And Not ddLider.SelectedValue = "-1" Then condLider = " AND vl.liderUID = " & ddLider.SelectedValue

        dsOperador.SelectCommand = "Select  'Todos los Operadores' as nombreCompleto, '-1' as usuarioId UNION Select '[' +cast(u.usuarioId as varchar) +'] ' + rtrim(u.nombrecompleto) as nombreCompleto,u.usuarioId from usuario u inner join viewLider vl on vl.usuarioId=u.usuarioId where u.perfilid in (26,28,32) and u.fechabaja is null" & condLider & " Order by usuarioId"
        ddOperador.DataBind()
        ddOperador.SelectedValue = -1
    End Sub

    Protected Sub RadGrid1_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gridTablero.ItemCommand
        If e.CommandName = "devolucion" And e.Item IsNot Nothing Then
            If IsNumeric(e.Item.Cells(3).Text) Then
                winMgr.Windows.Clear()
                Dim wDev = New RadWindow
                wDev.CenterIfModal = True
                wDev.Modal = True
                wDev.VisibleOnPageLoad = True
                Dim lbl As Label = New Label
                lbl.Text = getScalar("SELECT justificacion from coachingNuevo where id =" & e.Item.Cells(3).Text)
                wDev.Title = "Devolución"
                wDev.DestroyOnClose = True
                wDev.Width = 400
                wDev.Height = 300
                wDev.ContentContainer.Controls.Add(lbl)
                wDev.DestroyOnClose = True
                winMgr.Windows.Add(wDev)
            ElseIf e.CommandName = "replica" Then
                winMgr.Windows.Clear()
                Dim wDev = New RadWindow
                wDev.CenterIfModal = True
                wDev.Modal = True
                wDev.VisibleOnPageLoad = True
                Dim lbl As Label = New Label
                lbl.Text = getScalar("SELECT isnull(replica,'El operador aún no ha confirmado la devolución del monitoreo.') from coachingNuevo where id =" & e.Item.Cells(3).Text)
                wDev.Title = "Replica"
                wDev.DestroyOnClose = True
                wDev.Width = 400
                wDev.Height = 300
                wDev.ContentContainer.Controls.Add(lbl)
                wDev.DestroyOnClose = True
                winMgr.Windows.Add(wDev)
            ElseIf e.CommandName = "ver" Then
                winMgr.Windows.Clear()
                Dim wDev = New RadWindow
                wDev.CenterIfModal = True
                wDev.Modal = True
                wDev.VisibleOnPageLoad = True
                wDev.Title = "Detalle Monitoreo"
                wDev.NavigateUrl = "verCoachPopNuevo.aspx?coachingid=" & e.Item.Cells(3).Text
                wDev.Width = 800
                wDev.Height = 600
                wDev.DestroyOnClose = True
                winMgr.Windows.Add(wDev)
            End If
        End If
    End Sub

    Private Function buscarItemEnCombo(dd As RadComboBox, p2 As String) As String
        For Each i As RadComboBoxItem In dd.Items
            If i.Value = p2 Then Return p2
        Next
        Return Nothing
    End Function
    'para saber el primer dia del mes 
    Function PrimerDiaDelMes(ByVal dtmFecha As Date) As Date
        Return DateSerial(Year(dtmFecha), Month(dtmFecha), 1)
    End Function

    'para saber el ultimo dia del mes 
    Function UltimoDiaDelMes(ByVal dtmFecha As Date) As Date
        Return DateSerial(Year(dtmFecha), Month(dtmFecha) + 1, 0)
    End Function


    Protected Sub ddOperador_DataBound(sender As Object, e As EventArgs) Handles ddOperador.DataBound
        If Not IsPostBack Then
            dpDesde.SelectedDate = PrimerDiaDelMes(Now)
            dpHasta.SelectedDate = UltimoDiaDelMes(Now)
            actualizarGrid()
        End If
    End Sub

    Protected Sub gridTablero_ItemCreated(sender As Object, e As EventArgs) Handles gridTablero.DataBound
        For Each i As GridDataItem In gridTablero.Items

            For n As Integer = 11 To 26 Step 1
                Dim c As New TableCell
                c = i.Cells(n)
                If c.Text.Trim <> "&nbsp;" Then
                    c.Controls.Add(New CheckBox())
                    Dim Chk = TryCast(c.Controls(0), CheckBox)
                    Chk.Checked = c.Text = "True"
                    Chk.Text = " "
                    Chk.InputAttributes.Add("class", "css-checkbox")
                    Chk.Enabled = False
                    Chk.LabelAttributes.Add("class", "css-label lite-green-check")
                Else
                    c.Text = "-"
                    c.Font.Bold = True
                    c.HorizontalAlign = HorizontalAlign.Center
                    c.Font.Size = FontUnit.Large
                End If
            Next
        Next
    End Sub

    Protected Sub dpDesde_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles dpDesde.SelectedDateChanged
        actualizarGrid()
    End Sub

  
    Protected Sub btExportar_Click(sender As Object, e As EventArgs) Handles btExportar.Click
        Dim table As HtmlTable = radGridToHtmlTable(gridTablero)
        Dim desc = ddLider.SelectedItem.Text & " > " & ddOperador.SelectedItem.Text
        Dim rango = " | Desde: [" & dpDesde.SelectedDate & "] Hasta: [" & dpHasta.SelectedDate & "]"
        exportarAExcel("Reporte Monitoreo [" & Now & "]", desc, rango, table)
    End Sub

    Private Function radGridToHtmlTable(ByVal grid As RadGrid) As HtmlTable
        grid.AllowPaging = False
        actualizarGrid()
        Dim dataColumns As New ArrayList
        Dim columnGroups As New ArrayList
        Dim table As HtmlTable = New HtmlTable
        table.Rows.Add(New HtmlTableRow) 'Adding group headers
        table.Rows.Add(New HtmlTableRow) 'Adding global headers
        For Each col As GridColumn In grid.Columns
            If "GridCheckBoxColumn GridBoundColumn".Contains(col.ColumnType) Then
                Dim column = New ArrayList
                column.Add(col.OrderIndex)
                column.Add(col.ColumnType)
                dataColumns.Add(column)
                Dim cell1 As New HtmlTableCell
                Dim cell2 As New HtmlTableCell

                If col.ColumnGroupName = "" Then
                    cell1.RowSpan = 2
                    cell1.InnerText = col.HeaderText
                    table.Rows(0).Cells.Add(cell1)
                Else
                    If Not columnGroups.Contains(col.ColumnGroupName) Then
                        columnGroups.Add(col.ColumnGroupName)
                        cell1.InnerText = getGroupHeaderText(grid, col.ColumnGroupName)
                        cell1.ColSpan = countGroupColumns(grid, col.ColumnGroupName)
                        table.Rows(0).Cells.Add(cell1)
                        cell1.Align = "center"
                    End If
                    cell2.InnerText = col.HeaderText
                    table.Rows(1).Cells.Add(cell2)
                End If
            End If
        Next
        For hr = 0 To 1 Step 1
            table.Rows(hr).Style.Add("background-color", "#333333")
            table.Rows(hr).Style.Add("color", "white")
            table.Rows(hr).Style.Add("font-weight", "bold")
        Next
        For Each i As GridDataItem In grid.Items
            Dim row As New HtmlTableRow
            For Each c As ArrayList In dataColumns
                Dim cell As New HtmlTableCell
                If c(1) = "GridCheckBoxColumn" Then
                    Dim ctrl As Control = i.Cells(c(0)).Controls(0)
                    'Dim img As Image = TryCast(ctrl, Image)
                    'cell.InnerText = IIf(img.ImageUrl = "images/tick.png", "SI", "NO")
                    cell.InnerText = IIf(TryCast(ctrl, CheckBox).Checked, "SI", "NO")
                ElseIf c(1) = "GridBoundColumn" Then
                    cell.InnerText = HttpUtility.HtmlDecode(i.Cells(c(0)).Text)
                End If
                row.Cells.Add(cell)
            Next
            table.Rows.Add(row)
        Next
        grid.AllowPaging = False
        actualizarGrid()
        Return table
    End Function

    Public Function countGroupColumns(grid As RadGrid, groupName As String) As Integer
        Dim count = 0
        For Each col As GridColumn In grid.Columns
            If col.ColumnGroupName = groupName Then count += 1
        Next
        Return count
    End Function

    Protected Sub ddOperador_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddOperador.SelectedIndexChanged
        actualizarGrid()
    End Sub

    Private Function getGroupHeaderText(grid As RadGrid, p2 As Object) As String
        For Each g As GridColumnGroup In grid.MasterTableView.ColumnGroups
            If g.Name = p2 Then Return g.HeaderText
        Next
        Return ""
    End Function

    Private Sub exportarAExcel(ByVal titulo As String, desc As String, Rangofechas As String, ByRef t As HtmlTable)
        Try
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim panel As New Panel
            Dim lblTitle As New Label
            Dim lblDesc As New Label
            Dim panelDesc As New Panel
            Dim panelRangoFechas As New Panel
            Dim lblRango As New Label
            Dim panelEstadisticas As New Panel
            If estadisticas.Visible Then
                Dim lblEstadisticas = New Label
                lblEstadisticas.Text &= lblCumplimiento.Text & " " & lblValCumplimiento.Text & " | "
                lblEstadisticas.Text &= lblConCoaching.Text & " " & lblValConCoaching.Text & " | "
                lblEstadisticas.Text &= "Operadores sin monitoreo: " & lblValSinCoachings.Text & " | "
                lblEstadisticas.Text &= lblPEC.Text & " " & lblValPEC.Text & " | "
                lblEstadisticas.Text &= lblPENC.Text & " " & lblValPENC.Text
                lblEstadisticas.Font.Bold = True
                panelEstadisticas.Controls.Add(lblEstadisticas)
            End If

            lblRango.Text = Rangofechas
            lblTitle.Text = titulo
            lblTitle.Font.Size = New FontUnit(13)
            lblTitle.Font.Bold = True
            lblDesc.Text = desc
            lblDesc.Font.Bold = True
            lblRango.Font.Bold = True
            panelDesc.Controls.Add(lblDesc)
            panelDesc.Controls.Add(lblRango)
            panel.Controls.Add(lblTitle)
            panel.Controls.Add(panelDesc)
            If estadisticas.Visible Then panel.Controls.Add(panelEstadisticas)
            panel.Controls.Add(t)
            panel.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=ReporteMonitoreo_" & DateTime.Now.ToString("dd-MM-yyyy_HH.mm") & ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write("<p><br><p>")
            Response.Write(sb.ToString())
            Response.End()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddAutor_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddAutor.SelectedIndexChanged
        actualizarGrid()
    End Sub
End Class
