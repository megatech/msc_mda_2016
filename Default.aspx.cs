using System;
using System.Configuration;
using App_Code.TicketsLib;
using System.Data.SqlClient;


public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Usuario"] != null) {
            if (Request.QueryString["url"] != null) Response.Redirect(Request.QueryString["url"].ToString()); 
            Response.Redirect("~/Default2.aspx");
        }

        lblMensajeError.Text = "";
        if (!IsPostBack)
        {
            String usr = Request.QueryString["usr"];
            String msg = Request.QueryString["msg"];
            txtUsuario.Text = usr;
            lblMensajeError.Text = msg;
        }
        //lblVer.Text = Funciones.getScalar("Select version From proyectos where nombre='MSC_Speedy'").ToString();
        lblVer.Text = "201505.0";
    }

    protected void btnIngresar_Click(object sender, EventArgs e)
    {
        string connStr = ConfigurationManager.ConnectionStrings["TicketDB"].ConnectionString;
        int nCodePage = int.Parse(Config.getValue("Codepage"));
        Usuario u = new Usuario(connStr,nCodePage);

        try
        {
            u.Nick = this.txtUsuario.Text;
            u.Clave = this.txtClave.Text;
            bool ok = u.checkNick();
            //ok = true;
            if (ok)
            {
                Session["Usuario"] = u;
                Session["sessid"] = u.Nick;
                Session["perfil"] = Int32.Parse(u.Perfil);
                //verificarNotificacionesCoachings(u.UsuarioId);
                if (Request.QueryString["url"] != null) Response.Redirect(Request.QueryString["url"].ToString());
                Response.Redirect("~/Default2.aspx");
            }
            else
            {
                lblMensajeError.Text = "La clave o el usuario son erróneos";
            }
        }
        catch (CustomExceptions.UserLoginException ex)
        {
            lblMensajeError.Text = ex.Message;
            //enviarMail("jpcamejo@megatech.la", "ukolev@megatech.la,jpcamejo@megatech.la", "ERROR DE THERA", ex.ToString());
        }
 
    }

    private void verificarNotificacionesCoachings(string usrId)
    {
        SqlConnection SQL = new SqlConnection(ConfigurationManager.ConnectionStrings["TicketDB"].ToString());
        SqlCommand CMD = new SqlCommand("", SQL);
        SqlDataReader RS;
        CMD.CommandText = "SELECT id,fechaNotificacion,cantNotificaciones FROM coaching where confirmado Is Null And cantNotificaciones > 0 And userId=" + usrId;
        SQL.Open();
        RS = CMD.ExecuteReader();
        while (RS.Read())
        {
            notificarCoaching(usrId, int.Parse(RS[0].ToString()), DateTime.Parse(RS[1].ToString()), int.Parse(RS[2].ToString()));
        }
        RS.Close();
        SQL.Close();

    }

    private void notificarCoaching(string usrId, int id, DateTime fechaUltimaNotif, int cnt)
    {
        if (cnt > 2 || fechaUltimaNotif.Day == DateTime.Now.Day) { return; }
        if (cnt == 2) { notificarAlUsuario(usrId,id); notificarAlLider(usrId,id); return; }
        notificarAlUsuario(usrId,id);
    }

    private void notificarAlLider(string usrId, int id)
    {
        String fechaCoaching = Funciones.getScalar("Select fecha FROM coaching WHERE id=" + id).ToString();
        String nombreUsuario = Funciones.getScalar("Select nombre FROM usuario WHERE fechabaja is null AND usuarioId=" + usrId).ToString();
        String emailLider = Funciones.getScalar("SELECT top 1 EMail FROM usuario WHERE fechabaja is null AND perfilId=20 AND rtrim(observaciones) IN (SELECT observaciones FROM usuario WHERE usuarioId=" + usrId + ") AND usuarioId In (SELECT usuarioId FROM lider)").ToString();
        String cuerpo = "<html><body><div><h3>Confirmaci&oacute;n de coaching</h3></div>" +
            "<div>El usuario " + nombreUsuario + "registra un coaching de la fecha " + fechaCoaching + " pendiente de confirmación y ha sido notificado por 3ra vez.</div>" +
            "<div>Si desea ver el coaching al que hace referencia este mensaje, haga click en el siguiente enlace: <a href=http://mscmda.mega.com.ar/vercoach.aspx?coachingid=" + id + "> Ver Coaching </a></div></body></html>";
        FUNCIONES_MOD.mailmega2.enviamail("megarobot@megatech.la", emailLider, "megarobot@megatech.la", "Coaching sin confirmar", cuerpo);
    }

    private void notificarAlUsuario(string usrId, int id)
    {
        String fechaCoaching = Funciones.getScalar("Select fecha FROM coaching WHERE id=" + id).ToString();
        String emailUsuario = Funciones.getScalar("Select EMail FROM usuario WHERE fechabaja is null AND usuarioId=" + usrId).ToString();
        String cuerpo = "<html><body><div><h3>Confirmaci&oacute;n de coaching (" + fechaCoaching + ")</h3></div>" +
            "<div>Su usuario registra un coaching pendiente de confirmaci&oacute;n. Por favor, haga click en el siguiente enlace para finalizar dicha operación.</div>" +
            "<div><a href=http://mscmda.mega.com.ar/confirmarCoaching.aspx?id=" + id + "> Confirmar Coaching</a><div></body></html>";
        FUNCIONES_MOD.mailmega2.enviamail("megarobot@megatech.la", emailUsuario, "megarobot@megatech.la", "Confirmar Coaching [" + fechaCoaching + "]", cuerpo);
        registrarNotificacion(id);
    }

    private void registrarNotificacion(int id)
    {
        SqlConnection SQL = new SqlConnection(ConfigurationManager.ConnectionStrings["TicketDB"].ToString());
        SqlCommand CMD = new SqlCommand("", SQL);
        CMD.CommandText = "UPDATE coaching SET cantNotificaciones=cantNotificaciones+1, fechaNotificacion=getDate() WHERE id=" + id;
        SQL.Open();
        CMD.ExecuteNonQuery();
        SQL.Close();
    }

    public void enviarMail(string _from, string _to, string _subject, string _msg)
    {
        System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(_from, _to, _subject, _msg);
        message.IsBodyHtml = true;
        System.Net.Mail.SmtpClient emailClient = new System.Net.Mail.SmtpClient("mail.megatech.la");
        System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("jpcamejo@megatech.la", "kme.,b1486", "megatech");
        emailClient.UseDefaultCredentials = false;
        emailClient.Credentials = SMTPUserInfo;
        emailClient.Send(message);
    }

    protected void lnkCambioClave_Click(object sender, EventArgs e)
    {
        if (txtUsuario.Text.Trim().Length > 0)
        {
            Server.Transfer("CambioClave.aspx?usr=" + txtUsuario.Text.Trim());
        }
        else
        {
            lblMensajeError.Text = "Debe ingresar el nombre del usuario a cambiar de clave";
        }
    }

}
