﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using App_Code.TicketsLib;

public partial class usuarioagenda : System.Web.UI.Page
{
    public string usuarioid;
    protected void Page_Load(object sender, EventArgs e)
    {
        usuarioid = Request.QueryString["id"];
        if (!IsPostBack)
        {
            try
            {
                string condicion = Funciones.getScalar("SELECT isnull([Condicion],'') FROM[UsuarioCondicionAgenda] where UsuarioId = " + usuarioid).ToString();
                string popup = Funciones.getScalar("SELECT isnull([popup],'') FROM[UsuarioCondicionAgenda] where UsuarioId = " + usuarioid).ToString();
                txtop.Text = condicion;
                txtoppu.Text = popup;
            }
            catch (Exception)
            {

           
            }
       
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        string agestado = "";
        string agespe = "";
        string agnego = "";
        string and1 = "";
        string and2 = "";
        if (ckestado.Checked)
        {
            agestado = " Ticket.estadoid in ( " + GetCheckedItems(cbestado) + ") ";
            and1 = "and";
            and2 = "and";

        }
        if (ckespecialista.Checked)
        {
            agespe = " Ticket.Especialistaid in ( " + GetCheckedItems(cbespecialista) + ") ";
            agestado += and1;
            and2 = "and";
        }

        if (cknegocio.Checked)
        {

            agnego = " Ticket.empresapartnerid in ( " + GetCheckedItems(cbnegocio) + ") ";
            agespe += and2;
        }

        txtop.Text = agestado + agespe + agnego;
    }

    private string GetCheckedItems(RadComboBox cb, Boolean isString = false)
    {
        if (cb.CheckedItems.Count == 0 || cb.CheckedItems.Count == cb.Items.Count) return null;
        else {
            string result = "";
            if (isString)
            {
                foreach (var item in cb.CheckedItems) { result += "'" + item.Value + "',"; }
            }
            else {
                foreach (var item in cb.CheckedItems) { result += item.Value + ","; }
            }
            result = result.TrimEnd(',');
            return result == "" || result == "''" ? null : result;
        }
    }

    protected void btnguardar_Click(object sender, EventArgs e)
    {
        int row = Convert.ToInt16(Funciones.getScalar("SELECT count(*) FROM[UsuarioCondicionAgenda] where UsuarioId = " + usuarioid));
        if (row > 0)
        {
            Funciones.execSql("update UsuarioCondicionAgenda set condicion = '" + txtop.Text + "' where usuarioid =" + usuarioid );
        }
        else
        {
            Funciones.execSql("insert into usuariocondicionagenda (condicion,usuarioid) values ( '" + txtop.Text + "' , " + usuarioid +")");
        }
    }

    protected void btngenerarpu_Click(object sender, EventArgs e)
    {
        string agestado = "";
        string agespe = "";
        string agnego = "";
        string and1 = "";
        string and2 = "";
        if (ckestadopu.Checked)
        {
            agestado = " Ticket.estadoid in ( " + GetCheckedItems(cbestadopu) + ") ";
            and1 = "and";
            and2 = "and";
        }
        if (ckesppu.Checked)
        {
            agespe = " Ticket.Especialistaid in ( " + GetCheckedItems(cbesppu) + ") ";
            agestado += and1;
            and2 = "and";
        }

        if (cknegpu.Checked)
        {

            agnego = " Ticket.empresapartnerid in ( " + GetCheckedItems( cbnegpu) + ") ";
            agespe += and2;
        }

        txtoppu.Text = agestado + agespe + agnego;

    }

    protected void btnguardarpu_Click(object sender, EventArgs e)
    {
        int row = Convert.ToInt16(Funciones.getScalar("SELECT count(*) FROM[UsuarioCondicionAgenda] where UsuarioId = " + usuarioid));
        try
        {
            if (row > 0)
            {
                Funciones.execSql("update usuariocondicionagenda set popup = '" + txtoppu.Text + "' where usuarioid =" + usuarioid);
            }
            else
            {
                Funciones.execSql("insert into usuariocondicionagenda (popup,usuarioid) values ( '" + txtoppu.Text + "' ," + usuarioid + ")");
            }
            lblmsg.Text = "Los datos se guardaron correctamente";
            lblmsg.ForeColor = System.Drawing.Color.Green;
        }
        catch (Exception ex)
        {

            lblmsg.Text = "Error al guardar los datos " + ex.Message ;
            lblmsg.ForeColor = System.Drawing.Color.Red;
        }
       
    }
}