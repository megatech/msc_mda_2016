﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default2.aspx.vb" Inherits="Default2" MasterPageFile="TicketsPage.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" runat="server">
    <script src="js/calculosDuracionTkts.js"></script>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" runat="server">

    <div>
       
        <h2>
        <asp:Timer ID="Timer1" runat="server" Interval="20000">
        </asp:Timer>
        Bandeja de Tickets</h2>
        <telerik:RadTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="0">
            <Tabs>
                <telerik:RadTab runat="server" Selected="True" Text="BANDEJA">
                </telerik:RadTab>
                <telerik:RadTab runat="server" Text="PENDIENTES">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPage1" runat="server">
        </telerik:RadMultiPage>
         

    <table width="100%"><tr>
                    <td align="center">
                            <table>
        <tr>
            <td style="width: 164px" nowrap="nowrap">
                &nbsp;</td>
            <td nowrap="nowrap">
                <asp:Label ID="lblCantResTxt" runat="server" Text="Resultados:" Font-Bold="True"
                    ForeColor="#002448" Font-Size="14px"></asp:Label>&nbsp;<asp:Label ID="lblCantResTop" 
                    runat="server" Font-Bold="True" ForeColor="#004080" Font-Size="14px"></asp:Label>
            </td>
            <td width="200px" nowrap="nowrap">
                <asp:Label ID="lblPagTopTxt" runat="server" Text="Página:" Font-Bold="True"
                    ForeColor="#002448" Font-Size="15px"></asp:Label>
                <asp:Label ID="lblPagTop" 
                    runat="server" Font-Bold="True" ForeColor="#004080" Font-Size="15px"></asp:Label>
            </td>
            <td nowrap="nowrap">
                <asp:Label ID="lblResPorPagTopTxt" runat="server" Text="Resultados por página:" Font-Bold="True"
                    ForeColor="#002448" Font-Size="14px"></asp:Label>&nbsp;
                <asp:LinkButton ID="lnkBtTop10" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTop10_Click" Font-Size="14px">10</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkBtTop20" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTop20_Click" Font-Size="14px">20</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkBtTop30" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTop30_Click" Font-Size="14px">30</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkBtTop50" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTop50_Click" Font-Size="14px">50</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkBtTopTodos" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTopTodos_Click" Font-Size="14px">Todos</asp:LinkButton>
            </td>
            <td width="200px" align="right">
                <telerik:RadButton ID="btExportarTop" runat="server" OnClick="btExportar_Click" Text="Exportar a Excel"
                    CssClass="unblockUI" Skin="WebBlue" />
            </td>
        </tr>
    </table>
                    </td>
                </tr>
        <tr><td>
            <div id="exportacion">
        <asp:GridView ID="GridView1" ClientIDMode="Static" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataKeyNames="Ticket" DataSourceID="SqlDataSource1" 
            ForeColor="#333333" GridLines="None" AllowPaging="True" 
            AllowSorting="True" EmptyDataText="No existen datos para mostrar" >
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            
        <Columns>
            <%--<asp:ButtonField ButtonType="Image" CommandName="Select" HeaderText="Editar" 
                ImageUrl="~/Images/Edicion.jpg" Text="Editar" />--%>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink runat="server" Text="Editar" ImageUrl="~/Images/Edicion.jpg" target="_blank" NavigateUrl='<%# Eval("Ticket", "~/Editaticket.aspx?Id={0}")%>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Ticket" HeaderText="Ticket" />
            <asp:BoundField DataField="Ani" HeaderText="ANI" SortExpression="Ani" />              
            <asp:BoundField DataField="Cliente" HeaderText="Cliente" ReadOnly="True" 
                SortExpression="Cliente" />
            <asp:BoundField DataField="Lider" HeaderText="Líder" ReadOnly="True" 
                SortExpression="Lider" />
            <asp:BoundField DataField="Especialista" HeaderText="Especialista" 
                SortExpression="Especialista" />
            <asp:BoundField DataField="EstadoId" Visible="false" SortExpression="EstadoId"/>
            <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="EstadoId"/>
            <asp:BoundField DataField="FechaEstado" SortExpression="FechaEstado"  DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" HtmlEncode="false" HeaderText="F. Cambio Estado"/>
            <asp:BoundField DataField="Fecha creacion" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" HtmlEncode="false" 
                    HeaderText="Fecha de Creación" SortExpression="Fecha creacion" />
            <asp:TemplateField HeaderText="Tiempo de vida" SortExpression="Fecha creacion" ItemStyle-HorizontalAlign="Right">
            </asp:TemplateField>
            <asp:BoundField DataField="FechaUltimaModif" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" HtmlEncode="false" HeaderText="Última actualización" 
                    SortExpression="FechaUltimaModif" />
            <asp:BoundField DataField="FechaReContacto" SortExpression="FechaReContacto" HeaderText="Fecha Re-Contactar" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" HtmlEncode="false"/>
            <asp:BoundField DataField="motivo" SortExpression="motivo" HeaderText="Motivo Re-Contacto" HeaderStyle-Wrap="false"/>
            <asp:TemplateField ShowHeader="False"></asp:TemplateField>
            <asp:BoundField DataField="EstadoId" SortExpression="EstadoId" ShowHeader="false" HeaderStyle-CssClass="hiddenTh"/>
            <asp:BoundField DataField="FechaEstado" SortExpression="FechaEstado" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" HtmlEncode="false" HeaderStyle-CssClass="hiddenTh"/>
            <asp:BoundField DataField="Fecha creacion" visible="false" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" HtmlEncode="false"  HeaderText="Fecha de Creación" SortExpression="Fecha creacion" ItemStyle-Wrap="False"/>
            
            
        </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <AlternatingRowStyle BackColor="#dddddd" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
                </div>
        </td></tr>
        <tr><td  align="center">
             <table>
        <tr>
            <td style="width: 164px" nowrap="nowrap">
                &nbsp;</td>
            <td nowrap="nowrap">
                <asp:Label ID="lblCantResTxt0" runat="server" Text="Resultados:" Font-Bold="True"
                    ForeColor="#002448" Font-Size="15px"></asp:Label>&nbsp;<asp:Label ID="lblCantResBottom" 
                    runat="server" Font-Bold="True" ForeColor="#004080" Font-Size="15px"></asp:Label>
            </td>
            <td width="200px" nowrap="nowrap">
                <asp:Label ID="lblPagBottomTxt" runat="server" Text="Página:" Font-Bold="True"
                    ForeColor="#002448" Font-Size="15px"></asp:Label>
                <asp:Label ID="lblPagBottom" 
                    runat="server" Font-Bold="True" ForeColor="#004080" Font-Size="15px"></asp:Label>
            </td>
            <td nowrap="nowrap">
                <asp:Label ID="lblResPorPagTopTxt0" runat="server" 
                    Text="Resultados por página:" Font-Bold="True"
                    ForeColor="#002448" Font-Size="15px"></asp:Label>&nbsp;
                <asp:LinkButton ID="lnkBtBottom10" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTop10_Click" Font-Size="15px">10</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkBtBottom20" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTop20_Click" Font-Size="15px">20</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkBtBottom30" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTop30_Click" Font-Size="15px">30</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkBtBottom50" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTop50_Click" Font-Size="15px">50</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkBtBottomTodos" runat="server" Font-Bold="True" Font-Underline="True"
                    ForeColor="#004080" onclick="lnkBtTopTodos_Click" Font-Size="15px">Todos</asp:LinkButton>
            </td>
            <td width="200px" align="right">
                <telerik:RadButton ID="btExportarBottom" runat="server" 
                    OnClick="btExportar_Click" CssClass="unblockUI" Text="Exportar a Excel"
                    Skin="WebBlue" style="top: 0px; left: -1px" />
            </td>
        </tr>
    </table>
            </td></tr>
        </table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TicketDB %>">
        </asp:SqlDataSource>   
    <asp:HiddenField runat="server" ID="tkts" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="semaforos" ClientIDMode="Static" />
    </div>


</asp:Content>