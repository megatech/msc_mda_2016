﻿// JScript File
        function openPopup(pagina, opciones) {            
            //window.open(pagina,"Busqueda",opciones);
            window.showModalDialog(pagina,'','');
            return false;
        }
        
        function returnBusqueda(valorId, valorDescrip, campoId, campoDescrip) {
            
            window.opener.document.getElementById(campoId).value = valorId;
            window.opener.document.getElementById(campoDescrip).value = valorDescrip;
            window.close();
        } 
        function Call_Edit_Ticket(nro) {
            alert(nro);
            return false;
        } 

function isDate(strValue, blnFormat, strFormat) {
	var strDate, arrDate;
	var intDay, intMonth, intYear;
	var arrSep = new Array('-', ' ', '/', '.');
	var blnFound = false, blnLeap = false;
	var strSep = '/', intAux, strAux;
	var varReturn;

	if (blnFormat) varReturn = ''; else varReturn = false;

	strDate = strValue;
	if (strDate.length < 1) return varReturn;
	
	for (intAux = 0; intAux < arrSep.length; intAux++) {
		if (strDate.indexOf(arrSep[intAux]) != -1) {
			arrDate = strDate.split(arrSep[intAux]);
			
			if (arrDate.length == 3) {
				strSep = arrSep[intAux];
				intDay = parseInt(arrDate[0], 10);
				intMonth = parseInt(arrDate[1], 10);
				intYear = parseInt(arrDate[2], 10);
			} else if (arrDate.length == 2) {
				strSep = arrSep[intAux];
				intDay = parseInt(arrDate[0], 10);
				intMonth = parseInt(arrDate[1], 10);
				intYear = new Date();
				intYear = intYear.getFullYear();
			} else {
				return varReturn;
			}
			
			blnFound = true;
		}
	}

	if (blnFound == false) {
		if (strDate.length > 5) {
			intDay = parseInt(strDate.substr(0, 2), 10);
			intMonth = parseInt(strDate.substr(2, 2), 10);
			intYear = parseInt(strDate.substr(4), 10);
		}
	}
	
	if (intYear >= 0 && intYear <= 50) {
		intYear = intYear + 2000;
	} else if (intYear >= 51 && intYear <= 99) {
		intYear = intYear + 1900;
	}

	if (isNaN(intDay) || isNaN(intMonth) || isNaN(intYear)) return varReturn;

	if (intYear % 100 == 0) {
		if (intYear % 400 == 0) blnLeap = true;
	} else {
		if ((intYear % 4) == 0) blnLeap = true;
	}

	if (intYear > 2100) return varReturn;
	if (intMonth > 12 || intMonth < 1) return varReturn;
	if (intDay < 1)	return varReturn;
	if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intDay > 31)) return varReturn;
	if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intDay > 30)) return varReturn;
	if (intMonth == 2 && blnLeap == false && intDay > 28) return varReturn;
	if (intMonth == 2 && blnLeap == true && intDay > 29) return varReturn;

	if (blnFormat) {
		if (!strFormat || strFormat == 'dd/mm/yyyy') {
			strAux = new String(intDay);
			while (strAux.length < 2) strAux = '0' + strAux;
			strDate = strAux + strSep;
	
			strAux = new String(intMonth);
			while (strAux.length < 2) strAux = '0' + strAux;
			strDate = strDate + strAux + strSep;
	
			strAux = new String(intYear);
			while (strAux.length < 4) strAux = '0' + strAux;
			strDate = strDate + strAux;
		} else if (strFormat == 'dd/mm') {
			strAux = new String(intDay);
			while (strAux.length < 2) strAux = '0' + strAux;
			strDate = strAux + strSep;
	
			strAux = new String(intMonth);
			while (strAux.length < 2) strAux = '0' + strAux;
			strDate = strDate + strAux;
		}

		return strDate;
	} else {
		return true;
	}
}

function isTime(strValue, blnFormat) {
	var strTime, arrTime;
	var intHour, intMinute, intSecond;
	var arrSep = new Array(':', '.', ' ');
	var blnFound = false, blnSeconds = false;
	var strSep = ':', intAux, strAux;
	var varReturn;

	if (blnFormat) varReturn = ''; else varReturn = false;

	strTime = strValue;
	if (strTime.length < 1) return varReturn;
	
	for (intAux = 0; intAux < arrSep.length; intAux++) {
		if (strTime.indexOf(arrSep[intAux]) != -1) {
			arrTime = strTime.split(arrSep[intAux]);
			
			if (arrTime.length == 3) {
				//strSep = arrSep[intAux];
				intHour = parseInt(arrTime[0], 10);
				intMinute = parseInt(arrTime[1], 10);
				intSecond = parseInt(arrTime[2], 10);
				blnSeconds = true;
			} else if (arrTime.length == 2) {
				//strSep = arrSep[intAux];
				intHour = parseInt(arrTime[0], 10);
				intMinute = parseInt(arrTime[1], 10);
				intSecond = 0;
			} else {
				return varReturn;
			}
			
			blnFound = true;
		}
	}

	if (blnFound == false) {
		if (strTime.length > 5) {
			intHour = parseInt(strTime.substr(0, 2), 10);
			intMinute = parseInt(strTime.substr(2, 2), 10);
			intSecond = parseInt(strTime.substr(4), 10);
			blnSeconds = true;
		} else if (strTime.length > 3) {
			intHour = parseInt(strTime.substr(0, 2), 10);
			intMinute = parseInt(strTime.substr(2), 10);
			intSecond = 0;
		} else {
			intHour = parseInt(strTime, 10);
			intMinute = 0;
			intSecond = 0;
		}
	}
	
	if (isNaN(intHour) || isNaN(intMinute) || isNaN(intSecond)) return varReturn;

	if (intHour > 23 || intHour < 0) return varReturn;
	if (intMinute > 59 || intMinute < 0) return varReturn;
	if (intSecond > 59 || intSecond < 0) return varReturn;

	if (blnFormat) {
		strAux = new String(intHour);
		while (strAux.length < 2) strAux = '0' + strAux;
		strTime = strAux + strSep;
	
		strAux = new String(intMinute);
		while (strAux.length < 2) strAux = '0' + strAux;
		strTime = strTime + strAux;
	
		if (blnSeconds) {
			strAux = new String(intSecond);
			while (strAux.length < 2) strAux = '0' + strAux;
			strTime = strTime + strSep + strAux;
		}
		
		return strTime;
	} else {
		return true;
	}
}

function isNumeric(strValue, blnFormat, Min, Max, Decimales) {
	var iAux, iChr, bNeg = false, iDec = 0, sEnt, sDec;
	var strNum = ''; 

	for (iAux = 0; iAux < strValue.length; iAux++) {
		iChr = strValue.charCodeAt(iAux)

		if (iChr >= 48 && iChr <= 57) {
			strNum = strNum + strValue.charAt(iAux);
		} else if (iChr == 45 && !bNeg) {
			strNum = '-' + strNum; 
			bNeg = true;
		} else if (iChr == 46 && iDec == 0) {
			strNum = strNum + strValue.charAt(iAux);
			iDec = strNum.length;
		}
	}

	if (Decimales != null && Decimales >= 0) {
		if (iDec > 0) {
			sEnt = strNum.substr(0, iDec - 1);
			sDec = strNum.substr(iDec);
		} else {
			sEnt = strNum;
			sDec = '';
		}
		sDec = sDec.substr(0, Decimales);
		for (iAux = sDec.length; iAux < Decimales; iAux++) sDec = sDec + '0';
		strNum = sEnt + '.' + sDec;
	}
	
	if (strNum.charAt(strNum.length - 1) == '.') strNum = strNum.substr(0, strNum.length - 1);
	if (strNum.charAt(0) == '.') strNum = '0' + strNum;
	if (Min != null && parseFloat(strNum) < Min) strNum = '';
	if (Max != null && parseFloat(strNum) > Max) strNum = '';
	
	return strNum;	
}