﻿Imports App_Code.TicketsLib
Imports Telerik.Web.UI
Imports Popups

Partial Class Default4

    Inherits System.Web.UI.Page

    Protected Sub RadButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadButton1.Click

        Dim u As Usuario = CType(Session("Usuario"), Usuario)

        Dim ticket As String

        ticket = Int(Request.QueryString("id").ToString())
        Dim SQL As New System.Data.SqlClient.SqlConnection
        Dim CMD As New System.Data.SqlClient.SqlCommand
        Dim res As New System.Data.SqlClient.SqlCommand
        Dim lret As String

        SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
        SQL.Open()
        CMD.Connection = SQL
        If ddp1b.Visible Then

        End If
        Dim p1b = "'" & ddp1b.SelectedValue & "'"
        If ddp1b.Visible Then
            p1b = "NULL"
        End If

        CMD.CommandText = "insert into Encuesta_val ( p1,p1b,p2,p3,userid,fecha,ticketid) values ('" & ddp1.SelectedValue & "' , " & p1b & ", " & ddp2.SelectedValue & " , " & ddp3.SelectedValue & " , " & u.UsuarioId & " , '" & Date.Now & "' , " & ticket & " ) "

        lret = CMD.ExecuteNonQuery()
        SQL.Close()
        SQL.Dispose()
        CMD.Dispose()



        If lret <= 0 Then
            Response.Redirect("error.aspx")
        End If
        Dim ret As Dictionary(Of String, String) = New Dictionary(Of String, String)
        ret("EncuestaOk") = "OK"
        Popups.PopupMgr.Cerrar(Me, "retEncuesta", ret)
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim SQL As New System.Data.SqlClient.SqlConnection
        Dim CMD As New System.Data.SqlClient.SqlCommand
        Dim res As New System.Data.SqlClient.SqlCommand
        Dim lret As Integer


        SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
        SQL.Open()
        CMD.Connection = SQL
        CMD.CommandText = "select count(*) from encuesta_val where ticketid = " & Request.QueryString("id").ToString() & ""


        lret = CMD.ExecuteScalar()

        If (lret > 0) Then

            pTable.Visible = False
            RadButton1.Visible = False
            lblerror.Text = " La encuesta ya fue realizada"

        End If
        SQL.Close()
        SQL.Dispose()
        CMD.Dispose()



        If Not ClientScript.IsClientScriptBlockRegistered("cerrar") Then

            ' use next line for direct with <base target="_self"> between <Head> and </HEAD>
            Dim scrp As String = "<script> function cerrar() {window.close();} </script>"
            'Page.RegisterStartupScript("dlgBusqueda", scrp);
            ClientScript.RegisterStartupScript(Me.GetType(), "cerrar", scrp)

            Me.RadButton1.Attributes.Add("onClick", "cerrar();")

        End If
    End Sub



    Protected Sub ddp1_SelectedIndexChanged1(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddp1.SelectedIndexChanged
        ddp1b.Visible = ddp1.SelectedValue = "No"
    End Sub
End Class
