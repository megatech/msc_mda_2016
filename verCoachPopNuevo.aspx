﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="verCoachPopNuevo.aspx.vb" Inherits="verCoachPop" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Monitoreo</title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
                </asp:ScriptReference>
            </Scripts>
        </telerik:RadScriptManager>
    <div style="width: 100%" align="center"> 
    <asp:Panel runat="server" ID="pLider">
      <h2 align="center">Monitoreo&nbsp;
            <img ID="btPrint" runat="server" alt="IMPRIMIR" src="~/Images/imprimir.jpg" Title="Imprimir" style="width:21px;height:21px;" />
      </h2>

      <table runat="server" id="t1" align="center" style="width: 680px" caption="Monitoreo" title="Monitoreo" >
      <tr>
            <td align="right" style="width: 128px">
                Operador
            </td>
            <td align="left" style="width: 102px">
                  <asp:Label ID="lbloper" runat="server" Text=""></asp:Label>
            </td>
            <td align="right" style="width: 109px">
              Escucha
            </td>
            <td align="left" style="width: 131px">
              <asp:Label ID="lblescucha" runat="server" Text=""></asp:Label>
            </td>
      </tr>
      <tr>
         <td align="right" style="width: 128px">
             Fecha
         </td>
         <td align="left" style="width: 102px">
              <asp:Label ID="lblfecha" runat="server" Text=""></asp:Label>
         </td>
        <td align="right" style="width: 109px">
          <asp:ImageButton ID="play"
              runat="server" Height="22px" ImageUrl="~/Images/boton-play.gif" 
              Width="21px" />
          <asp:ImageButton ID="pausa" Visible="false" 
              runat="server" Height="22px" ImageUrl="~/Images/ButtonNormStop.jpg" 
              Width="21px" />
           <asp:HyperLink ID="descargar" runat="server">Descargar</asp:HyperLink>
         </td> 
         <td align="left"><asp:Label ID="lblerror" runat="server" Text="">
            </asp:Label>
         </td>
      </tr>
      <tr>
          <td align="right" style="width: 128px">
              SSID QOOM
          </td>
          <td align="left" style="width: 102px">
              <telerik:RadTextBox ID="SSIDQOOM" runat="server">
              </telerik:RadTextBox>
          </td>
          <td align="right" style="width: 109px">
              ANI
          </td>
          <td align="left" style="width: 131px">
              <telerik:RadTextBox ID="txtani" runat="server">
              </telerik:RadTextBox>
          </td>
      </tr>
      <tr>
          <td align="right" style="width: 128px">
           Tipo de problema
          </td>
          <td align="left" style="width: 102px">
              <asp:Label ID="lbltipoproblema" runat="server" Text="">
              </asp:Label>
          </td>
          <td align="right" style="width: 109px">
           TKT
          </td>
          <td align="left" style="width: 131px">
               <telerik:RadTextBox ID="txttkt" runat="server" AutoPostBack="True" EmptyMessage="Ingrese el nro. TKT">
               </telerik:RadTextBox>
         </td>
      </tr>
      <tr>
          <td align="right" style="width: 128px">
              Devolución
          </td>
          <td align="left" 
           style="width: 102px"><asp:Label ID="lbldev" runat="server" Text=""></asp:Label>
          </td>
          <td align="right" style="width: 109px">
              Autor
          </td>
          <td align="left" style="width: 131px">  
            <asp:Label ID="lblAutor" runat="server" Font-Bold="True"></asp:Label>
          </td>
      </tr>
   </table>
      <br />
      <br />

 <asp:Table ID="Table1" runat="server" Height="59px" Width="841px" 
        Caption="Usuario Final (Sin respeto incorrecto)" CaptionAlign="Top" 
        BorderColor="#3366FF" BorderStyle="Solid">
        <asp:TableRow ID="TableRow1" runat="server" TableSection="TableHeader">
            <asp:TableCell ID="TableCell1" runat="server" Width="40%" HorizontalAlign="Left">
                  1. Gestiona Incorrectamente → Deja asuntos pendientes.
            </asp:TableCell>
            <asp:TableCell ID="TableCell2" runat="server" Width="10%">
               <asp:Label ID="ecn1" runat="server">
               </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server" HorizontalAlign="Left">
                 2. Gestiona Incorrectamente → No cumple los procedimientos para la resolución.
            </asp:TableCell>
            <asp:TableCell ID="TableCell4" runat="server">
                <asp:Label ID="ecn2" runat="server"></asp:Label>  
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell5" runat="server" HorizontalAlign="Left" AutoPostBack="True" >
            3. Gestiona Incorrectamente → No toma la llamada de primera linea.
        </asp:TableCell>
        <asp:TableCell ID="TableCell6" runat="server">
          <asp:Label ID="ecn3" runat="server">
          </asp:Label>  
        </asp:TableCell>
            <asp:TableCell ID="TableCell7" runat="server" HorizontalAlign="Left">
                4. Gestiona Incorrectamente → Red sondeo árbol de diagnóstico.
            </asp:TableCell>
            <asp:TableCell ID="TableCell8" runat="server">
                  <asp:Label ID="ecn4" runat="server">
                  </asp:Label>  
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow3" runat="server">
            <asp:TableCell ID="TableCell9" runat="server" HorizontalAlign="Left">
                5. Informa Incorrectamente → Información desactualizada.
            </asp:TableCell>
            <asp:TableCell ID="TableCell10" runat="server">
            <asp:label ID="ecn5" runat="server" ></asp:label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow7" runat="server">
            <asp:TableCell ID="TableCell21" runat="server" HorizontalAlign="Left">
                6. Informa Incorrectamente → Plazos de solución.
            </asp:TableCell>
            <asp:TableCell ID="TableCell22" runat="server">
            <asp:label ID="ecn6" runat="server" ></asp:label>
            </asp:TableCell>
        </asp:TableRow>
  </asp:Table>
      
  <br />
  <br />
 <asp:Table ID="Table2" runat="server" Caption="Usuario Final (Con respeto incorrecto)"
        Width="841px" BorderColor="#3366FF" BorderStyle="Solid" >
        <asp:TableRow ID="TableRow4" runat="server" TableSection="TableHeader">
            <asp:TableCell ID="TableCell11" runat="server" Width="40%" HorizontalAlign="Left">
                1. Corte de llamada.
            </asp:TableCell>
            <asp:TableCell ID="TableCell12" runat="server" Width="10%">
               <asp:Label ID="ecc1" runat="server" >
               </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell13" runat="server" HorizontalAlign="Left">
                2. Falta actitud de servicio.	
            </asp:TableCell>
            <asp:TableCell ID="TableCell14" runat="server">
                  <asp:Label ID="ecc2" runat="server"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow5" runat="server">
            <asp:TableCell ID="TableCell15" runat="server" HorizontalAlign="Left">
                3. Maltrato a clientes.
            </asp:TableCell>
            <asp:TableCell ID="TableCell16" runat="server">
               <asp:label ID="ecc3" runat="server">
               </asp:label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell17" runat="server" HorizontalAlign="Left">
                4. Se abandona al cliente en hold.
            </asp:TableCell>
            <asp:TableCell ID="TableCell18" runat="server">
                  <asp:Label ID="ecc4" runat="server" >
                  </asp:Label>
            </asp:TableCell>
         </asp:TableRow>
    </asp:Table>
  <br />
  <br />
  <asp:Table ID="Table3" runat="server" Caption="Habilidades Blandas"   
        Width="841px" BorderColor="#3366FF" BorderStyle="Solid" >
        <asp:TableRow>
            <asp:TableCell Width="40%" HorizontalAlign="Left">
                1. Trato brindado.
            </asp:TableCell>
            <asp:TableCell Width="10%">
                 <asp:Label ID="enc1" runat="server" >
                 </asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Left">
               2. Voluntad para resolver.
            </asp:TableCell>
         <asp:TableCell>
             <asp:Label ID="enc2" runat="server" >
             </asp:Label>
         </asp:TableCell>
        </asp:TableRow>

       <asp:TableRow>
        <asp:TableCell Width="40%" HorizontalAlign="Left">
            3. Claridad en la explicación.
        </asp:TableCell>
        <asp:TableCell Width="10%">
            <asp:Label ID="enc3" runat="server" >
            </asp:Label>
         </asp:TableCell>
          <asp:TableCell HorizontalAlign="Left">
              4. Manejo de llamada.	
          </asp:TableCell>
          <asp:TableCell>
                <asp:Label ID="enc4" runat="server" >
                </asp:Label>
         </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow >
            <asp:TableCell HorizontalAlign="Left" >
                5. Información no crítica.
            </asp:TableCell>
         <asp:TableCell>
            <asp:Label ID="enc5" runat="server" >
            </asp:Label>
         </asp:TableCell>
          <asp:TableCell HorizontalAlign="Left" >
                6. Esperas y transferencias.
           </asp:TableCell>
         <asp:TableCell>
            <asp:Label ID="enc6" runat="server" >
            </asp:Label>
         </asp:TableCell>
         <asp:TableCell>
             <asp:Label ID="ENC" runat="server" ForeColor="Red">
             </asp:Label>
         </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

          <br />
          <br />
          <p> Justificación: </p>
      <telerik:RadTextBox ID="txtjustificacion" runat="server" TextMode="MultiLine" 
          Width="90%" Rows="10">
      </telerik:RadTextBox>
                <br />
                <br />          
       <p >
          Replica del operador [Opcional]:
          <asp:Panel ID="PanelReplica" runat="server">
              <telerik:RadTextBox ID="txtReplica" Runat="server"
                   EmptyMessage="Escriba aquí una replica de la devolución que acaba de leer..."
                   Height="130px" Rows="10" Width="754px" TextMode="MultiLine">
              </telerik:RadTextBox>
              <br />
              <br />
              <telerik:RadButton ID="btEnviarReplica" runat="server"
                   Text="Confirmar notificación del monitoreo">
              </telerik:RadButton>
          </asp:Panel>
          <p>
          </p>
          <br />
          <br />
          <br />
          <p>
          </p>
          <p>
          </p>
          <p>
          </p>
          <p>
          </p>
          <p>
          </p>
          <p>
          </p>
          <p>
          </p>
          <p>
          </p>
          <p>
          </p>
          <p>
          </p>
          <p>
          </p>
          </p>
          </asp:Panel>
    </div>

    </form>
</body>
</html>
