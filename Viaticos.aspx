﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Viaticos.aspx.vb" Inherits="Viaticos" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Viaticos</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h3 align="center" class="headerStyle ">
<telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
        Viáticos</h3>
    </div>
        <div>
            <telerik:RadGrid ID="RGVerviaticos" runat="server" Culture="es-ES" DataSourceID="SqlDataSource1" Skin="Silk">
                <MasterTableView AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                    <Columns>
                        <telerik:GridBoundColumn DataField="fecha" DataType="System.DateTime" FilterControlAltText="Filter fecha column" HeaderText="fecha" SortExpression="fecha" UniqueName="fecha">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Transporte" FilterControlAltText="Filter Transporte column" HeaderText="Transporte" SortExpression="Transporte" UniqueName="Transporte">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="km" DataType="System.Decimal" FilterControlAltText="Filter km column" HeaderText="km" SortExpression="km" UniqueName="km">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="costokm" DataType="System.Decimal" FilterControlAltText="Filter costokm column" HeaderText="costokm" SortExpression="costokm" UniqueName="costokm">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="total" DataType="System.Decimal" FilterControlAltText="Filter total column" HeaderText="total" SortExpression="total" UniqueName="total">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>

            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [fecha],  [transpub] as Transporte ,[km], [costokm], [total] FROM [viaticostkt] where ticketid = @ticketid order by fecha">
                <SelectParameters>
                    <asp:QueryStringParameter Name="ticketid" QueryStringField="id" />
                </SelectParameters>
            </asp:SqlDataSource>

        </div>

        <div>
            <table>
                <tr>
                    <td>
                        Tipo transporte:
                    </td>
                    <td>
                        <telerik:RadComboBox AutoPostBack="true"  ID="cbtipo" EmptyMessage="Selecione el tipo de viatico"  runat="server" Culture="es-ES" Skin="Silk">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="Transporte Publico" Value="pub" />
                                <telerik:RadComboBoxItem runat="server" Text="Movilidad Propia" Value="auto" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                </tr>
                </table>
            <asp:Panel ID="pnauto" runat="server" Visible="false" >
            <table id="auto" runat="server"  >
                <tr>
                    <td>
                        Valor KM:
                    </td>
                    <td>
                        <telerik:RadnumericTextBox ID="txtvalkm" runat="server" Skin="Silk" Culture="es-AR" DbValueFactor="1" LabelWidth="64px" MinValue="0" Type="Currency" Width="160px">
                            <NegativeStyle Resize="None" />
                            <NumberFormat ZeroPattern="$ n" />
                            <EmptyMessageStyle Resize="None" />
                            <ReadOnlyStyle Resize="None" />
                            <FocusedStyle Resize="None" />
                            <DisabledStyle Resize="None" />
                            <InvalidStyle Resize="None" />
                            <HoveredStyle Resize="None" />
                            <EnabledStyle Resize="None" />
                        </telerik:RadnumericTextBox>
                    </td>
                </tr>
                <tr>
                    <td>Cantidad:</td>
                    <td>
                        <telerik:RadnumericTextBox ID="txtcankm" runat="server" Skin="Silk" Culture="es-AR" DbValueFactor="1" LabelWidth="64px" MinValue="0" Width="160px"></telerik:RadnumericTextBox></td>
                </tr>
            </table>
                </asp:Panel>
            <asp:Panel ID="pnpubli" Visible="false"  runat="server">
              <table id="publico" runat="server" >
                <tr>
                    <td>
                        Tipo de transporte:
                    </td>
                    <td>
                        <telerik:RadComboBox ID="cbtipopublic" runat="server" Skin="Silk" Culture="es-ES">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="Subte" Value="subte" />
                                <telerik:RadComboBoxItem runat="server" Text="Colectivo" Value="colectivo" />
                                <telerik:RadComboBoxItem runat="server" Text="Tren" Value="tren" />
                                <telerik:RadComboBoxItem runat="server" Text="Taxi" Value="taxi" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td>Precio:</td>
                    <td>
                        <telerik:RadnumericTextBox ID="txtpubprecio" runat="server" Skin="Silk" Culture="es-AR" DbValueFactor="1" LabelWidth="64px" MaxValue="10000" MinValue="0" Type="Currency" Width="160px">
                            <NegativeStyle Resize="None" />
                            <NumberFormat ZeroPattern="$ n" />
                            <EmptyMessageStyle Resize="None" />
                            <ReadOnlyStyle Resize="None" />
                            <FocusedStyle Resize="None" />
                            <DisabledStyle Resize="None" />
                            <InvalidStyle Resize="None" />
                            <HoveredStyle Resize="None" />
                            <EnabledStyle Resize="None" />
                        </telerik:RadnumericTextBox></td>
                </tr>
            </table>
            </asp:Panel>
        </div>
        <div>
            <asp:Label ID="lblerr" runat="server" ForeColor="Red"  Text=""></asp:Label>
            <br />

            <telerik:RadButton ID="btnagregar" Visible="false"  runat="server" Text="Agregar" Skin="Silk"></telerik:RadButton>
        </div>


    </form>
</body>
</html>
