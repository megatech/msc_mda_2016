﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code.TicketsLib


Partial Class Default3
    Inherits System.Web.UI.Page

    Private Structure TKT
        Dim Tkt As Long
        Dim ArticuloId As Long
        Dim TicketPresupuestoId() As Long
        Dim lSerie() As String
        Dim lDeposito() As Long
        Dim sArticuloId() As String
    End Structure

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ususarioactual As Usuario 

        ususarioactual = Session("usuario")

        If ususarioactual.Perfil <> 1 Then
            Response.Redirect("Default2.aspx")
        End If

        Dim SQL As New SqlConnection
        Dim CMD As New SqlCommand
        Dim RS As SqlDataReader
        Dim lItem() As ListItem
        Dim lItemCount As Long = 0

        If Not IsPostBack Then
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
            SQL.Open()
            CMD.Connection = SQL
            CMD.CommandText = "SELECT estadoid,nombre FROM estado"
            RS = CMD.ExecuteReader
            While RS.Read
                ReDim Preserve lItem(lItemCount)
                lItem(lItemCount) = New ListItem
                lItem(lItemCount).Text = RS("nombre").ToString
                lItem(lItemCount).Value = RS("estadoid").ToString
                ddestado.Items.Add(lItem(lItemCount))
            End While
            RS.Close()
            SQL.Close()
            LblEstadoGeneral.Text = "Aguardando Operacion..."
        End If
    End Sub













    Protected Sub CmdAsignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdAsignar.Click
        Dim lTickets() As Long 'MATRIZ QUE CONTIENE LOS NROS DE TICKET A ASIGNAR
        Dim lPartes() As Long 'MATRIZ QUE CONTIENE EL ID DE TODAS LAS PARTES A ASIGNAR
        Dim bStock() As Boolean 'MATRIZ QUE CONTIENE SI LAS PARTES SE ENCUENTRAN EN STOCK
        Dim lDepositoId() As Long 'MATRIZ QUE CONTIENE EL DEPOSITO DE CADA PARTE A ASIGNAR
        Dim lTicketCount As Long = 0 'ESTA VARIABLE SE UTILIZA PARA REDIMENSIONAR Y ASIGNAR DINAMICAMENTE VALORES A LOS ARRAY
        Dim lPartesCount As Long = 0
        Dim bNotFound As Boolean = False 'ESTA VARIABLE SE UTILIZA PARA SABER SI AL MENOS UNA PARTE NO TIENE STOCK

        lEstado.Items.Clear()

        Dim TicketInfo() As TKT
        Dim lCantidadArticulos As Long = 0


        Dim SQL As New SqlConnection
        Dim CMD As New SqlCommand
        Dim RS As SqlDataReader

        'REALIZAMOS LA CONEXION AL SERVIDOR DE BASES DE DATOS
        SQL.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        SQL.Open()
        CMD.Connection = SQL

        'CORROBORAMOS QUE EXISTAN TICKETS EN EL ESTADO SELECCIONADO
        CMD.CommandText = "SELECT COUNT(*) from ticket where estadoid = '" & ddestado.SelectedValue & "'"
        Dim lCantidadTickets As Long = CMD.ExecuteScalar

        If lCantidadTickets = 0 Then
            'NO EXISTEN TICKETS EN EL ESTADO SELECCIONADO
            lEstado.Items.Add("No existen tickets en el estado seleccionado")
            lEstado.Items.Add("Abandoando ejecucion")
            LblEstadoGeneral.Text = "Ha fallado la asignacion, corrobore el Log"
            LblEstadoGeneral.ForeColor = Drawing.Color.IndianRed
            LblEstadoGeneral.Font.Bold = True
            SQL.Close()
            Exit Sub
        End If

        'CONSULTA QUE OBTIENE TODOS LOS NROS DE TICKET ASOCIADOS AL ESTADO SELECCIONADO
        CMD.CommandText = "SELECT ticketid from ticket where estadoid = '" & ddestado.SelectedValue & "'"
        RS = CMD.ExecuteReader

        While RS.Read
            ReDim Preserve TicketInfo(lTicketCount)
            TicketInfo(lTicketCount).Tkt = RS("ticketid")
            lEstado.Items.Add("Agregando el ticket " & RS("ticketid") & " a la coleccion")
            lTicketCount += 1
        End While

        RS.Close()

        'OBTENDREMOS AHORA LA LISTA DE PARTES ASIGNADAS A CADA TICKET
        lTicketCount = 0
        lEstado.Items.Add("")

        For lTicketCount = 0 To UBound(TicketInfo)
            CMD.CommandText = "SELECT COUNT(*) FROM ticketpresupuesto WHERE ticketid = " & TicketInfo(lTicketCount).Tkt
            Dim lCantidadPartes As Long = CMD.ExecuteScalar

            lEstado.Items.Add("Buscando partes para el ticket " & TicketInfo(lTicketCount).Tkt.ToString)

            If lCantidadPartes = 0 Then
                'EL TKT NO TIENE PARTES ASIGNADAS POR LO CUAL DEBERIAMOS ELIMINAR LA ENTRADA DE TICKETINFO
                'SI UN TKT NO TIENE PARTES ASIGNADAS SETEAMOS SU NRO DE TKT A 0 PARA FUTURA REFERENCIA

                lEstado.Items.Add("No existen partes para el ticket " & TicketInfo(lTicketCount).Tkt.ToString)


                TicketInfo(lTicketCount).Tkt = 0



                LblEstadoGeneral.Text = "Ha fallado la asignacion, corrobore el Log"

            Else
                'EL TKT TIENE AL MENOS UNA PARTE ASIGNADA, OBTENEMOS LOS TICKETPRESUPUESTOID
                CMD.CommandText = "SELECT ticketpresupuestoid,articuloid FROM ticketpresupuesto WHERE ticketid = " & TicketInfo(lTicketCount).Tkt
                RS = CMD.ExecuteReader
                lEstado.Items.Add("Se han hallado " & lCantidadPartes & " partes para el ticket " & TicketInfo(lTicketCount).Tkt.ToString)

                While RS.Read
                    'LA CANTIDAD DE ARTICULOS NROS DE SERIE Y DEPOSITOS SERA INDEFECTIBLEMENTE LA MISMA
                    ReDim Preserve TicketInfo(lTicketCount).TicketPresupuestoId(lCantidadArticulos)
                    ReDim Preserve TicketInfo(lTicketCount).lDeposito(lCantidadArticulos)
                    ReDim Preserve TicketInfo(lTicketCount).lSerie(lCantidadArticulos)
                    ReDim Preserve TicketInfo(lTicketCount).sArticuloId(lCantidadArticulos)

                    TicketInfo(lTicketCount).TicketPresupuestoId(lCantidadArticulos) = RS(0).ToString
                    TicketInfo(lTicketCount).sArticuloId(lCantidadArticulos) = RS(1).ToString
                    lEstado.Items.Add("Agregando informacion de estado para el ticket " & TicketInfo(lTicketCount).Tkt)
                    lCantidadArticulos += 1
                End While
                lCantidadArticulos = 0
                RS.Close()
            End If
            'AGREGAMOS UN SEPARADOR EN EL LOG
            lEstado.Items.Add("")
        Next

        'NOS ASEGURAMOS QUE EL RECORDSET ESTE CERRADO
        If Not RS.IsClosed Then RS.Close()

        'REINICIAMOS LOS CONTADORES

        lTicketCount = 0
        lCantidadArticulos = 0
        lEstado.Items.Add("Comenzando la busqueda de STOCK")
        lEstado.Items.Add("")
        SQL.Close()
        SQL.Open()
        CMD.Connection = Nothing
        CMD.Connection = SQL

        'RECORREMOS TODOS LOS TKT
        For lTicketCount = 0 To UBound(TicketInfo)
            'SI NRO DE TKT ES 0 ES PORQUE EL TKT NO TIENE PARTES


            If TicketInfo(lTicketCount).Tkt <> 0 Then
                'RECORREMOS TODAS LAS ASIGNACIONES DENTRO DE CADA TKT
                For lCantidadArticulos = 0 To UBound(TicketInfo(lTicketCount).lSerie)
                    'CORROBORAMOS PRIMERO QUE LA PARTE NO ESTE ASIGNADA
                    CMD.CommandText = "SELECT COUNT(*) FROM instanciaarticulo WHERE ticketpresupuestoid = " & TicketInfo(lTicketCount).TicketPresupuestoId(lCantidadArticulos).ToString
                    Dim ParteAsignada As Long = CMD.ExecuteScalar
                    If ParteAsignada > 0 Then
                        'ESTA PARTE YA ESTA ASIGNADA
                        'CMD.CommandText = "SELECT dep.nombre FROM instanciaarticulo INST inner join deposito_tk dep on INST.depositoid = dep.depositoid"
                        Dim sNombreDeposito As String = CMD.ExecuteScalar
                        'AGREGAMOS LA INFORMACION PARA EL USUARIO DE QUE LA PARTE YA ESTA ASIGNADA
                        lEstado.Items.Add("La parte " & TicketInfo(lTicketCount).sArticuloId(lCantidadArticulos) & " ya se encuentra asignada al deposito " & sNombreDeposito & " Asignada al ticket " & TicketInfo(lTicketCount).Tkt)
                        TicketInfo(lTicketCount).lSerie(lCantidadArticulos) = "0"
                        TicketInfo(lTicketCount).lDeposito(lCantidadArticulos) = -1
                        LblEstadoGeneral.Text = "Ha fallado la asignacion, corrobore el Log"

                    Else
                        'EN CASO DE QUE LA PARTE NO SE ENCUENTRE ASIGNADA, CORROBORAMOS QUE HAYA STOCK DE LA MISMA
                        CMD.CommandText = "SELECT COUNT(*) FROM ViewSeries (nolock) inner join viewdeposito vd on vd.depositoid = ViewSeries.depositoid WHERE ArticuloId = '" & TicketInfo(lTicketCount).sArticuloId(lCantidadArticulos) & "' AND  ViewSeries.DepositoId in (SELECT depositoid FROM [sc_megatech].[dbo].[DepositoFacturable] where DepositoFacturable = 0)"
                        Dim Stock As Long = CMD.ExecuteScalar
                        If Stock <= 0 Then
                            'NO HAY STOCK DE LA PARTE ASIGNADA
                            'SI EL USUARIO NO DESEA ASIGNAR PARTES PARCIALES ESTE NRO DE TICKET TAMBIEN SERA 0
                            'MOTIVO POR EL CUAL NO SE ASIGNARA NINGUNA DE LAS PARTES
                            If ChkAsignaParcial.Checked = False Then
                                lEstado.Items.Add("La parte " & TicketInfo(lTicketCount).sArticuloId(lCantidadArticulos) & " no se encuentra en stock en los depositos seleccionados no se asigna al ticket ")

                                LblEstadoGeneral.Text = "Ha fallado la asignacion, corrobore el Log"
                                TicketInfo(lTicketCount).lSerie(lCantidadArticulos) = "0"
                                TicketInfo(lTicketCount).lDeposito(lCantidadArticulos) = -1
                                TicketInfo(lTicketCount).Tkt = 0
                            Else
                                'ESTO NOS SIRVE PARA SABER QUE ESTA PARTE EN PARTICULAR NO ESTA EN STOCK
                                'MOSTRAMOS EL MENSAJE AL USUARIO SOBRE LA PARTE EN PARTICULAR Y EL NRO DE TKT
                                lEstado.Items.Add("La parte " & TicketInfo(lTicketCount).sArticuloId(lCantidadArticulos) & " no se encuentra en stock en los depositos seleccionados no se asigna al ticket " & TicketInfo(lTicketCount).Tkt)

                                LblEstadoGeneral.Text = "Ha fallado la asignacion, corrobore el Log"
                                TicketInfo(lTicketCount).lSerie(lCantidadArticulos) = "0"
                                TicketInfo(lTicketCount).lDeposito(lCantidadArticulos) = -1
                            End If



                        Else
                            'TENEMOS STOCK DE LA PARTE ASIGNADA, PROCEDEMOS A RECOGER LOS DATOS NECESARIOS
                            CMD.CommandText = "SELECT ViewSeries.DepositoId, vd.nombre as  Deposito,NROSERIE AS NroSeries FROM ViewSeries (nolock) inner join viewdeposito vd on vd.depositoid = ViewSeries.depositoid WHERE ArticuloId = '" & TicketInfo(lTicketCount).sArticuloId(lCantidadArticulos) & "' AND  ViewSeries.DepositoId in (SELECT depositoid FROM [sc_megatech].[dbo].[DepositoFacturable] where DepositoFacturable = 0) ORDER BY NROSERIE"
                            RS = CMD.ExecuteReader
                            RS.Read()
                            'YA TENEMOS CARGADOS EN EL RECORDSET LOS DATOS NECESARIOS (DEBERIA HABERLOS INDEFECTIBLEMENTE)
                            TicketInfo(lTicketCount).lDeposito(lCantidadArticulos) = RS(0)
                            TicketInfo(lTicketCount).lSerie(lCantidadArticulos) = Trim(RS(2))
                            lEstado.Items.Add("Se ha encontrado la parte en el deposito " & RS(1) & " y se gestiono para la asignacion al ticket " & TicketInfo(lTicketCount).Tkt)

                            RS.Close()
                        End If
                    End If
ParteNoEncontrada:
                Next
                lEstado.Items.Add("")
                lCantidadArticulos = 0
            End If
        Next

        lEstado.Items.Add("Se esta comenzando a asignar las partes gestionadas a los Tickets")


        lTicketCount = 0
        lCantidadArticulos = 0


        For lTicketCount = 0 To UBound(TicketInfo)

            'CORROBORAMOS SI A ESTE TICKET ES NECESARIO ASIGNARLE ALGUNA PARTE EN PARTICULAR

            If TicketInfo(lTicketCount).Tkt <> 0 Then
                'DEBEMOS ASIGNAR LAS PARTES CONTENIDAS EN ESTE TKT
                CMD.CommandText = "SP_RESERVA_REPUESTO_TICKET_SNP"
                CMD.CommandType = CommandType.StoredProcedure

                'COMENZAMOS A RECORRER LAS PARTES A ASIGNAR
                For lCantidadArticulos = 0 To UBound(TicketInfo(lTicketCount).lSerie)
                    'CORROBORAMOS SI SE CARGO EL NRO DE SERIE (NO SE CARGA EN CASO DE NO ESTAR LA PARTE EN STOCK)
                    If TicketInfo(lTicketCount).lDeposito(lCantidadArticulos) <> -1 Then
                        'EN ESTA INSTANCIA SIGNIFICA QUE TENEMOS PARTES A ASIGNAR, NO ESTA PREVIAMENTE ASIGNADAS Y SE ENCUENTRAN EN STOCK EN
                        'EL DEPOSITO ESPECIFICADO POR EL MIEMBRO lDeposito DE LA ESTRUCTURA
                        'ESTAMOS EN CONDICIONES DE EJECUTAR NUESTRO STORED PROCEDURE

                        'ESTABLECEMOS LOS ARGUMENTOS DE ENTRADA DEL SP
                        CMD.Parameters.AddWithValue("@TicketId", TicketInfo(lTicketCount).Tkt)
                        CMD.Parameters.AddWithValue("@TicketPresupuestoId", TicketInfo(lTicketCount).TicketPresupuestoId(lCantidadArticulos))
                        CMD.Parameters.AddWithValue("@DepositoId", TicketInfo(lTicketCount).lDeposito(lCantidadArticulos))
                        CMD.Parameters.AddWithValue("@NroSerie", Trim(TicketInfo(lTicketCount).lSerie(lCantidadArticulos)))
                        CMD.Parameters.AddWithValue("@UsuarioId", 216)
                        CMD.Parameters.AddWithValue("@tr", 1)

                        'ESTABLECEMOS LOS ARGUMENTOS DE SALIDA DEL SP
                        CMD.Parameters.Add("@fglError", System.Data.SqlDbType.SmallInt)
                        CMD.Parameters(CMD.Parameters.Count - 1).Direction = System.Data.ParameterDirection.Output
                        CMD.Parameters.Add("@DescError", System.Data.SqlDbType.VarChar, 200)
                        CMD.Parameters(CMD.Parameters.Count - 1).Direction = System.Data.ParameterDirection.Output

                        'Y TARATATANTATAN, EJECUTAMOS EL STORED PROCEDURE PARA EL TICKET ACTUAL CON LA PARTE ACTUAL
                        CMD.ExecuteNonQuery()

                        'AHORA CORROBORAMOS EL ESTADO DE LA EJECUCION PARA EL SP
                        If CMD.Parameters("@fglError").Value = 0 Then
                            'LA PARTE SE ASIGNO CORRECTAMENTE
                            lEstado.Items.Add("Se asigno correctamente la parte con Serie " & TicketInfo(lTicketCount).lSerie(lCantidadArticulos) & " al Ticket " & TicketInfo(lTicketCount).Tkt)
                        Else
                            'FALLO POR ALGUN MOTIVO
                            lEstado.Items.Add("Ocurrio un error asignando la parte con Serie " & TicketInfo(lTicketCount).lSerie(lCantidadArticulos) & " al Ticket " & TicketInfo(lTicketCount).Tkt)
                            LblEstadoGeneral.Text = "Ha fallado la asignacion, corrobore el Log"
                        End If
                        CMD.Parameters.Clear()
                    End If
                Next
                lCantidadArticulos = 0
            End If
        Next
        SQL.Close()

        'HEMOS FINALIZADO LA ASIGNACION, CORROBORAMOS SI SE PRESENTO AL MENOS UN ERROR EN CASO DE SER ASI
        'LO MOSTRAMOS AL USUARIO
        If InStr(LblEstadoGeneral.Text, "Ha fallado") <= 0 Then
            'TODO SALIO BIEN LO INFORMAMOS
            LblEstadoGeneral.ForeColor = Drawing.Color.BlueViolet
            LblEstadoGeneral.Font.Bold = True
            LblEstadoGeneral.Text = "Se han asignado correctamente las partes, corrobore el Log para mayor informacion"
        Else
            LblEstadoGeneral.ForeColor = Drawing.Color.IndianRed
            LblEstadoGeneral.Font.Bold = True
        End If
        'CMD.CommandText = "SELECT ViewSeries.DepositoId, vd.nombre as  Deposito,NROSERIE AS NroSeries FROM ViewSeries (nolock) inner join viewdeposito vd on vd.depositoid = ViewSeries.depositoid WHERE ArticuloId = '" & rsText(1) & "' AND  ViewSeries.DepositoId in (SELECT depositoid FROM [sc_megatech].[dbo].[DepositoFacturable] where DepositoFacturable = 0) ORDER BY NROSERIE"
    End Sub
End Class
