﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="verTktsEnProceso.aspx.vb" Inherits="verTktsEnProceso" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">

    <table align="center">
    <tr><td align="center" valign="middle" colspan="2" nowrap="nowrap">Lider:
        <asp:SqlDataSource ID="dsFiltros" runat="server" 
            SelectCommand="SELECT [liderId], [apellidoNombre] FROM [lider]" 
            ConnectionString="<%$ ConnectionStrings:TicketDB %>"></asp:SqlDataSource>
        <asp:DropDownList ID="ddFiltro" runat="server" AutoPostBack="True" 
            DataSourceID="dsFiltros" DataTextField="apellidoNombre" DataValueField="liderId">
                    </asp:DropDownList>
&nbsp;<asp:Label ID="Label2" runat="server" Text="Tickets:" 
        Font-Bold="True" Font-Size="18px"></asp:Label>
    &nbsp;<asp:Label ID="lblTotal" runat="server" Font-Bold="True" ForeColor="#264D9B" 
            Font-Size="18px"></asp:Label>
    &nbsp;&nbsp;
        <asp:ImageButton ID="ImageButton1" runat="server"
            ImageUrl="~/Images/update.jpg" Width="20px" CssClass="refreshBtn" />
    </td></tr>
        <tr><td colspan="2">Tipo:

            <asp:DropDownList ID="ddlneg" runat="server" AutoPostBack="true">
                <asp:ListItem Value="0" Selected="True">Seleccionar</asp:ListItem>
                <asp:ListItem Value="19532">Residenciales</asp:ListItem>
                <asp:ListItem Value="19535">Negocios</asp:ListItem>
            </asp:DropDownList>

            </td>
         
        </tr>
    <tr><td align="right" valign="middle" style="height: 30px" nowrap="nowrap">
                    <asp:Label ID="lblSel" runat="server" Font-Bold="False" Visible="False"></asp:Label>&nbsp;<asp:Label ID="Label3" runat="server" Text="Buscar Especialista" 
            Font-Bold="True" ForeColor="#333333"></asp:Label>
        :
        <asp:TextBox ID="txtBusqueda" runat="server" AutoPostBack="True"></asp:TextBox>
    &nbsp;&nbsp;</td><td align="char" valign="top" rowspan="2">



    <asp:Panel ID="Panel1" runat="server" BorderStyle="None" Visible="False">
                <table style="border: 1px solid #A7BDD6">
                <tr><td align="center">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="15px" 
                        ForeColor="#1A2742"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:Button ID="Button1" runat="server" Font-Bold="True" ForeColor="#666666" 
                            Text="X" ToolTip="Cerrar" />
                    </td>
                    </tr>
                <tr><td style="margin-left: 40px" colspan="2">
                 <asp:GridView ID="gvDetalleTkts" runat="server" CellPadding="2" 
            DataSourceID="dsTkts" ForeColor="#333333" GridLines="None" 
            AutoGenerateColumns="False" DataKeyNames="Ticket" AllowSorting="True">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:ButtonField ButtonType="Image" CommandName="ver" 
                    ImageUrl="~/Images/Edicion.jpg" />
                <asp:BoundField DataField="Ticket" HeaderText="Ticket" InsertVisible="False" 
                    ReadOnly="True" SortExpression="Ticket" />
                <asp:BoundField DataField="Estado" HeaderText="Estado" 
                    SortExpression="Estado" />
                <asp:BoundField DataField="Clain" HeaderText="Clain" 
                    SortExpression="Clain" />
                <asp:BoundField DataField="Tipo de Problema" HeaderText="Tipo de Problema" 
                    SortExpression="Tipo de Problema">
                <HeaderStyle Wrap="False" />
                </asp:BoundField>
                <asp:BoundField DataField="Detalle" HeaderText="Detalle" ReadOnly="True" 
                    SortExpression="Detalle" />
                <asp:BoundField DataField="Creación" HeaderText="Creación" ReadOnly="True" 
                    SortExpression="Creación" />
                <asp:BoundField DataField="minutos" HeaderText="Tiempo en proceso" 
                    ReadOnly="True" SortExpression="minutos" >
                <HeaderStyle Wrap="False" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsTkts" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TicketDB %>"></asp:SqlDataSource>

                </td></tr>
                </table>      
    </asp:Panel>
    </td></tr>
    <tr><td valign="top">
        <asp:GridView ID="gridTktsEnProc" runat="server" 
        AllowSorting="True" AutoGenerateColumns="False" CellPadding="2" 
        DataKeyNames="ID" DataSourceID="dsGridView" ForeColor="#333333" 
        GridLines="None" onrowcommand="GridView1_RowCommand">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                ReadOnly="True" SortExpression="ID" >
            <ItemStyle Width="26px" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="Especialista" HeaderText="Especialista" 
                ReadOnly="True" SortExpression="Especialista" >
            <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="Tickets en proceso" HeaderText="Tkts" 
                ReadOnly="True" SortExpression="Tickets en proceso" >
            <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="segmento" HeaderText="Segmento" 
                SortExpression="segmento" />
            <asp:ButtonField ButtonType="Button" CommandName="ver" Text="Ver &gt;&gt;" 
                ImageUrl="~/Images/lista.png">
            <ControlStyle Height="22px" Width="56px"/>
            <ItemStyle HorizontalAlign="Center" />
            </asp:ButtonField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" Height="14px" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsGridView" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>"
         SelectCommand="select e.especialistaID ID, max(ArticuloId), min(e.nombre) Especialista, count(*) 'Tickets en proceso',
eq.nombre segmento
from ticket tk INNER JOIN especialista e ON e.especialistaID=tk.especialistaID
inner join equipo eq on eq.equipoId=articuloID
where tk.estadoId=115
group by eq.nombre,e.especialistaId
order by eq.nombre,count(*) DESC"></asp:SqlDataSource>
    </td></tr>
    </table>



    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

