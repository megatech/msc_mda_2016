<%@ Page Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="EquiposAlta.aspx.cs" Inherits="EquiposAlta" %>

<asp:Content ID="TicketPage" ContentPlaceHolderID="cphTicket" runat="server">
    <asp:Panel ID="panelTicket" runat="server">
        <div style="text-align: center">
            <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 100%; border-bottom: black 1px solid">
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" bordercolor="white" bordercolordark="#ffffff" bordercolorlight="#ffffff">
                                <tr>
                                    <td align="right" width="10%">
                                        C�digo</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCodigo" runat="server" CssClass="TextoTextBoxReadonly" MaxLength="18"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvCodigo" runat="server" ControlToValidate="txtCodigo" ErrorMessage="Debe ingresar el C�digo">*</asp:RequiredFieldValidator>
                                        <asp:CustomValidator id="cvCodigo" runat="server" SetFocusOnError="true" 
                                              OnServerValidate="ValidaCodigoInexistente" 
                                              ControlToValidate="txtCodigo" 
                                              ErrorMessage="Ya existe el c�digo">
                                        </asp:CustomValidator> 
                                        
                                        
                                        </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Nombre</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNombre" runat="server" Width="90%" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ControlToValidate="txtNombre" ErrorMessage="Debe ingresar el Nombre">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="10%">
                                        Rubro</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlRubro1" runat="server"  Width="200"
                                        DataSourceID="odsRubro1" DataTextField="Nombre" DataValueField="Codigo">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsRubro1" Runat="server" 
                                            TypeName="App_Code.TicketsLib.TablasCombo"
                                            SelectMethod="getComboRubro"
                                        >
                                            <SelectParameters>
                                                <asp:Parameter Name="EmpresaId" Type="Int16" DefaultValue="1" />
                                                <asp:Parameter Name="esObligatorio" Type="Boolean" DefaultValue="True" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Marca</td>
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlMarca" runat="server"  Width="200"
                                        DataSourceID="odsMarca" DataTextField="Nombre" DataValueField="Codigo">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsMarca" Runat="server" 
                                            TypeName="App_Code.TicketsLib.TablasCombo"
                                            SelectMethod="getComboMarca"
                                        >
                                            <SelectParameters runat="server">
                                                <asp:Parameter Name="EmpresaId" Type="Int16" DefaultValue="1" />
                                                <asp:Parameter Name="esObligatorio" Type="Boolean" DefaultValue="True" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                    
                                        
                                       
                                    </td>
                                </tr>
                                
                                            <SelectParameters>
                                                <asp:Parameters Name="EmpresaId" Type="Int16" DefaultValue="1" />
                                                <asp:Parameters Name="esObligatorio" Type="Boolean" DefaultValue="False" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                </tr>                                
                            </table>
                    </td>
                </tr>
               
                 
            
                <tr>
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Ingresar" /></td>
                </tr>
                 
                 </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblError" runat="server" CssClass="labelError"></asp:Label>
                    <asp:ValidationSummary ID="validSummary" runat="server" />
                </td>
                </table>
            </tr>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelMensaje" runat="server">
        <table>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblMensajePanel" runat="server" CssClass="labelError"></asp:Label>
                </td>        
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
