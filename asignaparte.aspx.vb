﻿Imports App_Code.TicketsLib

Partial Class Default3
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SQL As New System.Data.SqlClient.SqlConnection
        Dim CMD As New System.Data.SqlClient.SqlCommand
        Dim RS As System.Data.SqlClient.SqlDataReader


        If Request.QueryString("id") Is Nothing Then
            Response.Redirect("default2.aspx")
        End If

        If Session("usuario") Is Nothing Then
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString)
        End If

        If Not IsPostBack Then
            'AGREGAMOS AL DD LA LISTA DE CONDICIONES
            Menu3.Items(0).Selected = True
            MV.ActiveViewIndex = 0

            ddcondicion.Items.Add("Con Cargo")
            ddcondicion.Items(ddcondicion.Items.Count - 1).Value = "0"
            ddcondicion.Items.Add("Sin Cargo")
            ddcondicion.Items(ddcondicion.Items.Count - 1).Value = "1"
            ddcondicion.Items.Add("Garantia")
            ddcondicion.Items(ddcondicion.Items.Count - 1).Value = "2"
            ddcondicion.Items.Add("Prestamo")
            ddcondicion.Items(ddcondicion.Items.Count - 1).Value = "3"
            ddcondicion.Items.Add("Falta Presupuestar")
            ddcondicion.Items(ddcondicion.Items.Count - 1).Value = "4"
            lblEstado.Text = "Aguardando Operacion"

            If Menu3.Items(1).Selected = True Then

            End If

        End If

    End Sub

    Protected Sub CmdCargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdCargar.Click
        Dim SQL As New System.Data.SqlClient.SqlConnection
        Dim CMD As New System.Data.SqlClient.SqlCommand
        Dim RS As System.Data.SqlClient.SqlDataReader
        Dim lViewItem() As ListItem
        Dim i As Long = 0

        lResultados.Items.Clear()

        Try
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
            SQL.Open()
            CMD.Connection = SQL
            CMD.CommandText = "SELECT nombre,articuloid FROM viewarticulo WHERE articuloid like '" & txtPN.Text & "%'"
            RS = CMD.ExecuteReader

            While RS.Read

                ReDim Preserve lViewItem(i)
                lViewItem(i) = New ListItem
                lViewItem(i).Text = RS(0)
                lViewItem(i).Value = RS(1)
                lResultados.Items.Add(lViewItem(i))
                i += 1
            End While

            RS.Close()
            SQL.Close()

        Catch ex As System.Data.SqlClient.SqlException
            lblEstado.Text = "Error: " & ex.ErrorCode
            Exit Sub
        Catch ex1 As InvalidOperationException
            lblEstado.Text = "Error " & ex1.Message
            Exit Sub
        Finally
            lblEstado.Text = "Exito!"
        End Try
    End Sub

    Protected Sub CmdAgregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdAgregar.Click
        Dim SQL As New System.Data.SqlClient.SqlConnection
        Dim CMD As New System.Data.SqlClient.SqlCommand
        Dim formOK As Boolean = True

        If lResultados.SelectedItem Is Nothing Then
            lblEstado.Text = "Debe seleccionar un elemento de la lista"
            formOK = False
            Exit Sub
        End If

        Select Case ddcondicion.SelectedValue
            Case "0"
                If Not IsNumeric(precio.Text) Then
                    lblEstado.Text = "Debe colocar el precio en rangos numericos"
                    formOK = False
                    Exit Sub
                End If
                If precio.Text = "" Then
                    lblEstado.Text = "Debe completar el campo pecio"
                    formOK = False
                    Exit Sub
                End If
            Case "1"
            Case "2"
                If precio.Text <> "0" Then
                    lblEstado.Text = "El campo precio debe ser 0 para la condicion dada"
                    formOK = False
                    Exit Sub
                End If
        End Select

        If formOK Then
            Try
                SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
                SQL.Open()
                CMD.Connection = SQL
                CMD.CommandText = "INSERT INTO ticketpresupuesto (ticketid,articuloid,condicionpago,precio) VALUES (" & _
                                  "'" & Request.QueryString("id") & "'," & _
                                  "'" & lResultados.SelectedValue & "'," & _
                                  "'" & ddcondicion.SelectedItem.Text & "'," & _
                                  "" & precio.Text & ")"

                If CMD.ExecuteNonQuery() <= 0 Then
                    lblEstado.Text = "Error actualizando los datos"
                Else
                    lblEstado.Text = "Datos actualizados Correctamente"
                End If

            Catch ex As System.Data.SqlClient.SqlException
                lblEstado.Text = ex.Message
            End Try
        End If
        SQL.Close()
    End Sub

    Protected Sub Menu3_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu3.MenuItemClick
        lParte.Items.Clear()
        lDeposito.Items.Clear()

        Dim SQL As New System.Data.SqlClient.SqlConnection
        Dim CMD As New System.Data.SqlClient.SqlCommand
        Dim RS As System.Data.SqlClient.SqlDataReader
        If e.Item.Text = "Asignar" Then
            MV.ActiveViewIndex = 1
            'DEBEMOS LLENAR LOS DATOS
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
            SQL.Open()
            CMD.Connection = SQL
            CMD.CommandText = "SELECT tk.ticketpresupuestoid, tk.articuloid as ID, AR.nombre, INST.remitoid from ticketpresupuesto TK inner join viewarticulo AR on AR.articuloid = TK.articuloid left join instanciaarticulo INST on TK.ticketpresupuestoid = INST.ticketpresupuestoid WHERE tk.ticketid = '" & Request.QueryString("id") & "'"
            RS = CMD.ExecuteReader
            While RS.Read
                If RS.IsDBNull(3) Then
                    lParte.Items.Add(RS(0) & " --- " & RS(1) & " --- " & RS(2) & " --- No asignada")
                Else
                    lParte.Items.Add(RS(0) & " --- " & RS(1) & " --- " & RS(2) & " --- " & RS(3).ToString)
                End If

            End While
            RS.Close()
            SQL.Close()
            LblOperacion.Text = "Aguardando Operacion..."
        Else
            MV.ActiveViewIndex = 0

        End If
    End Sub

    Protected Sub lParte_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lParte.SelectedIndexChanged
        Dim SQL As New System.Data.SqlClient.SqlConnection
        Dim CMD As New System.Data.SqlClient.SqlCommand
        Dim RS As System.Data.SqlClient.SqlDataReader
        Dim rsText() As String
        Dim lItems() As ListItem
        Dim lCount As Long = 0

        rsText = Split(lParte.SelectedItem.Text, " --- ")
        lDeposito.Items.Clear()


        SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
        SQL.Open()
        CMD.Connection = SQL
        CMD.CommandText = "SELECT ViewSeries.DepositoId, vd.nombre as  Deposito,NROSERIE AS NroSeries FROM ViewSeries (nolock) inner join viewdeposito vd on vd.depositoid = ViewSeries.depositoid WHERE ArticuloId = '" & rsText(1) & "' AND  ViewSeries.DepositoId in (SELECT depositoid FROM [sc_megasp2].[dbo].[DepositoFacturable] where DepositoFacturable = 0) ORDER BY NROSERIE"

        RS = CMD.ExecuteReader

        While RS.Read
            'NOS DESPLAZAMOS X EL RECORDSET
            ReDim Preserve lItems(lCount)
            lItems(lCount) = New ListItem
            lItems(lCount).Value = RS(0).ToString
            lItems(lCount).Text = RS(1) & " --- " & RS(2)
            lDeposito.Items.Add(lItems(lCount))
            lCount += 1
        End While
        RS.Close()
        SQL.Close()
    End Sub

    Protected Sub CmdAsignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdAsignar.Click
        On Error Resume Next

        Dim bFormOK As Boolean = True
        Dim SQL As New System.Data.SqlClient.SqlConnection
        Dim CMD As New System.Data.SqlClient.SqlCommand
        Dim datos As Usuario = Session("Usuario")
        Dim presupuestoID() As String
        Dim Deposito() As String

        'MANDAMOS AL USUARIO A LOGUEARSE PQ LA SESION EXPIRO
        If Session("usuario") Is Nothing Then Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString)

        If lParte.SelectedItem Is Nothing Or lDeposito.SelectedItem Is Nothing Then
            LblOperacion.Text = "Debe seleccionar una parte y un deposito desde el cual desea asignar la misma"
            bFormOK = False
        End If

        If bFormOK Then
            presupuestoID = Split(lParte.SelectedItem.Text, " --- ")
            Deposito = Split(lDeposito.SelectedItem.Text, " --- ")

            'Try
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
            SQL.Open()
            CMD.Connection = SQL
            CMD.CommandType = Data.CommandType.StoredProcedure

            CMD.CommandText = "SP_RESERVA_REPUESTO_TICKET_SNP"

            CMD.Parameters.AddWithValue("@TicketId", Request.QueryString("id"))

            CMD.Parameters.AddWithValue("@TicketPresupuestoId", Trim(presupuestoID(0)))

            CMD.Parameters.AddWithValue("@DepositoId", lDeposito.SelectedItem.Value)

            CMD.Parameters.AddWithValue("@NroSerie", Trim(Deposito(1)))

            CMD.Parameters.AddWithValue("@UsuarioId", 216)

            CMD.Parameters.AddWithValue("@tr", 1)


            CMD.Parameters.Add("@fglError", System.Data.SqlDbType.SmallInt)
            CMD.Parameters(CMD.Parameters.Count - 1).Direction = System.Data.ParameterDirection.Output

            CMD.Parameters.Add("@DescError", System.Data.SqlDbType.VarChar, 200)
            CMD.Parameters(CMD.Parameters.Count - 1).Direction = System.Data.ParameterDirection.Output


            CMD.ExecuteNonQuery()

            MsgBox(CMD.Parameters("@descError").Value)
            SQL.Close()
            'Catch ex As System.Data.SqlClient.SqlException
            'LblOperacion.Text = "Error: " & ex.Message
            'Exit Sub
            'End Try

            LblOperacion.Text = "La Parte se ha asignado correctamente!"
        End If
    End Sub
End Class
