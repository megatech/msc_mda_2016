using System;
using System.Data;
using System.Web.UI.WebControls;
using App_Code.TicketsLib;

public partial class busquedaCliente : System.Web.UI.Page
{
    private static log4net.ILog logger = log4net.LogManager.GetLogger("File");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtOpenerId.Text=Request.QueryString["campoid"];
            txtOpenerDescrip.Text = Request.QueryString["campodescrip"];
        }
    }

    private void loadGrilla()
    {
        try
        {
            FiltroCliente fc = new FiltroCliente();
            fc.Codigo = txtCodigo.Text;
            fc.RazonSocial = txtRazonSocial.Text;
            fc.Cuit = txtCuit.Text;
            fc.Telefono1 = txttelefono.Text ;

            

            DataSet ds = Clientes.getClientes(fc);
            if (ds != null)
            {
                gvBusqueda.DataSource = ds;
                gvBusqueda.DataBind();
            }
            else
            {
                lblError.Text = "No existen registros con ese criterio.";
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "Error: " + ex.Message;
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        loadGrilla();
    }

    protected void gvBusqueda_PageIndexChanging(object sender,GridViewPageEventArgs e)
    {
        gvBusqueda.PageIndex = e.NewPageIndex;
        loadGrilla();
    }

    protected void gvBusqueda_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // returnBusqueda(valorId, valorDescrip, campoId, campoDescrcip)
                Label lblCod = (Label)e.Row.FindControl("lblCodigo");
                Label lblDes = (Label)e.Row.FindControl("lblRazonSocial");

                //string cmd = "returnBusqueda('" + lblCod.Text.Trim() + "', '" +
                //                lblDes.Text.Trim() + "', '" +
                //                this.txtOpenerId.Text + "', '" +
                //                this.txtOpenerDescrip.Text + "');";

                string cmd = "returnBusqueda('" + lblCod.Text.Trim() + "', '" +
                lblDes.Text.Trim().Replace("'"," ") + "');";

                Button btn = (Button)e.Row.FindControl("btnSelect");
                btn.OnClientClick = cmd;
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "Error: " + ex.Message;
        }
    }
   
}
