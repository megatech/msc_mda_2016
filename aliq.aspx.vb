﻿Imports App_Code.TicketsLib

Partial Class aliq
    Inherits System.Web.UI.Page

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim especialista As String
        especialista = DropDownList1.SelectedValue
        Session.Add("especialista", especialista)
        Response.Redirect("liqui.aspx")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ususarioactual As Usuario

        ususarioactual = Session("usuario")

        If ususarioactual.Perfil <> 1 Then
            Response.Redirect("Default2.aspx")
        End If
    End Sub
End Class
