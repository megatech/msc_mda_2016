﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="agenda.aspx.vb" Inherits="Default2" MasterPageFile="TicketsPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" runat="server">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


    
    <div>
    <h2 align="center" >Agenda de Tickets</h2>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataKeyNames="Ticket" DataSourceID="SqlDataSource1" 
            ForeColor="#333333" GridLines="None" AllowPaging="True" 
            AllowSorting="True" EmptyDataText="No existen datos para mostrar" >
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

        <Columns>
               <asp:ButtonField ButtonType="Image" CommandName="Select" HeaderText="Editar" 
                   ImageUrl="~/Images/Edicion.jpg" Text="Editar" />
               <asp:BoundField DataField="Ticket" HeaderText="Ticket" />
                <asp:BoundField DataField="Clain" HeaderText="Clain" SortExpression="Clain" />
                <asp:BoundField DataField="Fecha creacion" HeaderText="Fecha creacion" 
                    SortExpression="Fecha creacion" />
               
                <asp:BoundField DataField="Cliente" HeaderText="Cliente" ReadOnly="True" 
                    SortExpression="Cliente" />
               
                <asp:BoundField DataField="fecha de inicio" HeaderText="fecha de inicio" 
                    SortExpression="fecha de inicio" />
                <asp:BoundField DataField="equipo" HeaderText="equipo" 
                    SortExpression="equipo" />
                <asp:BoundField DataField="s/n" HeaderText="s/n" SortExpression="s/n" />
                <asp:BoundField DataField="Especialista" HeaderText="Especialista" 
                    SortExpression="Especialista" />
                <asp:BoundField DataField="tipo problema" HeaderText="tipo problema" 
                    SortExpression="tipo problema" />
                <asp:BoundField DataField="Estado" HeaderText="Estado" 
                    SortExpression="Estado" />
                <asp:TemplateField HeaderText="Negocio" SortExpression="Negocio">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Negocio") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Negocio") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
               
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TicketDB %>">
        </asp:SqlDataSource>
    
    </div>


</asp:Content>