﻿
Partial Class usuarios
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("Usuario") Is Nothing Then
            Response.Redirect("~/Default.aspx?url=" + Request.Url.OriginalString)
        End If
    End Sub
End Class
