﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Login</title>
    <link href="App_Themes/Default/Default.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="text-align: center">
            <table style="width: 90%; height: 100%">
                <tr>
                    <td align="left" valign="top" width="120" class="style2"><asp:Image ID="imgLogo" 
                            runat="server" ImageUrl="~/Images/logomeganuevo.jpg" Height="46px" 
                            Width="104px" /></td>
                    <td style="text-align:center;">
                        <table style="margin-left:auto; margin-right:auto;" align="center">
                            <tr><td><strong style="font-size: x-large">Mega Service Center</strong></td></tr>
                            <tr style="border:none;"><td><strong style="font-size: small">Mesa de Ayuda - Dr. Speedy</strong></td></tr>
                        </table>

                    </td>
                    
                    <td class="style2"></td>
                </tr>

                <tr>
                    <td style="">
                    </td>
                    <td style="" valign="top">
                        <table border="0" cellpadding="3" style="width: 40%" class="TablaLogin" align="center">
                          <tr align="left" class="headerLogin">
                              <td colspan="2" class="style3">Login</td>
                          </tr>
                            <tr>
                                <td align="right" width="35%" class="TextoEtiqueta">Usuario:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtUsuario" runat="server" Width="140px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="right" class="TextoEtiqueta">Clave:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtClave" runat="server" TextMode="Password" Width="140px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="right">
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click"  />
                                    &nbsp;&nbsp;<asp:LinkButton
                                        ID="lnkCambioClave" runat="server" CssClass="urlLinks" OnClick="lnkCambioClave_Click">Cambiar clave</asp:LinkButton></td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <asp:Label ID="lblMensajeError" SkinID="lblMensajeError" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                    <td style="">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td align="right"><table align="center">
                    <tr>
                    <td>
                       <%--<asp:Image ID="Image1" runat="server" Height="30px" 
                            ImageUrl="~/Images/ie.jpg" />--%>
                    </td>
                   <%-- <td align="left" style="font-weight: bold; color: #4D4D4D;">Este Sitio Web fue 
                        desarrollado para su utilización exclusiva con el navegador Internet Explorer.<br />
                        Utilice dicho navegador
                        para garantizar la compatibilidad de todas sus funcionalidades.</td>
                    </tr>--%>
                    <tr><td></td></tr>
                    <tr><td colspan="2" align="center" style="font-weight: bold; color: #666666" 
                            class="style4">Versión: <asp:Label ID="lblVer" runat="server"></asp:Label></td></tr>
                    </table>
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    
    </div>
    </form>
</body>
</html>
