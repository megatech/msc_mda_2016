﻿// debe existir un span, que contenga la fecha desde la cual se desea calcular

function calcularDuracion(prefSpanFecha, prefSpanRes, id) {
    fc = new Date($("#" + prefSpanFecha + id).html());
    diff_ms = new Date().getTime() - fc.getTime();
    diff_ms = diff_ms / 1000;
    var segundos = Math.floor(diff_ms % 60);
    diff_ms = diff_ms / 60;
    var minutos = Math.floor(diff_ms % 60);
    diff_ms = diff_ms / 60;
    var horas = Math.floor(diff_ms % 24);
    var dias = Math.floor(diff_ms / 24);
    res = "";
    if (dias > 0) res = dias + 'd, ';
    if (segundos <= 9) segundos = '0' + segundos;
    if (minutos <= 9) minutos = '0' + minutos;
    if (horas <= 9) horas = '0' + horas;
    res = res + horas + ':' + minutos + ':' + segundos;
    $("#" + prefSpanRes + id).html(res);
}
function iniciarCalculosDuracion(pIdSpanFecha, pIdSpanResultado, idInputValores) {
    arr = $("#" + idInputValores).val().split(",");
    for (i in arr) {
        calcularDuracion(pIdSpanFecha, pIdSpanResultado, arr[i]);
    }
    t = setTimeout(function () { iniciarCalculosDuracion(pIdSpanFecha, pIdSpanResultado, idInputValores); }, 1000);
}

function actualizarSemaforos() {
    
    var idTkts = "tkts";
    var idSemaforos = "semaforos";
    var pIdFecha = "fEstado_";
    var pIdEstado = "estadoId_";
    var pIdFEstado = "fEstado_";
    var pIdSemaforo = "semaforo_";

    arrTkts = $("#" + idTkts).val().split(",");
    arrSem = $("#" + idSemaforos).val().split(",");

    for (i in arrTkts) {
        tkt = arrTkts[i];
        estado = $("#" + pIdEstado + tkt).html();
        var semaforos = $("#" + idSemaforos).val();

        fechaEstado = new Date($("#" + pIdFecha + tkt).html());
        diff_s = (new Date().getTime() - fechaEstado.getTime()) / 1000;

        
            tVerde = umbralSemaforo(estado, 1, semaforos);
            tAmarillo = umbralSemaforo(estado, 2, semaforos);
            //alert( diff_s + " " + tVerde + " " + tAmarillo);
            var strIMG = "";

            if (diff_s <= tVerde) {
                strIMG =  "verde";
            }
            else if (diff_s > tAmarillo) {
                strIMG = "rojo";
            }
            else { strIMG = "amarillo"; }
            strIMG = "<img style='width:12px;height:12px' src='Images/sem_" + strIMG + ".png' />" ;
            $("#" + pIdSemaforo + tkt).empty();
            $("#" + pIdSemaforo + tkt).append(strIMG);
    }
    t = setTimeout(function () { actualizarSemaforos(); }, 1000);
};

function umbralSemaforo(estado, umbral, semaforos) {
    var arrSemaforos = semaforos.split("|");
    for (i in arrSemaforos) {
        arrEstado = arrSemaforos[i].split(",");
        if (arrEstado[0] == estado) {
            return arrEstado[umbral];
        }
    }
    return null;
}

$(function () {
    iniciarCalculosDuracion("fechatkt_", "duraciontkt_", "tkts");
    actualizarSemaforos();
});
