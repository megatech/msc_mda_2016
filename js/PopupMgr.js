﻿function SavePopupValues(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg) {
        document.getElementById(arg.name).value = arg.val;
        __doPostBack('', '');
    }
}
function GetRadWindow() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}
function AdjustRadWidow() {
    var oWindow = GetRadWindow();
    setTimeout(function () { oWindow.autoSize(true); if ($telerik.isChrome || $telerik.isSafari) ChromeSafariFix(oWindow); }, 500);
}
function ChromeSafariFix(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    setTimeout(function () {
        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();

    }, 310);
}
function CloseAndReturn(elemId, val) {
    var oArg = new Object(); oArg.val = val;
    var oWnd = GetRadWindow();
    oArg.name = elemId;
    oWnd.close(oArg);
}
function AbrirPopup(url) {
    window.radopen(url, 'popup');
}
function AbrirPopup(url, idPopup) {
    window.radopen(url, idPopup);
}