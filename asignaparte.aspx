﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="asignaparte.aspx.vb" Inherits="Default3" MasterPageFile="TicketsPage.master" %>


<asp:Content ContentPlaceHolderID="cphTicket" runat="server" >
                    <asp:Menu ID="Menu3" runat="server" BackColor="#FFFBD6" 
                        DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="0.8em" 
                        ForeColor="#990000" StaticSubMenuIndent="10px" Height="16px" 
                        style="font-size: medium" Width="123px" 
        Orientation="Horizontal">
                        <DynamicHoverStyle BackColor="#990000" ForeColor="White" />
                        <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                        <DynamicMenuStyle BackColor="#FFFBD6" />
                        <DynamicSelectedStyle BackColor="#FFCC66" />
                        <Items>
                            <asp:MenuItem Text="Presupuesto" Value="Presupuesto">
                            </asp:MenuItem>
                            <asp:MenuItem Text="Asignar" Value="Asignar">
                            </asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#990000" ForeColor="White" />
                        <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                        <StaticSelectedStyle BackColor="#FFCC66" />
                    </asp:Menu>

       
    <br />
    <asp:MultiView ID="MV" runat="server">
        <asp:View ID="vASIGNA" runat="server">
        <div align="center">
        
            <asp:Table ID="TblAsignaBusqueda" runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        P/N
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="txtPN" runat="server"></asp:TextBox><asp:Button ID="CmdCargar" runat="server" Text="..." />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Nombre
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:ListBox ID="lResultados" runat="server"  Height="48"></asp:ListBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Condicion
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="ddcondicion" runat="server"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Precio (U$s)
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="precio" runat="server"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Estado
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label id="lblEstado" runat="server"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        
            
        
            <br />
        
            
        <asp:Button ID="CmdAgregar" Text="Cargar" runat="server" />
        </div>
        </asp:View>
        <asp:View ID="vPRESUPUESTO" runat="server"><div align="center">
        
            <asp:Label ID="LblOperacion" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
        
            Seleccione la parte<br />
            <br />
            <asp:ListBox ID="lParte" runat="server" AutoPostBack="True" Width="349px"></asp:ListBox>
            <br />
            <br />
            A continuación, seleccione el deposito y Nro de serie.<br />
            <br />
            <asp:ListBox ID="lDeposito" runat="server" Width="349px"></asp:ListBox>
        
            <br />
            <br />
            Finalmente Presione el boton &quot;Asignar&quot; para asignar la parte al Ticket<br />
            <br />
            <asp:Button ID="CmdAsignar" runat="server" Text="Asignar Parte" />
            <br />
            <br />
        
        </div>
        </asp:View>
    </asp:MultiView>

</asp:Content>