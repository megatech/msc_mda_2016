﻿Imports System.Data.SqlClient
Imports System.Data
Imports App_Code.TicketsLib

Partial Class cargacoaching
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim cant = getScalar("SELECT count(*) from coaching where userId=" & Request.QueryString("usuarioid") & " AND id in (select id from viewCoachingsCriticos) And month(fecha)=month(getDate())")
        'If cant > 2 Then Response.Redirect("regCoaching.aspx")
        lblfecha.Text = DateTime.Now
        lbloper.Text = Request.QueryString("usuarioid")
        lbloperName.Text = getScalar("SELECT ' - ' + RTRIM(nombreCompleto) as nombre FROM usuario WHERE usuarioId=" & lbloper.Text)

        If (pec1.SelectedValue = "0" Or pec2.SelectedValue = "0" Or pec3.SelectedValue = "0" Or pec4.SelectedValue = "0" Or
            pec5.SelectedValue = "0" Or pec6.SelectedValue = "0") Then
            ecc.Text = "0%"
            ecc.ForeColor = Drawing.Color.Red
        Else
            ecc.Text = "100%"
            ecc.ForeColor = Drawing.Color.Green
        End If

        If (pec7.SelectedValue = "0" Or pec8.SelectedValue = "0" Or pec9.SelectedValue = "0" Or pec10.SelectedValue = "0") Then
            ecc1.Text = "0%"
            ecc1.ForeColor = Drawing.Color.Red
        Else
            ecc1.Text = "100%"
            ecc1.ForeColor = Drawing.Color.Green
        End If

        ecc2.Text = CalculaPorcentaje().ToString() & "%"
        If CalculaPorcentaje() > 60 Then
            ecc2.ForeColor = Drawing.Color.Green
        Else
            ecc2.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Private Function CalculaPorcentaje() As Long
        Dim lPercent As Decimal
        If penc11.SelectedValue <> "0" Then lPercent += 16.7
        If penc12.SelectedValue <> "0" Then lPercent += 16.7
        If penc13.SelectedValue <> "0" Then lPercent += 16.7
        If penc14.SelectedValue <> "0" Then lPercent += 16.7
        If penc15.SelectedValue <> "0" Then lPercent += 16.7
        If penc16.SelectedValue <> "0" Then lPercent += 16.7
        Return lPercent
    End Function

    Protected Sub txttkt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txttkt.TextChanged
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim telefono As String
        If txttkt.Text = "" Then Exit Sub
        If Not IsNumeric(txttkt.Text) Then Exit Sub
        If Chkexperiencia.Checked = False Then
            sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("TicketDB").ToString()
            sql.Open()
            query.Connection = sql
            query.CommandText = " select top 1 telefono1 " & _
                                " from viewcliente " & _
                                " where clienteid = ( select clienteid from ticket where ticketid = " & txttkt.Text & " ) "
            telefono = query.ExecuteScalar()


            txtani.Text = telefono
            txtani.ReadOnly = True

            If txtani.Text = "" Then
                txttkt.BorderColor = Drawing.Color.Red
                txttkt.ForeColor = Drawing.Color.Red
                lbltipoproblema.Text = "No existe el TKT"
                lbltipoproblema.ForeColor = Drawing.Color.Red
            Else
                txttkt.BorderColor = Drawing.Color.Black
                txttkt.ForeColor = Drawing.Color.Black
                lbltipoproblema.ForeColor = Drawing.Color.Black
                query.CommandText = " select tipoproblema.nombre from ticket inner join tipoproblema on tipoproblema.idtipoproblema = ticket.idtipoproblema where ticketid = " & txttkt.Text & ""
                lbltipoproblema.Text = query.ExecuteScalar()
            End If
           

        Else
            sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("ExperienciaDB").ToString()

            sql.Open()
            query.Connection = sql
            query.CommandText = "SELECT telefono FROM ticket WHERE id = " & txttkt.Text
            txtani.Text = query.ExecuteScalar

            query.CommandText = "SELECT nombre from infotipogestion inner join ticket on ticket.idtipogestion = infotipogestion.id"
            lbltipoproblema.Text = query.ExecuteScalar
        End If
        query.Dispose()
        sql.Close()
        sql.Dispose()
    End Sub

    Protected Sub btnguardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        Try
            Dim usuarioCarga As Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
            Dim escucha As String
            'Dim fecha As String = Year(Date.Now) & "-" & Month(Date.Now) & "-" & Day(Date.Now) & " 00:00:00 "

            'Guardar DB
            Dim nombrefile As String
            nombrefile = Now.ToString("yyyyMMdd_HHmmss") & IIf(txtani.Text.Trim <> "", "_ani" & txtani.Text & "-", "") & "_oper" & lbloper.Text & ".wav"
            If (escuchasfile.HasFile = True) Then
                escucha = 1
            Else
                escucha = 0
            End If
            If escuchasfile.HasFile = True Then
                escuchasfile.PostedFile.SaveAs("C:\inetpub\wwwroot\Webs\escuchas_speedy\" & nombrefile.ToString())
            Else
                lblerror.Text = "Debe seleccionar una grabación"
                lblerror.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
            If txttkt.Text = "" Then
                lblerror.Text = "Debe ingresar un ticket"
                lblerror.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
            If SSIDQOOM.Text = "" Then
                lblerror.Text = "Debe ingresar el SSID QOOM "
                lblerror.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
            Dim sql As New Data.SqlClient.SqlConnection
            Dim query As New Data.SqlClient.SqlCommand
            sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("TicketDB").ToString()
            sql.Open()
            query.Connection = sql
            query.Parameters.Add(New SqlParameter("@usuarioCarga", usuarioCarga.UsuarioId))
            query.Parameters.Add(New SqlParameter("@userid", lbloper.Text))
            query.Parameters.Add(New SqlParameter("@ani", txtani.Text))
            query.Parameters.Add(New SqlParameter("@SSIDQOOM", SSIDQOOM.Text))
            'Dim fecha1 As New SqlParameter
            'fecha1.DbType = Data.DbType.Date
            'fecha1.Value = fecha
            'fecha1.ParameterName = "@fecha"
            'query.Parameters.Add(fecha1)
            query.Parameters.Add(New SqlParameter("@escucha", escucha))
            query.Parameters.Add(New SqlParameter("@devolucion", 0))

            If pec1.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec1", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec1", pec1.SelectedValue))
            End If
            If pec2.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec2", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec2", pec2.SelectedValue))
            End If
            If pec3.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec3", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec3", pec3.SelectedValue))
            End If
            If pec4.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec4", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec4", pec4.SelectedValue))
            End If

            If pec5.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec5", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec5", pec5.SelectedValue))
            End If
            If pec6.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec6", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec6", pec6.SelectedValue))
            End If
            If pec7.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec7", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec7", pec7.SelectedValue))
            End If
            If pec8.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec8", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec8", pec8.SelectedValue))
            End If
            If pec9.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec9", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec9", pec9.SelectedValue))
            End If
            If pec10.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@pec10", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@pec10", pec10.SelectedValue))
            End If

            If penc11.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@penc11", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@penc11", penc11.SelectedValue))
            End If
            If penc12.SelectedValue = "NULL" Then

                query.Parameters.Add(New SqlParameter("@penc12", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@penc12", penc12.SelectedValue))
            End If
            If penc13.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@penc13", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@penc13", penc13.SelectedValue))
            End If
            If penc14.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@penc14", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@penc14", penc14.SelectedValue))
            End If
            If penc15.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@penc15", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@penc15", penc15.SelectedValue))
            End If
            If penc16.SelectedValue = "NULL" Then
                query.Parameters.Add(New SqlParameter("@penc16", DBNull.Value))
            Else
                query.Parameters.Add(New SqlParameter("@penc16", penc16.SelectedValue))
            End If

            If Not (Chkexperiencia.Checked And txttkt.Text.Trim = "") Then query.Parameters.Add(New SqlParameter("@tkt", txttkt.Text))
            query.Parameters.Add(New SqlParameter("@justificacion", txtjustificacion.Text))
            query.Parameters.Add(New SqlParameter("@validado", DBNull.Value))
            query.Parameters.Add(New SqlParameter("@tipoproblema", lbltipoproblema.Text))
            query.Parameters.Add(New SqlParameter("@hash", "falta"))
            query.Parameters.Add(New SqlParameter("@archivo", nombrefile.ToString()))
            query.Parameters.Add(New SqlParameter("@monitoreo", usuarioCarga.Perfil = 17))
            query.CommandType = Data.CommandType.StoredProcedure
            query.CommandText = "SP_INSERT_COACHING_NUEVO"
            query.ExecuteNonQuery()
            query.Dispose()
            sql.Close()
            sql.Dispose()

            'Subir Archivo
            If usuarioCarga.Perfil <> 17 Then notificarCoaching()
            Response.Redirect("regcoaching.aspx")
        Catch ex As Exception
            lblerror.Text = "Se ha producido un error : " & ex.Message
        End Try
        
    End Sub

    Protected Sub Chkexperiencia_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chkexperiencia.CheckedChanged
        If txttkt.Text <> "" Then
            txttkt_TextChanged(txttkt, New System.EventArgs())
        End If
    End Sub

    Private Function getScalar(ByVal query As String) As Object
        Dim SQL As New SqlConnection
        Dim CMD As New SqlCommand
        Dim RES As Object
        Try
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ToString
            CMD.Connection = SQL
            CMD.CommandText = query
            SQL.Open()
            RES = CMD.ExecuteScalar()
            SQL.Close()
            Return RES
        Catch ex As Exception
            If SQL.State <> ConnectionState.Closed Then
                SQL.Close()
            End If
            Throw ex
        End Try
    End Function

    Private Sub notificarCoaching()
        Dim destinatario, subject, cuerpo, nombreUsuario As String
        nombreUsuario = getScalar("SELECT nombreCompleto FROM usuario WHERE usuarioId=" & Request.QueryString("usuarioid"))
        destinatario = getScalar("SELECT Email FROM Usuario WHERE usuarioId=" & Request.QueryString("usuarioid"))

        subject = "Devolución de Monitoreo [" & nombreUsuario & " - " & lblfecha.Text.Substring(0, 10) & "]"
        cuerpo = "<div style=""text-align:center;""><table style=""width:700px; margin:auto; font-weight:bold;"">" &
                "<tr><th style=""text-align:left; background-color:#333333; color:white;"">Devoluci&oacuten de monitoreo [" & nombreUsuario & " - " & lblfecha.Text.Substring(0, 10) & "]</th></tr>" &
                "<tr><td>Se ha cargado un nuevo monitoreo a su nombre en el sistema.</td></tr>" &
                "<tr><td>Por favor acceda al link que figura a continuaci&oacuten, para visualizar el detalle del mismo.</td>" &
                "<tr><td>Debajo del detalle, tendr&aacute un campo para enviar una replica y confirmar que ha sido notificado de esta devoluci&oacuten.</td></tr>" &
                "<tr><td style=""text-align:center""><a href=""" & generateLink() & """>" & generateLink() & "</a></td></tr>" &
                "</table> <div>"
        FUNCIONES_MOD.mailmega2.enviamail("alertas@megatech.la", destinatario, "alertas@megatech.la", subject, cuerpo)

    End Sub

    Public Function generateLink() As String
        Dim coachId = getScalar("Select top 1 id from coachingNuevo where userId=" & Request.QueryString("usuarioid") & " Order by id desc")
        Return "http://mscmda.mega.com.ar/vercoachNuevo.aspx" & "?coachingid=" & coachId
        'Return "http://192.168.1.35:8083/vercoachNuevo.aspx" & "?coachingid=" & coachId
    End Function
End Class
