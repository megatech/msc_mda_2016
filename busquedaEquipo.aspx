<%@ Page Language="C#" AutoEventWireup="true" CodeFile="busquedaEquipo.aspx.cs" Inherits="busquedaEquipo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <base target=_self />
    <script type="text/javascript" >
        //function returnBusqueda(valorId, valorDescrip, campoId, campoDescrip) {
        function returnBusqueda(valorId, valorDescrip) {
            /*
            window.opener.document.getElementById(campoId).value = valorId;
            window.opener.document.getElementById(campoDescrip).value = valorDescrip;
            window.close();
            */
            var ret=new Array( valorId,valorDescrip) ; 
            window.returnValue=ret; 
            window.close();             
        }      
    </script>
    <title>Busqueda</title>
</head>
<body bgcolor="silver" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">  
    <div>
        <table style="width: 100%" border=0 bordercolor="#000000" cellpadding="1" cellspacing="1">
            <tr class="headerStyle">
                <td align="center" colspan="2" style="height: 21px">
                    Filtros</td>
            </tr>
            <tr class="headerStyle">
                <td width="20%">
                    C�digo</td>
                <td style="width: 120px">
                    Nombre</td>
            </tr>
            <tr class="footerStyle">
                <td style="width: 100px; height: 21px">
                    <asp:TextBox ID="txtCodigo" runat="server" Width="53px"></asp:TextBox></td>
                <td style="width: 100px; height: 21px">
                    <asp:TextBox ID="txtNombre" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr class="footerStyle">
                <td align="center" colspan="2">
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" /></td>
            </tr>
        </table>
    <asp:GridView ID="gvBusqueda" runat="server" Width="100%" 
        AutoGenerateColumns="False" AllowPaging="True"
        OnPageIndexChanging="gvBusqueda_PageIndexChanging"
        OnRowDataBound="gvBusqueda_RowDataBound"
        HeaderStyle-CssClass="headerStyle"
        AlternatingRowStyle-CssClass="altRowStyle"
        RowStyle-CssClass="rowStyle"
        FooterStyle-CssClass="footerStyle"
        PagerStyle-CssClass="footerStyle" >
        <Columns>            
            <asp:BoundField DataField="EquipoID" Visible="False" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnSelect"
                    text="-"
                    Runat=Server
                    OnClientClick="" />
               </ItemTemplate>
              </asp:TemplateField>

            <asp:TemplateField SortExpression="Codigo" HeaderText="C&#243;digo">
                <HeaderStyle HorizontalAlign="Left" Width="20%"  />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label Runat="server" Text='<%# Bind("Codigo") %>' ID="lblCodigo"></asp:Label>
                </ItemTemplate>               
            </asp:TemplateField>
                        
            <asp:TemplateField SortExpression="Nombre" HeaderText="Nombre">
                <HeaderStyle HorizontalAlign="Left"   />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label Runat="server" Text='<%# Bind("Nombre") %>' ID="lblNombre"></asp:Label>
                </ItemTemplate>               
            </asp:TemplateField>                
            
            
        </Columns>
    </asp:GridView>
        <asp:Label ID="lblError" runat="server" Text=""></asp:Label><br />
        &nbsp;<asp:TextBox ID="txtOpenerId" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="txtOpenerDescrip" runat="server" Visible="False"></asp:TextBox></div>
    </form>
</body>
</html>
