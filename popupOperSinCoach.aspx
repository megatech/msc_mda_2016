﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="popupOperSinCoach.aspx.vb" Inherits="popupOperSinCoach" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
                </asp:ScriptReference>
            </Scripts>
        </telerik:RadScriptManager>
    <div style="margin:20px;">
        <telerik:RadGrid ID="RadGrid1" runat="server" CellSpacing="0" Culture="es-ES" DataSourceID="dsOperadoresSinCoaching" GridLines="None">
            <ExportSettings>
                <Pdf PageWidth="" />
            </ExportSettings>
<MasterTableView AutoGenerateColumns="False" DataKeyNames="usuarioId" DataSourceID="dsOperadoresSinCoaching">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="usuarioId" DataType="System.Decimal" FilterControlAltText="Filter usuarioId column" HeaderText="ID" ReadOnly="True" SortExpression="usuarioId" UniqueName="usuarioId">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="nombreCompleto" FilterControlAltText="Filter nombreCompleto column" HeaderText="Usuario" SortExpression="nombreCompleto" UniqueName="nombreCompleto">
        </telerik:GridBoundColumn>
        <telerik:GridButtonColumn ButtonType="PushButton" FilterControlAltText="Filter column column" Text="Cargar Coaching" UniqueName="column">
        </telerik:GridButtonColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
</MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
        </telerik:RadGrid>
        
        <asp:SqlDataSource ID="dsOperadoresSinCoaching" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [usuarioId], [nombreCompleto] FROM [viewOperadores]"></asp:SqlDataSource>
        
    </div>
    </form>
</body>
</html>
