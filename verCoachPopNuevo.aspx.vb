﻿Imports System.Data.SqlClient
Imports Telerik.Web.UI
Imports DAOs

Partial Class verCoachPop
    Inherits System.Web.UI.Page
    Private archivo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        If Request.QueryString("coachingid") Is Nothing Then
            Response.Write("Debe selecionar un operador")
            Exit Sub
        End If

        'If (IsPostBack = False) Then
        btPrint.Attributes.CssStyle.Add("cursor", "pointer")
        btPrint.Attributes.Add("target", "_blank")
        btPrint.Attributes.Add("onclick", "window.open('printCoach.aspx?id=" & Request.QueryString("coachingid") & "');")
        For Each c As Control In pLider.Controls
            If c.GetType.Name = "RadTextBox" Then
                Dim txt As RadTextBox = TryCast(c, RadTextBox)
                txt.ReadOnly = True
            ElseIf c.GetType.Name = "RadDatePicker" Then
                Dim dp As RadDatePicker = TryCast(c, RadDatePicker)
                dp.Enabled = False
            End If
        Next
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand

        sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("TicketDB").ToString()
        sql.Open()
        query.Connection = sql
        query.CommandText = " Select * from coachingNuevo where id = " & Request.QueryString("coachingid").ToString()
        Dim rs As SqlDataReader

        rs = query.ExecuteReader
        rs.Read()

        Session.Add("cid", rs(0).ToString)
        lbloper.Text = rs(1).ToString()
        txtani.Text = rs(2).ToString()
        txtani.ReadOnly = True
        SSIDQOOM.Text = rs(3).ToString
        SSIDQOOM.ReadOnly = True
        lblfecha.Text = rs(4).ToString


        If (rs(5) = True) Then
            lblescucha.Text = "Si"
            lblescucha.ForeColor = Drawing.Color.Green
            lblescucha.Font.Bold = True
        Else
            lblescucha.Text = "No"
            lblescucha.ForeColor = Drawing.Color.Red
            lblescucha.Font.Bold = True
        End If


        If (rs(6) = True) Then

            lbldev.Text = "Si"
            lbldev.ForeColor = Drawing.Color.Green
            lbldev.Font.Bold = True
        Else
            lbldev.Text = "No"
            lbldev.ForeColor = Drawing.Color.Red
            lbldev.Font.Bold = True
        End If


        If (rs(7) Is DBNull.Value) Then
            ecn1.Text = "N/A"
            ecn1.ForeColor = Drawing.Color.Blue
            ecn1.Font.Bold = True
        ElseIf (rs(7) = False) Then
            ecn1.Text = "Si"
            ecn1.ForeColor = Drawing.Color.Red
            ecn1.Font.Bold = True
        Else
            ecn1.Text = "No"
            ecn1.ForeColor = Drawing.Color.Green
            ecn1.Font.Bold = True
        End If


        If (rs(8) Is DBNull.Value) Then
            ecn2.Text = "N/A"
            ecn2.ForeColor = Drawing.Color.Blue
            ecn2.Font.Bold = True
        ElseIf (rs(8) = False) Then
            ecn2.Text = "Si"
            ecn2.ForeColor = Drawing.Color.Red
            ecn2.Font.Bold = True
        Else
            ecn2.Text = "No"
            ecn2.ForeColor = Drawing.Color.Green
            ecn2.Font.Bold = True
        End If



        If (rs(9) Is DBNull.Value) Then
            ecn3.Text = "N/A"
            ecn3.ForeColor = Drawing.Color.Blue
            ecn3.Font.Bold = True
        ElseIf (rs(9) = False) Then
            ecn3.Text = "Si"
            ecn3.ForeColor = Drawing.Color.Red
            ecn3.Font.Bold = True
        Else
            ecn3.Text = "No"
            ecn3.ForeColor = Drawing.Color.Green
            ecn3.Font.Bold = True
        End If

        If (rs(10) Is DBNull.Value) Then
            ecn4.Text = "N/A"
            ecn4.ForeColor = Drawing.Color.Blue
            ecn4.Font.Bold = True
        ElseIf (rs(10) = False) Then
            ecn4.Text = "Si"
            ecn4.ForeColor = Drawing.Color.Red
            ecn4.Font.Bold = True
        Else
            ecn4.Text = "No"
            ecn4.ForeColor = Drawing.Color.Green
            ecn4.Font.Bold = True
        End If

        If (rs(11) Is DBNull.Value) Then
            ecn5.Text = "N/A"
            ecn5.ForeColor = Drawing.Color.Blue
            ecn5.Font.Bold = True
        ElseIf (rs(11) = False) Then
            ecn5.Text = "Si"
            ecn5.ForeColor = Drawing.Color.Red
            ecn5.Font.Bold = True
        Else
            ecn5.Text = "No"
            ecn5.ForeColor = Drawing.Color.Green
            ecn5.Font.Bold = True
        End If

        If (rs(12) Is DBNull.Value) Then
            ecn6.Text = "N/A"
            ecn6.ForeColor = Drawing.Color.Blue
            ecn6.Font.Bold = True
        ElseIf (rs(12) = False) Then
            ecn6.Text = "Si"
            ecn6.ForeColor = Drawing.Color.Red
            ecn6.Font.Bold = True
        Else
            ecn6.Text = "No"
            ecn6.ForeColor = Drawing.Color.Green
            ecn6.Font.Bold = True
        End If

        If (rs(13) Is DBNull.Value) Then
            ecc1.Text = "N/A"
            ecc1.ForeColor = Drawing.Color.Blue
            ecc1.Font.Bold = True
        ElseIf (rs(13) = False) Then
            ecc1.Text = "Si"
            ecc1.ForeColor = Drawing.Color.Red
            ecc1.Font.Bold = True
        Else
            ecc1.Text = "No"
            ecc1.ForeColor = Drawing.Color.Green
            ecc1.Font.Bold = True
        End If

        If (rs(14) Is DBNull.Value) Then
            ecc2.Text = "N/A"
            ecc2.ForeColor = Drawing.Color.Blue
            ecc2.Font.Bold = True
        ElseIf (rs(14) = False) Then
            ecc2.Text = "Si"
            ecc2.ForeColor = Drawing.Color.Red
            ecc2.Font.Bold = True
        Else
            ecc2.Text = "No"
            ecc2.ForeColor = Drawing.Color.Green
            ecc2.Font.Bold = True
        End If


        If (rs(15) Is DBNull.Value) Then
            ecc3.Text = "N/A"
            ecc3.ForeColor = Drawing.Color.Blue
            ecc3.Font.Bold = True
        ElseIf (rs(15) = False) Then
            ecc3.Text = "Si"
            ecc3.ForeColor = Drawing.Color.Red
            ecc3.Font.Bold = True
        Else
            ecc3.Text = "No"
            ecc3.ForeColor = Drawing.Color.Green
            ecc3.Font.Bold = True
        End If

        If (rs(16) Is DBNull.Value) Then
            ecc4.Text = "N/A"
            ecc4.ForeColor = Drawing.Color.Blue
            ecc4.Font.Bold = True
        ElseIf (rs(16) = False) Then
            ecc4.Text = "Si"
            ecc4.ForeColor = Drawing.Color.Red
            ecc4.Font.Bold = True
        Else
            ecc4.Text = "No"
            ecc4.ForeColor = Drawing.Color.Green
            ecc4.Font.Bold = True
        End If






        If (rs(17) Is DBNull.Value) Then
            enc1.Text = "N/A"
            enc1.ForeColor = Drawing.Color.Blue
            enc1.Font.Bold = True
        ElseIf (rs(17) = True) Then
            enc1.Text = "Si"
            enc1.ForeColor = Drawing.Color.Green
            enc1.Font.Bold = True
        Else
            enc1.Text = "No"
            enc1.ForeColor = Drawing.Color.Red
            enc1.Font.Bold = True
        End If

        If (rs(18) Is DBNull.Value) Then
            enc2.Text = "N/A"
            enc2.ForeColor = Drawing.Color.Blue
            enc2.Font.Bold = True
        ElseIf (rs(18) = True) Then
            enc2.Text = "Si"
            enc2.ForeColor = Drawing.Color.Green
            enc2.Font.Bold = True
        Else
            enc2.Text = "No"
            enc2.ForeColor = Drawing.Color.Red
            enc2.Font.Bold = True
        End If

        If (rs(19) Is DBNull.Value) Then
            enc3.Text = "N/A"
            enc3.ForeColor = Drawing.Color.Blue
            enc3.Font.Bold = True
        ElseIf (rs(19) = True) Then
            enc3.Text = "Si"
            enc3.ForeColor = Drawing.Color.Green
            enc3.Font.Bold = True
        Else
            enc3.Text = "No"
            enc3.ForeColor = Drawing.Color.Red
            enc3.Font.Bold = True
        End If

        If (rs(20) Is DBNull.Value) Then
            enc4.Text = "N/A"
            enc4.ForeColor = Drawing.Color.Blue
            enc4.Font.Bold = True
        ElseIf (rs(20) = True) Then
            enc4.Text = "Si"
            enc4.ForeColor = Drawing.Color.Green
            enc4.Font.Bold = True
        Else
            enc4.Text = "No"
            enc4.ForeColor = Drawing.Color.Red
            enc4.Font.Bold = True
        End If

        If (rs(21) Is DBNull.Value) Then
            enc5.Text = "N/A"
            enc5.ForeColor = Drawing.Color.Blue
            enc5.Font.Bold = True
        ElseIf (rs(21) = True) Then
            enc5.Text = "Si"
            enc5.ForeColor = Drawing.Color.Green
            enc5.Font.Bold = True
        Else
            enc5.Text = "No"
            enc5.ForeColor = Drawing.Color.Red
            enc5.Font.Bold = True
        End If

        If (rs(22) Is DBNull.Value) Then
            enc6.Text = "N/A"
            enc6.ForeColor = Drawing.Color.Blue
            enc6.Font.Bold = True
        ElseIf (rs(22) = True) Then
            enc6.Text = "Si"
            enc6.ForeColor = Drawing.Color.Green
            enc6.Font.Bold = True
        Else
            enc6.Text = "No"
            enc6.ForeColor = Drawing.Color.Red
            enc6.Font.Bold = True
        End If

        txttkt.Text = rs(24).ToString
        txttkt.ReadOnly = True

        txtjustificacion.Text = rs(25).ToString

        txtjustificacion.ReadOnly = True

        lbltipoproblema.Text = rs(27).ToString
        lblAutor.Text = getScalar("Select Autor from viewTableroCoaching_Nuevo where id=" & Request.QueryString("coachingid").ToString())
        Session.Add("archivo", rs(29).ToString)


        query.Dispose()

        sql.Close()
        sql.Dispose()

        'End If
        descargar.Target = "_blank"

        descargar.NavigateUrl = "http://mscmda.mega.com.ar/files/" & Session("archivo").ToString
        habilitarReplica()
    End Sub

    Protected Sub play_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles play.Click
        Dim audio As New System.Media.SoundPlayer
        If Not Session("archivo") Is Nothing Then
            audio.SoundLocation = "C:\inetpub\wwwroot\Webs\escuchas_speedy\" & Session("archivo").ToString
            Try
                audio.Play()
                descargar.NavigateUrl = "C:\inetpub\wwwroot\Webs\escuchas_speedy\" & Session("archivo").ToString
            Catch ex As Exception
                lblerror.Text = "No se puede reproducir el archivo"
                lblerror.ForeColor = Drawing.Color.Red
                lblerror.Font.Size = FontUnit.Larger
                play.Visible = False
            End Try
            play.Visible = False
            pausa.Visible = True
        Else
            play.Visible = False
        End If
    End Sub

    Protected Sub pausa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles pausa.Click
        Dim audio As New System.Media.SoundPlayer
        audio.Stop()
        play.Visible = True
        pausa.Visible = False
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'Session.Remove("archivo")
    End Sub
    Protected Sub btEnviarReplica_Click(sender As Object, e As EventArgs) Handles btEnviarReplica.Click
        guardarReplica()
        notificarAlLider()
        Response.Redirect("vercoachNuevo.aspx?coachingid=" & Request.QueryString("coachingid"))
    End Sub

    Private Sub guardarReplica()
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("TicketDB").ToString()
        If txtReplica.Text.Trim = "" Then txtReplica.Text = "" Else txtReplica.Text = txtReplica.Text.Trim
        sql.Open()
        query.Connection = sql
        query.CommandText = " update coachingNuevo set replica = '" & txtReplica.Text.Replace("'", "''") & "' where id = " & Request.QueryString("coachingid")
        query.ExecuteNonQuery()
        query.Dispose()
        sql.Dispose()
        sql.Close()
    End Sub

    Private Sub habilitarReplica()
        Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
        Dim yaTieneReplica = getScalar("SELECT count(*) FROM coachingNuevo where replica IS NOT NULL AND id=" & Request.QueryString("coachingid")) > 0
        Dim esElUsuarioDelCoaching = u.UsuarioId = lbloper.Text.Trim

        If yaTieneReplica Then txtReplica.Text = getScalar("SELECT replica FROM coachingNuevo where id=" & Request.QueryString("coachingid"))

        If yaTieneReplica And txtReplica.Text.Trim = "" Then
            txtReplica.EmptyMessage = "El operador fué notificado, pero no hizo ningún comentario."
        ElseIf txtReplica.Text.Trim = "" Then
            If Not esElUsuarioDelCoaching Then
                txtReplica.EmptyMessage = "El operador aún no ha enviado ninguna replica."
            Else : txtReplica.EmptyMessage = "Escriba aquí una replica de la devolución que acaba de leer..."
            End If
        End If
        txtReplica.ReadOnly = yaTieneReplica Or Not esElUsuarioDelCoaching
        btEnviarReplica.Visible = Not yaTieneReplica And esElUsuarioDelCoaching

    End Sub

    Private Sub notificarAlLider()
        Dim destinatario, subject, cuerpo, cid, nombreUsuario, fecha As String
        fecha = lblfecha.Text.Trim.Substring(0, 10)
        cid = Request.QueryString("coachingId")
        nombreUsuario = getScalar("select nombrecompleto from usuario where usuarioId in (SELECT userId from coachingNuevo where id=" & cid & ")").ToString.Trim
        destinatario = getScalar("SELECT Email FROM Usuario WHERE usuarioId in (SELECT liderUID from viewLider where usuarioId=" & lbloper.Text.Trim & ")")
        subject = "Replica del Coaching " & cid & " [" & nombreUsuario & " - " & fecha & "]"
        cuerpo = "<div style=""text-align:center;""><table style=""width:700px; margin:auto; font-weight:bold;"">" &
                "<tr><th style=""text-align:left; background-color:#333333; color:white;"">Replica del coaching " & cid & " [" & nombreUsuario & " - " & fecha & "]</th></tr>" &
                "<tr><td>El operador ha enviado una replica del coaching realizado.</td></tr>" &
                "<tr><td>Por favor acceda al link que figura a continuaci&oacuten, para visualizar el detalle del mismo.</td>" &
                "<tr><td style=""text-align:center""><a href=""" & generateLink() & """>" & generateLink() & "</a></td></tr>" &
                "</table> <div>"
        'FUNCIONES_MOD.mailmega2.enviamail("megarobot@megatech.la", destinatario, "megarobot@megatech.la", subject, cuerpo)
        FUNCIONES_MOD.mailmega2.enviamail("alertas@megatech.la", "mostrovsky@megatech.la", "jsoto@megatech.la", subject, cuerpo)
    End Sub

    Public Function generateLink() As String
        Dim coachId = Request.QueryString("coachingid")
        'Return "http://mscmda.mega.com.ar/vercoach.aspx" & "?coachingid=" & coachId
        Return "http://mscmda.mega.com.ar/vercoachNuevo.aspx" & "?coachingid=" & coachId
    End Function

End Class
