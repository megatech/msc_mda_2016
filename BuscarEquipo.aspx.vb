﻿Imports Telerik.Web.UI

Partial Class BuscarEquipo
    Inherits System.Web.UI.Page
    Protected Sub RadGrid1_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gridResultados.ItemCommand
        If e.CommandName = "Select" Then
            Dim ret As Dictionary(Of String, String) = New Dictionary(Of String, String)
            ret("EquipoId") = e.Item.Cells(3).Text
            ret("Codigo") = e.Item.Cells(4).Text
            ret("Nombre") = e.Item.Cells(5).Text
            Popups.PopupMgr.Cerrar(Me, "retEquipo", ret)
        End If
    End Sub

    Protected Sub RadTextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        actualizarGrid()
    End Sub

    Protected Sub gridResultados_SortCommand(sender As Object, e As GridSortCommandEventArgs) Handles gridResultados.SortCommand
        actualizarGrid()
    End Sub

    Public Sub actualizarGrid()
        If txtBusqueda.Text.Trim.Length > 0 Then
            Dim txt = txtBusqueda.Text.Trim.Replace(" ", "%")
            Dim cond = " AND (Codigo like '%" & txt & "%' OR " &
                             "Nombre like '%" & txt & "%')"
            If tipo.SelectedValue <> "todo" Then cond = " AND " & tipo.SelectedValue & " like '%" & txt & "%'"
            dsEquipos.SelectCommand = "SELECT EquipoId, Codigo, Nombre  FROM Equipo WHERE 1=1 and FechaBaja IS NULL" & cond
            gridResultados.DataBind()
        End If
    End Sub

    Protected Sub imgBuscar_Click(sender As Object, e As ImageClickEventArgs) Handles imgBuscar.Click
        actualizarGrid()
    End Sub
End Class
