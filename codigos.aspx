﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="codigos.aspx.vb" Inherits="codigos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3 align="center">
        Códigos</h3>
<p>
    <asp:Table ID="Table1" runat="server" HorizontalAlign="Center" 
        BorderStyle="Double" GridLines="Both" Height="93px" Width="455px">
        <asp:TableRow runat="server" HorizontalAlign="Left" VerticalAlign="Middle">
            <asp:TableCell runat="server">Código</asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox ID="txtcodigo" runat="server" Width="300" Enabled="True"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">Tipo de Reparación</asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox ID="txttipo" runat="server" Width="300"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">Observaciones</asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox ID="txtobs" runat="server" TextMode="MultiLine" Width="300"></asp:TextBox></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ErrorMessage="Debe ingresar un Código" ControlToValidate="txtcodigo" 
        ForeColor="Red" InitialValue="0" style="text-align: center"></asp:RequiredFieldValidator>
        <br />

    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
        ErrorMessage="Debe ingresar un Tipo de Reparación" ControlToValidate="txttipo" 
        ForeColor="Red" InitialValue="0" style="text-align: center"></asp:RequiredFieldValidator>
 
</p>
    <p align="center">
        <asp:Button ID="guardar" runat="server" Text="Guardar" />
</p>
    <asp:SqlDataSource ID="Codigos" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        SelectCommand="SELECT [cod], [tipo] FROM [codrep]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Especialistas" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        
        SelectCommand="SELECT [EspecialistaId], [Nombre] FROM [especialista] WHERE (([Nombre] LIKE '%' + @Nombre + '%') AND ([Nombre] NOT LIKE '%' + @Nombre2 + '%') AND ([Nombre] NOT LIKE '%' + @Nombre3 + '%')) ORDER BY [Nombre]">
        <SelectParameters>
            <asp:Parameter DefaultValue="cas" Name="Nombre" Type="String" />
            <asp:Parameter DefaultValue="USAR" Name="Nombre2" Type="String" />
            <asp:Parameter DefaultValue="CASARINI" Name="Nombre3" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="insertar" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" >
    </asp:SqlDataSource>
</asp:Content>

