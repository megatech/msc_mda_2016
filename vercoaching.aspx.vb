﻿Imports Telerik.Web.UI

Partial Class verCoaching2
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "ver" Then
            Response.Redirect("verCoachingUsuario.aspx?id=" & GridView1.Rows(e.CommandArgument).Cells(0).Text)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("usuario") Is Nothing Then Response.Redirect("default.aspx")
        Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
        If u.Perfil = 26 Or u.Perfil = 32 Or u.Perfil = 28 Then
            Response.Redirect("verCoachingUsuario.aspx?id=" & u.UsuarioId)
        End If
    End Sub

    Protected Sub ddLider_DataBound(sender As Object, e As EventArgs) Handles ddLider.DataBound
        Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
        If Not IsPostBack Then
            Dim itm = ddLider.FindItemByValue(u.UsuarioId)

            If Not itm Is Nothing Then
                ddLider.SelectedValue = u.UsuarioId
                GridView1.DataBind()
            End If
        End If
    End Sub

    Protected Sub ddLider_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddLider.SelectedIndexChanged
        GridView1.DataBind()
    End Sub

End Class
