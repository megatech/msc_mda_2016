﻿
Partial Class Default2
    Inherits System.Web.UI.Page


    Dim sql As New Data.SqlClient.SqlConnection
    Dim query As New Data.SqlClient.SqlCommand
    Dim query2 As New Data.SqlClient.SqlCommand


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

      
        Dim preguntas() As String
        Dim uname As String
        Dim userid As String

        'MsgBox(Session("usuario").Perfil)




        SqlDataSource1.SelectCommand = "SELECT Ticket.TICKETID as 'Ticket' , Ticket.CLAINNRO as 'Clain' , " _
& "Ticket.FECHA as 'Fecha creacion', tir.Desde 'Fecha Re-Contacto', " _
& "Ticket.CLIENTEID + ' ' + VC.RazonSocial as 'Cliente' , " _
& "Ticket.FechaInicio as 'fecha de inicio',   Equipo.Nombre as 'equipo', Ticket.NroSerie as 's/n', " _
& "Especialista.Nombre as 'Especialista' , TipoProblema.Nombre as 'tipo problema' , " _
& "Estado.Nombre as 'Estado',   EmpresaPartner.Nombre as 'Negocio' " _
& "FROM Ticket (NOLOCK) " _
& "INNER JOIN Equipo (nolock) ON Ticket.ArticuloId = Equipo.EquipoId " _
& "INNER JOIN Especialista (nolock) ON Ticket.EspecialistaId = Especialista.EspecialistaId " _
& "INNER JOIN TipoProblema (nolock) ON Ticket.IdTipoProblema = TipoProblema.IdTipoProblema " _
& "INNER JOIN EmpresaPartner (nolock) ON Ticket.EmpresaPartnerId = EmpresaPartner.EmpresaPartnerId " _
& "INNER JOIN Estado (nolock) ON Ticket.EstadoId = Estado.EstadoId " _
& "INNER JOIN ViewCliente VC (nolock) ON Ticket.ClienteId = VC.ClienteId " _
& "INNER JOIN UbicacionContacto (nolock) ON Ticket.UbicacionContactoId = UbicacionContacto.UbicacionContactoId " _
& "LEFT JOIN ticketInfoRecontacto tir (nolock) ON Ticket.ticketID = tir.ticketId " _
& "WHERE 1 = 1 and  ticket.estadoid in (" & Request.QueryString("estado").ToString & ")"

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Query.Dispose()
        sql.Dispose()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        If e.CommandName = "Select" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = GridView1.Rows(index)
            Dim tktCell As TableCell = selectedRow.Cells(1)
            Dim tkt As String = tktCell.Text

            Response.Redirect("EditaTicket.aspx?id=" & tkt & "")

        End If
    End Sub
End Class
