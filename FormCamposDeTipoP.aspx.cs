﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Popups;
using System.Collections.Generic;
using App_Code.TicketsLib;
using Telerik.Web.UI;
public partial class FormCamposDeTipoP : System.Web.UI.Page
{
    #region Eventos
    protected void Page_Load(object sender, EventArgs e)
    {
        errorM.Text = "";
        if (!IsPostBack)
        {
            ValidatetktState();
            

        }


    }
    protected void btnaceptar_Click(object sender, EventArgs e)
    {
        try
        {
            int problema = int.Parse(Radtipo.SelectedItem.Value);
            if (ValidateProblemItem(problema))
            {
                if (ValidateControls())
                {

                    string str = WriteData();
                    Dictionary<String, String> valores = new Dictionary<string, string>();
                    valores["FormEquipo"] = str;
                    valores["Tproblema"] = Radtipo.SelectedItem.Value;
                    valores["Contacto"] = txtn.Text;
                    valores["so"]  =radSistemaOP.SelectedItem.Value;
                    valores["licenciaSO"] = RadTipoWin.Text;
                    valores["pc"] = radTipoPC.Text;
                    valores["antiguedad"] = txtAntiguedad.Text;
                    valores["procesador"] = txtprocesador.Text;
                    valores["ram"] = txtram.Text;
                    valores["tipoconexion"] = radTipoConexion.Text;
                    valores["modem"] = txtModeloMDM.Text;
                    valores["detalle"] = txtDetalle.Text;
                    PopupMgr.Cerrar(this, "FormEquipo", valores);
                }
                else
                {
                    errorM.Text = "Complete correctamente todos los campos";
                }
            }
            else
            {
                errorM.Text = "Error en la seleccion de tipo de problema";
            }




        }
        catch (Exception ex)
        {

            errorM.Text = ex.Message.ToString();
        }

    }
    protected void Radtipo_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        int problema = int.Parse(Radtipo.SelectedItem.Value);
        try
        {
            if (ValidateProblemItem(problema))
            {
                FilterControls(problema);
            }
            else
            {
                errorM.Text = "Error en la seleccion de tipo de problema";

            }
        }
        catch (Exception ex)
        {

            errorM.Text = ex.Message;
        }
      

    }
  
    # endregion
    #region CargarControles
    private void CargarComboTipoProblema()
    {
        Usuario usr = (Usuario)Session["Usuario"];
        DataTable dtprob = new DataTable();
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        System.Data.SqlClient.SqlDataAdapter Adapter;
        string query = "";
        query = "SELECT DISTINCT idtipoproblema,Nombre FROM dbo.tiposDeProblema(" + usr.Perfil + "," + Request.QueryString["id"] + ")";
        Adapter = new SqlDataAdapter(query, SQL);
        Adapter.Fill(dtprob);
        
        foreach (DataRow row in dtprob.Rows)
        {
            RadComboBoxItem item = new RadComboBoxItem();
            item.Text = row["Nombre"].ToString();
            item.Value = row["idtipoproblema"].ToString();
            Radtipo.Items.Add(item);
            item.DataBind();

        }




    }
    private void CargarControles(DataTable t)
    {
        foreach (DataRow r in t.Rows)
        {
            Radtipo.SelectedValue = r["idTipoProblema"].ToString();
            int filtro = int.Parse(r["idTipoProblema"].ToString());
            FilterControls(filtro);
            txtn.Text = r["contacto"].ToString();
            RadComboBoxItem pcitem = radTipoPC.FindItemByText(r["pc"].ToString(), false);
            if (pcitem == null) { } else { radTipoPC.SelectedValue = pcitem.Value; }
            RadComboBoxItem TipoCon = radTipoConexion.FindItemByText(r["tipoconexion"].ToString(), false);
            if (TipoCon == null) { } else { radTipoConexion.SelectedValue = TipoCon.Value; }
            txtAntiguedad.Text = r["antiguedad"].ToString();
            txtprocesador.Text = r["procesador"].ToString();
            txtram.Text = r["ram"].ToString();
            RadComboBoxItem TipoWin = RadTipoWin.FindItemByText(r["licenciaSO"].ToString(), false);
            if (TipoWin == null) { } else { RadTipoWin.SelectedValue = TipoWin.Value; }
            txtDetalle.Text = r["Detalle"].ToString();
            txtModeloMDM.Text = r["modem"].ToString();
            if ((int)r["sistemaOperativoId"] != 7)
            {
                radSistemaOP.SelectedValue = (int)r["sistemaOperativoId"] == 0 ? "1" : r["sistemaOperativoId"].ToString();
            }
        }



    }
    #endregion
    #region ValidacionyFiltros
    private bool ValidateControls()
    {

        if (txtn.Enabled)
            {
                if (txtn.Text.Trim() == "") { return false; };

            }
            if(radSistemaOP.Enabled)
            {
                if(radSistemaOP.SelectedItem==null){return false;}
            }
            if (RadTipoWin.Enabled)
            {
                if (RadTipoWin.SelectedItem==null){return false;}
            }
            if (radTipoPC.Enabled)
            {
                if (radTipoPC.SelectedItem==null){return false;}
            }
            if (radTipoConexion.Enabled)
            {
                if (radTipoConexion.SelectedItem == null) { return false; }
            }
            if (txtAntiguedad.Enabled)
            {
                if (txtAntiguedad.Text.Trim() == "") { return false; }
            }
            if (txtprocesador.Enabled)
            {
                if (txtprocesador.Text.Trim() == "") { return false; }
            }
            if (txtram.Enabled)
            {
                if (txtram.Text.Trim() == "") { return false; }
            }
            if (txtModeloMDM.Enabled)
            {
                if (txtModeloMDM.Text.Trim() == "") { return false; }
            }
            if (txtModeloMDM.Enabled)
            {
                if (txtModeloMDM.Text.Trim() == "") { return false; }
            }
            if (txtDetalle.Enabled)
            {
                if (txtDetalle.Text.Trim() == "") { return false; }
            }
            return true;
    }
    private void ValidatetktState()
    {
        
        if (CequearEstado())
        {
            DeshabilitarControles();
            errorM.Text = "El ticket se encuentra cerrado";
        }
        else
        {
            CargarComboTipoProblema();
            DataTable t = GetDatatkt();
            if (t.Rows.Count > 0)
            {
                CargarControles(t);
            }

        }

        
    }
    private void DeshabilitarControles()
    {

        Radtipo.Enabled = false;
        Radtipo.ClearSelection();
        RadTipoWin.Enabled = false;
        RadTipoWin.ClearSelection();
        radSistemaOP.Enabled = false;
        radSistemaOP.ClearSelection();
        radTipoPC.Enabled = false;
        radTipoPC.ClearSelection();
        radTipoConexion.Enabled = false;
        radTipoConexion.ClearSelection();
        txtn.Enabled = false;
        txtn.Text = string.Empty;
        txtModeloMDM.Enabled = false;
        txtModeloMDM.Text = string.Empty;
        txtprocesador.Enabled = false;
        txtprocesador.Text = string.Empty;
        txtram.Enabled = false;
        txtram.Text = string.Empty;
        txtAntiguedad.Enabled = false;
        txtAntiguedad.Text = string.Empty;
        txtDetalle.Enabled = false;
        txtDetalle.Text = string.Empty;
        btnaceptar.Enabled = false;

    }
    private void FilterControls(int _problema)
    {
        ShowControls();
        DataTable t = returnValidatedFields(_problema);
        foreach (DataRow r in t.Rows)
	{
        
        txtn.Enabled = Convert.ToBoolean(r["Contacto"].ToString());
        radSistemaOP.Enabled = Convert.ToBoolean(r["SO"].ToString());
        RadTipoWin.Enabled = Convert.ToBoolean(r["LicenciaSO"].ToString());
        radTipoPC.Enabled = Convert.ToBoolean(r["PC"].ToString());
        txtAntiguedad.Enabled = Convert.ToBoolean(r["Antiguedad"].ToString());
        txtprocesador.Enabled = Convert.ToBoolean(r["Procesador"].ToString());
        txtram.Enabled = Convert.ToBoolean(r["Ram"].ToString());
        radTipoConexion.Enabled = Convert.ToBoolean(r["TipoConexion"].ToString());
        txtModeloMDM.Enabled = Convert.ToBoolean(r["MOdeloMDM"].ToString());
	}
        

    }
    private void ShowControls()
    {
        txtAntiguedad.Enabled = true;
        txtDetalle.Enabled = true;
        txtModeloMDM.Enabled = true;
        txtn.Enabled = true;
        txtprocesador.Enabled = true;
        txtram.Enabled = true;
        //txtAntiguedad.Text = "";
        //txtDetalle.Text = "";
        //txtModeloMDM.Text = "";
        //txtn.Text = "";
        //txtprocesador.Text = "";
        //txtram.Text = "";
        //radSistemaOP.ClearSelection();
        radSistemaOP.Enabled = true;
        //radTipoPC.ClearSelection();
        radTipoPC.Enabled = true;
        radTipoConexion.ClearSelection();
        radTipoConexion.Enabled = true;
        //RadTipoWin.ClearSelection();
        RadTipoWin.Enabled = true;





    }
    private bool ValidateProblemItem(int _problema)
    {
        
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SqlCommand SQLC;
        string q = "SELECT COUNT(*) FROM camposProblema where IdTipoProblema =" + _problema;
        
        SQLC = new SqlCommand(q, SQL);
        try
        {
            SQL.Open();
            int res = (int)SQLC.ExecuteScalar();

            if (res > 0) { return true; } else { return false; }

        }
        catch (Exception ex)
        {

            throw ex;
        }
        finally
        {
            SQL.Close();
        }

    }


    #endregion
    #region Datos
    private DataTable GetDatatkt()
    {
        DataTable dtprob = new DataTable();
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        System.Data.SqlClient.SqlDataAdapter Adapter;
        string tkt = Request.QueryString["id"];
        if (!(tkt == ""))
        {
            string query = "SELECT  tk.idtipoProblema,iso.sistemaOperativoId, iso.licenciaSo, tp.* from ticket tk left join ticketProblema tp on tk.ticketId=tp.ticketId  left join itemSO iso on tk.ticketId=iso.ticketId where tk.ticketid=" + tkt;
            Adapter = new SqlDataAdapter(query, SQL);
            Adapter.Fill(dtprob);
        }
        return dtprob;

    }
    private bool CequearEstado()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();

        string sEstado = Request.QueryString["estado"];

        sEstado = sEstado.Trim();

        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT Ultimo FROM estado WHERE Nombre = '" + sEstado + "'";
        object estadoDeCierre = CMD.ExecuteScalar();

        if (estadoDeCierre.ToString() == "1")
        {
            //SIENDO QUE EL TICKET YA SE ENCUENTRA CERRADO EL USUARIO NO POSEE PRIVILEGIOS PARA EDITARLO
            
            SQL.Close();
            return true;
        }
        SQL.Close();
        return false;
    }
    private DataTable returnValidatedFields(int _TipoProblema)
    {
        DataTable campos = new DataTable();
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        System.Data.SqlClient.SqlDataAdapter Adapter;
        string q = "SELECT * FROM CamposProblema where IdTipoProblema =" + _TipoProblema;
        Adapter = new SqlDataAdapter(q, SQL);
        Adapter.Fill(campos);
        return campos;
    
    
    }

    #endregion
    #region OtrasFunciones
    private string WriteData()
    {
        string str = " Tipo Problema:" + Radtipo.Text.Trim() + "|" + " Contacto :" + txtn.Text.Trim() + "|" + " PC :" + radTipoPC.Text.Trim() + "|" +
            " Antiguedad :" + txtAntiguedad.Text.Trim() + "|" + " Procesador:" + txtprocesador.Text.Trim() + "|" +
            " Ram :" + txtram.Text.Trim() + "|" + " Sist.OP :" + radSistemaOP.Text.Trim() + "|" + " Windows :" + RadTipoWin.Text.Trim() + "|" +
            " Modem :" + txtModeloMDM.Text.Trim() + "|" + " TipoConexión :" + radTipoConexion.Text.Trim() + "|" + " Detalle :" + txtDetalle.Text.Trim();
        return str;
    }
    #endregion
}
    


    
             
           
               
               
                
            
         
                

                
        

