﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="liq.aspx.vb" Inherits="liq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
    <div align="right">
    </div>

          <div align="center">
          <table id="tb1" border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 100%; border-bottom: black 1px solid">
                <caption align="top" style="font-size: large">Actualización Liquidación CSA<br />
                    Nro. TKT
                    <asp:Label ID="lbtkt" runat="server" Text="" 
                        style="font-weight: 700; font-style: italic;"></asp:Label>
                </caption>
                
                    <br />
                    <tr >
                        <td align="center">
                            <table border="0" bordercolor="white" bordercolordark="#ffffff" 
                                bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr >
                                    <td align="right">
                                        &nbsp;</td>
                                    <td align="left" style="width: 161px">
                                        &nbsp;</td>
                                    <td align="right" style="width: 89px">
                                        &nbsp;</td>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                    <td align="right" style="width: 89px">
                                        &nbsp;</td>
                                    <td align="left" style="text-align: right">
                                        &nbsp;&nbsp;
                                        <asp:imageButton ID="Button2" runat="server" Text="Volver" 
                                            style="text-align: right;" Width="28px" Height="24px" ImageUrl="~/Images/arrow2_w.gif" 
            />
                                    </td>
                                </tr >
                                <tr >
            <td align="right"  style="width: 101px" >Viáticos KM</td >
                                    <td align="left" style="width: 161px">
                <asp:TextBox ID="km" runat="server" AutoPostBack="true" Width="68px">0</asp:TextBox>
                                    </td>
                                    <td align="right"  style="width: 89px">
                                        Precio x Km:</td>
                                    <td align="left">
                <asp:TextBox ID="pesos" runat="server" AutoPostBack="true" Width="61px">0</asp:TextBox> 
                                    </td>
                                    <td align="right" style="width: 89px">
                                        &nbsp;</td>
                                    
                                    <td>
                                          &nbsp;</td>
                                </tr >
                            </table>
                        </td>
                    </tr >
                    <tr >
                        <td align="center">
                            <table border="0" bordercolor="white" bordercolordark="#ffffff" 
                                bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr >
                                    <td align="right" style="width: 138px" >
                                        Total Viaticos $</td>
                                    <td align="left" style="width: 286px">
                <asp:Label ID="lbtotal" runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align="center" style="width: 133px" >
                <asp:Image ID="comimg" runat="server" Visible="False" Width="50" Height="50" />
                <br>

                <asp:HyperLink
                    ID="ampliar" Visible="false" runat="server" Target="_blank">Ampliar</asp:HyperLink>
                                    </td>
                                    <td align="left" >
                                        &nbsp;
                                        <asp:FileUpload ID="FileUpload1" runat="server" Width="252px" 
                                            style="text-align: right" />
                                        </td>
                                </tr >
                                <tr >
          <td align="right"  style="width: 138px" >Código Reparación:</td >
                                    <td align="left" style="width: 286px">
                                        <asp:Label ID="lbcodrep" runat="server" Text="" style="font-weight: 700"></asp:Label>
               
            

                                    </td>
                                    <td align="right" style="width: 133px" >
                                    Nuevo Cód. Rep.
                                        &nbsp;</td>
                                    <td align="left" >
            <asp:DropDownList ID="ddcodrep" runat="server" DataSourceID="sqlcodrep" 
        DataTextField="tipo" DataValueField="tipo" AutoPostBack="True" Height="19px" Width="168px">
                </asp:DropDownList>
               
            

                                    </td>
                                </tr >
                            </table>
                        </td>
                    </tr >
                    <tr >
                        <td align="center">
                            <table border="0" bordercolor="white" bordercolordark="#ffffff" 
                                bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr >
            <td   style="width: 193px; height: 44px;">Total Costo Reparacion</td >
            <td style="height: 44px; text-align: left;"  >$
                <asp:Label ID="costo" runat="server" Text="0"></asp:Label></td >
                                    <td align="right" width="15%" style="height: 44px">
                                        &nbsp;</td>
                                    <td align="left" width="35%" style="height: 44px">
                                        </td>
                                </tr >
                                <tr >
                                    <td align="left" style="width: 193px">
                                        <asp:Label ID="lbcerrado" runat="server" ForeColor="Red" 
                                            style="font-size: x-large; font-weight: 700" Text="El tkt esta cerrado" 
                                            Visible="False"></asp:Label>
                                    </td>
                                    <td align="left">
                                        &nbsp;</td>
                                    <td align="right">

                <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align="left" width="35%">
                                        <asp:CheckBox ID="ok" Visible="false" runat="server" />
                                    </td>
                                </tr >
                            </table>
                        </td>
                    </tr >
                    <tr >
                        <td align="center">
                            <table border="0" bordercolor="white" bordercolordark="#ffffff" 
                                bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" 
                                style="width: 100%; height: 97px;">
                                <tr >
               
                        <td>
                
    <asp:Button ID="Button1" runat="server" style="text-align: center" 
        Text="Actualizar" />
                
                        </td>
                    </tr >
                 
                </caption>
            </table>
     
    <br />
        </div>

    <asp:SqlDataSource ID="sqlcodrep" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        SelectCommand="SELECT [tipo] FROM [codrep]"></asp:SqlDataSource>

    </asp:Content>

