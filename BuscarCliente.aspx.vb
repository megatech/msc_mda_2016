﻿Imports Telerik.Web.UI

Partial Class BuscarCliente
    Inherits System.Web.UI.Page

    Protected Sub RadGrid1_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gridResultados.ItemCommand
        If e.CommandName = "Select" Then
            Dim ret As Dictionary(Of String, String) = New Dictionary(Of String, String)
            ret("clienteId") = e.Item.Cells(3).Text.Replace("'", "")
            ret("razonSocial") = e.Item.Cells(4).Text.Replace("'", "")
            Popups.PopupMgr.Cerrar(Me, "retCliente", ret)
        End If
    End Sub

    Protected Sub RadTextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        actualizarGrid()
    End Sub

    Protected Sub gridResultados_SortCommand(sender As Object, e As GridSortCommandEventArgs) Handles gridResultados.SortCommand
        actualizarGrid()
    End Sub

    Public Sub actualizarGrid()
        If txtBusqueda.Text.Trim.Length > 0 Then
            Dim txt = txtBusqueda.Text.Trim.Replace(" ", "%")
            Dim cond = " AND (ClienteId like '%" & txt & "%' OR " &
                             "razonSocial like '%" & txt & "%' OR " &
                             " domicilio like '% " & txt & "%' OR " &
                             " telefono1 like '%" & txt & "%' OR " &
                             " CUIT like '%" & txt & "%')"
            If tipo.SelectedValue <> "todo" Then cond = " AND " & tipo.SelectedValue & " like '%" & txt & "%'"
            dsClientes.SelectCommand = "SELECT Distinct ClienteId, RazonSocial, CUIT, Domicilio, Localidad FROM viewCliente vc WHERE 1=1 " & cond
            gridResultados.DataBind()
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim a = tipo.SelectedValue

    End Sub
End Class
