﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="CambioClave.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Login</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="text-align: center">
            <table style="width: 90%; height: 100%">
                <tr>
                    <td>&nbsp;</td>
                    <td >&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="">
                    </td>
                    <td style="" valign="top">
                        <table border="0" cellpadding="3" style="width: 40%" class="TablaLogin" align="center">
                          <tr align="left" class="headerLogin">
                              <td colspan="2">
                                  Cambio clave</td>
                          </tr>
                            <tr>
                                <td align="right" width="35%" class="TextoEtiqueta">Usuario:</td>
                                <td align="left">
                                    <asp:Label ID="lblUsuario" runat="server" SkinID="lblField"></asp:Label>
                                    </td>
                            </tr>
                            <tr>
                                <td align="right" class="TextoEtiqueta">
                                    Clave actual:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtClave" runat="server" TextMode="Password" Width="140px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvClave" runat="server" ErrorMessage="Debe ingresar la clave" ControlToValidate="txtClave">*</asp:RequiredFieldValidator>
                                    </td>
                            </tr>
                            <tr>
                                <td align="right" class="TextoEtiqueta">
                                    Nueva clave:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtNuevaClave" runat="server" TextMode="Password" Width="140px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvNuevaClave" runat="server" ErrorMessage="Debe ingresar la nueva clave" ControlToValidate="txtNuevaClave">*</asp:RequiredFieldValidator>
                                    </td>
                            </tr>
                            <tr>
                                <td align="right" class="TextoEtiqueta">
                                    Repetir:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtReClave" runat="server" TextMode="Password" Width="140px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvReClave" runat="server" ErrorMessage="Debe repetir la clave" ControlToValidate="txtReClave"></asp:RequiredFieldValidator>
                                    </td>
                            </tr>
                            <tr>
                                <td align="right">
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click"  />
                                    &nbsp;&nbsp;&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <asp:Label ID="lblMensajeError" SkinID="lblMensajeError" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <asp:ValidationSummary ID="validSummary" runat="server"
                                        Width="100%" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    
    </div>
    </form>
</body>
</html>
