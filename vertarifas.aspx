﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="vertarifas.aspx.vb" Inherits="vertarifas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">

<div align="center" style="width: 700px">
<h3 align="center" >Tarifas cargadas</h3>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" 
        GridLines="None" Width="478px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Codigo" HeaderText="Codigo" 
                SortExpression="Codigo" />
            <asp:BoundField DataField="Especialista" HeaderText="Especialista" 
                SortExpression="Especialista" />
            <asp:BoundField DataField="Costo" HeaderText="Costo" SortExpression="Costo" />
            <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" 
                SortExpression="Observaciones" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT cod as Codigo, especialista.nombre as Especialista, valor as Costo, obs as Observaciones  
FROM tarifario
inner join especialista on especialista.especialistaid = tarifario.cas
order by 2,1">
    </asp:SqlDataSource>
    </div>
</asp:Content>

