﻿Imports System
Imports System.Data
Imports System.Web.UI.WebControls
Imports App_Code.TicketsLib
Imports System.Text
Imports System.IO
Imports System.Web.UI.HtmlControls
Imports Telerik.Web.UI


Partial Class recorrido
    Inherits System.Web.UI.Page


    Dim sql As New Data.SqlClient.SqlConnection
    Dim query As New Data.SqlClient.SqlCommand
    Dim query2 As New Data.SqlClient.SqlCommand


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        'Dim preguntas() As String
        'Dim uname As String
        'Dim userid As String

        'MsgBox(Session("usuario").Perfil)



        'userid = "and ticketid = 0"

        'uname = Session.Item("sessid")


        'sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString

        'Dim i As Long = 0, j As Long = 0

        'sql.Open()
        'query.Connection = sql

        'query.CommandText = "select condicion from usuariocondicionagenda where usuarioid =(select top 1 usuarioid from usuario where nombre = '" & uname & "')"

        'Dim rs As System.Data.SqlClient.SqlDataReader = query.ExecuteReader

        'While rs.Read
        '    ReDim Preserve preguntas(0 To i)
        '    preguntas(i) = rs(0)
        '    i = i + 1
        'End While

        'If rs.HasRows Then


        '    rs.Close()

        '    userid = preguntas(0)

        '    userid = "and " & userid & ""



        'End If

        'sql.Close()

        '        SqlDataSource1.SelectCommand = "SELECT Ticket.TICKETID as 'Ticket' , Ticket.CLAINNRO as 'Clain' , " _
        '& "Ticket.FECHA as 'Fecha creacion', " _
        '& "Ticket.CLIENTEID + ' ' + VC.RazonSocial as 'Cliente' , " _
        '& "Ticket.FechaInicio as 'fecha de inicio',   Equipo.Nombre as 'equipo', Ticket.NroSerie as 's/n', " _
        '& "Especialista.Nombre as 'Especialista' , TipoProblema.Nombre as 'tipo problema' , " _
        '& "Estado.Nombre as 'Estado',   EmpresaPartner.Nombre as 'Negocio' " _
        '& "FROM Ticket (NOLOCK) " _
        '& "INNER JOIN Equipo (nolock) ON Ticket.ArticuloId = Equipo.EquipoId " _
        '& "INNER JOIN Especialista (nolock) ON Ticket.EspecialistaId = Especialista.EspecialistaId " _
        '& "INNER JOIN TipoProblema (nolock) ON Ticket.IdTipoProblema = TipoProblema.IdTipoProblema " _
        '& "INNER JOIN EmpresaPartner (nolock) ON Ticket.EmpresaPartnerId = EmpresaPartner.EmpresaPartnerId " _
        '& "INNER JOIN Estado (nolock) ON Ticket.EstadoId = Estado.EstadoId " _
        '& "INNER JOIN ViewCliente VC (nolock) ON Ticket.ClienteId = VC.ClienteId " _
        '& "INNER JOIN UbicacionContacto (nolock) ON Ticket.UbicacionContactoId = UbicacionContacto.UbicacionContactoId " _
        '& "WHERE 1 = 1 and Ticket.estadoid not in (2,10) " _
        '& "" & userid & ""

        'SqlDataSource1.Selectcommand = "Select TK.ticketid ,convert (char(20),TK.fecha,120) 'Fecha', convert (char(20),tk.FechaFin,120) 'Fecha Fin', " _
        '        & " Equipo.Nombre as 'Tipo Cliente',tk.clainnro, TK.NroSerie as 'Telefono',Estado.Nombre as 'Estado',VC.RazonSocial, vc.localidad, vc.domicilio " _
        '        & " from Ticket TK Left join Cliente_tk on  Cliente_tk.clienteid = TK.clienteid " _
        '        & " INNER JOIN Equipo (nolock) ON TK.ArticuloId = Equipo.EquipoId " _
        '        & " INNER JOIN TipoProblema (nolock) ON TK.IdTipoProblema = TipoProblema.IdTipoProblema " _
        '        & " INNER JOIN EmpresaPartner (nolock) ON TK.EmpresaPartnerId = EmpresaPartner.EmpresaPartnerId  " _
        '        & " INNER JOIN Estado (nolock) ON TK.EstadoId = Estado.EstadoId " _
        '        & " INNER JOIN ViewCliente VC (nolock) ON TK.ClienteId = VC.ClienteId " _
        '        & " INNER JOIN UbicacionContacto (nolock) ON TK.UbicacionContactoId = UbicacionContacto.UbicacionContactoId " _
        '        & " INNER JOIN Ubicacion (nolock) ON UbicacionContacto.UbicacionId = Ubicacion.UbicacionId " _
        '        & " INNER JOIN Especialista (nolock) ON TK.EspecialistaId = Especialista.EspecialistaId " _
        '        & " Where tk.estadoid in (135,139,140,123,118,119,155) and TK.EmpresaPartnerId in (57) " _
        '        & " order by tk.fecha"


    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        query.Dispose()
        sql.Dispose()
    End Sub

    'Protected Sub gv_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gv.RowCommand

    '    If e.CommandName = "Select" Then
    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim selectedRow As GridViewRow = gv.Rows(index)
    '        Dim tktCell As TableCell = selectedRow.Cells(1)
    '        Dim tkt As String = tktCell.Text

    '        Response.Redirect("EditaTicket.aspx?id=" & tkt & "")

    '    End If
    'End Sub

    Protected Sub SqlDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSource1.Selected
        lblCount.Text = IIf(e.AffectedRows > 0, String.Format("Resultados: {0}", e.AffectedRows), "")
    End Sub


    Private Sub exportarAExcel(titulo As String, gv As GridView)

        Dim sb As New StringBuilder()
        Dim sw As New StringWriter(sb)
        Dim htw As New HtmlTextWriter(sw)
        Dim pagina As New Page()
        '        pagina.StyleSheetTheme = "~/css/gv.css"

        'Dim form As dynamic = New HtmlForm()

        gv.AllowSorting = False
        gv.AllowPaging = False

        Dim grid As GridView = gv
        gv.Columns(0).Visible = False
        gv.Columns(1).Visible = False
        Dim t As New HtmlTable()
        Dim lblT As New Label()
        lblT.Text = titulo
        lblT.Font.Bold = True
        lblT.Font.Size = 14
        t.Attributes.Add("class", "tableContent")
        t.Rows.Add(New HtmlTableRow())
        t.Rows.Add(New HtmlTableRow())
        t.Rows(0).Cells.Add(New HtmlTableCell())
        t.Rows(1).Cells.Add(New HtmlTableCell())
        t.Rows(0).Cells(0).Controls.Add(lblT)
        t.Rows(1).Cells(0).Controls.Add(grid)

        grid.HeaderStyle.Font.Underline = False
        grid.HeaderStyle.ForeColor = System.Drawing.Color.White
        grid.EnableSortingAndPagingCallbacks = False

        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(t)
        form.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=ResultadosDeBusqueda_" & DateTime.Now.ToString("dd-MM-yyyy_HH.mm") & ".xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.[Default]
        Response.Write("<p><br><p>")
        Response.Write(sb.ToString())
        Response.[End]()
    End Sub

  

    Protected Sub exportar_Click(sender As Object, e As EventArgs) Handles exportar.Click
        'Dim sb As New StringBuilder()
        'Dim sw As New StringWriter(sb)
        'Dim htw As New HtmlTextWriter(sw)
        'Dim page As New Page()
        'Dim form As New HtmlForm()



        'gv.EnableViewState = False
        'page.EnableEventValidation = False
        'page.DesignerInitialize()
        'page.Controls.Add(form)
        'form.Controls.Add(gv)
        'page.RenderControl(htw)
        'Response.Clear()
        'Response.Buffer = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename= Recorido_FS_" & Date.Now & " .xls")
        'Response.Charset = "UTF-8"
        'Response.ContentEncoding = Encoding.[Default]
        'Response.Write(sb.ToString())
        'Response.[End]()

        gv.ExportSettings.ExportOnlyData = True
        gv.ExportSettings.OpenInNewWindow = True
        gv.MasterTableView.ExportToExcel()
        ''exportarAExcel("Coordinacion", gv)

       
       

    End Sub

    Protected Sub Actualizar_Click(sender As Object, e As EventArgs)
        gv.Rebind()
    End Sub
End Class
