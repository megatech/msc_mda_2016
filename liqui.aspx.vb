﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports App_Code.TicketsLib


Partial Class liq
    Inherits System.Web.UI.Page

    Dim SQL As New SqlConnection
    Dim CMD As New SqlCommand
    Dim cmd2 As New SqlCommand
    Dim RS As SqlDataReader
    Dim tRow() As TableRow
    Dim cID() As TableCell
    Dim cRazonSocial() As TableCell
    Dim cNombreEquipo() As TableCell
    Dim cEmpresaPartner() As TableCell
    Dim cEspecialista() As TableCell
    Dim cConfirmar() As TableCell
    Dim cViatico() As TableCell
    Dim cCosto() As TableCell
    Private chkBox() As CheckBox
    Private WithEvents chkBoxEventHandle As CheckBox

    Private triggers() As AsyncPostBackTrigger

    Dim Link() As HyperLink

    Dim HTMLTable As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        On Error Resume Next

        Dim ususarioactual As Usuario

        ususarioactual = Session("usuario")

        If ususarioactual.Perfil <> 1 Then
            Response.Redirect("Default2.aspx")
        End If

        Dim esp As String
        esp = Session("especialista")
        Dim i As Long

        If Session("especialista") Is Nothing Then Response.Redirect("aliq.aspx")


        SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
        SQL.Open()
        CMD.Connection = SQL
        CMD.CommandText = "SELECT tk.ticketid, vc.RazonSocial,va.Nombre,ep.Nombre,Especialista.nombre,tk.autorizado,lc.viatico,lc.viaticocant,costorep " & _
                        " FROM ticket tk inner join viewcliente vc on vc.clienteid = tk.clienteid " & _
                        "  inner join EmpresaPartner ep on Ep.EmpresaPartnerid = tk.EmpresaPartnerid inner join Especialista on Especialista.Especialistaid = tk.Especialistaid " & _
                        " inner join equipo va on va.equipoid = tk.articuloid inner join liquidacion_cas lc on lc.ticketid = tk.ticketid " & _
                        " WHERE(tk.especialistaid = " & esp & " And tk.autorizado = 0) "


        RS = CMD.ExecuteReader

        While RS.Read
            'CREAMOS LA FILA DINAMICA
            ReDim Preserve tRow(i)
            tRow(i) = New TableRow

            'CREAMOS LAS CELDAS DINAMICAS
            ReDim Preserve cID(i)
            cID(i) = New TableCell
            ReDim Preserve cRazonSocial(i)
            cRazonSocial(i) = New TableCell
            ReDim Preserve cNombreEquipo(i)
            cNombreEquipo(i) = New TableCell
            ReDim Preserve cEmpresaPartner(i)
            cEmpresaPartner(i) = New TableCell
            ReDim Preserve cEspecialista(i)
            cEspecialista(i) = New TableCell
            ReDim Preserve cConfirmar(i)
            cConfirmar(i) = New TableCell
            ReDim Preserve cViatico(i)
            cViatico(i) = New TableCell
            ReDim Preserve cCosto(i)
            cCosto(i) = New TableCell
            ReDim Preserve Link(i)
            Link(i) = New HyperLink

            Me.LoadViewState(TblConfirmar)

            'CREAMOS EL TRIGGER ASOCIADO AL CHECKBOX
            ReDim Preserve triggers(i)
            triggers(i) = New AsyncPostBackTrigger

            'CREAMOS EL CHECKBOX ASOCIADO AL REGISTRO
            ReDim Preserve chkBox(i)
            chkBox(i) = New CheckBox
            chkBox(i).ID = "chk" & i
            chkBox(i).Checked = True

            'AGREGAMOS EL MANEJADOR DEL EVENTO CHECKEDCHANGED ASOCIADO AL PUNTERO DE LA FUNCION CHECKEDCHANGED
            AddHandler (chkBox(i).CheckedChanged), AddressOf CheckedChanged
            'NECESARIO PARA LA SUMA AUTOMATICA
            chkBox(i).AutoPostBack = True
            'ASOCIAMOS UN TRIGGER AL MANEJADOR DE EVENTO ASOCIADO AL CONTROL CON ANTERIORIDAD NECESARIO PARA EL POSTBACK DEL CONTROL
            triggers(i).ControlID = chkBox(i).ID
            triggers(i).EventName = "CheckedChanged"

            'ASOCIAMOS EL TRIGGER AL CONTROL UPDATEPANEL ENCARGADO DE DISPARAR EL EVENTO ASOCIADO AL CHECKBOX QUE REALIZARA LA AUTOSUMA DE LOS 
            'VALORES ANTE UN POSIBLE POSTBACK REALIZADO POR EL CONTROL
            UPDATE.Triggers.Add(triggers(i))
            SC.RegisterAsyncPostBackControl(chkBox(i))

            'ESTABLECEMOS LAS OPCIONES PARA EL CAMPO DINAMICO ID
            cID(i).BorderStyle = BorderStyle.Double
            cID(i).BorderWidth = 2
            Link(i).Text = RS(0).ToString
            Link(i).NavigateUrl = "editaticket.aspx?id=" & RS(0).ToString
            cID(i).Controls.Add(Link(i))

            'ESTABLECEMOS LAS OPCIONES PARA EL CAMPO DINAMICO RAZON SOCIAL
            cRazonSocial(i).BorderStyle = BorderStyle.Double
            cRazonSocial(i).BorderWidth = 2
            cRazonSocial(i).Text = RS(1).ToString

            'ESTABLECEMOS LAS OPCIONES PARA EL CAMPO DINAMICO NOMBRE EQUIPO
            cNombreEquipo(i).BorderStyle = BorderStyle.Double
            cNombreEquipo(i).BorderWidth = 2
            cNombreEquipo(i).Text = RS(2).ToString

            'ESTABLECEMOS LAS OPCIONES PARA EL CAMPO DINAMICO NOMBREEMPRESAPARTNER
            cEmpresaPartner(i).BorderStyle = BorderStyle.Double
            cEmpresaPartner(i).BorderWidth = 2
            cEmpresaPartner(i).Text = RS(3).ToString

            'ESTABLECEMOS LAS OPCIONES PARA EL CAMPO DINAMICO ESPECIALISTA
            cEspecialista(i).BorderStyle = BorderStyle.Double
            cEspecialista(i).BorderWidth = 2
            cEspecialista(i).Text = RS(4).ToString
            'Viaticos

            cViatico(i).BorderStyle = BorderStyle.Double
            cViatico(i).BorderWidth = 2
            cViatico(i).Text = "$" & CStr(RS(6)) * CStr(RS(7))

            'Costo de reparacion

            cCosto(i).BorderStyle = BorderStyle.Double
            cCosto(i).ID = "precio" & i
            cCosto(i).BorderWidth = 2
            cCosto(i).Text = "$" & RS(8).ToString


            'AGREGAMOS EL CHECKBOX CORRESPONDIENTE A ESTA FILA DE LA BD
            chkBox(i).ID = "chk" & i

            cConfirmar(i).Controls.Add(chkBox(i))
            cConfirmar(i).BorderStyle = BorderStyle.Double
            cConfirmar(i).BorderWidth = 2

            'AGREGAMOS LAS FILAS ACTUALES A LA COLUMNA ACTUAL
            tRow(i).Cells.Add(cID(i))
            tRow(i).Cells.Add(cRazonSocial(i))
            tRow(i).Cells.Add(cNombreEquipo(i))
            tRow(i).Cells.Add(cEmpresaPartner(i))
            tRow(i).Cells.Add(cEspecialista(i))
            tRow(i).Cells.Add(cViatico(i))
            tRow(i).Cells.Add(cCosto(i))
            tRow(i).Cells.Add(cConfirmar(i))

            'AGREGAMOS LA FILA ACTUAL A LA TABLA
            cEspecialista(i).BorderStyle = BorderStyle.Double
            cEspecialista(i).BorderWidth = 2
            TblConfirmar.Rows.Add(tRow(i))

            'SUMAMOS UNO A LA VARIABLE PARA RECREAR LOS CAMPOS DE LA SIGUIENTE FILA
            i += 1
        End While

        i = 0

        If IsPostBack = False Then
            'SI LA PAGINA SE ESTA CARGANDO POR PRIMERA VEZ HACEMOS EL CALCULO CASO CONTRARIO
            'LO HACE EL TRIGGER EVENT HANDLER AGREGADO A CADA CHECKBOX
            Dim suma As Double
            For i = 1 To TblConfirmar.Rows.Count - 1
                If chkBox(i - 1).Checked = True Then
                    Dim numero1 As Double = Trim(Replace(TblConfirmar.Rows(i).Cells(5).Text, "$", ""))
                    Dim numero2 As Double = Trim(Replace(TblConfirmar.Rows(i).Cells(6).Text, "$", ""))

                    suma = suma + numero1 + numero2
                End If
            Next
            lbtotalneto.Text = suma
        End If
        'CERRAMOS TODAS LAS CONEXIONES
        RS.Close()
        SQL.Close()

        ''  Liquidacion.SelectCommand = "SELECT id, RazonSocial,NombreEquipo,NombreEmpresaPartner,Especialista,tk.autorizado FROM view_partes_cas inner join ticket tk on view_partes_cas.id = tk.ticketid WHERE (CantidadPartesBuenas = CantidadPartesMalas) and tk.especialistaid = " & esp & " ORDER BY ID,CodigoParte"

    End Sub

    Protected Sub CmdActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdActualizar.Click
        SQL.Open()

        Dim i = 0
        Dim j As Long = 0
        Dim checked As Boolean = False

        'COMPROBAMOS SI AL MENOS ALGUNO DE LOS CHECKBOX ESTA TILDADO
        For i = 0 To chkBox.Count - 1
            If chkBox(i).Checked = True Then
                checked = True
            End If
        Next

        'EN CASO DE NO HABER ESTADO TILDADO LA VARIABLE CHECKED CONTENDRA EL VALOR FALSE
        If checked = False Then
            LabelError.Text = "Debe seleccionar al menos una opcion"

            Exit Sub
        End If

        HTMLTable = "<table border=" & Chr(34) & "1" & Chr(34) & ">"
        HTMLTable = HTMLTable & vbCrLf & "<tr>"

        For i = 0 To TblConfirmar.Rows(0).Cells.Count - 2
            HTMLTable = HTMLTable & vbCrLf & "<td bgcolor=" & Chr(34) & "#cccccc" & Chr(34) & ">" & TblConfirmar.Rows(0).Cells(i).Text & "</td>"
        Next

        HTMLTable = HTMLTable & vbCrLf & "</tr>"

        i = 1


        For i = 1 To TblConfirmar.Rows.Count - 1
            'If chkBox(i - 1).Checked Then
            If i = 0 Then
                HTMLTable = HTMLTable & vbCrLf & "<tr bgcolor=" & Chr(34) & "#cccccc" & Chr(34) & ">"
            Else
                HTMLTable = HTMLTable & vbCrLf & "<tr>"
            End If

            For j = 0 To TblConfirmar.Rows(i).Cells.Count - 2
                HTMLTable = HTMLTable & vbCrLf & "<td border=" & Chr(34) & "1" & Chr(34) & ">"

                If j = 0 And (i >= 1) Then
                    HTMLTable = HTMLTable & vbCrLf & Link(i - 1).Text
                Else
                    HTMLTable = HTMLTable & vbCrLf & TblConfirmar.Rows(i).Cells(j).Text
                End If

                HTMLTable = HTMLTable & vbCrLf & "</td>"
            Next
            HTMLTable = HTMLTable & vbCrLf & "</tr>"
            'End If
        Next
        HTMLTable = HTMLTable & vbCrLf & "<tr>"
        HTMLTable = HTMLTable & vbCrLf & "<td colspan=" & Chr(34) & CStr(TblConfirmar.Rows(0).Cells.Count - 1) & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & ">Total: $" & lbtotalneto.Text & "</td>"
        HTMLTable = HTMLTable & vbCrLf & "</tr>"
        HTMLTable = HTMLTable & vbCrLf & "</table>"

        Dim FS As System.IO.FileStream
        FS = System.IO.File.Create("C:\Users\jsoto\Documents\Visual Studio 2010\WebSites\tsc\files\" & Session("especialista").ToString & ".html")
        FS.Close()

        Dim file As New System.IO.StreamWriter("C:\Users\jsoto\Documents\Visual Studio 2010\WebSites\tsc\files\" & Session("especialista").ToString & ".html")
        file.Write(HTMLTable)
        file.Close()

        i = 0
        TblConfirmar.Visible = False

        For i = 0 To chkBox.Count - 1
            'OBTENEMOS EL VALOR DE TILDE DE CADA CHECKBOX
            If chkBox(i).Checked = True Then
                'ESTE CHECKBOX ESTA TILDADO OBTENEMOS EL VALOR DE LA TABLA CORRESPONDIENTE A DICHO NRO DE TICKET
                Dim tkt As String = Link(i).Text

                CMD.CommandText = " update ticket set autorizado = 1 where ticketid =  " & tkt & " " & _
                " update liquidacion_cas set liquidacion_cas.autorizadook = 1 where liquidacion_cas.ticketid = " & tkt & " "
                CMD.ExecuteNonQuery()

            End If
        Next

        Dim correo As New System.Net.Mail.MailMessage
        Dim atach As New Attachment("C:\Users\jsoto\Documents\Visual Studio 2010\WebSites\tsc\files\" & Session.Item("especialista") & ".html")

        correo = New System.Net.Mail.MailMessage()
        correo.From = New MailAddress("jsoto@megatech.la")
        correo.To.Add("jsoto@megatech.la")
        correo.Subject = "Prueba de Correo"
        correo.IsBodyHtml = True
        correo.Attachments.Add(atach)
        correo.Body = HTMLTable
        correo.Priority = MailPriority.Normal

        Dim smtp As New System.Net.Mail.SmtpClient
        smtp.Host = "192.168.1.13"
        ''smtp.Credentials = New System.Net.NetworkCredential("usuario", "password")

        Try
            smtp.Send(correo)
            correo.Attachments.Remove(atach)
            LabelError.Text = "Mensaje enviado satisfactoriamente"
			SQL.Close()
            Response.Redirect("aliq.aspx")

        Catch ex As Exception
			If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()	
            LabelError.Text = "ERROR: " & ex.Message
        End Try
    End Sub

    Protected Sub CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'INSTANCIA NECESARIA DE CHECKBOX PARA DETERMINAR PROPIEDADES DEL SENDER (YA QUE EL SENDER ORIGINAL ES RECREADO CON CADA POSTBACK
        Dim send As CheckBox, suma As Double

        For i = 1 To TblConfirmar.Rows.Count - 1
            'ASOCIAMOS EL OBJETO CHECKBOX AL CONTROL EN CUESTION (OBJETO GESTIONADO DESDE VIEWSTATE)
            send = chkBox(i - 1)

            'DETERMINAMOS CUAL DE LOS OBJETOS GESTIONADOS DESDE EL VIEWSTATE GENERADO POR EL TRIGGER ASOCIADO AL CONTROLPANEL ESTA TILDADO
            'PARA PODER ASI ACTUAR EN CONSECUENCIA
            If send.Checked = True Then
                Dim numero1 As Double = Trim(Replace(TblConfirmar.Rows(i).Cells(5).Text, "$", ""))
                Dim numero2 As Double = Trim(Replace(TblConfirmar.Rows(i).Cells(6).Text, "$", ""))

                suma = suma + numero1 + numero2
            End If
        Next i
        'UNA VEZ REALIZADOS LOS CALCULOS NECESARIOS PROCEDEMOS A COMPLETAR EL VALOR DEL LABEL CON LA SUMA CORRESPONDIENTE DE VALORES
        lbtotalneto.Text = suma.ToString
    End Sub
End Class
