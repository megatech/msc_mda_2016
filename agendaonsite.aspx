﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="agendaOnsite.aspx.cs" Inherits="agendaOnsite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
        DataKeyNames="TICKET" DataSourceID="SqlDataSource1" ForeColor="#333333" 
        GridLines="None">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="TICKET" HeaderText="TICKET" InsertVisible="False" 
                ReadOnly="True" SortExpression="TICKET" />
            <asp:BoundField DataField="TECNICO o CAS" HeaderText="TECNICO o CAS" 
                SortExpression="TECNICO o CAS" />
            <asp:BoundField DataField="Horario1" HeaderText="Horario1" ReadOnly="True" 
                SortExpression="Horario1" />
            <asp:BoundField DataField="Horario2" HeaderText="Horario2" ReadOnly="True" 
                SortExpression="Horario2" />
            <asp:BoundField DataField="Horario3" HeaderText="Horario3" ReadOnly="True" 
                SortExpression="Horario3" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        onselecting="SqlDataSource1_Selecting" 
        SelectCommand="Select t.ticketId as Ticket, tec.nombre as 'Tecnico o CAS', 
case horario1 when '1' then '10 a 12' end as Horario1,
case horario2 when '1' then '13 a 15' end as Horario2,
case horario3 when '1' then '15 a 17' end as Horario3
from ticket t inner join ticketTecnico tt on t.ticketid=tt.ticketid inner join tecnico tec on tec.id=tt.tecnicoid">
    </asp:SqlDataSource>
</asp:Content>

