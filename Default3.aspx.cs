﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using App_Code.TicketsLib;


public partial class Default3 : System.Web.UI.Page
{
    private ArrayList seleccion;

    protected void Page_Load(object sender, EventArgs e)
    {
        Usuario usr = (Usuario)Session["Usuario"];
        if (usr == null)
        {
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString);
        }
        if (seleccion == null)
        {
            if ((ArrayList)Session["Seleccion"] == null) { Session["Seleccion"] = new ArrayList(); }
            seleccion = (ArrayList)Session["Seleccion"];
        }
        txtRes.Text = "";
        foreach (String itm in seleccion)
        { txtRes.Text += itm + " "; }

        if (!chkModMasiva.Enabled) { chkModMasiva.ToolTip = "Debe filtrar por Area, para habilitar la modificación masiva."; }
        else { chkModMasiva.ToolTip = ""; }
    }


    protected void btEdit_click(object sender, EventArgs e)
    {
        ImageButton bt = (ImageButton)sender;
        GridViewRow row = (GridViewRow)bt.NamingContainer;
        String id = row.Cells[2].Text.ToString().Trim();
        Response.Redirect("EditaTicket.aspx?id=" + id);
    }

    protected void refrescarSeleccion()
    {
        foreach (GridViewRow row in gvRes.Rows)
        {
            CheckBox chk = (CheckBox)row.Cells[0].FindControl("chk");
            String id = row.Cells[2].Text.ToString();
            if (chk.Checked && !seleccion.Contains(id)) { chk.Checked = false; }
            if (!chk.Checked) { chkAll.Checked = false; if (seleccion.Contains(id)) { chk.Checked = true; } }
            SetFocus(txtRes);
        }
    }

    protected void guardarSeleccion()
    {
        foreach (GridViewRow row in gvRes.Rows)
        {
            CheckBox chk = (CheckBox)row.Cells[0].FindControl("chk");
            String id = row.Cells[2].Text.ToString();
            if (chk.Checked && !seleccion.Contains(id)) { seleccion.Add(id); }
            if (!chk.Checked) { chkAll.Checked = false; if (seleccion.Contains(id)) { seleccion.Remove(id); } }
            SetFocus(txtRes);
        }

        seleccion.Sort();
        txtRes.Text = "";
        foreach (String itm in seleccion)
        { txtRes.Text += itm + " "; }
        Session["Seleccion"] = seleccion;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        gvRes.DataBind();
        if (ddArea.SelectedValue != "-1")
        {
            chkModMasiva.Enabled = true;
            if (chkModMasiva.Checked)
            {
                btCommit.Enabled = true;
                ddTecnicoNuevo.Enabled = true;
            }
            dsTecnicosYCAS2.SelectCommand =
                "SELECT 'Seleccione una opción' 'Nombre', '-1' id UNION " +
                "SELECT 'Aun no asignado' 'Nombre', 0 id UNION " +
                "SELECT Nombre, id FROM tecnico WHERE areaId=" + ddArea.SelectedValue + " order by id";
            ddTecnicoNuevo.DataBind();
        }
        else
        {
            chkModMasiva.Enabled = false;
            btCommit.Enabled = false;
            ddTecnicoNuevo.Enabled = false;

        }
    }


    protected void aplicarFiltros()
    {
        String condFiltros = "";
        if (ddArea.SelectedValue.ToString() != "-1") { condFiltros += " AND areaId =" + ddArea.SelectedValue; }
        if (ddTecnico.SelectedValue.ToString() != "-1" && ddTecnico.SelectedValue.ToString() != "0") { condFiltros += " AND tecnicoId =" + ddTecnico.SelectedValue; }
        if (ddTecnico.SelectedValue.ToString() == "0") { condFiltros += " AND tecnicoId is NULL"; }
        if (!dpFCreacionDesde.IsEmpty) { condFiltros += " AND [F. Creación] >= '" + dpFCreacionDesde.SelectedDate.Value.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!dpFCreacionHasta.IsEmpty) { condFiltros += " AND [F. Creación] <= '" + dpFCreacionHasta.SelectedDate.Value.AddHours(24).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!dpFUltimaModDesde.IsEmpty) { condFiltros += " AND [F. Última Modif.] >= '" + dpFUltimaModDesde.SelectedDate.Value.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!dpFUltimaModHasta.IsEmpty) { condFiltros += " AND [F. Última Modif.] <= '" + dpFUltimaModHasta.SelectedDate.Value.AddHours(24).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!dpFCoordinadaDesde.IsEmpty) { condFiltros += " AND [F.Coordinada] >= '" + dpFCoordinadaDesde.SelectedDate.Value.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!dpFCoordinadaHasta.IsEmpty) { condFiltros += " AND [F.Coordinada] <= '" + dpFCoordinadaHasta.SelectedDate.Value.AddHours(24).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!dpFSolucionDesde.IsEmpty) { condFiltros += " AND [F.Solución] >= '" + dpFSolucionDesde.SelectedDate.Value.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!dpFSolucionHasta.IsEmpty) { condFiltros += " AND [F.Solución] <= '" + dpFSolucionHasta.SelectedDate.Value.AddHours(24).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!dpFCargaDesde.IsEmpty) { condFiltros += " AND [F.Carga] >= '" + dpFCargaDesde.SelectedDate.Value.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!dpFCargaHasta.IsEmpty) { condFiltros += " AND [F.Carga] <= '" + dpFCargaHasta.SelectedDate.Value.AddHours(24).ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (ddProvincia.SelectedValue.ToString() != "-1") { condFiltros += " AND provinciaid ='" + ddProvincia.SelectedValue + "'"; }
        if (ddEstado.SelectedValue.ToString() != "-1") { condFiltros += " AND estadoid ='" + ddEstado.SelectedValue + "'"; }
        if (txtLocalidad.Text != "") { condFiltros += " AND localidad like '%" + txtLocalidad.Text + "%'"; }
        if (lstHorarios.Items[0].Selected) { condFiltros += " AND h1 = 1"; }
        if (lstHorarios.Items[1].Selected) { condFiltros += " AND h2 = 1"; }
        if (lstHorarios.Items[2].Selected) { condFiltros += " AND h3 = 1"; }
        if (chkFiltrarCerrados.Checked) { condFiltros += " AND cerrado=0"; }

        dsGridView.SelectCommand = "SELECT [Ticket], [Estado], [Cliente], [Localidad]," +
                                       "(case when h1=1 then '10a12 ' else NULL  end)+" +
                                       "(case  when h2=1  then '13a15 '  else NULL  end)+" +
                                       "(case  when h3=1  then '15a17 '  else NULL  end) as Horarios," +
                                       "[F.Coordinada] , [F. Creación] , [F. Ultima Modif.]" +
                               "FROM [ViewOnsite]" +
                               "WHERE 1=1" + condFiltros;

        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT [Ticket], [Estado], [Cliente], [Localidad]," +
                                       "(case when h1=1 then '10a12 ' else NULL  end)+" +
                                       "(case  when h2=1  then '13a15 '  else NULL  end)+" +
                                       "(case  when h3=1  then '15a17 '  else NULL  end) as Horarios," +
                                       "[F.Coordinada] , [F. Creación] , [F. Ultima Modif.]" +
                               "FROM [ViewOnsite]" +
                               "WHERE 1=1" + condFiltros;
        RS = CMD.ExecuteReader();
        int count = 0;
        while (RS.Read())
        { count += 1; }
        LblCntRes.Text = "Resultados: " + count.ToString();
		SQL.Close();

    }

    protected void gvRes_DataBinding(object sender, EventArgs e)
    {
        aplicarFiltros();
    }

    protected void gvRes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        guardarSeleccion();
        aplicarFiltros();
    }

    protected void gvRes_Sorting(object sender, GridViewSortEventArgs e)
    {
        guardarSeleccion();
        aplicarFiltros();
    }

    protected void gvRes_OnDataBound(object sender, EventArgs e)
    {
        refrescarSeleccion();
    }

    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        if (chkAll.Checked)
        {
            foreach (GridViewRow row in gvRes.Rows)
            {
                CheckBox chk = (CheckBox)row.Cells[0].FindControl("chk");
                String id = row.Cells[2].Text.ToString();
                chk.Checked = true;
            }
        }
        else
        {
            foreach (GridViewRow row in gvRes.Rows)
            {
                CheckBox chk = (CheckBox)row.Cells[0].FindControl("chk");
                String id = row.Cells[2].Text.ToString();
                chk.Checked = false;
            }
        }
        guardarSeleccion();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        seleccion.Clear();
        txtRes.Text = "";
        refrescarSeleccion();
    }
    protected void chkCommit_CheckedChanged(object sender, EventArgs e)
    {
        if (chkModMasiva.Checked)
        { ddTecnicoNuevo.Enabled = true; btCommit.Enabled = true; }
        else
        { ddTecnicoNuevo.Enabled = false; btCommit.Enabled = false; }
    }

    protected void chkChanged(object sender, EventArgs e)
    {
        guardarSeleccion();
    }
    protected void btCommit_Click(object sender, EventArgs e)
    {
        String updWhere = "";
        String log = "";
        if (seleccion.Count > 0)
        {
            for (int i = 0; i <= seleccion.Count - 1; i++)
            {
                if (i > 0) { updWhere += " OR ticket=" + seleccion[i]; }
                else { updWhere += " ticket=" + seleccion[i]; }
            }
        }

        String updTecnico = "";
        if (ddTecnicoNuevo.SelectedValue != "-1" && ddTecnicoNuevo.Enabled && ddTecnicoNuevo.SelectedValue != "0")
        { updTecnico += " SET tecnicoId=" + ddTecnicoNuevo.SelectedValue; }
        if (ddTecnicoNuevo.Enabled && ddTecnicoNuevo.SelectedValue == "0") { updTecnico += " SET tecnicoId= NULL"; }

        if (updTecnico != "" && seleccion.Count > 0)
        {
            System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
            System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
            System.Data.SqlClient.SqlDataReader RS;

            SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
            SQL.Open();
            CMD.Connection = SQL;
            CMD.CommandText = "SELECT ticket, cerrado, onsiteId, informeInterno, tecnicoId FROM viewOnsite WHERE" + updWhere;

            System.Data.SqlClient.SqlConnection SQL2 = new System.Data.SqlClient.SqlConnection();
            System.Data.SqlClient.SqlCommand CMD2 = new System.Data.SqlClient.SqlCommand();
            SQL2.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
            SQL2.Open();
            CMD2.Connection = SQL2;

            RS = CMD.ExecuteReader();

            while (RS.Read())
            {
                if (RS[1] == "1") { log += RS[0] + "-> ERROR - TICKET CERRADO. No se efectuó ninguna modificación." + Environment.NewLine; }
                else
                {
                    if (RS.IsDBNull(2))
                    {
                        CMD2.CommandText = "INSERT INTO tickettecnico(ticketId) VALUES(" + RS[0] + ")";
                        try { CMD2.ExecuteNonQuery(); }
                        catch (Exception ex)
                        { log += RS[0] + "-> ERROR EN LA ACTUALIZACIÓN:" + ex.Message + Environment.NewLine; }
                    }

                    Usuario usr = (Usuario)Session["Usuario"];
                    String ii= RS[3].ToString();
                    String logUpd = DateTime.Now.ToString() + " Modifico (" + usr.Nick.ToString().Trim() + ")" + Environment.NewLine + " Tecnico Asignado: " + ddTecnicoNuevo.SelectedItem.Text + Environment.NewLine;
                    String tecId = RS[4].ToString();
                    bool huboModificacion = !((RS.IsDBNull(4) && ddTecnicoNuevo.SelectedValue == "0") || (RS[4]==ddTecnicoNuevo.SelectedValue));
                    try
                    {
                        CMD2.CommandText = "UPDATE tickettecnico " + updTecnico + " WHERE ticketId=" + RS[0];
                        if (CMD2.ExecuteNonQuery() >= 0)
                        {
                            log += RS[0] + "-> TICKET ACTUALIZADO CORRECTAMENTE." + Environment.NewLine;
                            if (huboModificacion)
                            {
                                CMD2.CommandText = "UPDATE ticket SET informeInterno='" + RS[3] + logUpd + "' WHERE ticketId=" + RS[0];
                                CMD2.ExecuteNonQuery();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log += RS[0] + "-> ERROR EN LA ACTUALIZACIÓN:." + ex.Message + Environment.NewLine;
                    }
                }
                txtLog.Text = "Informe de actualización masiva:" + Environment.NewLine + log;
                txtLog.Visible = true;
                btInforme.Visible = true;
            }
            RS.Close();
            SQL.Close();
            SQL2.Close();
        }
    }
    protected void btInforme_Click(object sender, EventArgs e)
    {
        btInforme.Visible = false;
        txtLog.Visible = false;
    }
    protected void ddArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddArea.SelectedValue != "-1")
        {
           dsTecnicosYCAS.SelectCommand =
    "SELECT 'Seleccione una opción' 'Nombre', '-1' id UNION " +
    "SELECT 'Aun no asignado' 'Nombre', 0 id UNION " +
    "SELECT Nombre, id FROM tecnico WHERE areaId=" + ddArea.SelectedValue + " order by id";
            ddTecnico.DataBind();
        }
        else
        {
            dsTecnicosYCAS.SelectCommand =
     "SELECT 'Seleccione una opción' 'Nombre', '-1' id UNION " +
     "SELECT 'Aun no asignado' 'Nombre', 0 id UNION " +
     "SELECT Nombre, id FROM tecnico order by id";
            ddTecnico.DataBind();

        }

    }
}