﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="usuarioagenda.aspx.cs" Inherits="usuarioagenda" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

 
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <h3>Configuracion agenda</h3>
        </div>
    <div align="center">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
   <table>
       <tr><td colspan="2">
           <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label> </td></tr>
       <tr><td>
    <table title="Agenda">
        <tr>
           <td colspan="2">Agenda</td>
            </tr>
        <tr>
            <td>
                <asp:CheckBox ID="ckestado" ClientIDMode="Static" Text="Estado" runat="server" 
                   /></td>
           
             <td>
                 <telerik:RadComboBox ID="cbestado" ClientIDMode="Static"   CheckBoxes="True" EmptyMessage="Seleccione" Filter="Contains" runat="server" Culture="es-ES" DataSourceID="SqlDataSource1"
                      DataTextField="Nombre" DataValueField="EstadoId"></telerik:RadComboBox>
                 <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [EstadoId], [Nombre] FROM [estado] WHERE ([FechaBaja] IS NULL)"></asp:SqlDataSource>
             </td>
           
        </tr>
              <tr>
           
            <td>
                <asp:CheckBox ID="ckespecialista" ClientIDMode="Static" Text="Especialista" runat="server" /></td>
             <td>
                 <telerik:RadComboBox ID="cbespecialista" ClientIDMode="Static" EmptyMessage="Seleccione" CheckBoxes="True" Filter="Contains" runat="server" Culture="es-ES" DataSourceID="SqlDataSource2" DataTextField="Nombre" DataValueField="EspecialistaId"></telerik:RadComboBox>
                 <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [EspecialistaId], [Nombre] FROM [especialista] WHERE ([FechaBaja] IS NULL)"></asp:SqlDataSource>
             </td>
           
        </tr>
                  <tr>
           
            <td>
                <asp:CheckBox ID="cknegocio" Text="Negocio" ClientIDMode="Static" runat="server" /></td>
             <td>
                 <telerik:RadComboBox ID="cbnegocio" ClientIDMode="Static" EmptyMessage="Seleccione" CheckBoxes="True" Filter="Contains" runat="server" Culture="es-ES" DataSourceID="SqlDataSource3" DataTextField="Nombre" DataValueField="EmpresaPartnerId"></telerik:RadComboBox>
                 <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [EmpresaPartnerId], [Nombre] FROM [empresapartner] WHERE ([FechaBaja] IS NULL)"></asp:SqlDataSource>
             </td>
           
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadButton ID="btngenerar" runat="server" Text="Generar" OnClick="btngenerar_Click"></telerik:RadButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadTextBox ID="txtop" AutoPostBack="true" TextMode="MultiLine" Width="100%" Rows="6" runat="server"></telerik:RadTextBox>
            </td>
        </tr>
          <tr>
            <td colspan="2">
                <telerik:RadButton ID="btnguardar" runat="server" Text="Guardar" OnClick="btnguardar_Click"></telerik:RadButton>
            </td>
        </tr>
    </table>
          </td>
           <td>
<table title="PopUp">
      <tr>
           <td colspan="2">PopUp</td>
            </tr>
        <tr>
           
            <td>
                <asp:CheckBox ID="ckestadopu" ClientIDMode="Static" Text="Estado" runat="server" 
                   /></td>
           
             <td>
                 <telerik:RadComboBox ID="cbestadopu" ClientIDMode="Static"   CheckBoxes="True" EmptyMessage="Seleccione" Filter="Contains" runat="server" Culture="es-ES" DataSourceID="SqlDataSource1"
                      DataTextField="Nombre" DataValueField="EstadoId"></telerik:RadComboBox>
                 <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [EstadoId], [Nombre] FROM [estado] WHERE ([FechaBaja] IS NULL)"></asp:SqlDataSource>
             </td>
           
        </tr>
              <tr>
           
            <td>
                <asp:CheckBox ID="ckesppu" ClientIDMode="Static" Text="Especialista" runat="server" /></td>
             <td>
                 <telerik:RadComboBox ID="cbesppu" ClientIDMode="Static" EmptyMessage="Seleccione" CheckBoxes="True" Filter="Contains" runat="server" Culture="es-ES" DataSourceID="SqlDataSource2" DataTextField="Nombre" DataValueField="EspecialistaId"></telerik:RadComboBox>
                 <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [EspecialistaId], [Nombre] FROM [especialista] WHERE ([FechaBaja] IS NULL)"></asp:SqlDataSource>
             </td>
           
        </tr>
                  <tr>
           
            <td>
                <asp:CheckBox ID="cknegpu" Text="Negocio" ClientIDMode="Static" runat="server" /></td>
             <td>
                 <telerik:RadComboBox ID="cbnegpu" ClientIDMode="Static" EmptyMessage="Seleccione" CheckBoxes="True" Filter="Contains" runat="server" Culture="es-ES" DataSourceID="SqlDataSource3" DataTextField="Nombre" DataValueField="EmpresaPartnerId"></telerik:RadComboBox>
                 <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [EmpresaPartnerId], [Nombre] FROM [empresapartner] WHERE ([FechaBaja] IS NULL)"></asp:SqlDataSource>
             </td>
           
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadButton ID="btngenpu" runat="server" Text="Generar" OnClick="btngenerarpu_Click" style="top: 0px; left: 0px"></telerik:RadButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadTextBox ID="txtoppu" TextMode="MultiLine" Width="100%" Rows="6" runat="server"></telerik:RadTextBox>
            </td>
        </tr>
          <tr>
            <td colspan="2">
                <telerik:RadButton ID="btnguardpu" runat="server" Text="Guardar" OnClick="btnguardarpu_Click"></telerik:RadButton>
            </td>
        </tr>
    </table>

           </td>
           
            </tr>
        </table>   
    </div>
    </form>
</body>
</html>
