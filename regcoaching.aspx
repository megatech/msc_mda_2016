﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="regcoaching.aspx.vb" Inherits="regcoaching" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
    <table align="center"><tr><td style="font-weight:bold; text-align:right;">Líder:</td><td>
        <telerik:RadComboBox ID="ddLider" Runat="server"  Culture="es-ES" DataSourceID="dsLideres" DataTextField="nombre" DataValueField="usuarioId" AutoPostBack="True">
    </telerik:RadComboBox>
                              </td></tr></table>
    
    
    <asp:SqlDataSource ID="dsLideres" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="Select * from Lider"></asp:SqlDataSource>
    <p align="center"> <asp:Label ID="Label2" runat="server" 
            Text="Los usuarios resaltados en rojo poseen demasiados coachings con errores críticos. Por favor notifiquelo al supervisor." 
            ForeColor="Maroon" Visible="False"></asp:Label> </p>
    <div align="center">
    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" 
        GridLines="None" DataKeyNames="usuarioid" AllowPaging="True"
            AllowSorting="True">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="usuarioid" HeaderText="UID" 
                SortExpression="usuarioid" InsertVisible="False" ReadOnly="True" >
            <ControlStyle Width="30px" />
            </asp:BoundField>
            <asp:BoundField DataField="Nombre" HeaderText="Operador" 
                SortExpression="Nombre" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="CantEC" HeaderText="Coachings C/ Err. Críticos" 
            SortExpression="CantEC" >
            <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:ButtonField CommandName="1" Text="Ingresar" HeaderText="Nuevo" 
                ButtonType="Button"  />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        SelectCommand="SELECT MIN(usuarioid) as usuarioId, MIN(RTRIM(usuario.nombreCompleto)) as Nombre, count(ec.id) as CantEC 
FROM usuario LEFT JOIN coaching ON usuario.usuarioid = coaching.userid
Left Join viewCoachingsCriticos ec on ec.id=coaching.id AND datediff(mm, fecha, getdate()) = 0
WHERE usuario.perfilId in (26,29,28,32) and usuario.fechabaja is null 
and  usuario.observaciones = (select usuario.observaciones from usuario where usuarioId = @liderUID )
GROUP BY usuarioId ORDER BY Nombre">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddLider" PropertyName="SelectedValue" Name="liderUID" />
        </SelectParameters>
    </asp:SqlDataSource>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

