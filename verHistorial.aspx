﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="verHistorial.aspx.vb" Inherits="verHistorial" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>...</title>
    <style type="text/css">
        .style1
        {
            width: 10px;
        }
        .style2
        {
            width: 801px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:SqlDataSource ID="dsGV1" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                                
        ProviderName="<%$ ConnectionStrings:TicketDB.ProviderName %>" SelectCommand="SELECT ticketId 'ID', Estado, [Tipo de Problema] 'Tipo de problema', Problema, InformeCliente 'Resolución', InformeInterno 'Informe interno',FechaInicio, FechaUltimaModif, EmpresaPartnerId
        FROM ViewHistorial WHERE ClienteId = '0120966'">
                            </asp:SqlDataSource>
    <table width="100%"><tr><td class="style2"><asp:Label ID="lblTitulo" runat="server" 
            Text="Label" Font-Bold="True" Font-Size="15px"></asp:Label></td>
        <td class="style1">
        <table style="height: 20px"><tr><td>
            <asp:DropDownList ID="ddFiltros" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0">[Aplicar Filtro]</asp:ListItem>
                <asp:ListItem Value="1">Tickets ONSITE</asp:ListItem>
                <asp:ListItem Value="2">Ocultar Tickets ONSITE</asp:ListItem>
                <asp:ListItem Value="4">REMOTO</asp:ListItem>
                <asp:ListItem Value="5">TELEFONICO</asp:ListItem>
                <asp:ListItem Value="6">EN PROCESO</asp:ListItem>
            </asp:DropDownList>
            </td>
            <td nowrap="nowrap">
                <asp:CheckBox ID="chk1" runat="server" AutoPostBack="True" Checked="True" 
                    BackColor="#EBF0F1" Font-Bold="True" ForeColor="#33444D" Height="22px" 
                    Text="Otros Tickets" />
            </td>
            <td nowrap="nowrap">
                &nbsp;</td>
            <td nowrap="nowrap">
                <asp:CheckBox ID="chk2" runat="server" AutoPostBack="True" Checked="True" 
                    BackColor="LightGreen" Font-Bold="True" ForeColor="#004600" Height="22px" 
                    Text="Visitas Realizadas" />
            </td>
            <td nowrap="nowrap">
                &nbsp;</td><td nowrap="nowrap">
                <asp:CheckBox ID="chk3" runat="server" AutoPostBack="True" Checked="True" 
                    BackColor="#FCB1A9" Font-Bold="True" ForeColor="Maroon" Height="22px" 
                    Text="Zona Insegura" />
            </td>
            <td nowrap="nowrap">
                &nbsp;</td></tr></table>
        
        
        
        </td></tr></table>
        
                            <asp:GridView ID="gvTelRem" runat="server" Width="100%" 
                            AutoGenerateColumns="False"
                            HeaderStyle-CssClass="headerStyle"
                            AlternatingRowStyle-CssClass="altRowStyle"
                            RowStyle-CssClass="rowStyle"
                            FooterStyle-CssClass="footerStyle"
                            PagerStyle-CssClass="footerStyle" CellPadding="4" ForeColor="#333333" 
                                GridLines="Horizontal" DataSourceID="dsGV1" 
        DataKeyNames="ID" AllowSorting="True" >
<AlternatingRowStyle CssClass="altRowStyle" BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField>
                                     <ItemTemplate>
                                      <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/Edicion.jpg" Width="22" Height="22" />
                                     </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                                        ReadOnly="True" SortExpression="ID" />
                                    <asp:BoundField DataField="Estado" HeaderText="Estado" 
                                        SortExpression="Estado" />
                                    <asp:BoundField DataField="Tipo de problema" HeaderText="Tipo de problema" 
                                        SortExpression="Tipo de problema" />
                                    <asp:BoundField DataField="Problema" HeaderText="Problema" 
                                        SortExpression="Problema"/>
                                    <asp:BoundField DataField="Resolución" HeaderText="Resolución" 
                                        SortExpression="Resolución" />
                                    <asp:BoundField DataField="Informe interno" HeaderText="InformeInterno" 
                                        SortExpression="Informe interno" />
                                    <asp:BoundField DataField="Fecha" HeaderText="FechaCreación" 
                                        SortExpression="Fecha" />
                                    <asp:BoundField DataField="FechaUltimaModif" HeaderText="FechaUltimaModif" 
                                        SortExpression="FechaUltimaModif" />
                                    <asp:BoundField DataField="EmpresaPartnerId" HeaderText="" 
                                        SortExpression="EmpresaPartnerId"/>
                                </Columns>
                                <EditRowStyle BackColor="#999999" />

<FooterStyle CssClass="footerStyle" BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>

<HeaderStyle CssClass="headerStyle" BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>

<PagerStyle CssClass="footerStyle" BackColor="#284775" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<RowStyle CssClass="rowStyle" BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingHeaderStyle BackColor="#42576F" Font-Underline="true" /> 
                                <SortedDescendingHeaderStyle BackColor="#42576F" Font-Underline="true" />
                                
                        </asp:GridView>

        <br />

        </form>
</body>
</html>
