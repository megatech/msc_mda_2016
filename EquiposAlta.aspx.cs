using System;
using System.Web.UI.WebControls;
using App_Code.TicketsLib;

public partial class EquiposAlta : System.Web.UI.Page
{
    log4net.ILog logger = log4net.LogManager.GetLogger("File");
    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";
        Usuario usr = (Usuario)Session["Usuario"];
        if (usr == null)
        {
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString);
        }
        if (!IsPostBack)
        {
            panelMensaje.Visible = false;
            panelTicket.Visible = true;

            initEquipo();
           
            
        }
        


    }

    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try 
        {
            generoCliente();
        }
        catch (Exception ex) 
        {
            logger.Error("Error en Ingresar_Click: Original: " + ex.Message, ex);
        }
    }
    private void initEquipo()
    {
        odsRubro1.SelectParameters["EmpresaId"].DefaultValue= Config.getValue("EmpresaId");
        odsMarca.SelectParameters["EmpresaId"].DefaultValue = Config.getValue("EmpresaId");

    }
    private void generoCliente()
    {
        try
        {
            RecEquipo rc = getDataFromPanatalla();
            if (cvCodigo.IsValid )
                rc = Equipo.insertEquipo(rc);

            lblError.Text = rc.MensajeError;
            //txtCodigo.Text = rc.EquipoID;
            if (lblError.Text.Trim().Length == 0)
                btnSubmit.Visible = false;

        }
        catch (Exception ex)
        {
            logger.Error("Error en generoEquipo: " + ex.Message, ex);
            lblError.Text = "Error en generoEquipo: " + ex.Message;
        }
    }
    private RecEquipo getDataFromPanatalla()
    {
        RecEquipo rc = new RecEquipo();
        try
        {
            rc.Codigo = txtCodigo.Text.Trim();
            rc.EmpresaId = int.Parse(Config.getValue("EmpresaId"));
            rc.Nombre = txtNombre.Text;
            rc.RubroId1 = ddlRubro1.SelectedValue;
            rc.MarcaId = ddlMarca.SelectedValue;
          

            
        }
        catch (Exception ex)
        {
            lblError.Text = "[Error de conversión] Original: " + ex.Message;
            logger.Error("Error al tomar de pantalla", ex);
            
        }
        return rc;
    }
    private int getNumero(string str)
    {
        int n=0;
        try
        {
            n = int.Parse(str);
        }
        catch
        {
            n = 0;
        }
        return n;
    }
    private float getNumeroFloat(string str)
    {
        float n = 0;
        try
        {
            n = float.Parse(str);
        }
        catch
        {
            n = 0;
        }
        return n;
    }
    protected void ValidaNumeroEntero(object source, ServerValidateEventArgs args)
    {
        double nNro = 0;
        string sValue = args.Value;
        try
        {
            nNro = double.Parse(sValue);
            args.IsValid = true;
            TextBox txt = (TextBox)source;
            if (txt.ID.Equals("txtGtiaCompra") || txt.ID.Equals("txtGtiaVenta"))
            {
                if (nNro > 9999) 
                    args.IsValid = false;
            }
            if (txt.ID == "txtVigencia")
            {
                if (nNro > 99999)
                    args.IsValid = false;
            }

        }
        catch
        {
            args.IsValid = false;
        }
        
    }
    protected void ValidaCodigoInexistente(object source, ServerValidateEventArgs args)
    {
        try
        {
            string sCodigo = args.Value;
            args.IsValid = !(Equipo.ExisteCodigo(sCodigo));

        }
        catch
        {
            args.IsValid = false;
        }

    }
    protected void txtGtiaCompra_TextChanged(object sender, EventArgs e)
    {

    }
}
