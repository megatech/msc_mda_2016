﻿Imports Telerik.Web.UI

Partial Class popupOperSinCoach
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        dsOperadoresSinCoaching.SelectCommand = "SELECT distinct usuarioId , nombreCompleto FROM viewOperadores where usuarioId NOT IN " &
                    "(SELECT usuarioId FROM [viewTableroCoaching_Nuevo] where 1=1 " & Request.QueryString("q") & " )" & Request.QueryString("l")
        RadGrid1.DataBind()
    End Sub

    Protected Sub RadGrid1_DataBound(sender As Object, e As EventArgs) Handles RadGrid1.DataBound
        For Each i As GridDataItem In RadGrid1.Items
            For Each cell As TableCell In i.Cells
                Dim key = i.KeyValues.Replace("{", "").Replace("}", "").Split(":")(1).Replace("""", "")
                If cell.Controls.Count > 0 Then
                    TryCast(cell.Controls(0), Button).Font.Bold = True
                    TryCast(cell.Controls(0), Button).PostBackUrl = "cargaCoachingNuevo.aspx?usuarioid=" & key
                End If
            Next
        Next
    End Sub
End Class
