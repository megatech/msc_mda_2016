﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class verHistorial
    Inherits System.Web.UI.Page
    Private WithEvents imgBUTTON As ImageButton

    Private Function getClientName(ByVal id As String) As String
        Dim idCli = Request.QueryString("id").ToString
        Dim SQL As New SqlConnection()
        Dim CMD As New SqlCommand()

        SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ToString()
        Dim name As String = ""
        SQL.Open()
        CMD.Connection = SQL
        CMD.CommandText = "SELECT  RazonSocial FROM viewCliente WHERE ClienteId=" & idCli
        name = CMD.ExecuteScalar().ToString()
        SQL.Close()
        Return name
    End Function

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("id") = Nothing Then Response.Redirect("tickets.aspx")
        Dim idCli = Request.QueryString("id").ToString
        lblTitulo.Text = "Historial | (" & idCli & ") " & getClientName(idCli)
        actualizarGv()
        If gvTelRem.Rows.Count = 0 Then
            Dim Script As String = "<Script>" & _
                                   " window.close(); </Script>"
            Response.Write(Script)
        End If
    End Sub

    Private Sub actualizarGv()
        Dim idCli = Request.QueryString("id").ToString
        Dim yearCond = IIf(Request.QueryString("year") = "1", " AND YEAR(Fecha) = YEAR(GETDATE())", "")
        dsGV1.SelectCommand = "SELECT ticketId 'ID', Estado, [Tipo de Problema] 'Tipo de problema', Problema, InformeCliente 'Resolución', InformeInterno 'Informe interno',Fecha, FechaUltimaModif, EmpresaPartnerId " & _
             "FROM ViewHistorial WHERE ClienteId = " & idCli & yearCond & " Order by ID DESC"
        gvTelRem.DataBind()
    End Sub

    Protected Sub gvTelRem_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTelRem.DataBound
        gvTelRem.Columns(gvTelRem.Columns.Count - 1).HeaderText = ""
        
        For Each r As TableRow In gvTelRem.Rows
            Dim img As ImageButton = DirectCast(r.Cells(0).FindControl("btnEdit"), ImageButton)
            imgBUTTON = img
            img.Attributes.Add("OnClick", "window.open('editaTicket.aspx?id=" & r.Cells(1).Text.Trim & "', 'Ticket', 'titlebar=0,resizable=1,scrollbars=1,left=150,top=100,height=700,width=800,location=0,menubar=0,toolbar=0')")
            r.Cells(6).Text = r.Cells(6).Text.ToString.Replace(Environment.NewLine.ToString, "<br />")
            r.Cells(6).Style.Add("Min-Width", "80px")
            r.Cells(5).Style.Add("Min-Width", "80px")
            'r.Cells(4).Style.Add("Min-Width", "80px")
            If r.Cells(6).Text.Trim.Length > 0 Then r.Cells(6).ToolTip = HttpUtility.HtmlDecode(r.Cells(6).Text).Replace("<br />", Environment.NewLine)
            If r.Cells(6).Text.Length > 30 Then r.Cells(6).Text = r.Cells(6).Text.Substring(0, 30).Replace("<br />", " ") & " [...]"
            If r.Cells(4).Text.Trim.Length > 0 Then r.Cells(4).ToolTip = HttpUtility.HtmlDecode(r.Cells(4).Text).Replace("<br />", Environment.NewLine)
            If r.Cells(4).Text.Length > 40 Then r.Cells(4).Text = r.Cells(4).Text.Substring(0, 40).Replace("<br />", " ") & " [...]"
            If r.Cells(5).Text.Trim.Length > 0 Then r.Cells(5).ToolTip = HttpUtility.HtmlDecode(r.Cells(5).Text).Replace("<br />", Environment.NewLine)
            If r.Cells(5).Text.Length > 40 Then r.Cells(5).Text = r.Cells(5).Text.Substring(0, 40).Replace("<br />", " ") & " [...]"
        Next
        aplicarFiltros()
    End Sub


    Private Sub aplicarFiltros()
        If Request.QueryString("zi") = "1" Then
            chk1.Checked = False
            chk2.Checked = False
            chk1.Visible = False
            chk2.Visible = False
            ddFiltros.Visible = False

        End If

        If Request.QueryString("year") = "1" Then
            chk1.Checked = False
            chk3.Checked = False
            chk1.Visible = False
            chk3.Visible = False
            ddFiltros.Visible = False
        End If

        'For Each itm As ListItem In ddArea.Items
        'itm.Enabled = False
        'Next
        Dim cntVisitaRealizada As Long = 0
        Dim cntZonaInsegura As Long = 0
        Dim cntOtros As Long = 0
        For Each r As TableRow In gvTelRem.Rows
            r.Cells(r.Cells.Count - 1).Visible = True
            Dim esVisitaRealizada = r.Cells(2).Text.Contains("VISITA REALIZADA")
            Dim esZonaInsegura = r.Cells(2).Text.Contains("ZONA INSEGURA")
            Dim esOtro = Not esVisitaRealizada And Not esZonaInsegura
            If esOtro Then cntOtros += 1
            If esVisitaRealizada Then
                r.BackColor = Drawing.Color.LightGreen
                cntVisitaRealizada += 1
            End If
            If esZonaInsegura Then
                r.BackColor = System.Drawing.ColorTranslator.FromHtml("#FCB1A9")
                cntZonaInsegura += 1
            End If
            r.Visible = (chk1.Checked And esOtro) OrElse (chk2.Checked And esVisitaRealizada) OrElse (chk3.Checked And esZonaInsegura)
            If ddFiltros.SelectedValue <> "0" Then r.Visible = esVisibleAlFiltro(r)
            r.Cells(r.Cells.Count - 1).Visible = False
        Next

        chk1.Text = "Otros Tickets [" & cntOtros.ToString & "]"
        chk2.Text = "Visitas Realizadas [" & cntVisitaRealizada.ToString & "]"
        chk3.Text = "Zona Insegura [" & cntZonaInsegura.ToString & "]"
    End Sub

    Private Function esVisibleAlFiltro(ByVal r As TableRow) As Boolean
        Dim last As Long = r.Cells.Count - 1
        If ddFiltros.SelectedValue = "1" Then Return r.Cells(last).Text.Trim = "57" OrElse r.Cells(last).Text.Trim = "58" 'Sólo CAS y CALLE
        If ddFiltros.SelectedValue = "2" Then Return Not (r.Cells(last).Text.Trim = "57" OrElse r.Cells(last).Text.Trim = "58") 'Todo menos CAS y CALLE
        If ddFiltros.SelectedValue = "3" Then Return r.Cells(2).Text.Trim = "RECLAMO ONSITE" 'Sólo Reclamos
        If ddFiltros.SelectedValue = "4" Then Return r.Cells(2).Text.Trim = "REMOTO" 'Sólo Remoto
        If ddFiltros.SelectedValue = "5" Then Return r.Cells(2).Text.Trim = "TELEFONICO" 'Sólo Telefónico
        If ddFiltros.SelectedValue = "6" Then Return r.Cells(2).Text.Trim = "EN PROCESO" 'Sólo En Proceso
        Return False
    End Function


    Private Sub cargarArea(ByVal id As String)
        'ddArea.Items.FindByValue(id).Enabled = True
    End Sub

    Protected Sub chk1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk1.CheckedChanged
        aplicarFiltros()
    End Sub

    Protected Sub chk2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk2.CheckedChanged
        aplicarFiltros()
    End Sub

    Protected Sub chk3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk3.CheckedChanged
        aplicarFiltros()
    End Sub

    Protected Sub ddFiltros_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddFiltros.SelectedIndexChanged
        aplicarFiltros()
    End Sub

End Class
