﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="usuarios.aspx.vb" Inherits="usuarios" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
                </asp:ScriptReference>
            </Scripts>
        </telerik:RadScriptManager>
    <div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" SelectCommand="SELECT eu.UsuarioId UID, rtrim(u.nombreCompleto) Usuario, rtrim(e.EspecialistaId) EID, rtrim(e.nombre) Especialista, rtrim(l.nombre) Lider
FROM especialista e LEFT JOIN
especialistaUsuario eu ON eu.EspecialistaId=e.EspecialistaId INNER JOIN 
		 usuario u ON eu.UsuarioId=u.usuarioId LEFT JOIN
		 lider l ON CAST(l.liderId AS VARCHAR)=RTRIM(u.observaciones)
WHERE u.fechabaja is null and e.fechabaja is null
order by LiderId" ConnectionString="<%$ ConnectionStrings:TicketDB %>"></asp:SqlDataSource>
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" AllowSorting="True" CellSpacing="0" Culture="es-ES" DataSourceID="SqlDataSource1" GridLines="None" ShowGroupPanel="True">
            <ClientSettings AllowDragToGroup="True">
            </ClientSettings>
<MasterTableView AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="Lider" FilterControlAltText="Filter Lider column" HeaderText="Lider" ReadOnly="True" SortExpression="Lider" UniqueName="Lider" 
            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="UID" DataType="System.Decimal" FilterControlAltText="Filter UID column" HeaderText="UID" SortExpression="UID" UniqueName="UID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FilterControlWidth="100%" ItemStyle-Width="50px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Usuario" FilterControlAltText="Filter Usuario column" HeaderText="Usuario" ReadOnly="True" SortExpression="Usuario" UniqueName="Usuario" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="EID" FilterControlAltText="Filter EID column" HeaderText="EID" ReadOnly="True" SortExpression="EID" UniqueName="EID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" ItemStyle-HorizontalAlign="Right"  HeaderStyle-HorizontalAlign="Right" FilterControlWidth="100%"  ItemStyle-Width="50px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Especialista" FilterControlAltText="Filter Especialista column" HeaderText="Especialista" ReadOnly="True" SortExpression="Especialista" UniqueName="Especialista" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
        </telerik:GridBoundColumn>
        
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
</MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
