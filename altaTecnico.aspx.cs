﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code.TicketsLib;

public partial class Default4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Usuario usr = (Usuario)Session["Usuario"];

        if (usr == null) 
        { Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString); }
        else if (usr.Perfil != "1" && usr.Perfil != "20")
        { Response.Redirect("Default.aspx"); }
        
        if (ddArea.SelectedValue == "57") { lblApellido.Visible = true; txtApellido.Visible = true; }
        else { lblApellido.Visible = false; txtApellido.Visible = false; }

    }



    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;
        String nombre;
        if (ddArea.SelectedValue == "58") { nombre = txtNombre.Text.ToUpper(); }
        else { nombre = txtNombre.Text + " " + txtApellido.Text; }
        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT id, nombre from tecnico where areaId= "+ddArea.SelectedValue+" AND nombre like '" + nombre + "'";
        RS = CMD.ExecuteReader();

        RS.Read();

        if (RS.HasRows) { lblError.Text = "El técnico que intenta crear ya existe en la base de datos."; }
        else
        {
            lblError.Text = "";

            CMD.CommandText = "INSERT INTO tecnico(areaId,nombre,email) VALUES(" + ddArea.SelectedValue + ",'" + nombre + "','" + txtEmail.Text + "')";
            RS.Close();
            if (CMD.ExecuteNonQuery() > 0)
            {
                lblError.Text = "El técnico/CAS ha sido creado correctamente.";
            }
            else { lblError.Text = "No se pudo crear el técnico/CAS."; }
        }
        SQL.Close();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddArea.SelectedValue == "57") { lblApellido.Visible = true; txtApellido.Visible = true; }
        else { lblApellido.Visible = false; txtApellido.Visible = false; }
    }
}