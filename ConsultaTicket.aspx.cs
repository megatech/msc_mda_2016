using System;
using App_Code.TicketsLib;

public partial class ConsultaTicket : System.Web.UI.Page
{
    log4net.ILog logger = log4net.LogManager.GetLogger("File");
    int _idTicket;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            if (!IsPostBack)
            {
                putPanelMensaje(false);

                Usuario usr = (Usuario)Session["Usuario"];
                if (usr == null)
                {
                    Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString);
                }
             
                if (Request.QueryString["id"].Equals(""))
                {
                    lblMensajePanel.Text = "No existe Ticket para consultar";
                    putPanelMensaje(true);
                }
              
                else
                {
                    _idTicket = int.Parse(Request.QueryString["id"]);
                    TicketData tkt = App_Code.TicketsLib.Tickets.getTicket(_idTicket);
                    pasoPantalla(tkt);

                    if (usr.Perfil == "23")
                    {
                        string q = "select count(estadoid) from estado where EnviaMailInterno = 1 and estadoid = " + tkt.idEstado ;
                        int r = Convert.ToInt32(App_Code.TicketsLib.Funciones.getScalar(q).ToString());
                        if ( r > 0)
                        {
                            Response.Redirect("BusquedaTicket.aspx");
                        }
                    }

                }

               
            }
            loadScripts();
        }
        catch (Exception ex)
        {
            lblMensajePanel.Text = "Error: " + ex.Message;
            putPanelMensaje(true);
        }

    }

    private void loadScripts()
    {
    }
    private void putPanelMensaje(bool bOk)
    {
        if (bOk)
        {
            panelMensaje.Visible = true;
            panelTicket.Visible = false;
        }
        else
        {
            panelMensaje.Visible = false;
            panelTicket.Visible = true;
        }

    }
    private void pasoPantalla(TicketData tkt)
    {
        try
        {
            txtFecha.Text = tkt.FechaP;
            txtFechaInicio.Text = tkt.FechaInicioScreen;
            txtFechaFin.Text = tkt.FechaFinScreen;
            txtNroClaim.Text = tkt.nroClaim;
            txtTicket.Text = tkt.idTicket.ToString();
            lblNegocio.Text = tkt.Negocio;
            txtCodCliente.Text = tkt.CodigoCliente;
            txtNomCliente.Text = tkt.NombreCliente;
            lblUbicacion.Text = tkt.Ubicacion;
            lblContacto.Text = tkt.Contacto;
            txtCodEquipo.Text = tkt.CodigoEquipo;
            txtNombreEquipo.Text = tkt.NombreEquipo;
            txtSerie.Text = tkt.NroSerie;
            lblEspecialista.Text = tkt.Especialista;
            lblVendedor.Text = tkt.Vendedor;
            lblTipoProblema.Text = tkt.TipoProblema;
            lblPrioridad.Text = tkt.Prioridad;
            lblEstado.Text = tkt.Estado;
            lblIdEstadoActual.Text = tkt.idEstado.ToString();
            txtDetalle.Text = tkt.Problema;
            txtInformeCliente.Text = tkt.InformeCliente;
            txtInformeInterno.Text = tkt.InformeInterno;
            
            

        }
        catch (Exception ex)
        {
            logger.Error("Error en pasoPantalla: " + ex.Message, ex);
            lblError.Text = "Error al pasar a Pantalla: " + ex.Message;
        }
    }
    
}
