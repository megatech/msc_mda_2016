﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Encuesta.aspx.vb" Inherits="Default4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Encuesta</title>
    <script src="js/PopupMgr.js"></script>
</head>
<body style="width:700px; height:300px;">
   
    <form id="form1" runat="server">
     <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
    </telerik:RadScriptManager>
    <div align="center"  >
    <h2 >Encuesta de Satisfacción de clientes</h2>
    <h4>1 = Malo, 2 = No tan malo, 3 = Regular, 4 =Bueno, 5 = Muy Bueno</h4>
    <asp:Panel ID="pTable" runat="server">
    <table>
        <tr>
          <td align="left" style="font-weight: bold">1. Resolví su problema?</td>
          <td>
              <telerik:RadComboBox ZIndex=10000 ID="ddp1" runat="server" AutoPostBack="True">
                  <Items>
                    <telerik:RadComboBoxItem runat="server" Selected="True" Text="No" Value="No" />
                    <telerik:RadComboBoxItem runat="server" Text="Si" Value="Si" />
                 </Items>
              </telerik:radcombobox>
          </td>
        </tr>
        <tr>
            <td align="right" colspan="2" style="font-weight: bold">
                <telerik:RadComboBox ZIndex=10000 ID="ddp1b" Runat="server" Culture="es-ES" Width="300px">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="Por falta de capacidad/interacción con el representante" Value="1" />
                        <telerik:RadComboBoxItem runat="server" Text="El problema le corresponde a otra área/sector" Value="2" />
                    </Items>
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
          <td align="left" style="font-weight: bold">2. El trabajo que hicimos en esta oportunidad cumplió con lo esperado por ud.?</td>
          <td>
            <telerik:RadComboBox ZIndex=10000 ID="ddp2" runat="server">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Selected="True" Text="1" Value="1" />
                    <telerik:RadComboBoxItem runat="server" Text="2" Value="2" />
                    <telerik:RadComboBoxItem runat="server" Text="3" Value="3" />
                    <telerik:RadComboBoxItem runat="server" Text="4" Value="4" />
                    <telerik:RadComboBoxItem runat="server" Text="5" Value="5" />   
                </Items>
            </telerik:radcombobox>
          </td>
        </tr>
        <tr>
          <td align="left" style="font-weight: bold">3. Como calificaría el servicio de Dr. Speedy?</td>
          <td>
            <telerik:RadComboBox ZIndex=10000 ID="ddp3" runat="server">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Selected="True" Text="1" Value="1" />
                    <telerik:RadComboBoxItem runat="server" Text="2" Value="2" />
                    <telerik:RadComboBoxItem runat="server" Text="3" Value="3" />
                    <telerik:RadComboBoxItem runat="server" Text="4" Value="4" />
                    <telerik:RadComboBoxItem runat="server" Text="5" Value="5" /> 
                </Items>
            </telerik:radcombobox>
          </td>
        </tr>
    </table>
    </asp:Panel>
        
        <br />
        <telerik:RadButton ID="RadButton1" runat="server" Text="Guardar" Skin="Default">
        </telerik:RadButton>
    </div>
    <asp:Label ID="lblerror" runat="server" Text="" Font-Bold="true" Font-Size="Large" ForeColor="Red"></asp:Label>
    </form>
</body>
</html>
