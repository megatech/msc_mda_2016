using System;
using App_Code.TicketsLib;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Tickets : System.Web.UI.MasterPage

{

   protected void form1_Load(object sender, EventArgs e)
    {
        Usuario usr = (Usuario)Session["Usuario"];

        if (usr == null)
        {
            Response.Redirect("~/Default.aspx?url=" + Request.Url.OriginalString);
        }

        if (usr.Perfil == "1" || usr.Perfil == "20" || usr.Perfil == "25" || usr.Perfil == "17")
        {
            Menu2.Items[1].Items[4].Enabled = true;
            Menu2.Items[1].Items[5].Enabled = true;
            Menu2.Items[1].Items[6].Enabled = true;
            Menu2.Items[3].Items[4].Enabled = true;
            Menu2.Items[3].Items[3].Enabled = true;
        }
        else if (usr.Perfil == "23")
        {
            Menu2.Items[1].Enabled = false;
            Menu2.Items[3].Enabled = false;
            Menu2.Items[4].Enabled = false;
            Menu2.Items[5].Enabled = false;
          

        }
        else
        {
            Menu2.Items[1].Items[6].Enabled = false;
            Menu2.Items[1].Items[4].Enabled = false;
            Menu2.Items[1].Items[5].Enabled = false;
            Menu2.Items[3].Items[4].Enabled = false;
            Menu2.Items[3].Items[3].Enabled = false;
        }



    }

  

               
    protected void Button1_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("~/Default.aspx");
    }

    //protected void Timer1_Tick(object sender, EventArgs e)
    //{
    //    GridView gv = (GridView)cphTicket.FindControl("gridTktsEnProc");
    //    if (gv != null)
    //    {
    //        gv.DataBind();
    //    }

    //    GridView gv2 = (GridView)cphTicket.FindControl("gvDetalleTkts");
    //    if (gv2 != null)
    //    {
    //        gv2.DataBind();
    //    }

    //}
}
