﻿
Imports System.Net
Imports HtmlAgilityPack
Imports System.Data.SqlClient
Imports Telerik.Web.UI
Imports App_Code.TicketsLib
Imports SecurityHelper.AccessManager


Partial Class qlikview_cargaManual
    Inherits System.Web.UI.Page
    Private arrDataInsertsTDI As New ArrayList
    Private arrDataInsertsTLG As New ArrayList
    Private fecha As DateTime
    Private fechaSql As String
    Private arrLideres As New ArrayList
    Private arrOperadores As New ArrayList
    Private htmlTableTdi As HtmlTable
    Private htmlTableTLG As HtmlTable
    Private htmlTableFCR As HtmlTable


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Dim b = getScalar("SELECT count(*) FROM DatosTLG WHERE DATEPART(year,GETDATE())=DATEPART(year,fecha) AND DATEPART(month,GETDATE())=DATEPART(month,fecha) AND DATEPART(day,GETDATE())=DATEPART(day,fecha)") = 0
        If Not IsPostBack Then
            '           RadDatePicker1.SelectedDate = Now.Date.AddDays(-1)
            If Session("usuario") IsNot Nothing Then
                Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
                If Not u.Perfil = 1 And Not u.Perfil = 20 And Not u.Perfil = 17 Then
                    Response.Redirect("~/Default2.aspx")
                End If
            End If

            fechaSLAMes.SelectedDate = Now.Date.AddDays(-1)
            fFCR.SelectedDate = Now.Date.AddDays(-1)
            fechaGFL.SelectedDate = Now.Date
            lblStatusSLA.Text = ""
            If Request.QueryString("tab") = "tlg" Then
                RadTabStrip1.Tabs(2).Visible = True
                RadTabStrip1.Tabs(1).Visible = False
                RadTabStrip1.Tabs(0).Visible = False
                RadTabStrip1.SelectedIndex = 2
                viewTLG.Selected = True

            ElseIf Request.QueryString("tab") = "tdi" Then
                RadTabStrip1.Tabs(1).Visible = True
                RadTabStrip1.Tabs(2).Visible = False
                RadTabStrip1.Tabs(0).Visible = False
                RadTabStrip1.SelectedIndex = 1
                viewTDI.Selected = True
            ElseIf Request.QueryString("tab") = "fcr" Then
                RadTabStrip1.Tabs(3).Visible = True
                RadTabStrip1.Tabs(2).Visible = False
                RadTabStrip1.Tabs(1).Visible = False
                RadTabStrip1.Tabs(0).Visible = False
                RadTabStrip1.SelectedIndex = 3
                viewFCR.Selected = True
            End If
            '            ifrm.Attributes.Add("src", "slaMensual.aspx?fecha=" & RadDatePicker1.SelectedDate.Value.ToString("yyyy-MM-dd"))
        End If
    End Sub

    Private Sub extraerDatosTDI()
        Dim webcli = New WebClient()
        Dim currDate As String = fecha.ToString("yyyy-MM-dd")
        webcli.Credentials = obtenerCredenciales("tasa")
        Dim url = "http://10.244.41.26/tdi/Genesys/reporte/listado_rac.php?programa=2&pcrc=&f_i=" & currDate & "&f_f=" & currDate & "&proveedor=&site=10"
        Dim res = webcli.DownloadString(url)
        Dim tableString = SeleccionarEntre(res, "<table align='center' width='90%' border=1>", "</table>")
        htmlTableTdi = htmlStringToHtmlTable(tableString)
        'Literal1.Text = tableString
    End Sub

    Private Sub extraerDatosTLG()
        Dim webcli = New WebClient()
        webcli.Credentials = obtenerCredenciales("tasa")
        Dim res = webcli.DownloadString("http://tlg/drspeedymegatech/indicadores.asp")
        Dim tableString = SeleccionarEntre(res, "<table width=""350"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" class=""Tabla"">", "</table>")
        'Literal1.Text = tableString
        htmlTableTLG = htmlStringToHtmlTable(tableString)
    End Sub

    Public Function htmlStringToHtmlTable(ByVal html As String) As HtmlTable
        Dim htmlTable = New HtmlTable()
        Dim htmlDoc = New HtmlDocument()
        htmlDoc.LoadHtml(html)
        Dim tables As HtmlNodeCollection = htmlDoc.DocumentNode.SelectNodes("//table")
        Dim rows As HtmlNodeCollection = tables(0).SelectNodes(".//tr")
        For i As Integer = 0 To rows.Count - 1
            htmlTable.Rows.Add(New HtmlTableRow())
            Dim thcols As HtmlNodeCollection = rows(i).SelectNodes(".//th")
            If thcols IsNot Nothing Then
                For Each n As HtmlNode In thcols
                    Dim newThCell As New HtmlTableCell
                    newThCell.InnerText = n.InnerText()
                    htmlTable.Rows(i).Cells.Add(newThCell)
                Next
            End If
            Dim cols As HtmlNodeCollection = rows(i).SelectNodes(".//td")
            If cols IsNot Nothing Then
                For Each n As HtmlNode In cols
                    Dim newCell As New HtmlTableCell
                    newCell.InnerText = n.InnerText()
                    htmlTable.Rows(i).Cells.Add(newCell)
                Next
            End If
        Next
        Return htmlTable
    End Function

    Public Function SeleccionarEntre(ByVal texto As String, ByVal prefijo As String, ByVal sufijo As String) As String
        Dim html = texto
        Dim htmlCount = texto.Count
        Dim preIndex = html.IndexOf(prefijo.ToString())
        Dim suIndex = html.Substring(preIndex).IndexOf(sufijo, 1) + sufijo.Count + preIndex
        Dim cuenta = html.Count - suIndex + 1
        Dim res = html.Remove(suIndex, cuenta - 1)
        res = res.Substring(preIndex)
        Return res
    End Function

    Private Function getScalar(q As String) As Object
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand(q, SQL)
        Dim res As Object
        Try
            SQL.Open()
            res = CMD.ExecuteScalar()
            SQL.Close()
            Return res
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Function

    Private Sub InsertarDatosTDI()
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()
            For Each i In arrDataInsertsTDI
                CMD.CommandText = i.replace("''", "NULL")
                If CMD.ExecuteNonQuery() < 1 Then
                    Throw New Exception("Error al insertar los Datos.")
                End If
            Next
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Sub

    Private Sub InsertarDatosTLG()
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()
            For Each i In arrDataInsertsTLG
                CMD.CommandText = i
                If CMD.ExecuteNonQuery() < 1 Then
                    Throw New Exception("Error al insertar una fila.")
                End If
            Next
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Sub

    Private Sub obtenerArrLideres()
        For Each r As HtmlTableRow In htmlTableTdi.Rows
            If Not r Is htmlTableTdi.Rows(0) Then
                If Not arrLideres.Contains(r.Cells(3).InnerText.Trim) Then arrLideres.Add(r.Cells(3).InnerText.Trim)
            End If
        Next
    End Sub

    Private Sub obtenerArrOperadores()
        For Each r As HtmlTableRow In htmlTableTdi.Rows
            If Not r Is htmlTableTdi.Rows(0) Then
                Dim arrItm = New ArrayList
                arrItm.Add(r.Cells(2).InnerText.Trim)
                arrItm.Add(r.Cells(0).InnerText.Trim)
                arrItm.Add(r.Cells(1).InnerText.Trim)
                arrItm.Add(r.Cells(3).InnerText.Trim)
                If Not arrOperadores.Contains(arrItm) Then arrOperadores.Add(arrItm)
            End If
        Next
    End Sub

    Private Sub actualizarLideresEnDB(ByVal liders As ArrayList)
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()

            For Each i In liders
                If i <> "" Then
                    CMD.CommandText = "SELECT count(*) FROM liderTDI WHERE nombre='" & i & "'"
                    If CMD.ExecuteScalar() = 0 Then
                        Dim grupoId As String = obtenerGrupo(i)
                        CMD.CommandText = "INSERT INTO liderTDI(nombre,grupoid) values('" & i & "'," & grupoId & ")"
                        CMD.ExecuteNonQuery()
                    End If
                End If
            Next

            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al actualizar lider", ex)
        End Try
    End Sub

    Private Sub actualizarOperadoresEnDB(ByVal operadores As ArrayList)
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()

            For Each i In operadores
                CMD.CommandText = "SELECT count(*) FROM operadorTDI WHERE mgt='" & i(0) & "'"
                If CMD.ExecuteScalar() = 0 Then
                    CMD.CommandText = "SELECT id FROM liderTDI WHERE nombre ='" & i(3) & "'"
                    Dim liderId As String = CMD.ExecuteScalar()
                    If liderId = "" Then liderId = "NULL"
                    CMD.CommandText = "INSERT INTO operadorTDI(mgt,apellido,nombre,liderId) values('" & i(0) & "','" & i(1) & "','" & i(2) & "'," & liderId & ")"
                    CMD.ExecuteNonQuery()
                End If

            Next

            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
        End Try
    End Sub

    Private Function intervalToSeconds(str As String) As String
        Dim hhh As Integer = str.Split(":")(0)
        Dim mm As Integer = str.Split(":")(1)
        Dim ss As Integer = str.Split(":")(2)
        Dim ts As New TimeSpan(hhh, mm, ss)
        Return ts.TotalSeconds().ToString.Split(",")(0).Split(".")(0)
    End Function

    Private Sub GenerarInsertTDI()
        For Each r As HtmlTableRow In htmlTableTdi.Rows
            If r IsNot htmlTableTdi.Rows(0) Then
                Dim arrFields = New ArrayList
                arrFields.Add({"fecha", "'" & fechaSql & "'"})
                arrFields.Add({"liderId", fetchCell(r, 3, "lider")})
                arrFields.Add({"operadorId", fetchCell(r, 2, "operador")})
                arrFields.Add({"llamAtendidas", fetchCell(r, 8, "int")})
                arrFields.Add({"llamSalientes", fetchCell(r, 9, "int")})
                arrFields.Add({"llamCortas", fetchCell(r, 10, "int")})
                arrFields.Add({"llamTransferidas", fetchCell(r, 11, "int")})
                arrFields.Add({"talkTime", fetchCell(r, 12, "int")})
                arrFields.Add({"acw", fetchCell(r, 14, "int")})
                arrFields.Add({"waitTime", fetchCell(r, 16, "int")})
                arrFields.Add({"holdTime", fetchCell(r, 17, "int")})
                arrFields.Add({"nrRefrig", fetchCell(r, 19, "int")})
                arrFields.Add({"nrBanio", fetchCell(r, 20, "int")})
                arrFields.Add({"nrCoachDev", fetchCell(r, 21, "int")})
                arrFields.Add({"nrCapacitacion", fetchCell(r, 22, "int")})
                arrFields.Add({"nrResRec", fetchCell(r, 23, "int")})
                arrFields.Add({"nrOtros", fetchCell(r, 24, "int")})
                arrFields.Add({"nrDescVisual", fetchCell(r, 25, "int")})
                arrFields.Add({"nrCoachExpress", fetchCell(r, 26, "int")})
                arrFields.Add({"nrLectNovedades", fetchCell(r, 27, "int")})
                arrFields.Add({"nrVarios", fetchCell(r, 28, "int")})
                arrFields.Add({"nrNone", fetchCell(r, 29, "int")})
                arrFields.Add({"nrTotal", fetchCell(r, 30, "toSeconds")})
                arrFields.Add({"logProd", fetchCell(r, 31, "toSeconds")})
                arrFields.Add({"logTotal", fetchCell(r, 32, "toSeconds")})
                arrFields.Add({"tmo", fetchCell(r, 33, "decimal")})
                arrFields.Add({"occ", fetchCell(r, 34, "decimal")})
                arrFields.Add({"utilizacion", fetchCell(r, 35, "decimal")})
                'arrFields.Add({"fcr", fetchCell(r, 36, "decimal")})
                'arrFields.Add({"str", fetchCell(r, 37, "decimal")})

                Dim insQuery = "INSERT INTO datosTDI("
                For Each f In arrFields
                    If f IsNot arrFields(0) Then insQuery &= ","
                    insQuery &= f(0)
                Next
                insQuery &= ") VALUES("
                For Each f In arrFields
                    If f IsNot arrFields(0) Then insQuery &= ","
                    insQuery &= f(1)
                Next
                insQuery &= ")"
                arrDataInsertsTDI.Add(insQuery)
            End If
        Next

    End Sub

    Private Function fetchCell(ByRef r As HtmlTableRow, nro As Integer, tipo As String) As String
        If r.Cells.Count - 1 < nro Then Return "NULL"
        If tipo = "decimal" Then
            Return "'" & r.Cells(nro).InnerText.Replace(".", "").Replace(",", ".") & "'"
        ElseIf tipo = "varchar" Then
            Return "'" & r.Cells(nro).InnerText.Replace("'", "''") & "'"
        ElseIf tipo = "int" Then
            Return r.Cells(nro).InnerText
        ElseIf tipo = "toSeconds" Then
            Return intervalToSeconds(r.Cells(nro).InnerText)
        ElseIf tipo = "lider" Then
            Return getLider(r.Cells(nro).InnerText)
        ElseIf tipo = "operador" Then
            Return getOperador(r.Cells(nro).InnerText)
        End If
    End Function
    Private Sub GenerarInsertsTLG()
        Dim r As HtmlTableRow = htmlTableTLG.Rows(htmlTableTLG.Rows.Count - 1)
        Dim insert As String = "INSERT INTO datosTLG(fecha,gestionesOk,total) " &
            "VALUES('" & fechaSql & "'," & r.Cells(2).InnerText & "," & r.Cells(3).InnerText & ")"
        arrDataInsertsTLG.Add(insert)
    End Sub

    Private Function obtenerGrupo(i As Object) As String
        Return "NULL"
    End Function

    Private Function getOperador(ByVal mgt As String) As String
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("SELECT id FROM operadorTDI WHERE mgt='" & mgt & "'", SQL)
        Dim res As Integer
        Try
            SQL.Open()
            res = CMD.ExecuteScalar()
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al obtener operador", ex)
        End Try
        Return res
    End Function

    Private Function getLider(ByVal nombre As String) As String
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("SELECT id FROM liderTDI WHERE nombre='" & nombre.Replace("'", "''") & "'", SQL)
        Dim res As Integer
        Try
            SQL.Open()
            res = CMD.ExecuteScalar()
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al obtener lider", ex)
        End Try
        Return res
    End Function

    Private Sub procesarTDI()

        Dim faux, fHasta As DateTime
        faux = fIni.SelectedDate
        fHasta = fFin.SelectedDate
        If faux <= fHasta Then
            While faux.Date <= fHasta.Date
                fecha = faux
                fechaSql = fecha.ToString("yyyyMMdd HH:mm:ss")
                If getScalar("SELECT count(*) FROM datosTDI WHERE DATEPART(year,'" & fechaSql & "')=DATEPART(year,fecha) AND DATEPART(month,'" & fechaSql & "')=DATEPART(month,fecha) AND DATEPART(day,'" & fechaSql & "')=DATEPART(day,fecha)") = 0 Then
                    extraerDatosTDI()
                    obtenerArrLideres()
                    obtenerArrOperadores()
                    actualizarLideresEnDB(arrLideres)
                    actualizarOperadoresEnDB(arrOperadores)
                    GenerarInsertTDI()
                End If
                InsertarDatosTDI()
                arrDataInsertsTDI.Clear()
                faux = faux.AddDays(1)
            End While

        End If
    End Sub

    Private Sub procesarTLG()
        fecha = Now.Date
        fechaSql = Now.Date.ToString("yyyyMMdd HH:mm:ss")
        extraerDatosTLG()
        GenerarInsertsTLG()
        InsertarDatosTLG()
    End Sub

    Private Sub procesarCargaManualTLG()
        Dim v1 = txtGestionesOk.Text.Trim = "" OrElse Not esNumerico(txtGestionesOk.Text)

        Dim v3 = txtTotal.Text.Trim = "" OrElse Not esNumerico(txtTotal.Text)
        If v1 OrElse v3 Then
            lblValidacionTLG.Visible = True
        Else
            arrDataInsertsTLG.Clear()
            Dim fechaStr = fechaGFL.SelectedDate.Value.ToString("yyyyMMdd HH:mm:ss")
            Dim insert As String = "INSERT INTO datosTLG(fecha,gestionesOk,total) " &
        "VALUES('" & fechaStr & "'," & txtGestionesOk.Text.Trim & "," & txtTotal.Text.Trim & ")"
            arrDataInsertsTLG.Add(insert)
            InsertarDatosTLG()
        End If

    End Sub

    Private Function esNumerico(str As String) As Boolean
        For Each ch In str
            If Not ".,0123456789".Contains(ch) Then Return False
        Next
        Return True
    End Function

    Protected Sub btExtraerTDI_Click(sender As Object, e As EventArgs) Handles btExtraerTDI.Click
        Try
            procesarTDI()
            lblStatusTDI.ForeColor = lblMsgTLG.ForeColor
            lblStatusTDI.Text = "Los datos fueron cargados correctamente."
        Catch ex As Exception
            lblStatusTDI.ForeColor = lblValidacionTLG.ForeColor
            lblStatusTDI.Text = "Error: </br>" & ex.Message
        End Try

    End Sub

    Protected Sub btExtraerTLG_Click(sender As Object, e As EventArgs) Handles btExtraerTLG.Click
        Try
            If CheckBox1.Checked Then
                procesarCargaManualTLG()
            Else
                procesarTLG()
            End If
            lblStatusTLG.ForeColor = lblMsgTLG.ForeColor()
            lblStatusTLG.Text = "Los Datos fueron cargados correctamente."
        Catch ex As Exception
            lblStatusTLG.ForeColor = lblValidacionTLG.ForeColor
            If ex.Message.Contains("Infracción de la restricción PRIMARY KEY") Then
                lblStatusTLG.Text = "Error: Los datos de GFL de la fecha especificada, ya fueron cargados anteriormente."
            Else
                lblStatusTLG.Text = "Error: " & ex.Message
            End If
        End Try
    End Sub

    Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        For Each c In Panel1.Controls
            If c.GetType.Name = "RadDatePicker" Then
                Dim ct As New RadDatePicker
                ct = TryCast(c, RadDatePicker)
                ct.Enabled = CheckBox1.Checked
            ElseIf c.GetType.Name = "RadTextBox" Then
                Dim ct As New RadTextBox
                ct = TryCast(c, RadTextBox)
                ct.Enabled = CheckBox1.Checked
            End If
        Next
        lblStatusTLG.Text = ""
        lblValidacionTLG.Visible = False
        lblMsgTLG.Visible = Not CheckBox1.Checked
        If CheckBox1.Checked Then
            btExtraerTLG.Text = "Cargar Datos Ingresados"
        Else
            btExtraerTLG.Text = "Extraer Datos de TLG"
        End If
    End Sub

    Protected Sub btCargarSla_Click(sender As Object, e As EventArgs) Handles btCargarSla.Click
        Dim v1, v2, v3, v4, v5, v6, v7, v8 As Boolean
        If chkMes.Checked Then
            v1 = txtLlamRecibidas.Text.Trim = "" OrElse Not esNumerico(txtLlamRecibidas.Text) _
                 OrElse txtLlamRecibidasDia.Text.Trim = "" OrElse Not esNumerico(txtLlamRecibidasDia.Text)
            v2 = txtLlamAtendidas.Text.Trim = "" OrElse Not esNumerico(txtLlamAtendidas.Text) _
                 OrElse txtLlamAtendidasDia.Text.Trim = "" OrElse Not esNumerico(txtLlamAtendidasDia.Text)
            v3 = txtSLA.Text.Trim = "" OrElse Not esNumerico(txtSLA.Text) OrElse Decimal.Parse(txtSLA.Text.Trim.Replace(".", ",")) > 100 _
                 OrElse txtSLADia.Text.Trim = "" OrElse Not esNumerico(txtSLADia.Text) OrElse Decimal.Parse(txtSLADia.Text.Trim.Replace(".", ",")) > 100
            v4 = txtFCR.Text.Trim = "" OrElse Not esNumerico(txtFCR.Text) OrElse Decimal.Parse(txtFCR.Text.Trim.Replace(".", ",")) > 100
            v5 = txtSTR.Text.Trim = "" OrElse Not esNumerico(txtSTR.Text) OrElse Decimal.Parse(txtSTR.Text.Trim.Replace(".", ",")) > 100
            v7 = txtAbandonadasMes.Text.Trim = "" OrElse Not esNumerico(txtAbandonadasMes.Text) _
                 OrElse txtAbandonadasDia.Text.Trim = "" OrElse Not esNumerico(txtAbandonadasDia.Text)

        Else
            v1 = txtLlamRecibidasHora.Text.Trim = "" OrElse Not esNumerico(txtLlamRecibidasHora.Text)
            v2 = txtLlamAtendidasHora.Text.Trim = "" OrElse Not esNumerico(txtLlamAtendidasHora.Text)
            v3 = txtLlamAtendidas20Hora.Text.Trim = "" OrElse Not esNumerico(txtLlamAtendidas20Hora.Text) OrElse
                Decimal.Parse(txtSLAGFL.Text.Trim.Replace(".", ",")) > 100 OrElse Decimal.Parse(txtSLAOnsite.Text.Trim.Replace(".", ",")) > 100
            v4 = txtOperadoresPlanificados.Text.Trim = "" OrElse Not esNumerico(txtOperadoresPlanificados.Text.Trim)
            v5 = txtOperadoresLogueados.Text.Trim = "" OrElse Not esNumerico(txtOperadoresLogueados.Text.Trim)
            v6 = txtEnProcesoRemotoMR.Text.Trim = "" OrElse Not esNumerico(txtEnProcesoRemotoMR.Text.Trim)
            v8 = txtAbandonadas.Text.Trim = "" OrElse Not esNumerico(txtAbandonadas.Text.Trim)

        End If

        If v1 OrElse v2 OrElse v3 OrElse v4 OrElse v5 OrElse v6 OrElse v7 OrElse v8 Then
            lblValidacionSLA.Visible = True
            lblStatusSLA.Text = ""
        Else

            lblValidacionSLA.Visible = False

            If chkMes.Checked Then
                Try
                    cargarDatosSLA()
                    lblStatusSLA.ForeColor = lblMsgTLG.ForeColor
                    lblStatusSLA.Text = "Los datos fueron cargados correctamente"
                    chkMes.Checked = False
                    chkMes_CheckedChanged(Me, New EventArgs())
                Catch ex As Exception
                    lblStatusSLA.ForeColor = lblValidacionSLA.ForeColor
                    lblStatusSLA.Text = "Error al guardar los datos en la Base de datos. " & ex.Message
                End Try
            Else
                'cargarDatosOperadores()
                Try
                    enviarMail()
                Catch ex As Exception
                    lblStatusSLA.ForeColor = lblValidacionSLA.ForeColor
                    lblStatusSLA.Text = "Los datos fueron guardados correctamente, pero no se pudieron notificar por E-mail. " & ex.Message
                End Try
            End If
        End If
        chkMes_CheckedChanged(Me, New EventArgs())

    End Sub

    Private Sub cargarDatosSLA()
        Dim tabla = "slaDiario"

        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("INSERT INTO slaMensual(fecha,recibidas,abandonadas,sla,usuario,atendidasEn20,fcr,desconectadas) VALUES(", SQL)
        Try
            Dim usuario As Usuario = Session("Usuario")
            Dim usrName = usuario.Nick.Trim
            Dim abandonadasMes = Integer.Parse(txtAbandonadasMes.Text.Trim)
            Dim abandonadasDia = Integer.Parse(txtAbandonadasDia.Text.Trim)
            Dim recibidasDia = txtLlamRecibidasDia.Text.Trim
            Dim recibidasMes = txtLlamRecibidas.Text.Trim

            'Dim abandonadasDia = Integer.Parse(txtLlamRecibidasDia.Text.Trim) - Integer.Parse(txtLlamAtendidasDia.Text.Trim)
            Dim atendidasEn20, sla, fechaSla As String
            Dim atendidasEn20Dia, slaDia, str, fcr As String
            Dim TotalAtendidasDia, TotalAtendidasMes As String


            TotalAtendidasDia = txtLlamAtendidasDia.Text.Trim.Replace(",", ".")
            TotalAtendidasMes = txtLlamAtendidas.Text.Trim.Replace(",", ".")

            fcr = txtFCR.Text.Trim.Replace(",", ".")
            str = txtSTR.Text.Trim.Replace(",", ".")

            sla = txtSLA.Text.Trim.Replace(".", ",")
            slaDia = txtSLADia.Text.Trim.Replace(".", ",")

            atendidasEn20 = Math.Round((Integer.Parse(recibidasMes) * Decimal.Parse(sla)) / 100, 0)
            fechaSla = "'" & fechaSLAMes.SelectedDate.Value.ToString("yyyyMMdd HH:mm:ss") & "'"
            atendidasEn20Dia = Math.Round((Integer.Parse(recibidasDia) * Decimal.Parse(slaDia)) / 100, 0)
            Dim desconectadasDia = Integer.Parse(recibidasDia) - (Integer.Parse(abandonadasDia) + Integer.Parse(TotalAtendidasDia))
            Dim desconectadasMes = Integer.Parse(recibidasMes) - (Integer.Parse(abandonadasMes) + Integer.Parse(TotalAtendidasMes))

            SQL.Open()
            CMD.CommandText &= fechaSla & "," & txtLlamRecibidas.Text.Trim & "," & abandonadasMes & ",'" & sla.Replace(",", ".") & "','" & usrName & "'," & atendidasEn20 & ",'" & fcr & "'," & desconectadasMes & ")"
            If CMD.ExecuteNonQuery() < 1 Then
                Throw New Exception("Error al insertar datos del Mes.")
            End If
            CMD.CommandText = "INSERT INTO slaDiario(fecha,recibidas,abandonadas,sla,usuario,atendidasEn20,str,desconectadas) VALUES(" &
                fechaSla & "," & txtLlamRecibidasDia.Text.Trim & "," & abandonadasDia & ",'" & slaDia.Replace(",", ".") & "','" & usrName & "'," & atendidasEn20Dia & ",'" & str & "'," & desconectadasDia & ")"
            If CMD.ExecuteNonQuery() < 1 Then
                Throw New Exception("Error al insertar datos del Día.")
            End If
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Sub

    Public Sub cargarDatosOperadores()
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("INSERT INTO logOperadores(fecha,operadoresPlanificados,operadoresLogueados) VALUES( GETDATE(),", SQL)
        Dim operadoresPlanificados, operadoresLogueados As String
        operadoresPlanificados = txtOperadoresPlanificados.Text.Trim
        operadoresLogueados = txtOperadoresLogueados.Text.Trim
        Try
            SQL.Open()
            CMD.CommandText &= operadoresPlanificados & "," & operadoresLogueados & ")"
            If CMD.ExecuteNonQuery() < 1 Then
                Throw New Exception("Error al insertar datos de los operadores.")
            End If
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try

    End Sub

    'Protected Sub RadioButtonList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioButtonList1.SelectedIndexChanged
    '    If RadioButtonList1.SelectedIndex = 1 Then
    '        ifrm.Attributes.Add("src", "slaActual.aspx?fecha=" & RadDatePicker1.SelectedDate.Value.ToString("yyyy-MM-dd"))
    '    Else
    '        ifrm.Attributes.Add("src", "slaMensual.aspx?fecha=" & RadDatePicker1.SelectedDate.Value.ToString("yyyy-MM-dd"))
    '    End If
    'End Sub

    Private Function obtenerValores() As OrderedDictionary
        Dim datos = New OrderedDictionary()
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("SELECT TOP 1 * FROM slaMensual ORDER BY fecha Desc", SQL)
        Dim RS As SqlDataReader
        Try
            Dim abandonadasHora = txtAbandonadas.Text.Trim


            datos.Add("fechaHora", Now.ToString)

            datos.Add("recibidasHora", txtLlamRecibidasHora.Text.Trim)
            datos.Add("abandonadasHora", abandonadasHora.ToString)
            datos.Add("atendidasEn20Hora", txtLlamAtendidas20Hora.Text.Trim)
            datos.Add("atendidasHora", txtLlamAtendidasHora.Text.Trim)

            SQL.Open()
            RS = CMD.ExecuteReader
            RS.Read()
            datos.Add("fechaMes", RS("fecha").ToString)
            datos.Add("slaMes", RS("sla").ToString & "%")
            datos.Add("recibidasMes", RS("recibidas").ToString)
            datos.Add("abandonadasMes", RS("abandonadas").ToString)
            datos.Add("atendidasEn20Mes", RS("atendidasEn20").ToString)
            datos.Add("desconectadasMes", RS("desconectadas").ToString)
            datos.Add("fcr", RS("fcr").ToString & "%")
            datos.Add("tasaAbandonoMes", Math.Round((RS("abandonadas") * 100) / RS("recibidas"), 2).ToString)
            Dim desconectadas = Integer.Parse(datos("recibidasHora")) - (Integer.Parse(datos("abandonadasHora")) + Integer.Parse(datos("atendidasHora")))
            datos.Add("desconectadas", desconectadas.ToString)
            datos.Add("tasaAbandonoHora", Math.Round((abandonadasHora * 100) / (Integer.Parse(txtLlamRecibidasHora.Text) - desconectadas), 2).ToString)
            Dim slaHora As String = Math.Round((Integer.Parse(txtLlamAtendidas20Hora.Text.Trim) / (Integer.Parse(txtLlamRecibidasHora.Text.Trim) - Integer.Parse(desconectadas))) * 100, 2)
            datos.Add("slaHora", slaHora & "%")
            RS.Close()
            CMD.CommandText = "SELECT TOP 1 * FROM slaDiario ORDER BY fecha Desc"
            RS = CMD.ExecuteReader
            RS.Read()
            datos.Add("str", RS("str").ToString & "%")
            SQL.Close()
            Return datos
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try

    End Function

    Private Sub enviarMail()
        Dim datos As OrderedDictionary = obtenerValores()
        Dim pendTele = getScalar("Select count(*) From Ticket WHERE fechaBaja is null and estadoId=115 and articuloId=19532")
        Dim pendRemo = getScalar("Select count(*) From Ticket WHERE fechaBaja is null and estadoId=143 and articuloId=19532")
        Dim aRecontactar = getScalar("Select count(*) From Ticket WHERE fechaBaja is null and estadoId=156 and articuloId=19532")
        Dim remCaido = getScalar("Select count(*) From Ticket WHERE fechaBaja is null and estadoId=154 and articuloId=19532")
        Dim enProcMR = txtEnProcesoRemotoMR.Text.Trim
        Dim cod = "CargaSLA"
        Dim dest = getScalar("SELECT destinatarios FROM notificaciones WHERE id='" + cod + "'").ToString()
        Dim slaGFL = txtSLAGFL.Text.Replace(".", ",")
        Dim slaOnsite = txtSLAOnsite.Text.Replace(".", ",")
        Dim operadoresPlanificados = txtOperadoresPlanificados.Text.Trim
        Dim operadoresLogueados = txtOperadoresLogueados.Text.Trim


        ' ofrecidas - (abandonadas + atendidas)






        Dim msg = "<html><body><table>" &
        "<tr><td colspan=""2"" style=""font-weight:bold; font-size:18px;text-align:center;background-color:#333333;color:white;"">Mesa De Ayuda - Dr.Speedy</td></tr>" &
        "<tr><td colspan=""2"" style=""font-weight:bold; font-size:14px;text-align:center;color:white;background-color:#555555;"">Mediciones Del D&iacute;a</td></tr>" &
        "<tr style=""font-weight:bold;font-size:14px;""><td>Última actualización: </td><td style=""text-align:right"">" & datos("fechaHora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Ofrecidas:</td><td style=""text-align:right"">" & datos("recibidasHora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Atendidas &#60;30seg:</td><td style=""text-align:right"">" & datos("atendidasEn20Hora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Abandonadas:</td><td style=""text-align:right"">" & datos("abandonadasHora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Desconectadas:</td><td style=""text-align:right"">" & datos("desconectadas") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>SLA:</td><td style=""text-align:right"">" & datos("slaHora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Tasa Abandono:</td><td style=""text-align:right"">" & datos("tasaAbandonoHora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>En Proceso Telef&oacute;nico:</td><td style=""text-align:right"">" & pendTele & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>En Proceso Remoto:</td><td style=""text-align:right"">" & pendRemo & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>A Recontactar:</td><td style=""text-align:right"">" & aRecontactar & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Remoto Ca&iacute;do:</td><td style=""text-align:right"">" & remCaido & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>En Proceso Remoto (v&iacute;a MR):</td><td style=""text-align:right"">" & enProcMR & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>SLA Gestiones Fuera de Linea:</td><td style=""text-align:right"">" & slaGFL & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>SLA Visitas Domiciliarias:</td><td style=""text-align:right"">" & slaOnsite & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Operadores Planificados:</td><td style=""text-align:right"">" & operadoresPlanificados & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Operadores Logueados:</td><td style=""text-align:right"">" & operadoresLogueados & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>STR:</td><td style=""text-align:right"">" & datos("str") & "</td></tr>" &
        "<tr><td colspan=""2"" style=""font-weight:bold; font-size:14px; text-align:center;color:white;background-color:#555555;"">Valores Acumulados del Mes</td></tr>" &
        "<tr style=""font-weight:bold;font-size:14px;""><td>Última actualización: </td><td style=""text-align:right"">" & datos("fechaMes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Ofrecidas:</td><td style=""text-align:right"">" & datos("recibidasMes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Atendidas &#60;30seg:</td><td style=""text-align:right"">" & datos("atendidasEn20Mes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Abandonadas:</td><td style=""text-align:right"">" & datos("abandonadasMes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Desconectadas:</td><td style=""text-align:right"">" & datos("desconectadasMes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>SLA:</td><td style=""text-align:right"">" & datos("slaMes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Tasa Abandono:</td><td style=""text-align:right"">" & datos("tasaAbandonoMes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>FCR:</td><td style=""text-align:right"">" & datos("fcr") & "</td></tr>" &
        "</table></body></html>"

        FUNCIONES_MOD.mailmega2.enviamail("alertas@megatech.la", dest, "alertas@megatech.la", "MDA Dr.Speedy - SLA [" & Now.ToString & "]", msg)
    End Sub

    Protected Sub chkMes_CheckedChanged(sender As Object, e As EventArgs) Handles chkMes.CheckedChanged
        Panel2.Visible = chkMes.Checked
        Panel3.Visible = Not chkMes.Checked
        fechaSLAMes.Visible = chkMes.Checked
        lblValidacionSLA.Visible = False
        borrarTextboxes()
    End Sub

    Private Sub borrarTextboxes()
        For Each c As Control In Panel2.Controls
            If c.GetType.Name = "RadTextBox" Then
                Dim txt As New RadTextBox()
                txt = c
                txt.Text = ""
            End If
        Next
        For Each c As Control In Panel3.Controls
            If c.GetType.Name = "RadTextBox" Then
                Dim txt As New RadTextBox()
                txt = c
                txt.Text = ""
            End If
        Next
    End Sub

    'Protected Sub RadDatePicker1_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles RadDatePicker1.SelectedDateChanged
    '    ifrm.Attributes.Add("src", "slaActual.aspx?fecha=" & RadDatePicker1.SelectedDate.Value.ToString("yyyy-MM-dd"))
    'End Sub


    'para saber el primer dia del mes 
    Function PrimerDiaDelMes(ByVal dtmFecha As Date) As Date
        Return DateSerial(Year(dtmFecha), Month(dtmFecha), 1)
    End Function

    'para saber el ultimo dia del mes 
    Function UltimoDiaDelMes(ByVal dtmFecha As Date) As Date
        Return DateSerial(Year(dtmFecha), Month(dtmFecha) + 1, 0)
    End Function

    Protected Sub btExtraerFCR_Click(sender As Object, e As EventArgs) Handles btExtraerFCR.Click
        Try
            extraerDatosFCR(PrimerDiaDelMes(Now.Date), fFCR.SelectedDate.Value)
            lblStatusFCR.ForeColor = lblMsgTLG.ForeColor

            lblStatusFCR.Text = "Datos cargados correctamente."
        Catch ex As Exception
            lblStatusFCR.ForeColor = lblValidacionTLG.ForeColor()
            lblStatusFCR.Text = "Error: " & ex.Message
        End Try

    End Sub

    Private Sub extraerDatosFCR(ByVal desde As Date, ByVal hasta As Date)
        Dim webcli = New WebClient()
        webcli.Credentials = obtenerCredenciales("tasa")
        Dim url = "http://10.244.41.26/tdi/Herramientas de Gestion/fcr/main.php?programa=2&pcrc=2-26&proveedor=2-26-0&site=2-26-0-0&monitores=2-26-0-0-0&rac=2-26-0-0-0-0&Genesys=1&f1=" & desde.ToString("yyyy-MM-dd") & "&f2=" & hasta.ToString("yyyy-MM-dd")
        Dim res = webcli.DownloadString(url)
        Dim tableString = SeleccionarEntre(res, "<table class='tabla' width='80%'>", "</table>")
        htmlTableFCR = htmlStringToHtmlTable(tableString)
        Dim Total As Integer = htmlTableFCR.Rows(4).Cells(1).InnerText()
        Dim str As Integer = htmlTableFCR.Rows(4).Cells(12).InnerText()
        Dim fcr As Integer = htmlTableFCR.Rows(4).Cells(14).InnerText()
        GuardarDatosFCR(hasta, fcr, str, Total)
    End Sub

    Private Sub GuardarDatosFCR(ByVal fecha As Date, fcr As Integer, str As Integer, total As Integer)
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("INSERT INTO datosFCR(fecha,fcr,str,total) VALUES(", SQL)
        Dim fechaSql = "'" & fecha.ToString("yyyyMMdd HH:mm:ss") & "'"
        Try
            SQL.Open()
            CMD.CommandText &= fechaSql & "," & fcr & "," & str & "," & total & ")"
            If CMD.ExecuteNonQuery() < 1 Then
                Throw New Exception("Error al insertar datos de FCR y STR.")
            End If
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al insertar datos de FCR y STR.", ex)
        End Try
    End Sub

    Protected Sub fFCR_SelectedDateChanged(sender As Object, e As Calendar.SelectedDateChangedEventArgs) Handles fFCR.SelectedDateChanged
        lblStatusFCR.Text = ""
    End Sub


    Protected Sub txtAbandonadasMes_TextChanged(sender As Object, e As EventArgs) Handles txtAbandonadasMes.TextChanged

    End Sub
End Class
