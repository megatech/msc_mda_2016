﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="tdiView.aspx.vb" Inherits="qlikview_cargaManual" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
    <div>
        <telerik:RadTabStrip ID="RadTabStrip1" runat="server"  MultiPageID="RadMultiPage1" SelectedIndex="0">
            <Tabs>

                <telerik:RadTab runat="server" Text="Extracción TDI" PageViewID="viewTDI" Visible="True">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="1" Width="100%">
            <telerik:RadPageView ID="viewSLA" runat="server" Visible="False">
                <table style="width:100%;">
                    <tr><td class="auto-style1" style="white-space: nowrap">
                        Consulta de datos en TDI:
                    </td>
                        <td style="white-space: nowrap" align="right" nowrap="nowrap">
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True">Mensual</asp:ListItem>
                                <asp:ListItem>Diario</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td nowrap="nowrap" style="white-space: nowrap">
                            &nbsp;<telerik:RadDatePicker ID="RadDatePicker1" Runat="server" AutoPostBack="True" Culture="es-AR">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput AutoPostBack="True" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" LabelWidth="40%">
                                </DateInput>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <iframe id="ifrm" runat="server" src="slaActual.aspx" style="width:100%; height:400px;"></iframe>
                            <table align="center">
                                <tr>
                                    <td style="text-align: center; height: 31px;">
                                        <asp:CheckBox ID="chkMes" runat="server" AutoPostBack="True" Font-Bold="False" text="Cargar Datos TDI" />
                                        &nbsp;<telerik:RadDatePicker ID="fechaSLAMes" runat="server" Culture="es-AR" ToolTip="Hasta" Visible="False">
                                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                                <FastNavigationSettings CancelButtonCaption="Cancelar" DateIsOutOfRangeMessage="Fecha fuera de rango." EnableTodayButtonSelection="True" TodayButtonCaption="Hoy">
                                                </FastNavigationSettings>
                                            </Calendar>
                                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" LabelWidth="40%">
                                            </DateInput>
                                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: #333333; color: #FFFFFF; font-weight: bold; text-align: center;">Ingrese los datos solicitados y haga click en el botón para finalizar</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center">
                                        <asp:Panel ID="Panel2" runat="server" Wrap="False" Visible="false">
                                            <table>
                                               <tr><td style="text-align: center; font-weight: bold;">Valores Acumulados del Mes</td></tr>
                                                <tr><td style="text-align: center">
                                                    <telerik:RadTextBox ID="txtLlamRecibidas" runat="server" EmptyMessage="Llamadas Recibidas" ToolTip="Llamadas Recibidas">
                                                        <EmptyMessageStyle HorizontalAlign="Center" />
                                                        <HoveredStyle HorizontalAlign="Center" />
                                                        <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                    <telerik:RadTextBox ID="txtLlamAtendidas" runat="server" EmptyMessage="Llamadas Atendidas" ToolTip="Llamadas Atendidas">
                                                        <EmptyMessageStyle HorizontalAlign="Center" />
                                                        <HoveredStyle HorizontalAlign="Center" />
                                                        <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                    <telerik:RadTextBox ID="txtLlamAtendidas20" runat="server" EmptyMessage="Atendidas &lt;20 seg" ToolTip="Atendidas &lt;20 seg" Visible="False">
                                                        <EmptyMessageStyle HorizontalAlign="Center" />
                                                        <HoveredStyle HorizontalAlign="Center" />
                                                        <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                    <telerik:RadTextBox ID="txtSLA" runat="server" EmptyMessage="SLA" ToolTip="SLA">
                                                        <EmptyMessageStyle HorizontalAlign="Center" />
                                                        <HoveredStyle HorizontalAlign="Center" />
                                                        <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                </td></tr>
                                                <tr><td style="text-align: center; font-weight: bold;">Valores del Día</td></tr>
                                                <tr><td style="text-align: center">
                                                    <telerik:RadTextBox ID="txtLlamRecibidasDia" runat="server" EmptyMessage="Llamadas Recibidas" ToolTip="Llamadas Recibidas">
                                                        <EmptyMessageStyle HorizontalAlign="Center" />
                                                        <HoveredStyle HorizontalAlign="Center" />
                                                        <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                    <telerik:RadTextBox ID="txtLlamAtendidasDia" runat="server" EmptyMessage="Llamadas Atendidas" ToolTip="Llamadas Atendidas">
                                                        <EmptyMessageStyle HorizontalAlign="Center" />
                                                        <HoveredStyle HorizontalAlign="Center" />
                                                        <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                    <telerik:RadTextBox ID="txtLlamAtendidas20Dia" runat="server" EmptyMessage="Atendidas &lt;20 seg" ToolTip="Atendidas &lt;20 seg" Visible="False">
                                                        <EmptyMessageStyle HorizontalAlign="Center" />
                                                        <HoveredStyle HorizontalAlign="Center" />
                                                        <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                    <telerik:RadTextBox ID="txtSLADia" runat="server" EmptyMessage="SLA" ToolTip="SLA">
                                                        <EmptyMessageStyle HorizontalAlign="Center" />
                                                        <HoveredStyle HorizontalAlign="Center" />
                                                        <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                </td></tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="Panel3" runat="server" Wrap="False">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <telerik:RadTextBox ID="txtLlamRecibidasHora" runat="server" EmptyMessage="Llamadas Recibidas" ToolTip="Llamadas Recibidas">
                                                            <EmptyMessageStyle HorizontalAlign="Center" />
                                                            <HoveredStyle HorizontalAlign="Center" />
                                                            <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                        <telerik:RadTextBox ID="txtLlamAtendidasHora" runat="server" EmptyMessage="Llamadas Atendidas" ToolTip="Llamadas Atendidas">
                                                            <EmptyMessageStyle HorizontalAlign="Center" />
                                                            <HoveredStyle HorizontalAlign="Center" />
                                                            <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                    <telerik:RadTextBox ID="txtLlamAtendidas20Hora" runat="server" EmptyMessage="Atendidas &lt;20 seg" ToolTip="Atendidas &lt;20 seg">
                                                        <EmptyMessageStyle HorizontalAlign="Center" />
                                                        <HoveredStyle HorizontalAlign="Center" />
                                                        <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                        <telerik:RadTextBox ID="txtSLAHora" runat="server" EmptyMessage="SLA" ToolTip="SLA" Visible="False">
                                                            <EmptyMessageStyle HorizontalAlign="Center" />
                                                            <HoveredStyle HorizontalAlign="Center" />
                                                            <EnabledStyle HorizontalAlign="Center" />
                                                    </telerik:RadTextBox>
                                                        <telerik:RadTextBox ID="txtSLAGFL" runat="server" EmptyMessage="SLA Gest. Fuera de Linea" ToolTip="SLA Gestiones Fuera de Linea">
                                                            <EmptyMessageStyle HorizontalAlign="Center" />
                                                            <HoveredStyle HorizontalAlign="Center" />
                                                            <EnabledStyle HorizontalAlign="Center" />
                                                        </telerik:RadTextBox>
                                                        <telerik:RadTextBox ID="txtSLAOnsite" runat="server" EmptyMessage="SLA Visitas Domiciliarias" ToolTip="SLA Visitas Domiciliarias">
                                                            <EmptyMessageStyle HorizontalAlign="Center" />
                                                            <HoveredStyle HorizontalAlign="Center" />
                                                            <EnabledStyle HorizontalAlign="Center" />
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                            </table>

                                        </asp:Panel>

                                    </td>
                                </tr>
                                
                                <tr>
                                    <td style="text-align: center">
                                        <asp:Label ID="lblValidacionSLA" runat="server" Font-Bold="True" ForeColor="Maroon" Text="Datos Inválidos. Verifique los valores ingresados." Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <br />
                                        <telerik:RadButton ID="btCargarSla" runat="server" Height="35px" Text="Cargar Datos" Width="250px">
                                        </telerik:RadButton>
                                        &nbsp;<br />
                                        <br />
                                        <asp:Label ID="lblStatusSLA" runat="server" Font-Bold="True" ForeColor="Maroon"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>



            </telerik:RadPageView>
            <telerik:RadPageView ID="viewTDI" runat="server" Visible="True">
                <table align="left">
                    <tr>
                        <td style="text-align: left; background-color: #333333; color: #FFFFFF; font-weight: bold;">Seleccione el rango de fechas y haga click en el botón para extraer los datos</td>
                    </tr>
                    <tr>
                        <td style="text-align: left" align="left">&nbsp;<telerik:RadDatePicker ID="fIni" runat="server" Culture="es-AR" ToolTip="Desde">
                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                <FastNavigationSettings CancelButtonCaption="Cancelar" DateIsOutOfRangeMessage="Fecha fuera de rango." EnableTodayButtonSelection="True" TodayButtonCaption="Hoy">
                                </FastNavigationSettings>
                            </Calendar>
                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" EmptyMessage="Desde" LabelWidth="40%">
                            </DateInput>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            &nbsp;<telerik:RadDatePicker ID="fFin" runat="server" Culture="es-AR" ToolTip="Hasta">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                    <FastNavigationSettings CancelButtonCaption="Cancelar" DateIsOutOfRangeMessage="Fecha fuera de rango." EnableTodayButtonSelection="True" TodayButtonCaption="Hoy">
                                    </FastNavigationSettings>
                                </Calendar>
                                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  EmptyMessage="Hasta" LabelWidth="40%">
                                </DateInput>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align: left">
                            <telerik:RadButton ID="btExtraerTDI" runat="server" Height="35px" Text="Consultar Datos TDI" Width="250px">
                            </telerik:RadButton>
                            <telerik:RadButton ID="btExtraerTDI0" runat="server" Height="35px" Text="Exportar" Width="250px">
                            </telerik:RadButton>
                            <br />
                            <br />
                            <asp:Label ID="lblStatusTDI" runat="server" Font-Bold="True" ForeColor="Maroon"></asp:Label>
                            <br />
                            
                        </td>
                    </tr>
                </table>
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </telerik:RadPageView>
            <telerik:RadPageView ID="viewTLG" runat="server" Width="100%">
                <table align="center">
                    <tr>
                        <td style="background-color: #333333; color: #FFFFFF; font-weight: bold; text-align: center;" colspan="2">Seleccione el modo de carga y luego haga click en el botón para guardar los datos</td>
                    </tr>
                    <tr>
                        <td style="text-align: center; white-space: nowrap;">
                            <asp:CheckBox ID="CheckBox1" runat="server" Text="Carga manual:" AutoPostBack="True" />
                        </td>
                        <td style="text-align: center">
                            <asp:Panel ID="Panel1" runat="server" Wrap="False">
                                <telerik:RadDatePicker ID="fechaGFL" runat="server" Enabled="False" Culture="es-AR" ToolTip="Fecha">
                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                        <FastNavigationSettings CancelButtonCaption="Cancelar" DateIsOutOfRangeMessage="Fecha fuera de rango." EnableTodayButtonSelection="True" TodayButtonCaption="Hoy">
                                        </FastNavigationSettings>
                                    </Calendar>
                                    <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" LabelWidth="40%">
                                    </DateInput>
                                    <DatePopupButton CssClass="rcCalPopup rcDisabled" HoverImageUrl="" ImageUrl="" />
                                </telerik:RadDatePicker>
                                &nbsp;<telerik:RadTextBox ID="txtGestionesOk" runat="server" EmptyMessage="Gestiones OK" Enabled="False" ToolTip="Gestiones OK"></telerik:RadTextBox>
                                &nbsp;<telerik:RadTextBox ID="txtTotal" runat="server" EmptyMessage="Total" Enabled="False" ToolTip="Total"></telerik:RadTextBox>
                                &nbsp;</asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center" colspan="2">
                            <asp:Label ID="lblValidacionTLG" runat="server" Font-Bold="True" ForeColor="Maroon" Text="Datos Inválidos. Verifique los valores ingresados." Visible="False"></asp:Label>
                            <asp:Label ID="lblMsgTLG" runat="server" Font-Bold="True" ForeColor="#003366" Text="( Se extraerán los datos que figuran actualmente en TLG )"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; height: 78px;">
                            <telerik:RadButton ID="btExtraerTLG" runat="server" Height="35px" Text="Extraer Datos de TLG" Width="250px">
                            </telerik:RadButton>
                            &nbsp;<br />
                            <br />
                            <asp:Label ID="lblStatusTLG" runat="server" Font-Bold="True" ForeColor="Maroon"></asp:Label>
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
            
        </telerik:RadMultiPage>

        
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

