﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code.TicketsLib;

public partial class qlikview_changePass : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btCambiarPass_Click(object sender, EventArgs e)
    {
        if (txtNewPass.Text == "") { lblStatus.Text = "El password no puede ser vacío."; return; }
        if (txtConf.Text != txtNewPass.Text) { lblStatus.Text = "El password y la confirmación no coinciden."; return; }
        string oldPass = SecurityHelper.AES.Encrypt(txtPass.Text);
        string oldPassDB = Funciones.getScalar("SELECT pass FROM credenciales WHERE id = 1").ToString();
        if (oldPass != oldPassDB) {
            lblStatus.Text = "El password actual ingresado es incorrecto.";
            return;
        }
        else
        {
            try
            {
                string newPass = SecurityHelper.AES.Encrypt(txtNewPass.Text);
                SqlConnection SQL = new SqlConnection(Funciones.getConnString());
                SqlCommand CMD = new SqlCommand("UPDATE Credenciales SET pass='" + newPass + "'",SQL);
                SQL.Open();
                CMD.ExecuteNonQuery();
                SQL.Close();
                lblStatus.Text = "Password actualizado correctamente.";
            }
            catch (Exception ex) {
                lblStatus.Text = "Error al actualizar la password: " + ex.Message;
            }
        }
    }
}