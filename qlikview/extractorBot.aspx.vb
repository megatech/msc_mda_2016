﻿Imports System.Net
Imports HtmlAgilityPack
Imports System.Data.SqlClient
Imports SecurityHelper.AccessManager

Partial Class extractorBot
    Inherits System.Web.UI.Page
    Private arrDataInsertsTDI As New ArrayList
    Private arrDataInsertsTLG As New ArrayList
    Private fecha As DateTime
    Private fechaSql As String
    Private arrLideres As New ArrayList
    Private arrOperadores As New ArrayList
    Private htmlTableTdi As HtmlTable
    Private htmlTableTLG As HtmlTable
    Private htmlTableFCR As HtmlTable
    Private fIni As DateTime
    Private fFin As DateTime

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        tdi()
        tlg()
        fcr()
    End Sub

    Private Sub tdi()
        Try
            If Request.QueryString("tdi") = "true" Then
                fIni = Now.Date.AddDays(-1)
                fFin = Now.Date.AddDays(-1)
                procesarTDI()
                Response.Write("OK")
            End If
        Catch ex As Exception
            enviarMailTDI(ex.Message, fIni, fFin)
        End Try
    End Sub
    Private Sub tlg()
        Try
            If Request.QueryString("tlg") = "true" Then
                procesarTLG()
                Response.Write("OK")
            End If
        Catch ex As Exception
            enviarMailTLG(ex.Message, Now.Date)
        End Try
    End Sub
    Private Sub fcr()
        Try

            If Request.QueryString("fcr") = "true" Then
                extraerDatosFCR(PrimerDiaDelMes(Now.Date), Now.Date.AddDays(-1))
                Response.Write("OK")
            End If
        Catch ex As Exception
            enviarMailFCR(ex.Message, PrimerDiaDelMes(Now.Date), Now.Date.AddDays(-1))

        End Try
    End Sub

    'para saber el primer dia del mes 
    Function PrimerDiaDelMes(ByVal dtmFecha As Date) As Date
        Return DateSerial(Year(dtmFecha), Month(dtmFecha), 1)
    End Function

    'para saber el ultimo dia del mes 
    Function UltimoDiaDelMes(ByVal dtmFecha As Date) As Date
        Return DateSerial(Year(dtmFecha), Month(dtmFecha) + 1, 0)
    End Function

    Private Sub extraerDatosFCR(ByVal desde As Date, ByVal hasta As Date)
        Dim webcli = New WebClient()
        webcli.Credentials = obtenerCredenciales("tasa")
        Dim url = "http://10.244.41.26/tdi/Herramientas de Gestion/fcr/main.php?programa=2&pcrc=2-26&proveedor=2-26-0&site=2-26-0-0&monitores=2-26-0-0-0&rac=2-26-0-0-0-0&Genesys=1&f1=" & desde.ToString("yyyy-MM-dd") & "&f2=" & hasta.ToString("yyyy-MM-dd")
        Dim res = webcli.DownloadString(url)
        Dim tableString = SeleccionarEntre(res, "<table class='tabla' width='80%'>", "</table>")
        htmlTableFCR = htmlStringToHtmlTable(tableString)
        Dim Total As Integer = htmlTableFCR.Rows(4).Cells(1).InnerText()
        Dim str As Integer = htmlTableFCR.Rows(4).Cells(12).InnerText()
        Dim fcr As Integer = htmlTableFCR.Rows(4).Cells(14).InnerText()
        GuardarDatosFCR(hasta, fcr, str, Total)
    End Sub

    Private Sub GuardarDatosFCR(ByVal fecha As Date, fcr As Integer, str As Integer, total As Integer)
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("INSERT INTO datosFCR(fecha,fcr,str,total) VALUES(", SQL)
        Dim fechaSql = "'" & fecha.ToString("yyyyMMdd HH:mm:ss") & "'"
        Try
            SQL.Open()
            CMD.CommandText &= fechaSql & "," & fcr & "," & str & "," & total & ")"
            If CMD.ExecuteNonQuery() < 1 Then
                Throw New Exception("Error al insertar datos de FCR y STR.")
            End If
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al insertar datos de FCR y STR.", ex)
        End Try
    End Sub

    Private Sub extraerDatosTDI()
        Dim webcli = New WebClient()
        Dim currDate As String = fecha.ToString("yyyy-MM-dd")
        webcli.Credentials = obtenerCredenciales("tasa")
        Dim url = "http://10.244.41.26/tdi/Genesys/reporte/listado_rac.php?programa=2&pcrc=&f_i=" & currDate & "&f_f=" & currDate & "&proveedor=&site=10"
        Dim res = webcli.DownloadString(url)
        Dim tableString = SeleccionarEntre(res, "<table align='center' width='90%' border=1>", "</table>")
        htmlTableTdi = htmlStringToHtmlTable(tableString)
    End Sub

    Private Sub extraerDatosTLG()
        Dim webcli = New WebClient()
        webcli.Credentials = obtenerCredenciales("tlg")

        Dim res = webcli.DownloadString("http://tlg/drspeedymegatech/indicadores.asp")
        Dim tableString = SeleccionarEntre(res, "<table width=""350"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" class=""Tabla"">", "</table>")
        'Literal1.Text = tableString
        htmlTableTLG = htmlStringToHtmlTable(tableString)
    End Sub

    Public Function htmlStringToHtmlTable(ByVal html As String) As HtmlTable
        Dim htmlTable = New HtmlTable()
        Dim htmlDoc = New HtmlDocument()
        htmlDoc.LoadHtml(html)
        Dim tables As HtmlNodeCollection = htmlDoc.DocumentNode.SelectNodes("//table")
        Dim rows As HtmlNodeCollection = tables(0).SelectNodes(".//tr")
        For i As Integer = 0 To rows.Count - 1
            htmlTable.Rows.Add(New HtmlTableRow())
            Dim thcols As HtmlNodeCollection = rows(i).SelectNodes(".//th")
            If thcols IsNot Nothing Then
                For Each n As HtmlNode In thcols
                    Dim newThCell As New HtmlTableCell
                    newThCell.InnerText = n.InnerText()
                    htmlTable.Rows(i).Cells.Add(newThCell)
                Next
            End If
            Dim cols As HtmlNodeCollection = rows(i).SelectNodes(".//td")
            If cols IsNot Nothing Then
                For Each n As HtmlNode In cols
                    Dim newCell As New HtmlTableCell
                    newCell.InnerText = n.InnerText()
                    htmlTable.Rows(i).Cells.Add(newCell)
                Next
            End If
        Next
        Return htmlTable
    End Function

    Public Function SeleccionarEntre(ByVal texto As String, ByVal prefijo As String, ByVal sufijo As String) As String
        Dim html = texto
        Dim htmlCount = texto.Count
        Dim preIndex = html.IndexOf(prefijo.ToString())
        Dim suIndex = html.Substring(preIndex).IndexOf(sufijo, 1) + sufijo.Count + preIndex
        Dim cuenta = html.Count - suIndex + 1
        Dim res = html.Remove(suIndex, cuenta - 1)
        res = res.Substring(preIndex)
        Return res
    End Function

    Private Function getScalar(q As String) As Object
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand(q, SQL)
        Dim res As Object
        Try
            SQL.Open()
            res = CMD.ExecuteScalar()
            SQL.Close()
            Return res
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Function

    Private Sub InsertarDatosTDI()
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()
            For Each i In arrDataInsertsTDI
                CMD.CommandText = i.replace("''", "NULL")
                If CMD.ExecuteNonQuery() < 1 Then
                    Throw New Exception("Error al insertar una fila.")
                End If
            Next
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Sub

    Private Sub InsertarDatosTLG()
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()
            For Each i In arrDataInsertsTLG
                CMD.CommandText = i
                If CMD.ExecuteNonQuery() < 1 Then
                    Throw New Exception("Error al insertar una fila.")
                End If
            Next
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Sub

    Private Sub obtenerArrLideres()
        For Each r As HtmlTableRow In htmlTableTdi.Rows
            If Not r Is htmlTableTdi.Rows(0) Then
                If Not arrLideres.Contains(r.Cells(3).InnerText.Trim) Then arrLideres.Add(r.Cells(3).InnerText.Trim)
            End If
        Next
    End Sub

    Private Sub obtenerArrOperadores()
        For Each r As HtmlTableRow In htmlTableTdi.Rows
            If Not r Is htmlTableTdi.Rows(0) Then
                Dim arrItm = New ArrayList
                arrItm.Add(r.Cells(2).InnerText.Trim)
                arrItm.Add(r.Cells(0).InnerText.Trim)
                arrItm.Add(r.Cells(1).InnerText.Trim)
                arrItm.Add(r.Cells(3).InnerText.Trim)
                If Not arrOperadores.Contains(arrItm) Then arrOperadores.Add(arrItm)
            End If
        Next
    End Sub

    Private Sub actualizarLideresEnDB(ByVal liders As ArrayList)
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()

            For Each i In liders
                If i <> "" Then
                    CMD.CommandText = "SELECT count(*) FROM liderTDI WHERE nombre='" & i & "'"
                    If CMD.ExecuteScalar() = 0 Then
                        Dim grupoId As String = obtenerGrupo(i)
                        CMD.CommandText = "INSERT INTO liderTDI(nombre,grupoid) values('" & i & "'," & grupoId & ")"
                        CMD.ExecuteNonQuery()
                    End If
                End If
            Next

            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al actualizar lider", ex)
        End Try
    End Sub

    Private Sub actualizarOperadoresEnDB(ByVal operadores As ArrayList)
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()

            For Each i In operadores
                CMD.CommandText = "SELECT count(*) FROM operadorTDI WHERE mgt='" & i(0) & "'"
                If CMD.ExecuteScalar() = 0 Then
                    CMD.CommandText = "SELECT id FROM liderTDI WHERE nombre ='" & i(3) & "'"
                    Dim liderId As String = CMD.ExecuteScalar()
                    If liderId = "" Then liderId = "NULL"
                    CMD.CommandText = "INSERT INTO operadorTDI(mgt,apellido,nombre,liderId) values('" & i(0) & "','" & i(1) & "','" & i(2) & "'," & liderId & ")"
                    CMD.ExecuteNonQuery()
                End If

            Next

            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
        End Try
    End Sub

    Private Sub GenerarInsertTDI()
        For Each r As HtmlTableRow In htmlTableTdi.Rows
            If r IsNot htmlTableTdi.Rows(0) Then
                Dim arrFields = New ArrayList
                arrFields.Add({"fecha", "'" & fechaSql & "'"})
                arrFields.Add({"liderId", fetchCell(r, 3, "lider")})
                arrFields.Add({"operadorId", fetchCell(r, 2, "operador")})
                arrFields.Add({"llamAtendidas", fetchCell(r, 8, "int")})
                arrFields.Add({"llamSalientes", fetchCell(r, 9, "int")})
                arrFields.Add({"llamCortas", fetchCell(r, 10, "int")})
                arrFields.Add({"llamTransferidas", fetchCell(r, 11, "int")})
                arrFields.Add({"talkTime", fetchCell(r, 12, "int")})
                arrFields.Add({"acw", fetchCell(r, 14, "int")})
                arrFields.Add({"waitTime", fetchCell(r, 16, "int")})
                arrFields.Add({"holdTime", fetchCell(r, 17, "int")})
                arrFields.Add({"nrRefrig", fetchCell(r, 19, "int")})
                arrFields.Add({"nrBanio", fetchCell(r, 20, "int")})
                arrFields.Add({"nrCoachDev", fetchCell(r, 21, "int")})
                arrFields.Add({"nrCapacitacion", fetchCell(r, 22, "int")})
                arrFields.Add({"nrResRec", fetchCell(r, 23, "int")})
                arrFields.Add({"nrOtros", fetchCell(r, 24, "int")})
                arrFields.Add({"nrDescVisual", fetchCell(r, 25, "int")})
                arrFields.Add({"nrCoachExpress", fetchCell(r, 26, "int")})
                arrFields.Add({"nrLectNovedades", fetchCell(r, 27, "int")})
                arrFields.Add({"nrVarios", fetchCell(r, 28, "int")})
                arrFields.Add({"nrNone", fetchCell(r, 29, "int")})
                arrFields.Add({"nrTotal", fetchCell(r, 30, "toSeconds")})
                arrFields.Add({"logProd", fetchCell(r, 31, "toSeconds")})
                arrFields.Add({"logTotal", fetchCell(r, 32, "toSeconds")})
                arrFields.Add({"tmo", fetchCell(r, 33, "decimal")})
                arrFields.Add({"occ", fetchCell(r, 34, "decimal")})
                arrFields.Add({"utilizacion", fetchCell(r, 35, "decimal")})
                'arrFields.Add({"fcr", fetchCell(r, 36, "decimal")})
                'arrFields.Add({"str", fetchCell(r, 37, "decimal")})

                Dim insQuery = "INSERT INTO datosTDI("
                For Each f In arrFields
                    If f IsNot arrFields(0) Then insQuery &= ","
                    insQuery &= f(0)
                Next
                insQuery &= ") VALUES("
                For Each f In arrFields
                    If f IsNot arrFields(0) Then insQuery &= ","
                    insQuery &= f(1)
                Next
                insQuery &= ")"
                arrDataInsertsTDI.Add(insQuery)
            End If
        Next
    End Sub

    Private Function fetchCell(ByRef r As HtmlTableRow, nro As Integer, tipo As String) As String
        If r.Cells.Count - 1 < nro Then Return "NULL"
        If tipo = "decimal" Then
            Return "'" & r.Cells(nro).InnerText.Replace(".", "").Replace(",", ".") & "'"
        ElseIf tipo = "varchar" Then
            Return "'" & r.Cells(nro).InnerText.Replace("'", "''") & "'"
        ElseIf tipo = "int" Then
            Return r.Cells(nro).InnerText
        ElseIf tipo = "toSeconds" Then
            Return intervalToSeconds(r.Cells(nro).InnerText)
        ElseIf tipo = "lider" Then
            Return getLider(r.Cells(nro).InnerText)
        ElseIf tipo = "operador" Then
            Return getOperador(r.Cells(nro).InnerText)
        End If
    End Function

    Private Function intervalToSeconds(str As String) As String
        Dim hhh As Integer = str.Split(":")(0)
        Dim mm As Integer = str.Split(":")(1)
        Dim ss As Integer = str.Split(":")(2)
        Dim ts As New TimeSpan(hhh, mm, ss)
        Return ts.TotalSeconds().ToString.Split(",")(0).Split(".")(0)
    End Function

    Private Sub GenerarInsertsTLG()
        Dim r As HtmlTableRow = htmlTableTLG.Rows(htmlTableTLG.Rows.Count - 1)
        Dim insert As String = "INSERT INTO datosTLG(fecha,gestionesOk,total) " &
            "VALUES('" & fechaSql & "'," & r.Cells(2).InnerText & "," & r.Cells(3).InnerText & ")"
        arrDataInsertsTLG.Add(insert)
    End Sub

    Private Function obtenerGrupo(i As Object) As String
        Return "NULL"
    End Function

    Private Function getOperador(ByVal mgt As String) As String
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("SELECT id FROM operadorTDI WHERE mgt='" & mgt & "'", SQL)
        Dim res As Integer
        Try
            SQL.Open()
            res = CMD.ExecuteScalar()
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al obtener operador", ex)
        End Try
        Return res
    End Function

    Private Function getLider(ByVal nombre As String) As String
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("SELECT id FROM liderTDI WHERE nombre='" & nombre.Replace("'", "''") & "'", SQL)
        Dim res As Integer
        Try
            SQL.Open()
            res = CMD.ExecuteScalar()
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al obtener lider", ex)
        End Try
        Return res
    End Function

    Private Sub procesarTDI()
        Dim faux, fHasta As DateTime
        faux = fIni
        fHasta = fFin
        If faux <= fHasta Then
            While faux.Date <= fHasta.Date
                fecha = faux
                fechaSql = fecha.ToString("yyyyMMdd HH:mm:ss")
                If getScalar("SELECT count(*) FROM datosTDI WHERE DATEPART(year,'" & fechaSql & "')=DATEPART(year,fecha) AND DATEPART(month,'" & fechaSql & "')=DATEPART(month,fecha) AND DATEPART(day,'" & fechaSql & "')=DATEPART(day,fecha)") = 0 Then
                    extraerDatosTDI()
                    obtenerArrLideres()
                    obtenerArrOperadores()
                    actualizarLideresEnDB(arrLideres)
                    actualizarOperadoresEnDB(arrOperadores)
                    GenerarInsertTDI()
                End If
                InsertarDatosTDI()
                arrDataInsertsTDI.Clear()
                faux = faux.AddDays(1)
            End While
        End If
    End Sub

    Private Sub procesarTLG()
        fecha = Now.Date
        fechaSql = Now.Date.ToString("yyyyMMdd HH:mm:ss")
        extraerDatosTLG()
        GenerarInsertsTLG()
        InsertarDatosTLG()
    End Sub

    Private Sub enviarMailTDI(msgError As String, date1 As DateTime, date2 As DateTime)
        Dim cod = "ErrorExtraccionTDI"
        Dim dest = getScalar("SELECT destinatarios FROM notificaciones WHERE id='" + cod + "'").ToString()
        Dim msg = "<html><body><table>" &
            "<tr><td>Se produjo un error en la extracción automatica del sistema TDI<td></tr>" &
            "<tr><td>Error: " & msgError & "<td></tr>" &
            "<tr><td>Realize la extracción del siguiente período: [" & date1 & "] hasta [" & date2 & "] manualmente desde <a href=""http://mscmda.mega.com.ar/Default.aspx?url=http://mscmda.mega.com.ar/tscspd/qlikview/cargaManual.aspx?tab=tdi"">AQUI</a><td></tr>" &
            "</table></body></html>"
        FUNCIONES_MOD.mailmega2.enviamail("megarobot@megatech.la", dest, "megarobot@megatech.la", "MDA Dr.Speedy - Error en la extracción automatica de TDI", msg)
    End Sub

    Private Sub enviarMailTLG(msgError As String, date1 As DateTime)
        Dim cod = "ErrorExtraccionTLG"
        Dim dest = getScalar("SELECT destinatarios FROM notificaciones WHERE id='" + cod + "'").ToString()
        Dim msg = "<html><body><table>" &
            "<tr><td>Se produjo un error en la extracción automatica del sistema TLG<td></tr>" &
            "<tr><td>Error: " & msgError & "<td></tr>" &
            "<tr><td>Realize la extracción de la fecha: [" & date1 & "], manualmente desde <a href=""http://mscmda.mega.com.ar/Default.aspx?url=http://mscmda.mega.com.ar/tscspd/qlikview/cargaManual.aspx?tab=tlg"">AQUI</a><td></tr>" &
            "</table></body></html>"
        FUNCIONES_MOD.mailmega2.enviamail("megarobot@megatech.la", dest, "megarobot@megatech.la", "MDA Dr.Speedy - Error en la extracción automatica de TLG", msg)
    End Sub

    Private Sub enviarMailFCR(msgError As String, date1 As DateTime, date2 As DateTime)
        Dim cod = "ErrorExtraccionTDI"
        Dim dest = getScalar("SELECT destinatarios FROM notificaciones WHERE id='" + cod + "'").ToString()
        Dim msg = "<html><body><table>" &
            "<tr><td>Se produjo un error en la extracción automatica de FCR y STR, desde el sistema TDI<td></tr>" &
            "<tr><td>Error: " & msgError & "<td></tr>" &
            "<tr><td>Realize la extracción del siguiente período: [" & date1 & "] hasta [" & date2 & "], manualmente desde <a href=""http://mscmda.mega.com.ar/tscspd/Default.aspx?url=http://mscmda.mega.com.ar/tscspd/qlikview/cargaManual.aspx?tab=fcr"">AQUI</a><td></tr>" &
            "</table></body></html>"
        FUNCIONES_MOD.mailmega2.enviamail("megarobot@megatech.la", dest, "megarobot@megatech.la", "MDA Dr.Speedy - Error en la extracción automatica de TLG", msg)
    End Sub
End Class
