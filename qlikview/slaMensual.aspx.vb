﻿
Partial Class slaMensual
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim dt As Date

        If Request.QueryString("fecha") IsNot Nothing Then
            dt = Date.Parse(Request.QueryString("fecha"))
        Else
            dt = UltimoDiaDelMes(Now.Date)
        End If

        f1.Value = PrimerDiaDelMes(dt).ToString("yyyy-MM-dd")
        f2.Value = dt.ToString("yyyy-MM-dd")
    End Sub

    'para saber el primer dia del mes 
    Function PrimerDiaDelMes(ByVal dtmFecha As Date) As Date
        Return DateSerial(Year(dtmFecha), Month(dtmFecha), 1)
    End Function

    'para saber el ultimo dia del mes 
    Function UltimoDiaDelMes(ByVal dtmFecha As Date) As Date
        Return DateSerial(Year(dtmFecha), Month(dtmFecha) + 1, 0)
    End Function
End Class
