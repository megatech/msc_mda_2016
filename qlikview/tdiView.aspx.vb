﻿
Imports System.Net
Imports HtmlAgilityPack
Imports System.Data.SqlClient
Imports Telerik.Web.UI
Imports App_Code.TicketsLib
Imports System.IO
Imports SecurityHelper

Partial Class qlikview_cargaManual
    Inherits System.Web.UI.Page
    Private arrDataInsertsTDI As New ArrayList
    Private arrDataInsertsTLG As New ArrayList
    Private fecha As DateTime
    Private fechaSql As String
    Private arrLideres As New ArrayList
    Private arrOperadores As New ArrayList
    Private htmlTableTdi As HtmlTable
    Private htmlTableTLG As HtmlTable


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        'Dim b = getScalar("SELECT count(*) FROM DatosTLG WHERE DATEPART(year,GETDATE())=DATEPART(year,fecha) AND DATEPART(month,GETDATE())=DATEPART(month,fecha) AND DATEPART(day,GETDATE())=DATEPART(day,fecha)") = 0
        If Not IsPostBack Then
            RadDatePicker1.SelectedDate = Now.Date.AddDays(-1)
            If Session("usuario") IsNot Nothing Then
                Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
                If Not u.Perfil = 1 And Not u.Perfil = 20 Then
                    Response.Redirect("~/Default2.aspx")
                End If
            End If
            fechaSLAMes.SelectedDate = Now.Date.AddDays(-1)
            fechaGFL.SelectedDate = Now.Date
            lblStatusSLA.Text = ""
            fIni.SelectedDate = Now.AddDays(-1)
            fFin.SelectedDate = Now.AddDays(-1)
        End If
    End Sub

    Private Sub extraerDatosTDI()
        Dim webcli = New WebClient()
        Dim desde As String = fIni.SelectedDate.Value.ToString("yyyy-MM-dd")
        Dim hasta As String = fFin.SelectedDate.Value.ToString("yyyy-MM-dd")
        webcli.Credentials = AccessManager.obtenerCredenciales("tasa")
        Dim url = "http://10.244.41.26/tdi/Genesys/reporte/listado_rac.php?programa=2&pcrc=&f_i=" & desde & "&f_f=" & hasta & "&proveedor=&site=10"
        Dim res = webcli.DownloadString(url)
        Dim tableString = SeleccionarEntre(res, "<table align='center' width='90%' border=1>", "</table>")
        htmlTableTdi = htmlStringToHtmlTable(tableString)
        Literal1.Text = tableString
    End Sub

    Private Sub extraerDatosTLG()
        Dim webcli = New WebClient()
        webcli.Credentials = AccessManager.obtenerCredenciales("tasa")
        Dim res = webcli.DownloadString("http://tlg/drspeedymegatech/indicadores.asp")
        Dim tableString = SeleccionarEntre(res, "<table width=""350"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" class=""Tabla"">", "</table>")
        'Literal1.Text = tableString
        htmlTableTLG = htmlStringToHtmlTable(tableString)
    End Sub

    Public Function htmlStringToHtmlTable(ByVal html As String) As HtmlTable
        Dim htmlTable = New HtmlTable()
        Dim htmlDoc = New HtmlDocument()
        htmlDoc.LoadHtml(html)
        Dim tables As HtmlNodeCollection = htmlDoc.DocumentNode.SelectNodes("//table")
        Dim rows As HtmlNodeCollection = tables(0).SelectNodes(".//tr")
        For i As Integer = 0 To rows.Count - 1
            htmlTable.Rows.Add(New HtmlTableRow())
            Dim thcols As HtmlNodeCollection = rows(i).SelectNodes(".//th")
            If thcols IsNot Nothing Then
                For Each n As HtmlNode In thcols
                    Dim newThCell As New HtmlTableCell
                    newThCell.InnerText = n.InnerText()
                    htmlTable.Rows(i).Cells.Add(newThCell)
                Next
            End If
            Dim cols As HtmlNodeCollection = rows(i).SelectNodes(".//td")
            If cols IsNot Nothing Then
                For Each n As HtmlNode In cols
                    Dim newCell As New HtmlTableCell
                    newCell.InnerText = n.InnerText()
                    htmlTable.Rows(i).Cells.Add(newCell)
                Next
            End If
        Next
        Return htmlTable
    End Function

    Public Function SeleccionarEntre(ByVal texto As String, ByVal prefijo As String, ByVal sufijo As String) As String
        Dim html = texto
        Dim htmlCount = texto.Count
        Dim preIndex = html.IndexOf(prefijo.ToString())
        Dim suIndex = html.Substring(preIndex).IndexOf(sufijo, 1) + sufijo.Count + preIndex
        Dim cuenta = html.Count - suIndex + 1
        Dim res = html.Remove(suIndex, cuenta - 1)
        res = res.Substring(preIndex)
        Return res
    End Function

    Private Function getScalar(q As String) As Object
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand(q, SQL)
        Dim res As Object
        Try
            SQL.Open()
            res = CMD.ExecuteScalar()
            SQL.Close()
            Return res
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Function

    Private Sub InsertarDatosTDI()
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()
            For Each i In arrDataInsertsTDI
                CMD.CommandText = i.replace("''", "NULL")
                If CMD.ExecuteNonQuery() < 1 Then
                    Throw New Exception("Error al insertar los Datos.")
                End If
            Next
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Sub

    Private Sub InsertarDatosTLG()
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()
            For Each i In arrDataInsertsTLG
                CMD.CommandText = i
                If CMD.ExecuteNonQuery() < 1 Then
                    Throw New Exception("Error al insertar una fila.")
                End If
            Next
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Sub

    Private Sub obtenerArrLideres()
        For Each r As HtmlTableRow In htmlTableTdi.Rows
            If Not r Is htmlTableTdi.Rows(0) Then
                If Not arrLideres.Contains(r.Cells(3).InnerText.Trim) Then arrLideres.Add(r.Cells(3).InnerText.Trim)
            End If
        Next
    End Sub

    Private Sub obtenerArrOperadores()
        For Each r As HtmlTableRow In htmlTableTdi.Rows
            If Not r Is htmlTableTdi.Rows(0) Then
                Dim arrItm = New ArrayList
                arrItm.Add(r.Cells(2).InnerText.Trim)
                arrItm.Add(r.Cells(0).InnerText.Trim)
                arrItm.Add(r.Cells(1).InnerText.Trim)
                arrItm.Add(r.Cells(3).InnerText.Trim)
                If Not arrOperadores.Contains(arrItm) Then arrOperadores.Add(arrItm)
            End If
        Next
    End Sub

    Private Sub actualizarLideresEnDB(ByVal liders As ArrayList)
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()

            For Each i In liders
                If i <> "" Then
                    CMD.CommandText = "SELECT count(*) FROM liderTDI WHERE nombre='" & i & "'"
                    If CMD.ExecuteScalar() = 0 Then
                        Dim grupoId As String = obtenerGrupo(i)
                        CMD.CommandText = "INSERT INTO liderTDI(nombre,grupoid) values('" & i & "'," & grupoId & ")"
                        CMD.ExecuteNonQuery()
                    End If
                End If
            Next

            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al actualizar lider", ex)
        End Try
    End Sub

    Private Sub actualizarOperadoresEnDB(ByVal operadores As ArrayList)
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("", SQL)
        Try
            SQL.Open()

            For Each i In operadores
                CMD.CommandText = "SELECT count(*) FROM operadorTDI WHERE mgt='" & i(0) & "'"
                If CMD.ExecuteScalar() = 0 Then
                    CMD.CommandText = "SELECT id FROM liderTDI WHERE nombre ='" & i(3) & "'"
                    Dim liderId As String = CMD.ExecuteScalar()
                    If liderId = "" Then liderId = "NULL"
                    CMD.CommandText = "INSERT INTO operadorTDI(mgt,apellido,nombre,liderId) values('" & i(0) & "','" & i(1) & "','" & i(2) & "'," & liderId & ")"
                    CMD.ExecuteNonQuery()
                End If

            Next

            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
        End Try
    End Sub

    Private Function intervalToSeconds(str As String) As String
        Dim hhh As Integer = str.Split(":")(0)
        Dim mm As Integer = str.Split(":")(1)
        Dim ss As Integer = str.Split(":")(2)
        Dim ts As New TimeSpan(hhh, mm, ss)
        Return ts.TotalSeconds().ToString.Split(",")(0).Split(".")(0)
    End Function

    Private Sub GenerarInsertTDI()
        For Each r As HtmlTableRow In htmlTableTdi.Rows
            If r IsNot htmlTableTdi.Rows(0) Then
                Dim arrFields = New ArrayList
                arrFields.Add({"fecha", "'" & fechaSql & "'"})
                arrFields.Add({"liderId", fetchCell(r, 3, "lider")})
                arrFields.Add({"operadorId", fetchCell(r, 2, "operador")})
                arrFields.Add({"llamAtendidas", fetchCell(r, 8, "int")})
                arrFields.Add({"llamSalientes", fetchCell(r, 9, "int")})
                arrFields.Add({"llamCortas", fetchCell(r, 10, "int")})
                arrFields.Add({"llamTransferidas", fetchCell(r, 11, "int")})
                arrFields.Add({"talkTime", fetchCell(r, 12, "int")})
                arrFields.Add({"acw", fetchCell(r, 14, "int")})
                arrFields.Add({"waitTime", fetchCell(r, 16, "int")})
                arrFields.Add({"holdTime", fetchCell(r, 17, "int")})
                arrFields.Add({"nrRefrig", fetchCell(r, 19, "int")})
                arrFields.Add({"nrBanio", fetchCell(r, 20, "int")})
                arrFields.Add({"nrCoachDev", fetchCell(r, 21, "int")})
                arrFields.Add({"nrCapacitacion", fetchCell(r, 22, "int")})
                arrFields.Add({"nrResRec", fetchCell(r, 23, "int")})
                arrFields.Add({"nrOtros", fetchCell(r, 24, "int")})
                arrFields.Add({"nrDescVisual", fetchCell(r, 25, "int")})
                arrFields.Add({"nrCoachExpress", fetchCell(r, 26, "int")})
                arrFields.Add({"nrLectNovedades", fetchCell(r, 27, "int")})
                arrFields.Add({"nrVarios", fetchCell(r, 28, "int")})
                arrFields.Add({"nrNone", fetchCell(r, 29, "int")})
                arrFields.Add({"nrTotal", fetchCell(r, 30, "toSeconds")})
                arrFields.Add({"logProd", fetchCell(r, 31, "toSeconds")})
                arrFields.Add({"logTotal", fetchCell(r, 32, "toSeconds")})
                arrFields.Add({"tmo", fetchCell(r, 33, "decimal")})
                arrFields.Add({"occ", fetchCell(r, 34, "decimal")})
                arrFields.Add({"utilizacion", fetchCell(r, 35, "decimal")})
                'arrFields.Add({"fcr", fetchCell(r, 36, "decimal")})
                'arrFields.Add({"str", fetchCell(r, 37, "decimal")})

                Dim insQuery = "INSERT INTO datosTDI("
                For Each f In arrFields
                    If f IsNot arrFields(0) Then insQuery &= ","
                    insQuery &= f(0)
                Next
                insQuery &= ") VALUES("
                For Each f In arrFields
                    If f IsNot arrFields(0) Then insQuery &= ","
                    insQuery &= f(1)
                Next
                insQuery &= ")"
                arrDataInsertsTDI.Add(insQuery)
            End If
        Next

    End Sub

    Private Function fetchCell(ByRef r As HtmlTableRow, nro As Integer, tipo As String) As String
        If r.Cells.Count - 1 < nro Then Return "NULL"
        If tipo = "decimal" Then
            Return "'" & r.Cells(nro).InnerText.Replace(".", "").Replace(",", ".") & "'"
        ElseIf tipo = "varchar" Then
            Return "'" & r.Cells(nro).InnerText.Replace("'", "''") & "'"
        ElseIf tipo = "int" Then
            Return r.Cells(nro).InnerText
        ElseIf tipo = "toSeconds" Then
            Return intervalToSeconds(r.Cells(nro).InnerText)
        ElseIf tipo = "lider" Then
            Return getLider(r.Cells(nro).InnerText)
        ElseIf tipo = "operador" Then
            Return getOperador(r.Cells(nro).InnerText)
        End If
    End Function
    Private Sub GenerarInsertsTLG()
        Dim r As HtmlTableRow = htmlTableTLG.Rows(htmlTableTLG.Rows.Count - 1)
        Dim insert As String = "INSERT INTO datosTLG(fecha,gestionesOk,total,gfl) " &
            "VALUES('" & fechaSql & "'," & r.Cells(2).InnerText & "," & r.Cells(3).InnerText & ",'" & r.Cells(4).InnerText.Replace(",", ".") & "')"
        arrDataInsertsTLG.Add(insert)
    End Sub

    Private Function obtenerGrupo(i As Object) As String
        Return "NULL"
    End Function

    Private Function getOperador(ByVal mgt As String) As String
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("SELECT id FROM operadorTDI WHERE mgt='" & mgt & "'", SQL)
        Dim res As Integer
        Try
            SQL.Open()
            res = CMD.ExecuteScalar()
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al obtener operador", ex)
        End Try
        Return res
    End Function

    Private Function getLider(ByVal nombre As String) As String
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("SELECT id FROM liderTDI WHERE nombre='" & nombre.Replace("'", "''") & "'", SQL)
        Dim res As Integer
        Try
            SQL.Open()
            res = CMD.ExecuteScalar()
            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw New Exception("Error al obtener lider", ex)
        End Try
        Return res
    End Function

    Private Sub procesarTDI()

        Dim faux, fHasta As DateTime
        faux = fIni.SelectedDate
        fHasta = fFin.SelectedDate
        fecha = faux
        fechaSql = fecha.ToString("yyyyMMdd HH:mm:ss")
        extraerDatosTDI()
    End Sub
    Private Sub procesarTLG()
        fecha = Now.Date
        fechaSql = Now.Date.ToString("yyyyMMdd HH:mm:ss")
        extraerDatosTLG()
        GenerarInsertsTLG()
        InsertarDatosTLG()
    End Sub

    Private Sub procesarCargaManualTLG()
        Dim v1 = txtGestionesOk.Text.Trim = "" OrElse Not esNumerico(txtGestionesOk.Text)

        Dim v3 = txtTotal.Text.Trim = "" OrElse Not esNumerico(txtTotal.Text)
        If v1 OrElse v3 Then
            lblValidacionTLG.Visible = True
        Else
            arrDataInsertsTLG.Clear()
            Dim fechaStr = fechaGFL.SelectedDate.Value.ToString("yyyyMMdd HH:mm:ss")
            Dim insert As String = "INSERT INTO datosTLG(fecha,gestionesOk,total) " &
        "VALUES('" & fechaStr & "'," & txtGestionesOk.Text.Trim & "," & txtTotal.Text.Trim & ")"
            arrDataInsertsTLG.Add(insert)
            InsertarDatosTLG()
        End If

    End Sub

    Private Function esNumerico(str As String) As Boolean
        For Each ch In str
            If Not ".,0123456789".Contains(ch) Then Return False
        Next
        Return True
    End Function

    Protected Sub btExtraerTDI_Click(sender As Object, e As EventArgs) Handles btExtraerTDI.Click
        Try
            procesarTDI()
            lblStatusTDI.ForeColor = lblMsgTLG.ForeColor
            lblStatusTDI.Text = "Los datos fueron cargados correctamente."
        Catch ex As Exception
            lblStatusTDI.ForeColor = lblValidacionTLG.ForeColor
            lblStatusTDI.Text = "Error: </br>" & ex.Message
        End Try

    End Sub

    Protected Sub btExtraerTLG_Click(sender As Object, e As EventArgs) Handles btExtraerTLG.Click
        Try
            If CheckBox1.Checked Then
                procesarCargaManualTLG()
            Else
                procesarTLG()
            End If
            lblStatusTLG.ForeColor = lblMsgTLG.ForeColor()
            lblStatusTLG.Text = "Los Datos fueron cargados correctamente."
        Catch ex As Exception
            lblStatusTLG.ForeColor = lblValidacionTLG.ForeColor
            If ex.Message.Contains("Infracción de la restricción PRIMARY KEY") Then
                lblStatusTLG.Text = "Error: Los datos de GFL de la fecha especificada, ya fueron cargados anteriormente."
            Else
                lblStatusTLG.Text = "Error: " & ex.Message
            End If
        End Try
    End Sub

    Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        For Each c In Panel1.Controls
            If c.GetType.Name = "RadDatePicker" Then
                Dim ct As New RadDatePicker
                ct = TryCast(c, RadDatePicker)
                ct.Enabled = CheckBox1.Checked
            ElseIf c.GetType.Name = "RadTextBox" Then
                Dim ct As New RadTextBox
                ct = TryCast(c, RadTextBox)
                ct.Enabled = CheckBox1.Checked
            End If
        Next
        lblStatusTLG.Text = ""
        lblValidacionTLG.Visible = False
        lblMsgTLG.Visible = Not CheckBox1.Checked
        If CheckBox1.Checked Then
            btExtraerTLG.Text = "Cargar Datos Ingresados"
        Else
            btExtraerTLG.Text = "Extraer Datos de TLG"
        End If
    End Sub

    Protected Sub btCargarSla_Click(sender As Object, e As EventArgs) Handles btCargarSla.Click
        Dim v1, v2, v3
        If chkMes.Checked Then
            v1 = txtLlamRecibidas.Text.Trim = "" OrElse Not esNumerico(txtLlamRecibidas.Text) _
                 OrElse txtLlamRecibidasDia.Text.Trim = "" OrElse Not esNumerico(txtLlamRecibidasDia.Text)
            v2 = txtLlamAtendidas.Text.Trim = "" OrElse Not esNumerico(txtLlamAtendidas.Text) _
                 OrElse txtLlamAtendidasDia.Text.Trim = "" OrElse Not esNumerico(txtLlamAtendidasDia.Text)
            v3 = txtSLA.Text.Trim = "" OrElse Not esNumerico(txtSLA.Text) OrElse Decimal.Parse(txtSLA.Text.Trim.Replace(".", ",")) > 100 _
                 OrElse txtSLADia.Text.Trim = "" OrElse Not esNumerico(txtSLADia.Text) OrElse Decimal.Parse(txtSLADia.Text.Trim.Replace(".", ",")) > 100
        Else
            v1 = txtLlamRecibidasHora.Text.Trim = "" OrElse Not esNumerico(txtLlamRecibidasHora.Text)
            v2 = txtLlamAtendidasHora.Text.Trim = "" OrElse Not esNumerico(txtLlamAtendidasHora.Text)
            v3 = txtLlamAtendidas20Hora.Text.Trim = "" OrElse Not esNumerico(txtLlamAtendidas20Hora.Text) OrElse _
                Decimal.Parse(txtSLAGFL.Text.Trim.Replace(".", ",")) > 100 OrElse Decimal.Parse(txtSLAOnsite.Text.Trim.Replace(".", ",")) > 100

        End If

        If v1 OrElse v2 OrElse v3 Then
            lblValidacionSLA.Visible = True
            lblStatusSLA.Text = ""
        Else

            lblValidacionSLA.Visible = False

            If chkMes.Checked Then
                Try
                    cargarDatosSLA()
                    lblStatusSLA.ForeColor = lblMsgTLG.ForeColor
                    lblStatusSLA.Text = "Los datos fueron cargados correctamente"
                    chkMes.Checked = False
                    chkMes_CheckedChanged(Me, New EventArgs())
                Catch ex As Exception
                    lblStatusSLA.ForeColor = lblValidacionSLA.ForeColor
                    lblStatusSLA.Text = "Error al guardar los datos en la Base de datos. " & ex.Message
                End Try
            Else
                enviarMail()
                Try
                Catch ex As Exception
                    lblStatusSLA.ForeColor = lblValidacionSLA.ForeColor
                    lblStatusSLA.Text = "Los datos fueron guardados correctamente, pero no se pudieron notificar por E-mail. " & ex.Message
                End Try
            End If
        End If
        chkMes_CheckedChanged(Me, New EventArgs())

    End Sub

    Private Sub cargarDatosSLA()
        Dim tabla = "slaDiario"

        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("INSERT INTO slaMensual(fecha,recibidas,abandonadas,sla,usuario,atendidasEn20) VALUES(", SQL)
        Try
            Dim usuario As Usuario = Session("Usuario")
            Dim usrName = usuario.Nick.Trim
            Dim abandonadas = Integer.Parse(txtLlamRecibidas.Text.Trim) - Integer.Parse(txtLlamAtendidas.Text.Trim)
            Dim abandonadasDia = Integer.Parse(txtLlamRecibidasDia.Text.Trim) - Integer.Parse(txtLlamAtendidasDia.Text.Trim)
            Dim atendidasEn20, sla, fechaSla As String
            Dim atendidasEn20Dia, slaDia As String

            sla = txtSLA.Text.Trim.Replace(".", ",")
            atendidasEn20 = Math.Round((Integer.Parse(txtLlamRecibidas.Text.Trim) * Decimal.Parse(sla)) / 100, 0)
            fechaSla = "'" & fechaSLAMes.SelectedDate.Value.ToString("yyyyMMdd HH:mm:ss") & "'"
            slaDia = txtSLADia.Text.Trim.Replace(".", ",")
            atendidasEn20Dia = Math.Round((Integer.Parse(txtLlamRecibidasDia.Text.Trim) * Decimal.Parse(sla)) / 100, 0)

            SQL.Open()
            CMD.CommandText &= fechaSla & "," & txtLlamRecibidas.Text.Trim & "," & abandonadas & ",'" & sla.Replace(",", ".") & "','" & usrName & "'," & atendidasEn20 & ")"
            If CMD.ExecuteNonQuery() < 1 Then
                Throw New Exception("Error al insertar datos del Mes.")
            End If
            CMD.CommandText = "INSERT INTO slaDiario(fecha,recibidas,abandonadas,sla,usuario,atendidasEn20) VALUES(" &
                fechaSla & "," & txtLlamRecibidasDia.Text.Trim & "," & abandonadasDia & ",'" & slaDia.Replace(",", ".") & "','" & usrName & "'," & atendidasEn20Dia & ")"
            If CMD.ExecuteNonQuery() < 1 Then
                Throw New Exception("Error al insertar datos del Día.")
            End If

            SQL.Close()
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try
    End Sub

    Protected Sub RadioButtonList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        If RadioButtonList1.SelectedIndex = 1 Then
            ifrm.Attributes.Add("src", "slaActual.aspx?fecha=" & RadDatePicker1.SelectedDate.Value.ToString("yyyy-MM-dd"))
        Else
            ifrm.Attributes.Add("src", "slaMensual.aspx?fecha=" & RadDatePicker1.SelectedDate.Value.ToString("yyyy-MM-dd"))
        End If
    End Sub

    Private Function obtenerValores() As OrderedDictionary
        Dim datos = New OrderedDictionary()
        Dim SQL As New SqlConnection(ConfigurationManager.ConnectionStrings("ticketDB").ToString)
        Dim CMD As New SqlCommand("SELECT TOP 1 * FROM slaMensual ORDER BY fecha Desc", SQL)
        Dim RS As SqlDataReader
        Try
            Dim abandonadasHora = Integer.Parse(txtLlamRecibidasHora.Text.Trim) - Integer.Parse(txtLlamAtendidasHora.Text.Trim)
            Dim slaHora As String = Math.Round((Integer.Parse(txtLlamAtendidas20Hora.Text) / Integer.Parse(txtLlamRecibidasHora.Text)) * 100, 2)
            datos.Add("fechaHora", Now.ToString)
            datos.Add("slaHora", slaHora & "%")
            datos.Add("recibidasHora", txtLlamRecibidasHora.Text.ToString)
            datos.Add("abandonadasHora", abandonadasHora.ToString)
            datos.Add("atendidasEn20Hora", txtLlamAtendidas20Hora.Text.ToString)
            SQL.Open()
            RS = CMD.ExecuteReader
            RS.Read()
            datos.Add("fechaMes", RS("fecha").ToString)
            datos.Add("slaMes", RS("sla").ToString & "%")
            datos.Add("recibidasMes", RS("recibidas").ToString)
            datos.Add("abandonadasMes", RS("abandonadas").ToString)
            datos.Add("atendidasEn20Mes", RS("atendidasEn20").ToString)
            SQL.Close()
            Return datos
        Catch ex As Exception
            If SQL.State <> Data.ConnectionState.Closed Then SQL.Close()
            Throw ex
        End Try

    End Function

    Private Sub enviarMail()
        Dim datos As OrderedDictionary = obtenerValores()
        Dim pendTele = getScalar("Select count(*) From Ticket WHERE fechaBaja is null and estadoId=148")
        Dim pendRemo = getScalar("Select count(*) From Ticket WHERE fechaBaja is null and estadoId=143")
        Dim cod = "CargaSLA"
        Dim dest = getScalar("SELECT destinatarios FROM notificaciones WHERE id='" + cod + "'").ToString()
        Dim slaGFL = txtSLAGFL.Text.Replace(".", ",")
        Dim slaOnsite = txtSLAOnsite.Text.Replace(".", ",")
        Dim msg = "<html><body><table>" &
        "<tr><td colspan=""2"" style=""font-weight:bold; font-size:18px;text-align:center;background-color:#333333;color:white;"">Mesa De Ayuda - Dr.Speedy</td></tr>" &
        "<tr><td colspan=""2"" style=""font-weight:bold; font-size:14px;text-align:center;color:white;background-color:#555555;"">Mediciones Del D&iacute;a</td></tr>" &
        "<tr style=""font-weight:bold;font-size:14px;""><td>Última actualización: </td><td style=""text-align:right"">" & datos("fechaHora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Recibidas:</td><td style=""text-align:right"">" & datos("recibidasHora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Atendidas &#60;20seg:</td><td style=""text-align:right"">" & datos("atendidasEn20Hora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Abandonadas:</td><td style=""text-align:right"">" & datos("abandonadasHora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>SLA:</td><td style=""text-align:right"">" & datos("slaHora") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Tkts Pendientes (Telef&oacute;nico):</td><td style=""text-align:right"">" & pendTele & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Tkts Pendientes (Remoto):</td><td style=""text-align:right"">" & pendRemo & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>SLA Gestiones Fuera de Linea:</td><td style=""text-align:right"">" & slaGFL & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>SLA Visitas Domiciliarias:</td><td style=""text-align:right"">" & slaOnsite & "</td></tr>" &
        "<tr><td colspan=""2"" style=""font-weight:bold; font-size:14px; text-align:center;color:white;background-color:#555555;"">Valores Acumulados del Mes</td></tr>" &
        "<tr style=""font-weight:bold;font-size:14px;""><td>Última actualización: </td><td style=""text-align:right"">" & datos("fechaMes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Recibidas:</td><td style=""text-align:right"">" & datos("recibidasMes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Atendidas &#60;20seg:</td><td style=""text-align:right"">" & datos("atendidasEn20Mes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>Llamadas Abandonadas:</td><td style=""text-align:right"">" & datos("abandonadasMes") & "</td></tr>" &
        "<tr style=""font-weight:bold;""><td>SLA:</td><td style=""text-align:right"">" & datos("slaMes") & "</td></tr>" &
        "</table></body></html>"

        FUNCIONES_MOD.mailmega2.enviamail("megarobot@megatech.la", dest, "megarobot@megatech.la", "MDA Dr.Speedy - SLA [" & Now.ToString & "]", msg)

    End Sub

    Protected Sub chkMes_CheckedChanged(sender As Object, e As EventArgs) Handles chkMes.CheckedChanged
        Panel2.Visible = chkMes.Checked
        Panel3.Visible = Not chkMes.Checked
        fechaSLAMes.Visible = chkMes.Checked
        lblValidacionSLA.Visible = False
        borrarTextboxes()
    End Sub

    Private Sub borrarTextboxes()
        For Each c As Control In Panel2.Controls
            If c.GetType.Name = "RadTextBox" Then
                Dim txt As New RadTextBox()
                txt = c
                txt.Text = ""
            End If
        Next
        For Each c As Control In Panel3.Controls
            If c.GetType.Name = "RadTextBox" Then
                Dim txt As New RadTextBox()
                txt = c
                txt.Text = ""
            End If
        Next
    End Sub

    Protected Sub RadDatePicker1_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles RadDatePicker1.SelectedDateChanged
        ifrm.Attributes.Add("src", "slaActual.aspx?fecha=" & RadDatePicker1.SelectedDate.Value.ToString("yyyy-MM-dd"))
    End Sub

    Protected Sub btExtraerTDI0_Click(sender As Object, e As EventArgs) Handles btExtraerTDI0.Click

        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)

        Dim pagina = New Page()
        Dim Form = New HtmlForm()

        Dim t As HtmlTable = New HtmlTable()
        Dim lblT As Label = New Label()
        lblT.Text = "Extracción TDI [ " & fIni.SelectedDate.Value.ToString("dd/MM/yyyy") & "_" & fFin.SelectedDate.Value.ToString("dd/MM/yyyy") & " ]"
        lblT.Font.Bold = True
        lblT.Font.Size = 14
        t.Rows.Add(New HtmlTableRow())
        t.Rows.Add(New HtmlTableRow())
        t.Rows(0).Cells.Add(New HtmlTableCell())
        t.Rows(1).Cells.Add(New HtmlTableCell())
        t.Rows(0).Cells(0).Controls.Add(lblT)
        t.Rows(1).Cells(0).InnerHtml = Literal1.Text
        pagina.Controls.Add(Form)
        Form.Controls.Add(t)
        Form.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=" & lblT.Text & ".xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
    End Sub
End Class
