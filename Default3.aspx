﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="Default3.aspx.cs" Inherits="Default3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
    <table align="left" style="border: 1px solid #51596F">
        <tr>
            <td colspan="6" nowrap="nowrap" style="height: 34px">
                Area:
                <asp:DropDownList ID="ddArea" runat="server" DataSourceID="dsArea" 
                    DataTextField="Nombre" DataValueField="EmpresaPartnerId" 
                    AutoPostBack="True" onselectedindexchanged="ddArea_SelectedIndexChanged">
                </asp:DropDownList>
            &nbsp; Tecnico/CAS:
                <asp:DropDownList ID="ddTecnico" runat="server" DataSourceID="dsTecnicosYCAS" 
                    DataTextField="Nombre" DataValueField="id">
                </asp:DropDownList>
            &nbsp;&nbsp; Estado:
                <asp:DropDownList ID="ddEstado" runat="server" DataSourceID="dsEstado" 
                    DataTextField="Estado" DataValueField="EstadoId">
                </asp:DropDownList>
            &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Fecha de creación</td>
            <td>
                F. última 
                <br />
                modificación</td>
            <td>
                Fecha coordinada</td>
            <td>
                Fecha
                <br />
                de Solución</td>
            <td>
                Fecha
                <br />
                de carga</td>
        </tr>
        <tr>
            <td>
                Desde:</td>
            <td >
                <telerik:RadDatePicker ID="dpFCreacionDesde" Runat="server" Culture="es-ES" 
                    Width="100px">
                </telerik:RadDatePicker>
            </td>
            <td >
                <telerik:RadDatePicker ID="dpFUltimaModDesde" Runat="server" Width="100px">
                </telerik:RadDatePicker>
            </td>
            <td >
                <telerik:RadDatePicker ID="dpFCoordinadaDesde" Runat="server" Width="100px">
                </telerik:RadDatePicker>
            </td>
            <td >
                <telerik:RadDatePicker ID="dpFSolucionDesde" Runat="server" Width="100px">
                </telerik:RadDatePicker>
            </td>
            <td>
                <telerik:RadDatePicker ID="dpFCargaDesde" Runat="server" Width="100px">
                </telerik:RadDatePicker>
            </td>
        </tr>
        <tr>
            <td>
                Hasta:</td>
            <td >
                <telerik:RadDatePicker ID="dpFCreacionHasta" Runat="server" Width="100px">
                </telerik:RadDatePicker>
            </td>
            <td>
                <telerik:RadDatePicker ID="dpFUltimaModHasta" Runat="server" Width="100px">
                </telerik:RadDatePicker>
            </td>
            <td>
                <telerik:RadDatePicker ID="dpFCoordinadaHasta" Runat="server" Width="100px">
                </telerik:RadDatePicker>
            </td>
            <td>
                <telerik:RadDatePicker ID="dpFSolucionHasta" Runat="server" Width="100px">
                </telerik:RadDatePicker>
            </td>
            <td >
                <telerik:RadDatePicker ID="dpFCargaHasta" Runat="server" Width="100px">
                </telerik:RadDatePicker>
            </td>
        </tr>
        <tr>
            <td colspan="6" nowrap="nowrap">
                <asp:Label ID="Label4" runat="server" Text="Provincia: "></asp:Label>
                <asp:DropDownList ID="ddProvincia" runat="server" 
                    DataSourceID="dsProvincia" DataTextField="Nombre" 
                    DataValueField="provinciaId">
                </asp:DropDownList>
            &nbsp;&nbsp;<asp:Label ID="Label1" runat="server" Text="Localidad/Barrio: "></asp:Label>
            <asp:TextBox ID="txtLocalidad" runat="server" Text=""></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="6" nowrap="nowrap" width="60px" align="justify" >
                Horarios:&nbsp;
                <asp:CheckBoxList ID="lstHorarios" runat="server" RepeatDirection="Horizontal" 
                    DataSourceID="dsHorarios" DataTextField="descripcion" DataValueField="id" 
                    RepeatLayout="Flow">
                </asp:CheckBoxList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp; <asp:CheckBox ID="chkFiltrarCerrados" runat="server" Checked="True" 
                    Text="Filtrar tickets cerrados" />
                &nbsp;<table align="right" style="border-collapse: collapse"><tr><td>
                    <asp:Button ID="Button1" runat="server" Text="Aplicar Filtros" Width="200px" 
                    onclick="Button1_Click" />
                </td></tr></table>
                </td>
        </tr>
        </table>
    <!-- 
            <asp:TemplateField>
                <ItemTemplate >
                    <asp:CheckBox runat="server" ID="chk"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate >
                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/Edicion.jpg" Width="22" Height="22" />
                </ItemTemplate>
            </asp:TemplateField>
    -->
    <asp:SqlDataSource ID="dsProvincia" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        
        
        SelectCommand="SELECT 'Seleccione una opción' 'Nombre', '-1' ProvinciaId UNION
SELECT  distinct [provincia], provinciaId FROM [ViewOnsite] vo order by provinciaId"></asp:SqlDataSource>
            <asp:SqlDataSource ID="dsHorarios" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                    SelectCommand="SELECT * FROM franjahoraria"></asp:SqlDataSource>
                <asp:SqlDataSource ID="dsTecnicosYCAS" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                    SelectCommand="SELECT 'Seleccione una opción' 'Nombre', '-1' id UNION
                    SELECT 'Aun no asignado' 'Nombre', 0 id UNION
SELECT Nombre, id FROM tecnico order by id"></asp:SqlDataSource>
                <asp:SqlDataSource ID="dsArea" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                    
                    SelectCommand="SELECT 'Seleccione una opción' 'Nombre', '-1' EmpresaPartnerId UNION
Select Nombre, EmpresaPartnerId from empresapartner where empresapartnerId=57 or empresaPartnerId=58">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsEstado" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                    
                    SelectCommand="SELECT 'Seleccione una opción' 'Estado', '-1' 'EstadoId' UNION 
SELECT distinct v.Estado, v.EstadoId from viewOnsite v 
ORDER BY EstadoId">
                </asp:SqlDataSource>
            
    <script language="javascript" type="text/javascript">
    </script>
</asp:Content>

<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <asp:SqlDataSource ID="dsGridView" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        SelectCommand="SELECT [Ticket], [Estado], [Cliente], [Localidad], (case 
 when h1=1 
 then '10a12 ' 
 else NULL 
 end)+
(case 
 when h2=1 
 then '13a15 ' 
 else NULL 
 end)+
(case 
 when h3=1 
 then '15a17 ' 
 else NULL 
 end) as Horarios, [F.Coordinada] , [F. Creación] , [F. Ultima Modif.]  FROM [ViewOnsite]">
    </asp:SqlDataSource>
    <table style="width: 50%;" align="center">
        <tr>
            <td colspan="2" align="center">
                <asp:TextBox ID="txtLog" runat="server" Height="47px" TextMode="MultiLine" 
                    Width="100%" Font-Bold="True" ForeColor="#333333" ReadOnly="True" 
                    Visible="False"></asp:TextBox>
                <asp:Button ID="btInforme" runat="server" onclick="btInforme_Click" 
                    Text="Ocultar informe" Visible="False" />
                <asp:GridView ID="gvRes" runat="server" AllowPaging="True" AllowSorting="True" 
        AutoGenerateColumns="False" CellPadding="4"
        ForeColor="#333333" GridLines="None" HorizontalAlign="Center"
        DataKeyNames="Ticket" OnSorting="gvRes_Sorting" OnPageIndexChanging="gvRes_PageIndexChanging" 
        OnDataBound="gvRes_OnDataBound" OnDataBinding="gvRes_DataBinding"
        CssClass="gridViewOnsite" DataSourceID="dsGridView">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
                    <asp:TemplateField>
                <ItemTemplate >
                    <asp:CheckBox runat="server" ID="chk" OnCheckedChanged="chkChanged" AutoPostBack="True" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate >
                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/Edicion.jpg" Width="22" Height="22" OnClick="btEdit_click"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Ticket" HeaderText="Ticket" InsertVisible="False" 
                ReadOnly="True" SortExpression="Ticket" />
            <asp:BoundField DataField="Estado" HeaderText="Estado" 
                SortExpression="Estado" />
            <asp:BoundField DataField="Cliente" HeaderText="Cliente" 
                SortExpression="Cliente" />
            <asp:BoundField DataField="Localidad" HeaderText="Localidad" 
                SortExpression="Localidad" />
            <asp:BoundField DataField="Horarios" HeaderText="Horarios" ReadOnly="True" 
                SortExpression="Horarios" />
            <asp:BoundField DataField="F.Coordinada" HeaderText="F.Coordinada" 
                SortExpression="F.Coordinada" />
            <asp:BoundField DataField="F. Creación" HeaderText="F. Creación" 
                SortExpression="F. Creación" />
            <asp:BoundField DataField="F. Ultima Modif." HeaderText="F. Ultima Modif." 
                SortExpression="F. Ultima Modif." />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
                </td>
        </tr>
        <tr>
            <td valign="middle">
                <asp:CheckBox ID="chkAll" runat="server" AutoPostBack="True" 
                    oncheckedchanged="chkAll_CheckedChanged" 
                    Text="Seleccionar todo (página actual)" />
&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                    Text="Borrar Selección (de todas las páginas)" />
            </td>
            <td align="right">
                &nbsp;<asp:Label ID="LblCntRes" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:TextBox ID="txtRes" runat="server" Height="47px" TextMode="MultiLine" 
                    Width="100%" Font-Bold="True" ForeColor="#333333" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkModMasiva" runat="server" AutoPostBack="True" 
                    oncheckedchanged="chkCommit_CheckedChanged" Text="Modificación masiva" 
                    Enabled="False" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Asignar Tecnico/CAS:
                <asp:DropDownList ID="ddTecnicoNuevo" runat="server" DataSourceID="dsTecnicosYCAS2" 
                    DataTextField="Nombre" DataValueField="id" Enabled="False">
                </asp:DropDownList>
                &nbsp;</td>
            <td align="right" nowrap="nowrap">
                <asp:Button ID="btCommit" runat="server" Enabled="False" 
                    Text="Efectuar Cambios" Width="200px" onclick="btCommit_Click" />
            </td>
        </tr>
    </table>
                <asp:SqlDataSource ID="dsTecnicosYCAS2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                    SelectCommand="SELECT 'Seleccione una opción' 'Nombre', '-1' id UNION
                    SELECT 'Aun no asignado' 'Nombre', 0 id UNION
SELECT Nombre, id FROM tecnico order by id"></asp:SqlDataSource>
                <asp:SqlDataSource ID="dsEstado2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                    
                    SelectCommand="SELECT 'Seleccione una opción' 'Estado', '-1' 'EstadoId' UNION 
SELECT distinct v.Estado, v.EstadoId from viewOnsite v 
ORDER BY EstadoId">
                </asp:SqlDataSource>
            
    </asp:Content>


