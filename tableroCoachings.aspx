﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="tableroCoachings.aspx.vb" Inherits="tableroCoachings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server"><div>
    <telerik:RadComboBox ZIndex=10000 ID="ddLider" Runat="server" Culture="es-ES" DataSourceID="dsLider" DataTextField="apellidoNombre" DataValueField="usuarioId" Width="250px" AutoPostBack="True" Filter="Contains" HighlightTemplatedItems="True">
    </telerik:RadComboBox>
    <asp:SqlDataSource ID="dsLider" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="Select  'Todos los Lideres' as apellidonombre, '-1' as usuarioId UNION SELECT [apellidoNombre], [usuarioId] FROM [lider] order by usuarioId"></asp:SqlDataSource>
&nbsp;<telerik:RadComboBox ZIndex=10000 ID="ddOperador" Runat="server" Culture="es-ES" DataSourceID="dsOperador" DataTextField="nombrecompleto" DataValueField="usuarioId" Width="250px" AutoPostBack="True" Filter="Contains" HighlightTemplatedItems="True">
    </telerik:RadComboBox>
    <asp:SqlDataSource ID="dsOperador" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="Select  'Todos los Operadores' as nombreCompleto, '-1' as usuarioId UNION Select '[' +cast(especialistaId as varchar) +'] ' + rtrim(nombrecompleto) nombreCompleto,usuarioId from viewOperadores order by usuarioId"></asp:SqlDataSource>
    <telerik:RadComboBox ZIndex=10000 ID="ddAutor" Runat="server" Culture="es-ES" Width="250px" AutoPostBack="True" Filter="Contains" HighlightTemplatedItems="True">
        <Items>
            <telerik:RadComboBoxItem runat="server" Text="Todos" Value="todos" />
            <telerik:RadComboBoxItem runat="server" Text="Sólo Lideres" ToolTip="Sólo coachings realizados por el lider del operador" Value="lid" />
            <telerik:RadComboBoxItem runat="server" Text="Sólo Monitoreo" ToolTip="Coachings no realizados por el lider del operador" Value="mon" />
        </Items>
    </telerik:RadComboBox>
&nbsp;<telerik:RadDatePicker ID="dpDesde" Runat="server" Culture="es-AR" AutoPostBack="True">
<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"></Calendar>

<DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" LabelWidth="40%"  EmptyMessage="Desde" ToolTip="Desde" AutoPostBack="True"></DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
    </telerik:RadDatePicker>
&nbsp;<telerik:RadDatePicker ID="dpHasta" Runat="server" Culture="es-AR" AutoPostBack="True">
<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"></Calendar>

<DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" LabelWidth="40%"  EmptyMessage="Hasta" ToolTip="Hasta" AutoPostBack="True"></DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
    </telerik:RadDatePicker>
    </div>
    <div style="margin-top:10px;">
        
        <div runat="server" id="estadisticas" style=" margin-bottom:15px;margin-top:15px;background-color: #cad3d8"><table style="margin:auto; text-align:center;"><tr><td nowrap="nowrap">
        <asp:Label ID="lblCumplimiento" runat="server" Text="Cumplimiento:" Font-Bold="True"></asp:Label>
<asp:Label ID="lblValCumplimiento" runat="server" Font-Bold="True" ForeColor="#003366"></asp:Label>
&nbsp;&nbsp;&nbsp;<asp:Label ID="lblConCoaching" runat="server" Text="Operadores con monitoreo:" Font-Bold="True"></asp:Label>
            <asp:Label ID="lblValConCoaching" runat="server" Font-Bold="True" ForeColor="#003366"></asp:Label>
&nbsp;&nbsp;&nbsp;<a ID="lnkOperSinCoachings" style="color:#204886; text-decoration:underline; font-weight:bold;" href="javascript:window.radopen(null,'popupOperSinCoach'); return false;">Operadores sin monitoreo:</a>
            <input type="hidden" id="filters" runat="server" />
            <asp:Label ID="lblValSinCoachings" runat="server" Font-Bold="True" ForeColor="#003366"></asp:Label>
&nbsp;&nbsp;&nbsp;<asp:Label ID="lblPEC" runat="server" Text="Promedio PEC:" Font-Bold="True"></asp:Label>
            <asp:Label ID="lblValPEC" runat="server" Font-Bold="True" ForeColor="#003366"></asp:Label>
&nbsp;&nbsp;&nbsp;<asp:Label ID="lblPENC" runat="server" Text="Promedio PENC:" Font-Bold="True"></asp:Label>
            <asp:Label ID="lblValPENC" runat="server" Font-Bold="True" ForeColor="#003366"></asp:Label></td></tr>
        </table></div>
    
    <telerik:RadGrid ID="gridTablero" runat="server" AllowPaging="True" AllowSorting="True"  CellSpacing="0" Culture="es-ES" DataSourceID="dsTablero" GridLines="None" AutoGenerateColumns="False" >
        <ExportSettings>
            <Pdf PageWidth="" />
        </ExportSettings>
        <MasterTableView DataKeyNames="id">
<CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" ShowRefreshButton="False" ShowExportToExcelButton="True" ShowExportToPdfButton="True"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
<Columns>
<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="ver" Text="Ver Monitoreo" ImageUrl="images/coach.png" FilterControlAltText="Filter ver column" HeaderTooltip="Ver Monitoreo" UniqueName="ver">
<ItemStyle Width="30px"></ItemStyle>
</telerik:GridButtonColumn>
<telerik:GridBoundColumn DataField="id" ReadOnly="True" HeaderText="ID" SortExpression="id" UniqueName="id" DataType="System.Int32" FilterControlAltText="Filter id column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="Operador" HeaderText="Operador" SortExpression="Operador" UniqueName="Operador" FilterControlAltText="Filter Operador column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="Autor" HeaderText="Autor" SortExpression="Autor" UniqueName="Autor" FilterControlAltText="Filter Autor column" DataType="System.String" ></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="Lider" HeaderText="Lider" SortExpression="Lider" UniqueName="Lider" FilterControlAltText="Filter Lider column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="fecha" HeaderText="Fecha" SortExpression="fecha" UniqueName="fecha" DataType="System.DateTime" FilterControlAltText="Filter fecha column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="fechaDevolucion" HeaderText="Fecha Devoluci&#243;n" SortExpression="fechaDevolucion" UniqueName="fechaDevolucion" DataType="System.DateTime" FilterControlAltText="Filter fechaDevolucion column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="column1" ReadOnly="True" HeaderText="PEC (%)" SortExpression="column1" UniqueName="column1" FilterControlAltText="Filter column1 column">
<HeaderStyle Wrap="False"></HeaderStyle>
</telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="column2" ReadOnly="True" HeaderText="PENC (%)" SortExpression="column2" UniqueName="column2" DataType="System.Int32" FilterControlAltText="Filter column2 column">
<HeaderStyle Wrap="False"></HeaderStyle>
</telerik:GridBoundColumn>
<%--<telerik:GridBoundColumn DataField="ECN1" ColumnGroupName="ecn" HeaderText="" SortExpression="ECN1" ShowSortIcon="False" UniqueName="ECN1" DataType="System.Boolean" FilterControlAltText="Filter ECN1 column">
<ItemStyle Width="20px"></ItemStyle>
</telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECN2" ColumnGroupName="ecn" HeaderText="" SortExpression="ECN2" UniqueName="ECN2" DataType="System.Boolean" FilterControlAltText="Filter ECN2 column">
<ItemStyle Width="20px"></ItemStyle>
</telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECN3" ColumnGroupName="ecn" HeaderText="" SortExpression="ECN3" UniqueName="ECN3" DataType="System.Boolean" FilterControlAltText="Filter ECN3 column">
<ItemStyle Width="20px"></ItemStyle>
</telerik:GridBoundColumn>--%>
<%--<telerik:GridBoundColumn DataField="ECN4" ColumnGroupName="ecn" HeaderText="P4" SortExpression="ECN4" UniqueName="ECN4" DataType="System.Boolean" FilterControlAltText="Filter ECN4 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECN5" ColumnGroupName="ecn" HeaderText="P5" SortExpression="ECN5" UniqueName="ECN5" DataType="System.Boolean" FilterControlAltText="Filter ECN5 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU1" ColumnGroupName="ecu" HeaderText="P1" SortExpression="ECU1" UniqueName="ECU1" DataType="System.Boolean" FilterControlAltText="Filter ECU1 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU2" ColumnGroupName="ecu" HeaderText="P2" SortExpression="ECU2" UniqueName="ECU2" DataType="System.Boolean" FilterControlAltText="Filter ECU2 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU3" ColumnGroupName="ecu" HeaderText="P3" SortExpression="ECU3" UniqueName="ECU3" DataType="System.Boolean" FilterControlAltText="Filter ECU3 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU4" ColumnGroupName="ecu" HeaderText="P4" SortExpression="ECU4" UniqueName="ECU4" DataType="System.Boolean" FilterControlAltText="Filter ECU4 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU5" ColumnGroupName="ecu" HeaderText="P5" SortExpression="ECU5" UniqueName="ECU5" DataType="System.Boolean" FilterControlAltText="Filter ECU5 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU6" ColumnGroupName="ecu" HeaderText="P6" SortExpression="ECU6" UniqueName="ECU6" DataType="System.Boolean" FilterControlAltText="Filter ECU6 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU7" ColumnGroupName="ecu" HeaderText="P7" SortExpression="ECU7" UniqueName="ECU7" DataType="System.Boolean" FilterControlAltText="Filter ECU7 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC1" ColumnGroupName="enc" HeaderText="P1" SortExpression="ENC1" UniqueName="ENC1" DataType="System.Boolean" FilterControlAltText="Filter ENC1 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC2" ColumnGroupName="enc" HeaderText="P2" SortExpression="ENC2" UniqueName="ENC2" DataType="System.Boolean" FilterControlAltText="Filter ENC2 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC3" ColumnGroupName="enc" HeaderText="P3" SortExpression="ENC3" UniqueName="ENC3" DataType="System.Boolean" FilterControlAltText="Filter ENC3 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC4" ColumnGroupName="enc" HeaderText="P4" SortExpression="ENC4" UniqueName="ENC4" DataType="System.Boolean" FilterControlAltText="Filter ENC4 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC5" ColumnGroupName="enc" HeaderText="P5" SortExpression="ENC5" UniqueName="ENC5" DataType="System.Boolean" FilterControlAltText="Filter ENC5 column"></telerik:GridBoundColumn>--%>
<telerik:GridBoundColumn DataField="ECN1" ColumnGroupName="ecn" HeaderText="P1" SortExpression="ECN1" UniqueName="ECN1" DataType="System.Boolean" FilterControlAltText="Filter ECN1 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECN2" ColumnGroupName="ecn" HeaderText="P2" SortExpression="ECN2" UniqueName="ECN2" DataType="System.Boolean" FilterControlAltText="Filter ECN2 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECN3" ColumnGroupName="ecn" HeaderText="P3" SortExpression="ECN3" UniqueName="ECN3" DataType="System.Boolean" FilterControlAltText="Filter ECN3 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECN4" ColumnGroupName="ecn" HeaderText="P4" SortExpression="ECN4" UniqueName="ECN4" DataType="System.Boolean" FilterControlAltText="Filter ECN4 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECN5" ColumnGroupName="ecn" HeaderText="P5" SortExpression="ECN5" UniqueName="ECN5" DataType="System.Boolean" FilterControlAltText="Filter ECN5 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECN6" ColumnGroupName="ecn" HeaderText="P6" SortExpression="ECN6" UniqueName="ECN6" DataType="System.Boolean" FilterControlAltText="Filter ECN6 column"></telerik:GridBoundColumn>
    
    <telerik:GridBoundColumn DataField="ECU1" ColumnGroupName="ecu" HeaderText="P7" SortExpression="ECU1" UniqueName="ECU1" DataType="System.Boolean" FilterControlAltText="Filter ECU1 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU2" ColumnGroupName="ecu" HeaderText="P8" SortExpression="ECU2" UniqueName="ECU2" DataType="System.Boolean" FilterControlAltText="Filter ECU2 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU3" ColumnGroupName="ecu" HeaderText="P9" SortExpression="ECU3" UniqueName="ECU3" DataType="System.Boolean" FilterControlAltText="Filter ECU3 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ECU4" ColumnGroupName="ecu" HeaderText="P10" SortExpression="ECU4" UniqueName="ECU4" DataType="System.Boolean" FilterControlAltText="Filter ECU4 column"></telerik:GridBoundColumn>

<telerik:GridBoundColumn DataField="ENC1" ColumnGroupName="enc" HeaderText="P11" SortExpression="ENC1" UniqueName="ENC1" DataType="System.Boolean" FilterControlAltText="Filter ENC1 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC2" ColumnGroupName="enc" HeaderText="P12" SortExpression="ENC2" UniqueName="ENC2" DataType="System.Boolean" FilterControlAltText="Filter ENC2 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC3" ColumnGroupName="enc" HeaderText="P13" SortExpression="ENC3" UniqueName="ENC3" DataType="System.Boolean" FilterControlAltText="Filter ENC3 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC4" ColumnGroupName="enc" HeaderText="P14" SortExpression="ENC4" UniqueName="ENC4" DataType="System.Boolean" FilterControlAltText="Filter ENC4 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC5" ColumnGroupName="enc" HeaderText="P15" SortExpression="ENC5" UniqueName="ENC5" DataType="System.Boolean" FilterControlAltText="Filter ENC5 column"></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="ENC6" ColumnGroupName="enc" HeaderText="P16" SortExpression="ENC6" UniqueName="ENC6" DataType="System.Boolean" FilterControlAltText="Filter ENC6 column"></telerik:GridBoundColumn>

<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="devolucion" Text="Justificaci&#243;n" ImageUrl="images/comment.png" FilterControlAltText="Filter devolucion column" HeaderTooltip="Justificaci&#243;n" UniqueName="devolucion" HeaderText="devolucion">
<ItemStyle Width="30px"></ItemStyle>
</telerik:GridButtonColumn>
<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="replica" Text="R&#233;plica" ImageUrl="images/feedback.png" FilterControlAltText="Filter replica column" HeaderTooltip="Replica" UniqueName="replica" HeaderText="replica">
<ItemStyle Width="30px"></ItemStyle>
</telerik:GridButtonColumn>
</Columns>
<ColumnGroups>
<%--<telerik:GridColumnGroup HeaderText="Errores Cr&#237;ticos de Negocio" Name="ecn">--%>
<telerik:GridColumnGroup HeaderText="Usuario Final (Sin respeto incorrecto)" Name="ecn">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
</telerik:GridColumnGroup>
<%--<telerik:GridColumnGroup HeaderText="Errores Cr&#237;ticos del Usuario" Name="ecu">--%>
<telerik:GridColumnGroup HeaderText="Usuario Final (Con respeto incorrecto)" Name="ecu">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
</telerik:GridColumnGroup>
<%--<telerik:GridColumnGroup HeaderText="Errores No Cr&#237;ticos" Name="enc">--%>
<telerik:GridColumnGroup HeaderText="Habilidades Blandas" Name="enc">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
</telerik:GridColumnGroup>
    
</ColumnGroups>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>

<PagerStyle PageSizes="10;25;50;100" AlwaysVisible="True" PageSizeControlType="RadComboBox"></PagerStyle>
        </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
    </telerik:RadGrid>
        

        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Right" Height="40">
            <br />
            <telerik:RadButton runat="server" ID="btExportar" Text="Exportar a Excel" Skin="Default" CssClass="unblockUI"  Font-Bold="True"></telerik:RadButton>
        </asp:Panel>

    <telerik:RadWindowManager ID="winMgr" runat="server" Skin="Metro">
        <Windows>
            <telerik:RadWindow runat="server" ID="justificacion" Title="Justificación" Height="400px" InitialBehavior="Resize, Close, Reload" InitialBehaviors="Resize, Close, Reload" Modal="True" ReloadOnShow="True"  Width="500px" KeepInScreenBounds="true"></telerik:RadWindow>
            <telerik:RadWindow runat="server" ID="replica" Title="Replica" Height="400px" InitialBehavior="Resize, Close, Reload" InitialBehaviors="Resize, Close, Reload" Modal="True" ReloadOnShow="True"  Width="500px" KeepInScreenBounds="true"></telerik:RadWindow>
            <telerik:RadWindow runat="server" ID="monitoreo" Title="Monitoreo" Height="700px" InitialBehavior="Resize, Close, Reload" InitialBehaviors="Resize, Close, Reload" Modal="True" ReloadOnShow="True"  Width="800px" KeepInScreenBounds="true"></telerik:RadWindow>
            <telerik:RadWindow runat="server" ID="operSinCoach" Title="Operadores sin monitoreo" OpenerElementID="lnkOperSinCoachings" NavigateUrl="popupOperSinCoach.aspx" Height="700px" InitialBehavior="Resize, Close, Reload" InitialBehaviors="Resize, Close, Reload" KeepInScreenBounds="true" Modal="True" ReloadOnShow="True"  Width="800px"></telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
   <%-- <asp:SqlDataSource ID="dsTablero" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [id], usuarioId,[Operador],[Lider], [fecha], [fechaDevolucion], pec,penc,[PEC(%)] AS column1, [PENC(%)] AS column2, [ECN1], [ECN2], [ECN3], [ECN4], [ECN5], [ECU1], [ECU2], [ECU3], [ECU4], [ECU5], [ECU6], [ECU7], [ENC1], [ENC2], [ENC3], [ENC4], [ENC5], [Autor] FROM [viewTableroCoaching]"></asp:SqlDataSource>--%>
   <asp:SqlDataSource ID="dsTablero" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>"
        SelectCommand="SELECT [id], usuarioId,[Operador],[Lider], [fecha], [fechaDevolucion], pec,penc, [PEC(%)] AS column1, [PENC(%)] AS column2, [ECN1], [ECN2], [ECN3], [ECN4], [ECN5],[ECN6], [ECU1], [ECU2],  [ECU3], [ECU4], [ENC1], [ENC2], [ENC3], [ENC4], [ENC5],[ENC6],[Autor] FROM [viewTableroCoaching_Nuevo]"></asp:SqlDataSource>
                    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

