using System;
using App_Code.TicketsLib;

public partial class ClientesAlta : System.Web.UI.Page
{
    log4net.ILog logger = log4net.LogManager.GetLogger("File");
    protected void Page_Load(object sender, EventArgs e)
    {
        Usuario usr = (Usuario)Session["Usuario"];
        if (usr == null)
        {
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString);
        }
        if (!IsPostBack)
        {
            cargarLocalidades();
            panelMensaje.Visible = false;
            panelTicket.Visible = true;

            //initCliente();
        }
        


    }

    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try 
        {
            generoCliente();
        }
        catch (Exception ex) 
        {
            logger.Error("Error en Ingresar_Click: Original: " + ex.Message, ex);
        }
    }
    private void initCliente()
    {
        //recCliente rc = new recCliente();

    }
    private void generoCliente()
    {
        try
        {
            recCliente rc = getDataFromPanatalla();
            rc = Clientes.insertCliente(rc);
            lblError.Text = rc.MensajeError;
            txtCodigo.Text = rc.ClienteID;
            if (txtCodigo.Text.Trim().Length>0)
                btnSubmit.Visible = false;

        }
        catch (Exception ex)
        {
            logger.Error("Error en generoTicket: " + ex.Message, ex);
            lblError.Text = "Error en generoTicket: " + ex.Message;
        }
    }
    private recCliente getDataFromPanatalla()
    {
        recCliente rc = new recCliente();
        try
        {
            rc.EmpresaId = int.Parse(Config.getValue("EmpresaId"));
            rc.RazonSocial = txtRazonSocial.Text;
            rc.Fantasia = txtFantasia.Text;
            rc.CategoriaIVA = ddlCategoriaIva.SelectedValue;
            rc.Documento = txtDocumento.Text;
            rc.Domicilio = txtDomicilio.Text;
            rc.Localidad = txtLocalidad.Text;
            rc.Provincia = ddlProvincia.SelectedValue;
            rc.Telefono = txtTelefono.Text;
            rc.Fax = txtFax.Text;
            rc.Email = txtEmail.Text;
            rc.Observaciones = txtObservaciones.Text;
        }
        catch (Exception ex)
        {
            lblError.Text = "[Error de conversi�n] Original: " + ex.Message;
            logger.Error("Error al tomar de pantalla", ex);
            
        }
        return rc;
    }
    protected void txtTelefono_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtTelefono_TextChanged1(object sender, EventArgs e)
    {

    }

    protected void cargarPartidos()
    {
        System.Data.SqlClient.SqlConnection sql = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand query = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader rs;

        ddlPartido.Items.Clear();
        sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketDB"].ToString();
        sql.Open();
        query.Connection = sql;
        query.CommandText = "SELECT id,nombre FROM partido order by nombre";
        ddlPartido.Items.Add("TODOS");
        ddlPartido.Items[0].Value = "-1";
        ddlPartido.Items[0].Selected = true;
        rs = query.ExecuteReader();

        while (rs.Read())
            {
             ddlPartido.Items.Add(rs[1].ToString());
             ddlPartido.Items[ddlPartido.Items.Count - 1].Value = rs[0].ToString();
            }
        rs.Close();
        sql.Close();


        ddlPartido.Visible = ddlProvincia.SelectedValue == "902";
        lblPartido.Visible = ddlProvincia.SelectedValue == "902";
    }
    protected void cargarLocalidades()
        {
        System.Data.SqlClient.SqlConnection sql = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand query = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader rs;
        String condPartido = "";

        if(ddlPartido.SelectedValue != "-1") { condPartido = " AND EXISTS (Select * from partido where partido.id = '" + ddlPartido.SelectedValue + "' AND l.partidoId = partido.id )"; }
        ddlLocalidad.Items.Clear();
        sql.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketDB"].ToString();
        sql.Open();
        query.Connection = sql;
        query.CommandText = "SELECT l.id, l.nombre FROM localidad l WHERE l.provinciaId = '" + ddlProvincia.SelectedValue + "'" + condPartido + " order by l.nombre";

        rs = query.ExecuteReader();
        ddlLocalidad.Items.Add("[Seleccione una opci�n]");
        ddlLocalidad.Items[0].Value = "-1";
        ddlLocalidad.Items[0].Selected = true;

        while (rs.Read())
        {
            ddlLocalidad.Items.Add(rs[1].ToString().Split('(')[0]);
            ddlLocalidad.Items[ddlLocalidad.Items.Count - 1].Value = rs[0].ToString();
        }
        ddlLocalidad.Items.Add("[Localidad no encontrada]");
        ddlLocalidad.Items[ddlLocalidad.Items.Count - 1].Value = "FALSE";
        rs.Close();
        sql.Close();

        }

    protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        cargarPartidos();
        cargarLocalidades();
    }
    protected void ddlPartido_SelectedIndexChanged(object sender, EventArgs e)
    {
        cargarLocalidades();
    }
    protected void ddlLocalidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLocalidad.SelectedValue == "FALSE") { txtLocalidad.Visible = true; txtLocalidad.Text = ""; }
        else txtLocalidad.Visible = false;
        if (ddlLocalidad.SelectedValue != "FALSE" && ddlLocalidad.SelectedValue != "-1")
        { txtLocalidad.Text = ddlLocalidad.SelectedItem.Text; }
    }
}
