﻿Imports App_Code.TicketsLib
Imports System.Data

Partial Class vercliente

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Session("Usuario") Is Nothing) Then
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString)
        End If


        'If Request.QueryString("codcte") = "" Then Response.Redirect("Default.aspx")
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim rs
        Dim sql2 As New Data.SqlClient.SqlConnection
        Dim query2 As New Data.SqlClient.SqlCommand



        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql
        query.CommandText = "SELECT id,nombre FROM localidad WHERE provinciaId =2"

        sql2.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql2.Open()
        query2.Connection = sql2


        rs = query.ExecuteReader()
        While rs.read()
            query2.CommandText = "UPDATE localidad SET nombre='" & rs(1).ToString().Split("(")(0).Trim.Replace("'", "''") & "' where id=" & rs(0).ToString()
            query2.ExecuteNonQuery()
        End While
        rs.Close()
        sql.Close()
        sql2.Close()


        ddlPartido.Visible = ddlProvincia.SelectedValue = "902"
        lblPartido.Visible = ddlProvincia.SelectedValue = "902"

    End Sub

    Private Function SelectInCB(ByVal Cadena As String, ByVal CB As DropDownList) As Boolean
        Dim ret As Boolean = False
        Dim objItem As ListItem
        For Each objItem In CB.Items
            If objItem.Value = Cadena Then
                objItem.Selected = True
                ret = True
            Else : objItem.Selected = False
            End If
        Next
        Return ret
    End Function

    Protected Sub cargarPartidos()
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim rs

        ddlPartido.Items.Clear()
        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql
        query.CommandText = "SELECT id,nombre FROM partido order by nombre"
        ddlPartido.Items.Add("TODOS")
        ddlPartido.Items(0).Value = -1
        rs = query.ExecuteReader()
        While rs.read()
            ddlPartido.Items.Add(rs(1))
            ddlPartido.Items(ddlPartido.Items.Count() - 1).Value = rs(0)
        End While
        rs.Close()
        sql.Close()


        ddlPartido.Visible = ddlProvincia.SelectedValue = "902"
        lblPartido.Visible = ddlProvincia.SelectedValue = "902"
    End Sub

    Protected Sub seleccionarPartido()
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim rs
        Dim localidadActual = ddlLocalidad.SelectedValue.ToString()
        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql
        query.CommandText = "SELECT partido.id FROM partido right join localidad on localidad.partidoId=partido.id Where localidad.id = " & localidadActual
        rs = query.ExecuteReader()
        rs.read()
        If Not IsDBNull(rs(0)) Then ddlPartido.SelectedValue = rs(0)
        rs.Close()
        sql.Close()
        cargarLocalidades()
        ddlLocalidad.SelectedValue = localidadActual
    End Sub

    Protected Sub cargarLocalidades()
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim rs As Data.SqlClient.SqlDataReader
        Dim condPartido As String = ""

        If ddlPartido.SelectedValue <> "-1" Then condPartido = " AND EXISTS (Select * from partido where partido.id = '" & ddlPartido.SelectedValue & "' AND l.partidoId = partido.id )"
        ddlLocalidad.Items.Clear()
        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql
        query.CommandText = "SELECT l.id, l.nombre FROM localidad l WHERE l.provinciaId = '" & ddlProvincia.SelectedValue & "'" & condPartido & " order by l.nombre"

        rs = query.ExecuteReader()
        ddlLocalidad.Items.Add("Seleccione una opción")
        ddlLocalidad.Items(0).Value = -1
        While rs.read()
            ddlLocalidad.Items.Add(rs(1).ToString.Split("(")(0))
            ddlLocalidad.Items(ddlLocalidad.Items.Count() - 1).Value = rs(0)
        End While
        ddlLocalidad.Items.Add("LOCALIDAD NO ENCONTRADA")
        ddlLocalidad.Items(ddlLocalidad.Items.Count() - 1).Value = "FALSE"

        rs.Close()
        sql.Close()
    End Sub


    Protected Sub btnactualiza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnactualiza.Click
        If ddlLocalidad.SelectedValue <> "FALSE" Then
            If ddlLocalidad.SelectedValue <> "-1" Then txtLocalidad.Text = ddlLocalidad.SelectedItem.Text
        End If


        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim datoscli As Long

        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql

        query.CommandText = "UPDATE ViewCliente " & _
                                    " SET RazonSocial ='" & txtRazonSocial.Text & "' , " & _
                                    " NombreFantasia = '" & txtFantasia.Text & "' , Domicilio = '" & txtDomicilio.Text & "', Localidad = '" & txtLocalidad.Text & "'," & _
                                    " provinciaid = " & ddlProvincia.SelectedValue & ", Zip = '" & txtCodPostal.Text & "' , Telefono1 = '" & txtTelefono.Text & "' , " & _
                                    " Telefono2 = '" & txtcel.Text & "' , telefono3 = '" & txtEmail.Text & "', observaciones = '" & txtObservaciones.Text & "' , documento = '" & txtDocumento.Text & "' " & _
                                    " WHERE clienteid = " & txtCodigo.Text & ""

        datoscli = query.ExecuteNonQuery()
        If datoscli <= 0 Then
            ' hay error}
            lblError.Text = "No se pudo modificar el cliente "

        Else
            ' datos cli = cantidad de registros

            lblError.ForeColor = Drawing.Color.Green


            lblError.Text = "Se actualizo correctamente el cliente "

        End If

        sql.Close()
    End Sub

    Protected Sub ddlProvincia_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProvincia.DataBound
        Dim sql As New Data.SqlClient.SqlConnection
        Dim query As New Data.SqlClient.SqlCommand
        Dim datoscli As IDataReader
        ddlCategoriaIva.Enabled = False

        Dim Clienteid As String

        Clienteid = Request.QueryString("codcte")

        txtCodigo.Text = Request.QueryString("codcte").ToString


        sql.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDB").ToString
        sql.Open()
        query.Connection = sql

        query.CommandText = "SELECT Top 1 CategoriaIvaId,RazonSocial,NombreFantasia, Domicilio, Localidad, provinciaid," & _
                            " Zip, Telefono1, Telefono2, telefono3, observaciones,documento " & _
                            " FROM ViewCliente(nolock) " & _
                            " WHERE(FechaBaja Is NULL And clienteid = " & Clienteid & ")"


        datoscli = query.ExecuteReader
        datoscli.Read()


        If Not datoscli.IsDBNull(1) Then txtRazonSocial.Text = datoscli(1)
        If Not datoscli.IsDBNull(2) Then txtFantasia.Text = datoscli(2)
        If Not datoscli.IsDBNull(3) Then txtDomicilio.Text = datoscli(3)
        If Not datoscli.IsDBNull(4) Then txtLocalidad.Text = datoscli(4)
        If Not datoscli.IsDBNull(6) Then txtCodPostal.Text = datoscli(6)
        If Not datoscli.IsDBNull(7) Then txtTelefono.Text = datoscli(7)
        If Not datoscli.IsDBNull(8) Then txtcel.Text = datoscli(8)
        If Not datoscli.IsDBNull(9) Then txtEmail.Text = datoscli(9)
        If Not datoscli.IsDBNull(10) Then txtObservaciones.Text = datoscli(10)
        If Not datoscli.IsDBNull(11) Then txtDocumento.Text = datoscli(11)
        If Not datoscli.IsDBNull(5) Then SelectInCB(datoscli(5), ddlProvincia) 'ddlProvincia.SelectedValue = datoscli(5)
        ''If Not datoscli.IsDBNull(0) Then ddlCategoriaIva.SelectedValue = datoscli(0)
        cargarPartidos()
        cargarLocalidades()
        datoscli.Close()
        sql.Close()
    End Sub

    Protected Sub ddlPartido_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPartido.SelectedIndexChanged
        cargarLocalidades()
    End Sub

    Protected Sub ddlProvincia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProvincia.SelectedIndexChanged
        cargarPartidos()
        cargarLocalidades()
    End Sub

    Protected Sub ddlLocalidad_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocalidad.SelectedIndexChanged
        If ddlLocalidad.SelectedValue = "FALSE" Then
            txtLocalidad.ReadOnly = False
            txtLocalidad.BackColor = Drawing.Color.White
            SetFocus(txtLocalidad)
            txtLocalidad.Text = ""
        Else
            txtLocalidad.ReadOnly = True
            txtLocalidad.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC4")
            If ddlProvincia.SelectedValue = "902" Then seleccionarPartido()
        End If

    End Sub

End Class
