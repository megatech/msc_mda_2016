﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="vercliente.aspx.vb" Inherits="vercliente" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 35%;
        }
        .style2
        {
            width: 15%;
        }
        .style3
        {
            width: 10%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <div style="text-align: center">
            <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 100%; border-bottom: black 1px solid">
                <tr>
                    <td align="center">
                            <table cellpadding="2" cellspacing="0" style="width: 100%" border="0" >
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Código</td>
                                    <td align="left" class="style1">
                                        <asp:TextBox ID="txtCodigo" runat="server" ReadOnly="true" CssClass="TextoTextBoxReadonly"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Razón Social</td>
                                    <td align="left" class="style1">
                                        <asp:TextBox ID="txtRazonSocial" runat="server" Width="74%" MaxLength="60"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvRazonSocial" runat="server" ControlToValidate="txtRazonSocial" ErrorMessage="Debe ingresar la Razón Social">*</asp:RequiredFieldValidator>
                       <td></td><td> <asp:Label ID="lblError" runat="server" CssClass="labelError"></asp:Label></td>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Fantasía</td>
                                    <td align="left" class="style1">
                                        <asp:TextBox ID="txtFantasia" runat="server" Width="74%" MaxLength="40"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Categoría IVA</td>
                                    <td align="left" class="style1">
                                        <asp:DropDownList ID="ddlCategoriaIva" runat="server" 
                                        DataSourceID="odsCategoriaIva" DataTextField="Nombre" DataValueField="Codigo">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsCategoriaIva" Runat="server" 
                                        TypeName="App_Code.TicketsLib.TablasCombo"
                                        SelectMethod="getComboCategoriaIVA" />
                                        
                                        <td align="right" style="width: 5%">
                                        Documento</td>
                                    <td align="left" style="width: 21%">
                                        <asp:TextBox ID="txtDocumento" runat="server" MaxLength="15" Width="169px"></asp:TextBox></td>

                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" >
                                <tr>
                                    <td align="right" class="style3">
                                        Provincia</td>
                                    <td align="left" class="style2">
                                        <asp:DropDownList ID="ddlProvincia" runat="server" 
                                        DataSourceID="odsProvincia" DataTextField="Nombre" DataValueField="Codigo" 
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsProvincia" Runat="server" 
                                        TypeName="App_Code.TicketsLib.TablasCombo"
                                        SelectMethod="getComboProvincia" />
                                    </td>
                                    <td align="right" width="15%">
                                        <asp:Label ID="lblPartido" runat="server" Text="Partido"></asp:Label>
                                    </td>
                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlPartido" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="style3">
                                        Localidad</td>
                                    <td align="left" class="style2">
                                        <asp:TextBox ID="txtLocalidad" runat="server" Width="60%" MaxLength="40" 
                                            ReadOnly="True" BackColor="#FFFFCC"></asp:TextBox></td>
                                    <td align="right">
                                        Nueva Localidad</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLocalidad" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>   
                                <tr>
                                    <td align="right" class="style3">
                                        Domicilio</td>
                                    <td align="left" class="style2">
                                        <asp:TextBox ID="txtDomicilio" runat="server" Width="60%" MaxLength="40"></asp:TextBox></td>
                                    <td align="right" width="15%">
                                        Cod. Postal</td>
                                    <td align="left" width="25%">
                                        <asp:TextBox ID="txtCodPostal" runat="server" MaxLength="8"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right" class="style3">
                                        Teléfono</td>
                                    <td align="left" class="style2">
                                        <asp:TextBox ID="txtTelefono" runat="server" MaxLength="30" 
                                            Width="70%"                                
                                            ></asp:TextBox>
                                           
                                            </td>
                                    <td align="right" width="15%">
                                        Celular</td>
                                    <td align="left" width="25%">
                                        <asp:TextBox ID="txtcel" runat="server" MaxLength="30" Width="70%"></asp:TextBox></td>

                                </tr>  
                                <tr>
                                    <td align="right" class="style3">
                                        e-Mail</td>
                                    <td align="left"colspan="3">
                                        <asp:TextBox ID="txtEmail" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                                        
                                        </td>
                                </tr>                                    
                                <tr>
                                    <td align="right" class="style3">
                                        Observaciones</td>
                                    <td align="left"colspan="3">
                                        <asp:TextBox ID="txtObservaciones" runat="server" Width="80%" MaxLength="100" Rows="5" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>                                                                                              
                            </table>
                    </td>
                </tr>
                 <tr>
                 <td>
                 
                     <asp:Button ID="btnactualiza" runat="server" Text="Actualizar" />
                 
                 </td>
                 </tr>
            
              
                 <tr>
                    <td align="left" >
                        <asp:ValidationSummary ID="validSummary" runat="server" />
                    </td>
                        
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelMensaje" runat="server">
        <table>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblMensajePanel" runat="server" CssClass="labelError"></asp:Label>
                </td>        
            </tr>
        </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
