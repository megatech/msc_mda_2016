<%@ Page Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="ClientesAlta.aspx.cs" Inherits="ClientesAlta" %>

<asp:Content ID="TicketPage" ContentPlaceHolderID="cphTicket" runat="server">
    <asp:Panel ID="panelTicket" runat="server">
        <div style="text-align: center">
            <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 100%; border-bottom: black 1px solid">
                <tr>
                    <td align="center">
                            <table cellpadding="2" cellspacing="0" style="width: 100%" border="0" >
                                <tr>
                                    <td align="right" style="width: 5%">
                                        C�digo</td>
                                    <td align="left" style="width: 44%">
                                        <asp:TextBox ID="txtCodigo" readonly="true" runat="server" CssClass="TextoTextBoxReadonly"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Raz�n Social</td>
                                    <td align="left" style="width: 44%; font-size: medium;">
                                        <asp:TextBox ID="txtRazonSocial" runat="server" Width="74%" MaxLength="60"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvRazonSocial" runat="server" 
                                            ControlToValidate="txtRazonSocial" ErrorMessage="Debe ingresar la Raz�n Social" 
                                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Fantas�a</td>
                                    <td align="left" style="width: 44%">
                                        <asp:TextBox ID="txtFantasia" runat="server" Width="74%" MaxLength="40"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        Categor�a IVA</td>
                                    <td align="left" style="width: 44%">
                                        <asp:DropDownList ID="ddlCategoriaIva" runat="server" 
                                        DataSourceID="odsCategoriaIva" DataTextField="Nombre" DataValueField="Codigo">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsCategoriaIva" Runat="server" 
                                        TypeName="App_Code.TicketsLib.TablasCombo"
                                        SelectMethod="getComboCategoriaIVA" />
                                        
                                        <td align="right" style="width: 5%">
                                        Documento</td>
                                    <td align="left" style="width: 21%">
                                        <asp:TextBox ID="txtDocumento" runat="server" MaxLength="15" Width="169px"></asp:TextBox></td>

                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" >
                                <tr>
                                    <td align="right" width="15%">
                                        Domicilio</td>
                                    <td align="left"colspan="6">
                                        <asp:TextBox ID="txtDomicilio" runat="server" Width="60%" MaxLength="40"></asp:TextBox></td>
                                </tr>                             
                                <tr>
                                    <td align="right" width="15%">
                                        Provincia</td>
                                    <td align="left" colspan="2">
                                        <asp:DropDownList ID="ddlProvincia" runat="server" AutoPostBack="True" 
                                            DataSourceID="odsProvincia" DataTextField="Nombre" DataValueField="Codigo" 
                                            onselectedindexchanged="ddlProvincia_SelectedIndexChanged" Width="200px">
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="odsProvincia" Runat="server" 
                                            SelectMethod="getComboProvincia" TypeName="App_Code.TicketsLib.TablasCombo" />
                                    </td>
                                    <td align="right" colspan="2">
                                        <asp:Label ID="lblPartido" runat="server" Text="Partido" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:DropDownList ID="ddlPartido" runat="server" AutoPostBack="True" 
                                            onselectedindexchanged="ddlPartido_SelectedIndexChanged" Visible="False">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="15%">
                                        Localidad</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLocalidad" runat="server" AutoPostBack="True" 
                                            Height="22px" onselectedindexchanged="ddlLocalidad_SelectedIndexChanged" 
                                            Width="250px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtLocalidad" runat="server" MaxLength="40" Visible="False"></asp:TextBox>
                                    </td>
                                    <td align="left" colspan="2">
                                        Cod. Postal</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCodPostal" runat="server" MaxLength="8"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr>
                                    <td align="right">
                                        Tel�fono</td>
                                    <td align="left" colspan="2" style="width: 33%">
                                        <asp:TextBox ID="txtTelefono" runat="server" MaxLength="30" 
                                            ontextchanged="txtTelefono_TextChanged1" Width="70%"></asp:TextBox>
                                    </td>
                                    <td align="right" colspan="2" style="width: 5%">
                                        Fax</td>
                                    <td align="left" width="25%" colspan="2">
                                        <asp:TextBox ID="txtFax" runat="server" MaxLength="30" Width="70%"></asp:TextBox>
                                    </td>
                                </tr>  
                                <tr>
                                    <td align="right" width="15%">
                                        e-Mail</td>
                                    <td align="left"colspan="6">
                                        <asp:TextBox ID="txtEmail" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                                        
                                        </td>
                                </tr>                                    
                                <tr>
                                    <td align="right" width="15%">
                                        Observaciones</td>
                                    <td align="left"colspan="6">
                                        <asp:TextBox ID="txtObservaciones" runat="server" Width="80%" MaxLength="100" Rows="5" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>                                                                                              
                            </table>
                    </td>
                </tr>
                 
            
                <tr>
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Ingresar" /></td>
                </tr>
                 <tr>
                    <td align="left" >
                        <asp:Label ID="lblError" runat="server" CssClass="labelError"></asp:Label>
                        <asp:ValidationSummary ID="validSummary" runat="server" />
                    </td>
                        
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelMensaje" runat="server">
        <table>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblMensajePanel" runat="server" CssClass="labelError"></asp:Label>
                </td>        
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
