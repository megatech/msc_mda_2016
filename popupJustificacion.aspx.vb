﻿
Partial Class popupJustificacion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim id = Request.QueryString("id")
        Dim justifiacion = DAOs.getScalar("SELECT justificacion from coachingNuevo where id=" & id)
        If justifiacion.Trim = "" Then
            lblJustificacion.Text = "No se ha cargado ninguna justificación."
        Else
            lblJustificacion.Text = justifiacion.trim
        End If
    End Sub
End Class
