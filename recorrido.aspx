﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="recorrido.aspx.vb" Inherits="recorrido" MasterPageFile="TicketsPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" runat="server">
    

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

        
    
    <div>
    <h2 align="center" >Agenda de Tickets</h2>
    <table width="100%"><tr>
                    <td align="center">
                        <asp:Label ID="lblCount" runat="server" Font-Bold="True" ForeColor="#313D59"></asp:Label>
                    </td>
                </tr></table>
        
        <%--<asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataKeyNames="ticketid" DataSourceID="SqlDataSource1" 
            ForeColor="#333333" GridLines="None" 
            AllowSorting="True" EmptyDataText="No existen datos para mostrar" >
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

        <Columns>
               <asp:BoundField DataField="ticketid" HeaderText="ticketid" InsertVisible="False" ReadOnly="True" SortExpression="ticketid" />
                <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" ReadOnly="True" />
                <asp:BoundField DataField="Fecha Fin" HeaderText="Fecha Fin" 
                    SortExpression="Fecha Fin" ReadOnly="True" />
               
                <asp:BoundField DataField="Fecha Asignacion" HeaderText="Fecha Asignacion" 
                    SortExpression="Fecha Asignacion" ReadOnly="True" />
               
                <asp:BoundField DataField="Horas habiles" HeaderText="Horas habiles" 
                    SortExpression="Horas habiles" ReadOnly="True" />
                <asp:BoundField DataField="Tipo Cliente" HeaderText="Tipo Cliente" 
                    SortExpression="Tipo Cliente" />
                <asp:BoundField DataField="clainnro" HeaderText="clainnro" SortExpression="clainnro" />
                <asp:BoundField DataField="Telefono" HeaderText="Telefono" 
                    SortExpression="Telefono" />
                <asp:BoundField DataField="Estado" HeaderText="Estado" 
                    SortExpression="Estado" />
                <asp:BoundField DataField="RazonSocial" HeaderText="RazonSocial" 
                    SortExpression="RazonSocial" />
                
               
               <asp:BoundField DataField="localidad" HeaderText="localidad" SortExpression="localidad" />
               <asp:BoundField DataField="domicilio" HeaderText="domicilio" SortExpression="domicilio" />
               <asp:BoundField DataField="Negocio" HeaderText="Negocio" SortExpression="Negocio" />
                
               
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>--%>
       
        <div style="text-align:center">
            <asp:Button ID="Actualizar" runat="server" OnClick="Actualizar_Click" Text="Actualizar"/>
        </div>
        <div>
        <telerik:RadGrid id="gv" runat="server" DataSourceID="SqlDataSource1" Culture="es-ES" ShowGroupPanel="True" Skin="Silk" style="padding-top:4px">
            <ClientSettings AllowDragToGroup="True" >
            </ClientSettings>
            <MasterTableView DataSourceID="SqlDataSource1">
               <Columns>
                   <telerik:GridTemplateColumn 
                        UniqueName="TemplateLinkColumn" 
                        AllowFiltering="false" 
                        HeaderText="Ver Ticket">
                            <ItemTemplate>
                                <asp:HyperLink ID="Link" runat="server" ImageUrl ="~/Images/Seleccion.jpg" Target="_blank"  NavigateUrl='<%# Eval("ticketid", "~/EditaTicket.aspx?id={0}")%>'></asp:HyperLink>
                            </ItemTemplate>
                   </telerik:GridTemplateColumn>
               </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="Select TK.ticketid ,convert (char(20),TK.fecha,120) 'Fecha', convert (char(20),tk.FechaFin,120) 'Fecha Fin', Equipo.Nombre as 'Tipo Cliente',tk.clainnro, TK.NroSerie as 'Telefono',Estado.Nombre as 'Estado',VC.RazonSocial, vc.localidad, vc.domicilio, tec.Nombre 'Tecnico' ,tkc.fechaCoordinada,tkc.fechaSolucion,tkc.fechaCarga, DATENAME(dw,tkc.fechaCoordinada) as DiaSemana from Ticket TK Left join Cliente_tk on  Cliente_tk.clienteid = TK.clienteid INNER JOIN Equipo (nolock) ON TK.ArticuloId = Equipo.EquipoId INNER JOIN TipoProblema (nolock) ON TK.IdTipoProblema = TipoProblema.IdTipoProblema INNER JOIN EmpresaPartner (nolock) ON TK.EmpresaPartnerId = EmpresaPartner.EmpresaPartnerId INNER JOIN Estado (nolock) ON TK.EstadoId = Estado.EstadoId INNER JOIN ViewCliente VC (nolock) ON TK.ClienteId = VC.ClienteId   INNER JOIN UbicacionContacto (nolock) ON TK.UbicacionContactoId = UbicacionContacto.UbicacionContactoId INNER JOIN Ubicacion (nolock) ON UbicacionContacto.UbicacionId = Ubicacion.UbicacionId INNER JOIN Especialista (nolock) ON TK.EspecialistaId = Especialista.EspecialistaId left JOIN tickettecnico TKC on tkc.ticketid = tk.ticketid left JOIN tecnico tec on tec.id = tkc.tecnicoId Where tk.estadoid in (135,139,140,123,118,119,155,162) and TK.EmpresaPartnerId in (54,55,56,57,58) and tk.Fecha > DATEADD(month, -12, getdate())">
        </asp:SqlDataSource>
        </div>
    <div id="Div1" runat="server">
        <telerik:RadButton ID="exportar" runat="server" Text="Exportar Xls" CssClass="unblockUI"></telerik:RadButton>
    </div>
    </div>
    

</asp:Content>