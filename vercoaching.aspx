﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="verCoaching.aspx.vb" Inherits="verCoaching2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">

<table align="center"><tr><td align="right" style="font-weight: bold">Lider:</td><td>
    <telerik:RadComboBox ID="ddLider" Runat="server"  Culture="es-ES" DataSourceID="dsLideres" DataTextField="nombre" DataValueField="usuarioId" AutoPostBack="True">
    </telerik:RadComboBox></td></tr></table>
    
    <asp:SqlDataSource ID="dsLideres" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="Select * from Lider"></asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" 
        GridLines="None" AllowPaging="True" AllowSorting="True" 
        HorizontalAlign="Center" PageSize="15">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="UID" HeaderText="UID" InsertVisible="False" 
                ReadOnly="True" SortExpression="UID" />
            <asp:BoundField DataField="Operador" HeaderText="Operador" 
                SortExpression="Operador" >
            <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:ButtonField ButtonType="Button" Text="Ver" CommandName="ver" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" SelectCommand="select u.usuarioid As UID, u.nombreCompleto as Operador FROM usuario u WHERE u.fechaBaja IS NULL AND (RTRIM(u.observaciones) IN (SELECT RTRIM(observaciones) FROM usuario where usuarioId = @liderUID)) and u.perfilId in (26,29,28,32) ORDER BY Operador" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddLider" Name="liderUID" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

