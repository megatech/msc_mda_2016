<%@ Page Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="BusquedaTicket.aspx.cs" Inherits="BusquedaTicket" %>

<asp:Content ID="TicketPage" ContentPlaceHolderID="cphTicket" runat="server">
     <%--<script src="js/jquery-1.8.2.js" type="text/javascript"></script>--%>
        <div style="text-align: center">
            <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 85%; border-bottom: black 1px solid">
                <tr>
                    <td align="center">
                            <table  cellpadding="2" cellspacing="0" style="width: 100%" border="0" bordercolor="#ffffff" bordercolordark="#ffffff" bordercolorlight="#ffffff"  >
                                <tr>
                                    <td width="15%" align="right">Fecha Creaci�n </td>
                                    <td width="20%" align="left" nowrap="nowrap">
                                        <asp:TextBox ID="txtFechaInicio" runat="server" Width="10px" Visible="False"></asp:TextBox>
                                        <telerik:RadDatePicker ID="rdpDesde" Runat="server" Culture="es-AR" 
                                            onselecteddatechanged="rdpDesde_SelectedDateChanged" Width="120px" 
                                            onload="rdpDesde_Load">
<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
    <FastNavigationSettings CancelButtonCaption="Cancelar" 
        EnableTodayButtonSelection="True" TodayButtonCaption="Hoy">
    </FastNavigationSettings>
                                            </Calendar>

<DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" EnableSingleInputRendering="True" LabelWidth="64px"></DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                        </telerik:RadDatePicker> Hasta
                                        <asp:TextBox ID="txtFechaFin" runat="server" Width="10px" Visible="False"></asp:TextBox>
                                        <telerik:RadDatePicker ID="rdpHasta" Runat="server" 
                                            onselecteddatechanged="rdpHasta_SelectedDateChanged" Width="120px" 
                                            onload="rdpHasta_Load" Culture="es-AR">
<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
    <FastNavigationSettings CancelButtonCaption="Cancelar" 
        EnableTodayButtonSelection="True" TodayButtonCaption="Hoy">
    </FastNavigationSettings>
                                            </Calendar>

<DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" EnableSingleInputRendering="True" LabelWidth="64px"></DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                        </telerik:RadDatePicker>
                                        <asp:CompareValidator ID="comVFechaFin" runat="server" ErrorMessage="Fecha Fin debe ser mayor a Inicio." 
                                        ControlToValidate="txtFechaFin" ControlToCompare="txtFechaInicio" Operator="GreaterThanEqual">*</asp:CompareValidator>                                         
                                    </td>
                                    <td width="5%" align="right">ANI</td>
                                    <td align="left" style="width: 19%">
                                        <asp:TextBox ID="txtNroClaim" runat="server" AutoPostBack="True" OnTextChanged="txtNroClaim_TextChanged" ></asp:TextBox>
                                    </td>
                                    <td align="right" nowrap="nowrap">Ticket
                                        <asp:TextBox ID="txtTicket" runat="server"></asp:TextBox>&nbsp;
                                    </td>
                                </tr>

                                <tr>
                                    <td width="10%" align="right">Negocio</td>
                                    <td width="40%" align="left">
                                        <asp:TextBox ID="txtNegocio" runat="server" Width="10px" Visible="False"></asp:TextBox>                                                
                                        <telerik:RadComboBox ZIndex=10000  ID="rcNegocio" 
                                            Runat="server" DataSourceID="dsNegocio"  
                                            DataTextField="Nombre" DataValueField="Nombre" 
                                            onselectedindexchanged="rcNegocio_SelectedIndexChanged" OnDataBound="rcNegocio_DataBound" NoWrap="True" 
                                            Width="220px" Filter="Contains" HighlightTemplatedItems="True">
                                        </telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsNegocio" runat="server"
                                      ConnectionString="<%$ ConnectionStrings:TicketDB %>"
                                            SelectCommand="SELECT [EmpresaPartnerId], [Nombre] FROM [empresapartner] WHERE ([FechaBaja] IS NULL) ORDER BY Nombre">
                                       </asp:SqlDataSource>
                                    </td>
                                    <td width="10%" align="right">Cliente</td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtCodCliente" runat="server" Width="25%" ></asp:TextBox>&nbsp;
                                        <asp:ImageButton ID="imgBuscar" runat="server" ImageUrl="~/Images/search.png" OnClientClick="AbrirPopup('buscarCliente.aspx','buscarCliente'); return false;"/> &nbsp;

                                        <asp:TextBox ID="txtNomCliente" runat="server" Width="60%" ></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    
                                    <td width="10%" align="right">Equipo</td>
                                    <td width="40%" align="left" style="width: 0%">
                                        <telerik:RadComboBox ZIndex=10000 ID="ddEquipo" 
                                            Runat="server" 
                                            NoWrap="True" 
                                            Width="230px" DataSourceID="dsEquipo" DataTextField="Nombre" 
                                            DataValueField="Codigo" Filter="Contains" HighlightTemplatedItems="True">
                                        </telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsEquipo" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                                            SelectCommand="SELECT '-1' As Codigo, 'Seleccione un valor' As Nombre UNION 
SELECT [Codigo], [Nombre] FROM [equipo] WHERE ([FechaBaja] IS NULL)">
                                        </asp:SqlDataSource>
                                    </td>

                                    <td width="40%" align="right" style="width: 20%">
                                        Nro. Serie</td>

                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtNroSerie" runat="server" Width="210px"></asp:TextBox>                                                
                                    </td>
                                </tr>

                                <tr>
                                    
                                    <td width="10%" align="right">Lider</td>
                                    <td width="40%" align="left" style="width: 0%">
                                        <telerik:RadComboBox ZIndex=10000 ID="rcLider" AutoPostBack=true 
                                            Runat="server" DataSourceID="dsLider" 
                                            DataTextField="Nombre" DataValueField="liderId" 
                                            NoWrap="True" 
                                            Width="260px" ondatabound="rcLider_DataBound" 
                                            onselectedindexchanged="rcLider_SelectedIndexChanged" Filter="Contains" 
                                            HighlightTemplatedItems="True">
                                        </telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsLider" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:TicketDB %>"                                           
                                            
                                            SelectCommand="Select nombre , liderid FROM lider">
                                        </asp:SqlDataSource>
                                    </td>
                                    <td width="40%" align="right" style="width: 20%">
                                        Estado</td>
                                    <td align="left" colspan="2">
                                        
                                        <telerik:RadComboBox ZIndex=10000 OnDataBound ="txtestado_DataBound" ID="txtEstado" 
                                            Runat="server" DataSourceID="DSEstados" 
                                            DataTextField="Nombre" DataValueField="Nombre" NoWrap="True" Width="220px" 
                                            Filter="Contains" HighlightTemplatedItems="True">
                                        </telerik:RadComboBox>

                                        <asp:SqlDataSource ID="DSEstados" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                                            SelectCommand="select '[' + CAST(EstadoId as varchar) + '] '+ Estado.Nombre As CodNombre, Estado.Nombre from Estado  ORDER BY Estado.Nombre "></asp:SqlDataSource>

                                    </td>
                                    

                                </tr>

                                <tr>
                                    
                                    <td width="10%" align="right">Especialista</td>
                                    <td width="40%" align="left" style="width: 0%">
                                        <asp:TextBox ID="txtEspecialista" runat="server" Width="10px" Visible="False" ></asp:TextBox>
                                        <telerik:RadComboBox ZIndex=10000 OnDataBound="rcEspecialista_DataBound" ID="rcEspecialista" 
                                            Runat="server" DataSourceID="dsEspecialista" 
                                            DataTextField="Nombre" DataValueField="soloid" 
                                            onselectedindexchanged="rcEspecialista_SelectedIndexChanged" NoWrap="True" 
                                            Width="260px" Filter="Contains" HighlightTemplatedItems="True">
                                        </telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsEspecialista" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:TicketDB %>"                                           
                                            SelectCommand="Select especialistaId as soloid, '['+CAST(especialistaid AS VARCHAR)+'] '+nombre as nombre FROM especialista WHERE fechabaja is null order by soloid">
                                        </asp:SqlDataSource>
                                    </td>
                                    
                                    <td width="40%" align="right" style="width: 20%">
                                        T�cnico/CAS</td>
                                    
                                    <td width="40%" align="left" colspan="2">
                                        
                                        <telerik:RadComboBox ZIndex=10000 OnDataBound ="ddTecnico_DataBound" ID="ddTecnico" 
                                            Runat="server" DataSourceID="DSTecnico" NoWrap="True" Width="220px" 
                                            DataTextField="Nombre" DataValueField="id" Filter="Contains" 
                                            HighlightTemplatedItems="True">
                                        </telerik:RadComboBox>

                                        <asp:SqlDataSource ID="DSTecnico" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                                            SelectCommand="SELECT [id], [Nombre] FROM [tecnico] WHERE fechabaja IS NULL
Order By AreaId,Nombre" 
                                            ProviderName="<%$ ConnectionStrings:TicketDB.ProviderName %>" 
                                            ></asp:SqlDataSource>
                                    </td>
                                    

                                </tr>     

                                <tr>
                                    
                                    <td width="10%" align="right" rowspan="2">T.Problema</td>
                                    <td width="40%" align="left" rowspan="2" style="width: 0%">
                                        
                                        <telerik:RadComboBox ZIndex=10000 ID="txtProblema" Runat="server" 
                                            DataSourceID="DStipoproblema" DataTextField="Nombre" 
                                            DataValueField="idTipoProblema" OnDataBound ="txtProblema_DataBound" NoWrap="True" 
                                            Width="230px" Filter="Contains" HighlightTemplatedItems="True">
                                        </telerik:RadComboBox>
                                    
                                        <asp:SqlDataSource ID="DStipoproblema" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                                            SelectCommand="SELECT idTipoProblema,[Nombre] FROM [tipoproblema] WHERE fechabaja is null Order by Nombre"></asp:SqlDataSource>
                                    
                                    </td>
                                    
                                    <td width="40%" align="right" style="width: 20%">
                                        
                                        Fecha/Hora
                                        <br />
                                        Re-Contacto</td>
                                    
                                    <td align="left" colspan="2" style="width: 80%">
                                        <table>
                                            <tr>
                                                <td align="left">
                                                    <telerik:RadDatePicker ID="dpFechaRecontactar" runat="server" Culture="es-AR">
<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True" Culture="es-ES" FastNavigationNextText="&amp;lt;&amp;lt;">
    <FastNavigationSettings CancelButtonCaption="Cancelar" EnableTodayButtonSelection="True" TodayButtonCaption="Hoy">
    </FastNavigationSettings>
                                                        </Calendar>

<DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" LabelWidth="40%">
<EmptyMessageStyle Resize="None"></EmptyMessageStyle>

<ReadOnlyStyle Resize="None"></ReadOnlyStyle>

<FocusedStyle Resize="None"></FocusedStyle>

<DisabledStyle Resize="None"></DisabledStyle>

<InvalidStyle Resize="None"></InvalidStyle>

<HoveredStyle Resize="None"></HoveredStyle>

<EnabledStyle Resize="None"></EnabledStyle>
</DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td align="left">
                                                    <telerik:RadTimePicker ID="tpHoraRecontactar" runat="server" SkipMinMaxDateValidationOnServer="True" Culture="es-AR" Width="80px">
<TimeView CellSpacing="-1" Culture="es-AR" TimeFormat="HH:mm" Interval="00:30:00" StartTime="08:00:00" EndTime="23:00:00" RenderDirection="Vertical">
                                            </TimeView>
                                            <TimePopupButton HoverImageUrl="" ImageUrl="" />

<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True" Culture="es-ES" FastNavigationNextText="&amp;lt;&amp;lt;"></Calendar>

<DateInput DisplayDateFormat="HH:mm" DateFormat="HH:mm" LabelWidth="40%">
<EmptyMessageStyle Resize="None"></EmptyMessageStyle>

<ReadOnlyStyle Resize="None"></ReadOnlyStyle>

<FocusedStyle Resize="None"></FocusedStyle>

<DisabledStyle Resize="None"></DisabledStyle>

<InvalidStyle Resize="None"></InvalidStyle>

<HoveredStyle Resize="None"></HoveredStyle>

<EnabledStyle Resize="None"></EnabledStyle>
</DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl="" CssClass="aspNetDisabled aspNetDisabled rcCalPopup" Enabled="False" Visible="False"></DatePopupButton>
                                                    </telerik:RadTimePicker>
                                                </td>
                                            </tr>
                                            </table>
                                    </td>
                                    

                                </tr> 
                                                                                            
                                <tr>
                                    
                                    <td width="40%" align="right" style="width: 20%">
                                        
                                        Motivo Re-Contacto</td>
                                    
                                    <td align="left" colspan="2" style="width: 80%">
                                        <telerik:RadComboBox ID="ddMotivoRecontacto" runat="server" Width="250px" Culture="es-ES" DataSourceID="dsMotivoRecontacto0" DataTextField="nombre" DataValueField="id" Filter="Contains" HighlightTemplatedItems="True"></telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsMotivoRecontacto0" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT 0 id, 'Seleccione un motivo' nombre union SELECT [id], [nombre] FROM [motivoRecontacto]"></asp:SqlDataSource>
                                    </td>
                                    

                                </tr> 
                                                                                            
                                <tr>
                                    
                                    <td align="center" colspan="5" style="width: 50%">
                                        <asp:Button ID="btnbuscar" runat="server" Text="Buscar" Width="30%" OnClick="btnbuscar_Click" CssClass="btnSubmit" />
                                    </td>
                                    

                                </tr> 
                                                                                            
                            </table>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Label ID="lblError" runat="server" CssClass="labelError"></asp:Label>          
      </div>
      
</asp:Content>
<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 98%; border-bottom: black 1px solid">
        <tr>
            <td align="center">
                <asp:Label ID="lblCount" runat="server" Font-Bold="True" ForeColor="#313D59"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="gvBusqueda" runat="server" Width="100%" 
                            AutoGenerateColumns="False" AllowPaging="True"
                            OnPageIndexChanging="gvBusqueda_PageIndexChanging" 
                            OnSorting="gvBusqueda_Sorting"
                            OnRowDataBound="gvBusqueda_RowDataBound"
                            HeaderStyle-CssClass="headerStyle"
                            AlternatingRowStyle-CssClass="altRowStyle"
                            RowStyle-CssClass="rowStyle"
                            FooterStyle-CssClass="footerStyle"
                            PagerStyle-CssClass="footerStyle" CellPadding="4" ForeColor="#333333" 
                                GridLines="None" DataKeyNames="TicketNro"
                                AllowSorting="True" DataSourceID="dsGridResultados" 
                                ondatabound="gvBusqueda_DataBound" >
                    <AlternatingRowStyle CssClass="altRowStyle" BackColor="White" ForeColor="#284775">
                    </AlternatingRowStyle>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnSelect" runat="server" ImageUrl="~/Images/Seleccion.jpg" Width="22" Height="22" />
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <%--<ItemTemplate>
                                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/Edicion.jpg" Width="22" Height="22" />
                            </ItemTemplate>--%>
                            <ItemTemplate>
                             <asp:HyperLink runat="server" id="btnEdit" Text="Editar" ImageUrl="~/Images/Edicion.jpg" target="_blank" NavigateUrl='<%# Eval("TicketNro", "~/Editaticket.aspx?Id={0}")%>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="TicketNro" HeaderText="Nro.Ticket" 
                                    InsertVisible="False" ReadOnly="True" SortExpression="TicketNro" />
                        <asp:BoundField DataField="Razon_Social" HeaderText="Raz�n Social" HeaderStyle-Wrap="False" 
                                    ReadOnly="True" SortExpression="Razon_Social" />
                        <asp:BoundField DataField="ClainNro" HeaderText="Nro.Clain" 
                                    ReadOnly="True" SortExpression="ClainNro" />
                        <asp:BoundField DataField="NroSerie" HeaderText="Nro.Serie" ReadOnly="True" 
                                    SortExpression="NroSerie" Visible="False" />
                        <asp:BoundField DataField="Nombre_Negocio" HeaderText="Negocio" 
                                    ReadOnly="True" SortExpression="Nombre_Negocio" />
                        <asp:BoundField DataField="Lider" HeaderText="Lider" 
                                    ReadOnly="True" SortExpression="Lider" >
                        <ItemStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Especialista" HeaderText="Especialista" 
                                    ReadOnly="True" SortExpression="Especialista" >
                        <ItemStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Nombre_Equipo" HeaderText="Equipo" ReadOnly="True" 
                                    SortExpression="Nombre_Equipo" />
                        <asp:BoundField DataField="Estado" HeaderText="Estado" 
                                    SortExpression="Estado" />
                        <asp:BoundField DataField="Tipo_Problema" HeaderText="Tipo Problema" 
                                    ReadOnly="True" SortExpression="Tipo_Problema" />
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha Creaci�n" 
                                    SortExpression="Fecha"  >
                        </asp:BoundField>
                        <asp:BoundField DataField="UltimaModif" HeaderText="�ltima Modif." 
                                    SortExpression="UltimaModif"  >
                        </asp:BoundField>
                        <asp:BoundField DataField="Detalle" HeaderText="Detalle" Visible="False" />
                        <asp:BoundField DataField="FechaRecontacto" HeaderText="Fecha Recontacto" />
                        <asp:BoundField DataField="MotivoRecontacto" HeaderText="Motivo Recontacto" Visible="False"/>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle CssClass="footerStyle" BackColor="#5D7B9D" Font-Bold="True" ForeColor="White">
                    </FooterStyle>
                    <HeaderStyle CssClass="headerStyle" BackColor="#5D7B9D" Font-Bold="True" ForeColor="White">
                    </HeaderStyle>
                    <PagerStyle CssClass="footerStyle" BackColor="#284775" ForeColor="White" HorizontalAlign="Center">
                    </PagerStyle>
                    <RowStyle CssClass="rowStyle" BackColor="#F7F6F3" ForeColor="#333333">
                    </RowStyle>
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:SqlDataSource ID="dsGridResultados" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
                                onselected="dsGridResultados_Selected"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btExportar" runat="server" onclick="btExportar_Click" 
                                            Text="Exportar a Excel" />
            </td>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="retCliente" ClientIDMode="Static"/>    
      <telerik:radwindowmanager id="WinMgr" runat="server" skin="Metro" AutoSize="true">
            <Windows>
               <telerik:RadWindow ID="buscarCliente" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true">
               </telerik:RadWindow>
               <telerik:RadWindow ID="buscarEquipo" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true">
               </telerik:RadWindow>
            </Windows>
    </telerik:radwindowmanager>
</asp:Content>

