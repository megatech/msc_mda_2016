﻿<%@ Page Title="" Language="VB" MasterPageFile="~/TicketsPage.master" AutoEventWireup="false" CodeFile="verCoachingUsuario.aspx.vb" Inherits="verCoachigUsuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTicket" Runat="Server">
    <asp:Label ID="lblError" runat="server"></asp:Label>
<asp:Panel ID="panelData" runat="server">
<table align="center"><tr><td colspan=2 align="center" style="font-weight: bold">Monitoreos registrados de <asp:Label ID="lblUserName" runat="server" Text="Label"></asp:Label> </td></tr>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Año"></asp:Label>
        </td>
        <td style="width: 50%">
            <asp:DropDownList ID="ddAnio" runat="server" AutoPostBack="True" 
                DataSourceID="SqlDataSource2" DataTextField="texto" DataValueField="anio">
            </asp:DropDownList>
        </td>
    </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        ProviderName="<%$ ConnectionStrings:TicketDB.ProviderName %>" 
        SelectCommand="SELECT YEAR(getDate()) AS texto, YEAR(getDate()) AS anio"></asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" 
        GridLines="None" HorizontalAlign="Center">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Mes" HeaderText="Mes" 
                ReadOnly="True" SortExpression="Mes" />
            <asp:TemplateField HeaderText="Con Err.Críticos">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sin Err.Críticos">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    </asp:Panel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" SelectCommand="
            SELECT  
            CASE MONTH(fecha)
            WHEN 1 THEN 'Enero' 
            WHEN 2 THEN 'Febrero' 
            WHEN 3 THEN 'Marzo' 
            WHEN 4 THEN 'Abril' 
            WHEN 5 THEN 'Mayo' 
            WHEN 6 THEN 'Junio' 
            WHEN 7 THEN 'Julio' 
            WHEN 8 THEN 'Agosto' 
            WHEN 9 THEN 'Septiembre'
            WHEN 10 THEN 'Octubre'
            WHEN 11 THEN 'Noviembre'
            WHEN 12 THEN 'Diciembre'
            END as Mes
            FROM coaching
            WHERE userId = 581" 
        ConnectionString="<%$ ConnectionStrings:TicketDB %>" 
        ProviderName="<%$ ConnectionStrings:TicketDB.ProviderName %>"></asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

