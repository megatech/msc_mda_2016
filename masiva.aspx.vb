﻿Imports System.Data.SqlClient
Imports App_Code.TicketsLib

Partial Class masiva
    Inherits System.Web.UI.Page
    Dim directorio As String


    Protected Sub btnsubir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubir.Click

        If (FileUpload1.HasFile = True) Then
            Dim filepath As String = FileUpload1.PostedFile.FileName
            Dim pat As String = "\\(?:.+)\\(.+)\.(.+)"
            Dim r As Regex = New Regex(pat)
            'run
            Dim m As Match = r.Match(filepath)
            Dim file_ext As String = m.Groups(2).Captures(0).ToString()
            Dim filename As String = m.Groups(1).Captures(0).ToString()
            Dim file As String = "masivo2" & "." & file_ext

            If Not FileUpload1.PostedFile.ContentLength <= 1048576 Then
                lblStatus.Text = "El tamaño maximo permitido por el servidor es de 1 MB"
                Exit Sub
            End If

            If (file_ext <> "txt") And (file_ext <> "TXT") Then
                lblstatus.Text = "Debe seleccionar un archivo txt"
                Exit Sub
            End If


            'Guarda el archivo en el file server. 
            FileUpload1.PostedFile.SaveAs("\\192.168.1.8\masivo\" & file & "")


            Dim SQL As New SqlConnection
            Dim query As New SqlCommand

            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ConnectionString
            SQL.Open()
            query.Connection = SQL
            query.CommandText = "DROP TABLE BULKTICKET"
            query.ExecuteNonQuery()

            query.CommandText = "CREATE TABLE BULKTICKET (TICKETID INT,InformeInterno text ) " & _
                     " BULK INSERT BULKTICKET FROM '\\192.168.1.8\masivo\" & file & "' WITH ( FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n' )"


            query.ExecuteNonQuery()
            query.Connection.Close()
            SQL.Close()

            Response.Redirect("cargamasiva.aspx")

        End If



    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ususarioactual As Usuario

        ususarioactual = Session("usuario")

        If ususarioactual.Perfil <> 1 Then
            Response.Redirect("Default2.aspx")
        End If

        directorio = "\\192.168.1.8\masivo"
        lblstatus.Text = ""
    End Sub
End Class
