<%@ Page Language="C#" MasterPageFile="~/TicketsPage.master" AutoEventWireup="true" CodeFile="EditaTicket.aspx.cs" Inherits="EditaTicket" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="TicketPage" ContentPlaceHolderID="cphTicket" runat="server">
   
   <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   <link rel="stylesheet" href="css/popupBootstrap.css">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#myModal').modal({ show: false });
            //Sys.Application.add_load(ShowWindow);
            var b = document.getElementById("txtDetalle");
            b.addEventListener("click", LanzarpopUP, false);

        });

        function LanzarpopUP()
        {
            var z = document.getElementById("txtTicket").value;
            var e = document.getElementById("lblEstado").textContent;
            AbrirPopup('FormCamposDeTipoP.aspx?id=' + z + '&estado=' + e  , 'popup2')
            
        }
        
       
    
    </script>
    <asp:Panel ID="panelTicket" runat="server" >
    <div align="right"></div>
        <div style="text-align: center">
            <table border="1" cellspacing="5" style="border-right: black 1px solid; border-top: black 1px solid;
                border-left: black 1px solid; width: 100%; border-bottom: black 1px solid">
                <caption align="top"><u><b>Modificaci�n de Ticket</b></u></caption>
                <caption>
                    <br />
                  
                    <tr >
                        <td align="middle">
                            <asp:Label ID="lblError" runat="server" CssClass="labelError"></asp:Label>
                            <asp:ValidationSummary ID="validSummary0" runat="server" Height="33px" />
                        </td>
                    </tr >
                    <tr>
                        <td align="center">
                            <asp:LinkButton ID="lnkZonaInsegura" runat="server" BackColor="#FCB1A9" 
                                EnableViewState="False" Font-Bold="True" Font-Size="12px" 
                                ForeColor="#4A0000" onclick="lnkZonaInsegura_Click" Visible="False"> EL CLIENTE RESIDE EN ZONA INSEGURA</asp:LinkButton>
                                                        <table border="0" bordercolor="white" bordercolordark="#ffffff" 
                                bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="right">
                                        Fecha de creaci�n</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFecha" runat="server" CssClass="TextoTextBoxReadonly" 
                                            ReadOnly="true"></asp:TextBox>
                                    </td>
                                    <td align="right">
                                        F.Inicio</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextoTextBoxReadonly" 
                                            Width="70px" ></asp:TextBox>
                                    </td>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td style="width:34px;">
            <asp:ImageButton runat="server" ID="btviatico" ImageUrl="~/Images/viaticos.png" Width="30px" ToolTip="viaticos"/>
        </td>
                                    <td align="left" style="text-align: right">
                                   
                                    <asp:ImageButton runat="server" AlternateText="Encuesta" id="btn_encuesta"
                                            Height="21px" ImageUrl="~/Images/encuesta2.jpg" 
                                            Width="21px" ToolTip="Encuesta" />

                                        <asp:ImageButton ID="ImageButton1" runat="server" AlternateText="IMPRIMIR" 
                                            Height="21px" ImageUrl="~/Images/imprimir.jpg" onclick="ImageButton1_Click" 
                                            Width="21px" ToolTip="Imprimir" />
                                        <asp:ImageButton ID="btnverpartes" runat="server" AlternateText="PARTES" 
                                            Height="21px" ImageUrl="~/Images/repuestos.jpg" OnClick="btnverpartes_Click" 
                                            Text="Ver Partes" Width="21px" ToolTip="Asignaci�n de Partes" />
                                        &nbsp;<asp:ImageButton ID="btnliq" runat="server" AlternateText="CAS" Height="21px" 
                                            ImageUrl="~/Images/cas.jpg" OnClick="btnliq_Click" Text="Cas" Visible="false" 
                                            Width="21px" />
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        ANI</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNroClaim" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                    <td align="right">
                                        Ticket</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTicket" runat="server" CssClass="TextoTextBoxReadonly" ClientIDMode="Static"
                                            ReadOnly="true"></asp:TextBox>
                                    </td>
                                    <td align="right">
                                        F. Fin</td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextoTextBoxReadonly" 
                                            Width="70px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr >
                        <td align="center">
                            <table border="0" bordercolor="white" bordercolordark="#ffffff" 
                                bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="right" style="width: 10%">
                                        Area</td>
                                    <td align="left" style="width: 26%">
                                        <asp:Label ID="lblNegocio" runat="server" CssClass="TextoTextBoxReadonly" 
                                            Visible="False" />
                                        <asp:DropDownList ID="ddEmpresaPartner" runat="server" AutoPostBack="True" 
                                            onselectedindexchanged="ddEmpresaPartner_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right" style="width: 6%">
                                        <asp:LinkButton ID="lnvercliente" runat="server" onclick="lnvercliente_Click" 
                                            ToolTip="Ver/Editar Cliente">Cliente</asp:LinkButton>
                                    </td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="txtCodCliente" runat="server" AutoPostBack="True" 
                                            OnTextChanged="txtCodCliente_TextChanged" Width="50px"></asp:TextBox>
                                        &nbsp;<asp:ImageButton ID="imgBuscar" runat="server" CausesValidation="false" 
                                            ImageUrl="~/Images/search.png"  ToolTip="Buscar Cliente" OnClientClick="AbrirPopup('buscarCliente.aspx','buscarCliente'); return false;" />
                                        &nbsp;<asp:TextBox ID="txtNomCliente" runat="server" CssClass="TextoTextBoxReadonly" 
                                            ReadOnly="True" Width="220px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" style="width: 10%">
                                        Ubicaci�n</td>
                                    <td align="left" style="width: 26%">
                                        <asp:Label ID="lblUbicacion" runat="server" CssClass="TextoTextBoxReadonly" 
                                            Visible="False" />
                                        <asp:DropDownList ID="ddUbicacion" runat="server" 
                                            onselectedindexchanged="ddUbicacion_SelectedIndexChanged" 
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right" style="width: 6%">
                                        Contacto</td>
                                    <td align="left">
                                        <asp:Label ID="lblContacto" runat="server" CssClass="TextoTextBoxReadonly" 
                                            Visible="False" />
                                        <asp:DropDownList ID="ddContacto" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr >
                                <tr>
                                    <td align="right" colspan="4">
                                        <table>
                                            <tr>
                                                <td bgcolor="LightBlue">
                                                    <asp:LinkButton ID="lnkHistorial" runat="server" Font-Bold="True" 
                                                        ForeColor="#002953" onclick="lnkHistorial_Click">Historial de casos[ ]</asp:LinkButton>
                                                </td>
                                                <td bgcolor="LightGreen">
                                                    <asp:LinkButton ID="lnkOnsitesDelA�o" runat="server" Font-Bold="True" 
                                                        ForeColor="#003300" onclick="lnkOnsitesDelA�o_Click">Visitas Realizadas en 2012[ ]</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr >
                    <tr >
                        <td align="center">
                            <table border="0" bordercolor="white" bordercolordark="#ffffff" 
                                bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr >
                                    <td align="right" style="width: 11%">
                                        Nro. Serie</td>
                                    <td align="left" style="width: 169px">
                                        <asp:TextBox ID="txtSerie" runat="server" Width="122px" ReadOnly="true" ></asp:TextBox>
                                    </td>
                                    <td align="right" style="width: 11%">
                                        <asp:Label ID="lblFechaRecontactar" runat="server" Text="Fecha a contactar"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 11%">
                                        <telerik:RadDateTimePicker ID="dpFechaRecontactar" Runat="server" SkipMinMaxDateValidationOnServer="True" Culture="es-AR" EnableTyping="false" Skin="Silk">
                                            <TimeView CellSpacing="-1" Culture="es-AR" TimeFormat="HH:mm" Interval="00:30:00" StartTime="08:00:00" EndTime="23:00:00" RenderDirection="Vertical">
                                            </TimeView>
                                            <TimePopupButton HoverImageUrl="" ImageUrl="" />
                                            <Calendar Culture="es-ES" EnableWeekends="True" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" Skin="Silk">
                                                <FastNavigationSettings CancelButtonCaption="Cancelar" EnableTodayButtonSelection="True" TodayButtonCaption="Hoy">
                                                </FastNavigationSettings>
                                            </Calendar>
                                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy HH:mm" LabelWidth="40%" SkipMinMaxDateValidationOnServer="True">
                                                <EmptyMessageStyle Resize="None" />
                                                <ReadOnlyStyle Resize="None" />
                                                <FocusedStyle Resize="None" />
                                                <DisabledStyle Resize="None" />
                                                <InvalidStyle Resize="None" />
                                                <HoveredStyle Resize="None" />
                                                <EnabledStyle Resize="None" />
                                            </DateInput>
                                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                        </telerik:RadDateTimePicker>
                                    </td>
                                    <td align="right" style="width: 11%">
                                        <asp:Label ID="lblTecnico" runat="server" Text="T�cnico:" Visible="False" Width="100px"></asp:Label>
                                        &nbsp;<asp:Label ID="lbltxtVendedor" runat="server" Text="Vendedor:" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 190px">
                                        <telerik:RadComboBox ZIndex=10000 ID="ddTecnico" Runat="server" Filter="Contains" HighlightTemplatedItems="True" Width="230px">
                                        </telerik:RadComboBox>
                                        <asp:Label ID="lblVendedor" runat="server" Text="Vendedor" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" rowspan="4" style="width: 17%" width="35%" valign="top">
                                        <table style="width: 100%; vertical-align: top;">
                                            <tr>
                                                <td style="height: 23px; width: 74px">
                                                    &nbsp;</td>
                                                <td style="height: 23px">
                                                    <asp:Label ID="lblHorarios" runat="server" Text="Horarios:" Visible="False"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 23px; width: 74px">
                                                    &nbsp;</td>
                                                <td style="height: 23px">
                                                    <asp:CheckBoxList ID="lstHorarios" runat="server" AutoPostBack="True" 
                                                        Height="27px" Visible="False" Width="101px">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr >
                                <tr>
                                    <td align="right" width="15%" rowspan="2">
                                        Segmento</td>
                                    <td align="left" width="35%" rowspan="2">
                                        <asp:TextBox ID="txtCodEquipo" runat="server" AutoPostBack="True" 
                                            ontextchanged="txtCodEquipo_TextChanged" Width="50px"></asp:TextBox>
                                        <asp:ImageButton ID="imgBuscarEquipo" runat="server" CausesValidation="false" 
                                            ImageUrl="~/Images/search.png" OnClientClick="AbrirPopup('buscarEquipo.aspx','popup'); return false;" />
                                        <asp:TextBox ID="txtEquipoId" runat="server" CssClass="TextoTextBoxReadonly" 
                                            ReadOnly="True" Width="192px"></asp:TextBox>
                                        <br />
                                        <asp:TextBox ID="TxtNombreEquipo" runat="server" 
                                            CssClass="TextoTextBoxReadonly" ReadOnly="True" Width="273px"></asp:TextBox>
                                    </td>
                                    <td align="right" width="100px" rowspan="2">
                                        <asp:Label ID="lblMotivoRecontacto" runat="server" Text="Motivo de Re-Contacto"></asp:Label>
                                    </td>
                                    <td align="left" width="100px" rowspan="2">
                                        <telerik:RadComboBox ID="ddMotivoRecontacto" Runat="server" Culture="es-ES" datasourceid="dsMotivoRecontacto0" DataTextField="nombre" DataValueField="id" Filter="Contains" HighlightTemplatedItems="True" Width="230px" ZIndex="10000">
                                        </telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsMotivoRecontacto0" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT 0 id, 'Seleccione un motivo' nombre union SELECT [id], [nombre] FROM [motivoRecontacto]"></asp:SqlDataSource>
                                    </td>
                                    <td align="right" width="100px">
                                        <asp:Label ID="lblFCoordinada" runat="server" Text="F. Coordinada" Width="100px"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <telerik:RadDateTimePicker ID="RDTCoordinada" Runat="server" Culture="es-ES" 
                                            MinDate="1900-01-01" PopupDirection="BottomLeft" Skin="Silk" Width="150px">
                                            <TimeView CellPadding="0" Columns="2" CssClass="timeViews" Culture="es-ES" 
                                                EndTime="16:59:59" HeaderText="Horarios" Interval="00:30:00" 
                                                StartTime="10:00:00">
                                            </TimeView>
                                            <TimePopupButton HoverImageUrl="" ImageUrl="" />
                                            <Calendar Skin="WebBlue" UseColumnHeadersAsSelectors="False" 
                                                UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                <FastNavigationSettings CancelButtonCaption="Cancelar" EnableTodayButtonSelection="True" OkButtonCaption="Aceptar" TodayButtonCaption="Hoy">
                                                            </FastNavigationSettings>
                                            </Calendar>
                                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                            </DateInput>
                                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                        </telerik:RadDateTimePicker>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="100px">
                                        <asp:Label ID="lblFSolucion" runat="server" Text="F. de Soluci�n" Width="100px"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <telerik:RadDateTimePicker ID="RDTSolucion" Runat="server" Culture="es-ES" 
                                            MinDate="1900-01-01" PopupDirection="BottomLeft" Skin="Silk" Width="150px">
                                            <TimeView CellPadding="0" Columns="6" CssClass="timeViews" Culture="es-ES" 
                                                HeaderText="Horarios" Interval="00:10:00" StartTime="08:00:00">
                                            </TimeView>
                                            <TimePopupButton HoverImageUrl="" ImageUrl="" />
                                            <Calendar Skin="WebBlue" UseColumnHeadersAsSelectors="False" 
                                                UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                <FastNavigationSettings CancelButtonCaption="Cancelar" EnableTodayButtonSelection="True" OkButtonCaption="Aceptar" TodayButtonCaption="Hoy">
                                                            </FastNavigationSettings>
                                            </Calendar>
                                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                            </DateInput>
                                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                        </telerik:RadDateTimePicker>
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right">
                                        Especialista</td>
                                    <td align="left">
                                        <asp:Label ID="lblEspecialista" runat="server" 
                                            CssClass="TextoTextBoxReadonly" Visible="False"/>
                                        <telerik:RadComboBox ZIndex=10000 ID="ddEspecialista" Runat="server" Filter="Contains" HighlightTemplatedItems="True" Width="270px">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td align="right" width="100px">
                                        &nbsp;</td>
                                    <td align="left" width="100px">
                                        &nbsp;</td>
                                    <td align="right" width="100px">
                                        <asp:Label ID="lblFCarga" runat="server" Text="F. de Carga de datos" Width="139px"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <telerik:RadDateTimePicker ID="RDTCarga" Runat="server" Culture="es-ES" 
                                            MinDate="1900-01-01" PopupDirection="BottomLeft" 
                                            Skin="Silk" Width="150px">
                                            <TimeView CellSpacing="-1" Columns="6" Culture="es-ES" HeaderText="Horarios" 
                                                Interval="00:10:00" StartTime="08:00:00">
                                            </TimeView>
                                            <TimePopupButton HoverImageUrl="" ImageUrl="" />
                                            <Calendar Skin="Silk" UseColumnHeadersAsSelectors="False" 
                                                UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                <FastNavigationSettings CancelButtonCaption="Cancelar" EnableTodayButtonSelection="True" OkButtonCaption="Aceptar" TodayButtonCaption="Hoy">
                                                            </FastNavigationSettings>
                                            </Calendar>
                                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                            </DateInput>
                                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                        </telerik:RadDateTimePicker>
                                    </td>
                                </tr >
                            </table>
                        </td>
                    </tr >
                    <tr >
                        <td align="center">
                                                                      <table border="0" bordercolor="white" bordercolordark="#ffffff" 
                                bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr >
                                    <td align="right" width="15%">
                                        T.Problema</td>
                                    <td align="left" width="35%">
                                        <asp:DropDownList Enabled="false" ID="ddTipoProblema" runat="server" 
                                            onselectedindexchanged="ddTipoProblema_SelectedIndexChanged">
                                        </asp:DropDownList>
                                       
                                    </td>
                                    <td align="right" width="15%">
                                        Prioridad</td>
                                    <td align="left" width="35%">
                                        <asp:DropDownList ID="ddPrioridad" runat="server">
                                            <asp:ListItem Selected="True">Baja</asp:ListItem>
                                            <asp:ListItem>Media</asp:ListItem>
                                            <asp:ListItem>Alta</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr >
                                <tr >
                                    <td align="right" width="15%">
                                        Estado</td>
                                    <td align="left" width="35%">
                                        <asp:Label ID="lblEstado" runat="server" CssClass="TextoTextBoxReadonly" ClientIDMode="Static" />
                                        <asp:Label ID="lblIdEstadoActual" runat="server" 
                                            CssClass="TextoTextBoxReadonly" Visible="false" />
                                    </td>
                                    <td align="right">
                                        <asp:TextBox ID="txtEmailAvast" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left">
                                        &nbsp;
                                        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                                            Text="Enviar Mail" />
                                        <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                    </td>
                                </tr >
                                <tr><td align="right" width="15%"><asp:Label ID="lbl_Motivo_Tec_1" runat="server" ClientIDMode="Static" Text="Motivo de la derivacion" Visible="false" /> </td><td><asp:Label ID="lbl_Motivo_Tec_2" runat="server" CssClass="TextoTextBoxReadonly" ClientIDMode="Static" visible="false"/></td><td></td><td></td></tr>

                                <%--<tr >
                                    <td align="right" width="15%">
                                        Sistema Operativo</td>
                                    <td align="left" width="35%">
                                        <telerik:RadComboBox ZIndex=10000 ID="ddSistemaOperativo" Runat="server" AutoPostBack="True" Culture="es-ES" DataSourceID="dsSistOperativo" DataTextField="nombre" DataValueField="id" Filter="Contains" HighlightTemplatedItems="True" OnSelectedIndexChanged="RadComboBox1_SelectedIndexChanged" Width="230px">
                                        </telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsServicePack" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [servicePackId], [nombre] FROM [servicePackSO]"></asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsArquitectura" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [arquitecturaId], [nombre] FROM [arquitecturaSO]"></asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsSistOperativo" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT -1 as id, 'Seleccione un valor' as nombre UNION 
SELECT [id], [nombre] FROM [sistemaOperativo] WHERE ([fechaBaja] IS NULL)"></asp:SqlDataSource>
                                    </td>
                                    <td align="right">&nbsp;</td>
                                    <td align="left">
                                        &nbsp;</td>
                                </tr >
                                <tr>
                                    <td align="right" width="15%">
                                        <asp:Label ID="lblArquitecturaSO" runat="server" Text="Arquitectura - SP" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" width="35%">
                                        <asp:RadioButtonList ID="arquitectura" runat="server" CssClass="radioAsButton" DataSourceID="dsArquitectura" DataTextField="nombre" DataValueField="arquitecturaId" RepeatDirection="Horizontal">
                                        </asp:RadioButtonList>
                                        <asp:RadioButtonList ID="servicePack" runat="server" CssClass="radioAsButton" DataSourceID="dsServicePack" DataTextField="nombre" DataValueField="servicePackId" Font-Bold="False" RepeatDirection="Horizontal">
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="right">&nbsp;</td>
                                    <td align="left">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" width="15%">&nbsp;</td>
                                    <td align="left" width="35%">
                                        <asp:CheckBox ID="chkSOModificado" runat="server" CssClass="customCheck" Font-Bold="True" ForeColor="#666666" Text="Versi�n modificada" />
                                    </td>
                                    <td align="right">&nbsp;</td>
                                    <td align="left">&nbsp;</td>
                                </tr>
                           --%> </table>
                            <table border="0" bordercolor="white" bordercolordark="#ffffff" bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="right" width="10%">Motivo del Llamado</td>
                                    <td align="left">
                                    <asp:TextBox ID="txtDetalle" runat="server" Rows="5" TextMode="MultiLine" Width="98%" ReadOnly="true" ClientIDMode="Static" ></asp:TextBox>
                                    </td>                                                                                                                                                       
                                </tr>
                            </table>
                        </td>
                    </tr >
                    <tr >
                        <td align="center" style="height: 268px">
                            <table border="0" bordercolor="white" bordercolordark="#ffffff" 
                                bordercolorlight="#ffffff" cellpadding="2" cellspacing="0" 
                                style="width: 100%; height: 278px;">
                                <tr >
                                    <td align="right" style="height: 111px" valign="top" width="15%">
                                        Nuevo Estado</td>
                                    <td align="left" style="height: 111px" valign="top">
                                        &nbsp;<asp:DropDownList ID="ddlNuevoEstado" runat="server" 
                                            DataSourceID="odsNuevoEstado" DataTextField="Nombre" 
                                            DataValueField="Codigo"
                                            onselectedindexchanged="ddlNuevoEstado_SelectedIndexChanged" 
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:Button ID="btEstados" runat="server"  
                                            onclick="btEstados_Click" Text="+" Font-Bold="False" Visible="False"/>
                                        <asp:SqlDataSource ID="odsNuevoEstado" Runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>"
                                            SelectCommand="SELECT	0 AS Codigo,	'[Ninguno]' AS Nombre UNION 
                                            SELECT	e.EstadoId Codigo,	e.Nombre FROM
                                            Estado (nolock) e	INNER JOIN permisoEstado pe 
                                             ON pe.estadoId = e.estadoId	AND pe.perfilId = @perfil
	                                            AND pe.fechabaja IS NULL WHERE	e.FechaBaja IS NULL	
                                             AND e.EstadoId IN ( SELECT	EstadoIdFin	FROM Accion (nolock)
											WHERE	CONVERT (INT, EstadoIdIni) = @EstadoActual)" >
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="lblIdEstadoActual" Name="EstadoActual" PropertyName="Text" Type="Int32" />
                                                <asp:SessionParameter Name="perfil" SessionField="perfil" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                        <asp:Button ID="btn_reIngreso" runat="server" OnClick="btn_reIngreso_Click" Text="Re-Ingreso" Enabled="false" />
                                    </td>
                                    <td align="right" style="height: 111px" valign="top" width="15%">
                                        Informe Interno</td>
                                    <td align="left" style="height: 111px" valign="top" width="35%">
                                        <asp:TextBox ID="txtInformeInterno" runat="server" 
                                            CssClass="TextoTextBoxReadonly" Height="105px" ReadOnly="true" Rows="3" 
                                            TextMode="MultiLine" Width="90%"></asp:TextBox>
                                    </td>
                                </tr >
                                <tr >
                                    <td align="right" valign="top">
                                        Resoluci�n</td>
                                    <td align="left" valign="top">
                                        <asp:TextBox ID="txtInformeCliente" runat="server" Height="160px" Rows="3" 
                                            TextMode="MultiLine" Width="90%"></asp:TextBox>
                                    </td>
                                    <td align="right" valign="top">
                                        Agregar a Informe Interno</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAgregar" runat="server" Height="153px" Rows="3" 
                                            TextMode="MultiLine" Width="90%"></asp:TextBox>
                                    </td>
                                </tr >
                            </table>
                        </td>
                    </tr >
                    <tr >
                        <td>
                            <telerik:RadButton ID="btnSubmit" runat="server" Text="Guardar" OnClick="btnSubmit_Click" CssClass="btnSubmit" Height="26px" Width="160px"></telerik:RadButton>
                        </td>
                    </tr >
                    <tr >
                        <td align="left" style="height: 104px">
                            <asp:Label ID="lblError1" runat="server" CssClass="labelError"></asp:Label>
                            <asp:ValidationSummary ID="validSummary" runat="server" />
                        </td>
                    </tr >
                    

                </caption>
            <tr>
<div class="container">
    <caption>
        
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Motivo de la Derivacion
                            <button class="close" data-dismiss="modal" type="button">
                                �
                            </button>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            Seleccione el motivo:</p>
                        <p id="main">
                        <img id="img_Motivo_Tec" src="Images/laptop.png" />
                        <asp:DropDownList ID="DropMotivo" runat="server" AutoPostBack="True" CssClass="form-control" DataSourceID="Motivo_Derivado_Tec" DataTextField="Descripcion" DataValueField="ID_DerivadoTecnico" OnSelectedIndexChanged="ddMotivo_Derivacion_Tec">
                            <asp:ListItem Selected="True" Value="0">Seleccione un Motivo</asp:ListItem>
                        </asp:DropDownList></p>
                    </div>
                    <div class="modal-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </caption>
  
</div>
</tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelMensaje" runat="server">
        <table>
            <tr >
                <td align="left" >
                    <asp:Label ID="lblMensajePanel" runat="server" CssClass="labelError"></asp:Label>
                </td>        
            </tr >
        </table>
    </asp:Panel>
    <asp:HiddenField runat="server" ID="retEncuesta" ClientIDMode="Static"/>  
    <telerik:radwindowmanager id="WinMgr" runat="server" skin="Metro" AutoSize="true" Modal="true" OnClientClose="SavePopupValues" ShowContentDuringLoad="false" KeepInScreenBounds="true">
            <Windows>
                <telerik:RadWindow ID="popEncuesta" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true"></telerik:RadWindow>
             <telerik:RadWindow ID="popup" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true"></telerik:RadWindow>
                <telerik:RadWindow ID="popup2" runat="server" OnClientClose="SavePopupValues" MinWidth="700" MinHeight="800" AutoSize="true"  Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true"></telerik:RadWindow>
            </Windows>
    </telerik:radwindowmanager>
    
      <asp:HiddenField runat ="server" ID="FormEquipo" ClientIDMode="Static" />
      <asp:HiddenField runat="server" ID="retCliente" ClientIDMode="Static"/>    
      <asp:HiddenField runat="server" ID="retEquipo" ClientIDMode="Static"/>    
        <asp:SqlDataSource ID="Motivo_Derivado_Tec" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" SelectCommand="SELECT [ID_DerivadoTecnico], [Descripcion] FROM [motivoDerivacion] ORDER BY [Descripcion]"></asp:SqlDataSource>
        <telerik:radwindowmanager id="Radwindowmanager1" runat="server" skin="Metro" AutoSize="true">
            <Windows>
               <telerik:RadWindow ID="buscarCliente" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true">
               </telerik:RadWindow>
               <telerik:RadWindow ID="buscarEquipo" runat="server" OnClientClose="SavePopupValues" AutoSize="true" Modal="true" ShowContentDuringLoad="false" KeepInScreenBounds="true">
               </telerik:RadWindow>
            </Windows>
    </telerik:radwindowmanager>
</asp:Content>
