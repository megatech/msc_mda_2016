<%@ Page Language="C#" AutoEventWireup="true" CodeFile="busquedaCalendar.aspx.cs" Inherits="busquedaEquipo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <base target=_self />
     <asp:Literal id="ltlJS" runat="server"></asp:Literal>
    <title>Calendario</title>
</head>
<body bgcolor="silver" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">  
        <table style="width: 100%" border=0 bordercolor="#000000" cellpadding="1" cellspacing="1" align="center">
            <tr>
                <td align="center" >
                    <asp:Calendar id="calBusqueda" runat="server" 
                                 OnSelectionChanged="calBusqueda_SelectionChanged" 
                                 DayNameFormat="Shortest" 
                                 FirstDayOfWeek="Monday"
                                 Height="200px" Width="220px" BackColor="#FFFFCC" BorderColor="#FFCC66" BorderWidth="1px" Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" ShowGridLines="True">
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <SelectorStyle BackColor="#FFCC66" />
                        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                        <OtherMonthDayStyle ForeColor="#CC9966" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt"
                            ForeColor="#FFFFCC" />
                </asp:Calendar>

                </td>
            </tr>
        </table>
        <asp:Label ID="lblError" runat="server" Text=""></asp:Label><br />
    </form>
</body>
</html>
