using System;
using App_Code.TicketsLib;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using System.IO;
using FUNCIONES_MOD;
using System.Net;
using Popups;
using System.Collections.Generic;
using System.Collections;

public partial class EditaTicket : System.Web.UI.Page
{
    private TicketData unTicket{
        get { return (TicketData)((Hashtable)Session["InfoTickets"])[_idTicket]; }
        set { ((Hashtable)Session["InfoTickets"])[_idTicket] = value; }
    }

    log4net.ILog logger = log4net.LogManager.GetLogger("File");
    int _idTicket;
    #region EVENTOS
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropMotivo.DataBind();
            DropMotivo.Items.Insert(0, new ListItem("Seleccione un motivo", "0"));
        }


        _idTicket = int.Parse(Request.QueryString["id"]);
        if (Session["InfoTickets"] == null) {
            Session["InfoTickets"] = new Hashtable();
        }
        if (unTicket == null) { unTicket = new TicketData(); }

        btn_encuesta.OnClientClick = "AbrirPopup('Encuesta.aspx?id=" + Request.QueryString["id"] + "', 'popEncuesta'); return false;";
        //txtEmailAvast.Visible = lblEstado.Text.Trim() == "MASIVO AVAST";
        //lblEmail.Visible = lblEstado.Text.Trim() == "MASIVO AVAST";
        //Button1.Visible = lblEstado.Text.Trim() == "MASIVO AVAST";

        if (retCliente.Value != "")
        {            
            txtCodCliente.Text = PopupMgr.LeerDato(retCliente, "clienteId");
            txtNroClaim.Text = PopupMgr.LeerDato(retCliente, "Ani");
            txtSerie.Text = PopupMgr.LeerDato(retCliente, "Ani");
            txtNomCliente.Text = PopupMgr.LeerDato(retCliente, "razonSocial", true);
            txtCodCliente_TextChanged(this, new EventArgs());
        }
        if (retEquipo.Value != "")
        {
            txtCodEquipo.Text = PopupMgr.LeerDato(retEquipo, "Codigo");
            txtEquipoId.Text = PopupMgr.LeerDato(retEquipo, "EquipoId", true);
            txtCodEquipo_TextChanged(this, new EventArgs());
        }

        if (FormEquipo.Value != "")
        {

            txtDetalle.Text = PopupMgr.LeerDato(FormEquipo, "FormEquipo");
            unTicket.popContacto = PopupMgr.LeerDato(FormEquipo, "Contacto");
            unTicket.popSO = Convert.ToInt32(PopupMgr.LeerDato(FormEquipo, "so"));
            unTicket.poplicenciaSO = PopupMgr.LeerDato(FormEquipo, "licenciaSO");
            unTicket.popPC = PopupMgr.LeerDato(FormEquipo, "pc");
            unTicket.popAntiguedad = PopupMgr.LeerDato(FormEquipo, "antiguedad");
            unTicket.popProcesador = PopupMgr.LeerDato(FormEquipo, "procesador");
            unTicket.popRam = PopupMgr.LeerDato(FormEquipo, "ram");
            unTicket.popTipoconexion = PopupMgr.LeerDato(FormEquipo, "tipoconexion");
            unTicket.popmodem = PopupMgr.LeerDato(FormEquipo, "modem");
            unTicket.popDetalle = PopupMgr.LeerDato(FormEquipo, "detalle");
            ddTipoProblema.SelectedValue = PopupMgr.LeerDato(FormEquipo, "Tproblema");
            //((Hashtable)Session["InfoTickets"])[_idTicket] = unTicket;
        }
        
      
        registrarScripts();
        //try {
       
        ddUbicacion.AutoPostBack = true;
        ddEspecialista.AutoPostBack = true;
        ddEmpresaPartner.AutoPostBack = true;
        ddlNuevoEstado.AutoPostBack = true;

        if (!IsPostBack)
        {
            
            putPanelMensaje(false);

            Usuario usr = (Usuario)Session["Usuario"];
            if (usr == null)
            {
                Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString);
            }
            if (usr.Perfil == "15" || usr.Perfil == "25" || usr.Perfil == "26" || usr.Perfil == "32")
            {
                btn_reIngreso.Enabled = true;
            }
            if (usr.Perfil == "23") { Response.Redirect("ConsultaTicket.aspx?id=" + Request.QueryString["id"]); }
            if (Request.QueryString["id"] == null || Request.QueryString["id"].Equals(""))
            {
                lblMensajePanel.Text = "No existe Ticket para editar";
                putPanelMensaje(true);
            }
           
            
            else
            {
                _idTicket = int.Parse(Request.QueryString["id"]);
                App_Code.TicketsLib.TicketData tkt = App_Code.TicketsLib.Tickets.getTicket(_idTicket);
                unTicket.popContacto = tkt.popContacto;
                unTicket.popSO = tkt.popSO;
                unTicket.poplicenciaSO = tkt.poplicenciaSO;
                unTicket.popPC = tkt.popPC;
                unTicket.popAntiguedad = tkt.popAntiguedad;
                unTicket.popProcesador = tkt.popProcesador;
                unTicket.popRam = tkt.popRam;
                unTicket.popTipoconexion = tkt.popTipoconexion;
                unTicket.popmodem = tkt.popmodem;
                unTicket.popDetalle = tkt.popDetalle;
                //((Hashtable)Session["InfoTickets"])[_idTicket] = unTicket;
                odsNuevoEstado.SelectParameters["EstadoActual"].DefaultValue = tkt.idEstado.ToString();
                odsNuevoEstado.SelectParameters["perfil"].DefaultValue = usr.Perfil;
                ddlNuevoEstado.DataBind();
                pasoPantalla(tkt);
                int cantidadUbicaciones = loadUbicaciones(txtCodCliente.Text);

                if (cantidadUbicaciones > 0)
                {
                    loadContactos(ddUbicacion.Items[0].Value);
                    LlenaUbicacionContacto();
                }

                Session.Add("ticket", txtTicket.Text);

                cargaEspecialista();
                cargaTipoProblema();
                cargaEmpresaPartner();
                SelectCurrentContacto();
                loadContactos(ddUbicacion.SelectedValue);
                SelectCurrentContacto();
                cargarTecnicos();
                cargarHorarios();
                cargarHorarioActual();
                cargarTecnicoActual();
                cargarFechasOnsite();
                mostrarCamposOnsite();
                btEstados.Visible = puedeForzarEstados();
                habilitarInfoRecontacto();
                DeshabilitaFormulario();
                CargaMotivo();


            }

        }
       // loadScripts();
        lnvercliente.OnClientClick = "AbrirPopup('verCliente.aspx?codcte=" + txtCodCliente.Text + "','popup'); return false;";
      
        ActualizarDatosHistorial();
        if (!lnkHistorial.Text.Contains("[0]"))
        {
            lnkHistorial.OnClientClick = "AbrirPopup('verHistorial.aspx?id=" + txtCodCliente.Text + "','popup'); return false;";

        }
        if (!lnkOnsitesDelA�o.Text.Contains("[0]"))
        {
            lnkOnsitesDelA�o.OnClientClick = "AbrirPopup('verHistorial.aspx?year=1&id=" + txtCodCliente.Text + "','popup'); return false;";

        }
          
            
        
        
        /* }
         catch (Exception ex)
         {
            lblMensajePanel.Text = "Error: " + ex.Message;
            putPanelMensaje(true);
         }*/
        Boolean bAdmin = esPerfil("1") || esPerfil("20");
        if (bAdmin) { ddlNuevoEstado.Enabled = true; }

        if (ddEmpresaPartner.SelectedValue != "57" && ddEmpresaPartner.SelectedValue != "58") { btviatico.Visible = false; }
        btviatico.OnClientClick = "AbrirPopup('Viaticos.aspx?id=" + Request.QueryString["id"] + "&t=" + ddTecnico.SelectedValue.ToString() + "', 'popup2'); return false;";

    }
    protected void btnverpartes_Click(object sender, EventArgs e)
    {
        Response.Redirect("partes.aspx?id=" + txtTicket.Text + "");

    }
    protected void btnliq_Click(object sender, EventArgs e)
    {
        Response.Redirect("liq.aspx?id=" + txtTicket.Text + "");

    }
    protected void txtCodEquipo_TextChanged(object sender, EventArgs e)
        {
        mostrarCamposOnsite();
        try
        {
            FiltroEquipo fc = new FiltroEquipo();
            fc.Codigo = txtCodEquipo.Text;
            FiltroEquipo fco = Equipo.getUnEquipo(fc);
            if (fco == null)
            {
                lblError.Text = "No se pudo recuperar datos de ese Equipo.";
            }
            else
            {
                txtEquipoId.Text = fco.EquipoId;
                TxtNombreEquipo.Text = fco.Nombre;
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "Error: " + ex.Message;
        }
    }
    protected void ImageButton1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        Response.Redirect("repcalle.aspx?id=" + txtTicket.Text + "");
    }
    protected void txtCodCliente_TextChanged(object sender, EventArgs e)
    {

        FiltroCliente fc = new FiltroCliente();
        fc.Codigo = txtCodCliente.Text;
        FiltroCliente fco = Clientes.getUnCliente(fc);


        if (fco != null)
        {
            txtCodCliente.Text = fco.ClienteId;
            txtNomCliente.Text = fco.RazonSocial;
            txtNroClaim.Text = fco.Telefono1;
            txtSerie.Text = fco.Telefono1;
        }

        else
        {
            //NO EXISTE EL CLIENTE
            lblError.Text = "El cliente especificado no existe";
            lblError.Visible = true;
            txtCodCliente.Text = "";
            txtNomCliente.Text = "";
            return;
        }

        int cantidadUbicaciones = loadUbicaciones(fco.ClienteId);

        if (cantidadUbicaciones > 0)
        {
            loadContactos(ddUbicacion.Items[0].Value);
        }
        LlenaUbicacionContacto();
        //YA CARGAMOS LAS UBICACIONES
        ActualizarDatosHistorial();
    }
    protected void ddlUbicacion_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        loadContactos(ddUbicacion.SelectedValue);
    }
    protected void ddUbicacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadContactos(ddUbicacion.SelectedValue);
    }
    protected void ddTipoProblema_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
   
    protected void ddlNuevoEstado_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        AbrirPopupEnvioTec();
       
        sincronizarEstadoArea();
        cargarTecnicos();
        cargarTecnicoActual();
        mostrarCamposOnsite();
        habilitarInfoRecontacto();
    }
    protected void ddEmpresaPartner_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (estado() == "111") sincronizarAreaEstado();
        cargarTecnicos();
        cargarTecnicoActual();
        mostrarCamposOnsite();
    }

    protected void lnkHistorial_Click(object sender, EventArgs e)
    {
        //if (!lnkHistorial.Text.Contains("[0]"))
        //{
        //    String strHistorial = "<script>" +
        //    "window.open('verHistorial.aspx?id=" + txtCodCliente.Text + "', 'Historial', 'location=0,titlebar=0,resizable=1,scrollbars=1,left=100,top=300,height=600,width=800,location=0,menubar=0,toolbar=0');" +
        //    "</script>";

        //    Page.RegisterStartupScript("vh", strHistorial);
        //}
    }
    protected void lnkZonaInsegura_Click(object sender, EventArgs e)
    {
        String strHistorial = "<script>" +
        "window.open('verHistorial.aspx?zi=1&id=" + txtCodCliente.Text + "', 'Historial', 'titlebar=0,resizable=1,scrollbars=1,left=100,top=300,height=600,width=800,location=0,menubar=0,toolbar=0');" +
        "</script>";
        Page.RegisterStartupScript("zi", strHistorial);
    }
    protected void lnkOnsitesDelA�o_Click(object sender, EventArgs e)
    {
        //if (!lnkOnsitesDelA�o.Text.Contains("[0]"))
        //{
        //    String strHistorial = "<script>" +
        //    "window.open('verHistorial.aspx?year=1&id=" + txtCodCliente.Text + "', 'Historial', 'titlebar=0,resizable=1,scrollbars=1,left=100,top=300,height=600,width=800,location=0,menubar=0,toolbar=0');" +
        //    "</script>";
        //   Page.RegisterStartupScript("va", strHistorial);
        //}
      
    }
    protected void btEstados_Click(object sender, EventArgs e)
    {
        if (btEstados.Text == "-")
        {
            ddlNuevoEstado.Enabled = true;
            ddlNuevoEstado.Items.Clear();
            ddlNuevoEstado.DataBind();
            btEstados.Text = "+";
        }
        else
        {
            cargarEstadosRestantes();
            btEstados.Text = "-";
        }
        

    }
    protected void ddEspecialista_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void encuesta_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {

    }
    protected void lnvercliente_Click(object sender, EventArgs e)
    {

    }
    protected void imgBuscarEquipo_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (!validamail.validar_Mail(txtEmailAvast.Text.Trim())) { lblEmail.Text = "email invalido"; return; }
        else
        {
            FileStream fs1 = new FileStream(Server.MapPath("Images/logoAvast.png"), FileMode.Open, FileAccess.Read);
            Attachment att1 = new Attachment(fs1, "logoAvast.png", System.Net.Mime.MediaTypeNames.Application.Octet);
            att1.ContentId = "imagen1";
            FileStream fs2 = new FileStream(Server.MapPath("Images/imagenAvast.png"), FileMode.Open, FileAccess.Read);
            Attachment att2 = new Attachment(fs2, "logoAvast.png", System.Net.Mime.MediaTypeNames.Application.Octet);
            att2.ContentId = "imagen2";

            String cuerpo = leehtml.readhtm(Server.MapPath("cuerpo.html"));

            FileStream fs3 = new FileStream(Server.MapPath("Images/logomeganuevo.jpg"), FileMode.Open, FileAccess.Read);
            Attachment logo = new Attachment(fs3, "logomeganuevo.jpg", System.Net.Mime.MediaTypeNames.Application.Octet);
            logo.ContentId = "logo";
            try
            {
                if (mailmega2.enviamail("megarobot@megatech.la", txtEmailAvast.Text.Trim(), "megarobot@megatech.la", "REPARAR PROBLEMA AVAST", cuerpo, logo, att1, att2)) { lblEmail.Text = "Email enviado correctamente"; }
                else { lblEmail.Text = "Error en el env�o."; }
            }
            catch (Exception ex) { lblEmail.Text = "Error en el env�o." + ex.Message; }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Usuario usr = (Usuario)Session["Usuario"];
        if (!VerificarEnvioTecnico()) {
            lblError.Text = "Debe seleccionar Motivo por el cual se envia tecnico.";
            return;
        }

        sincronizarAreaEspecialista();
        sincronizarEstadoArea();
        sincronizarAreaEstado();
        if (txtDetalle.Text == "") { lblError.Text = "Primero debe cargar el formulario del equipo"; return; }
        if (esEstadoOnsite() && !validarDatosOnsite()) { return; }
        if (RDTSolucion.IsEmpty && RDTSolucion.Visible && RDTSolucion.Enabled && esEstadoCierre())
        {

            lblError.Text = "Primero debe ingresar la fecha de soluci�n";

            return;
        }
        
        if (ddlNuevoEstado.SelectedValue.ToString() == "113")
        {
            // Verifico si se hizo la encuesta
            if (!Encuestaok())
            {

                lblError.Text = "Primero debe completar la encuesta";

                return;
            }
        }
        if (detectaryCorregirArea())
        {
            if (esAreaCalle())
            {
                lblError.Text = "Se han modificado el AREA y ESTADO del ticket, de acuerdo la zona de residencia del cliente. Indique las franjas horarias.";
                mostrarCamposOnsite();
            }
            else if (esAreaCAS())
            {
                generoTicket();
                lblError.Text = "Se han modificado el AREA y ESTADO del ticket, de acuerdo a la zona de residencia del cliente.";
            }
        }
        else
        {
            lblError.Text = "";
            //if (esAreaCalle() && lstHorarios.SelectedItem == null) lblError.Text = "Aun no se han guardado los cambios. Indique las franjas horarias";
            //else
            //{
                if (ddEspecialista.SelectedItem.Text == "SIN ESPECIALISTA")
                {
                    //Usuario usr = (Usuario)Session["Usuario"];
                    ddEspecialista.SelectedValue = Funciones.getScalar("SELECT especialistaId FROM especialistaUsuario WHERE usuarioId =" + usr.UsuarioId).ToString();
                }
                registrarEspecialistayNegocio();
                //if (esAreaCalle()) registrarHorarios();
                if (esAreaOnsite()) { registrarTecnicos(); registrarFechas(); }
                generoTicket();
            //}
        }
    }

    protected void ddMotivo_Derivacion_Tec(object sender, EventArgs e) 
    {
 
    }
    #endregion

    #region METODOS DE CARGA
    private void cargarFechasOnsite()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;

        CMD.CommandText = "SELECT fechaCarga,fechaSolucion,fechaCoordinada FROM tickettecnico WHERE ticketId=" + txtTicket.Text;
        RS = CMD.ExecuteReader();
        RS.Read();

        if (RS.HasRows)
        {
            if (!RS.IsDBNull(0)) RDTCarga.SelectedDate = DateTime.Parse(RS[0].ToString());
            if (!RS.IsDBNull(1)) RDTSolucion.SelectedDate = DateTime.Parse(RS[1].ToString());
            if (!RS.IsDBNull(2)) RDTCoordinada.SelectedDate = DateTime.Parse(RS[2].ToString());
        }
        RS.Close();
        SQL.Close();
    }
    private void cargarEstadosRestantes()
    {
        Usuario usr = (Usuario)Session["Usuario"];
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;

        CMD.CommandText = "select EstadoId,nombre from estado where fechabaja is null";

        RS = CMD.ExecuteReader();
        ddlNuevoEstado.Items.Clear();
        ddlNuevoEstado.Items.Add("[Ninguno]");
        ddlNuevoEstado.Items[ddlNuevoEstado.Items.Count - 1].Value = "0";
        while (RS.Read())
        {
            ddlNuevoEstado.Items.Add(RS[1].ToString());
            ddlNuevoEstado.Items[ddlNuevoEstado.Items.Count - 1].Value = RS[0].ToString();
        }
        SetFocus(ddlNuevoEstado);
        RS.Close();
        SQL.Close();
    }
    private void LlenaUbicacionContacto()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;

        CMD.CommandText = "select UBI.ubicacionid, UBICON.ubicacioncontactoid FROM ubicacion UBI inner join ubicacioncontacto UBICON on UBICON.ubicacionid = UBI.ubicacionid inner join ticket TK on TK.ubicacioncontactoid = UBICON.ubicacioncontactoid WHERE TK.clienteid = '" + txtCodCliente.Text + "'";

        RS = CMD.ExecuteReader();

        RS.Read();


        //YA TENEMOS EL NOMBRE DEL CONTACTO ASOCIADO AL CLIENTE, LO SELECCIONAMOS EN EL COMBO BOX
        ddUbicacion.SelectedValue = RS[0].ToString();

        //AHORA SELECCIONAMOS EL CONTACTO 
        ddContacto.SelectedValue = RS[1].ToString();
        RS.Close();
        SQL.Close();
    }
    private void registrarScripts()
    {
        String strCode1 = "<script> " +
    "function habilitar() " +
    "{ if (document.getElementById('ctl00_cphTicket_validSummary').innerHTML != '') {document.getElementById('ctl00_cphTicket_btnSubmit').disabled = false;}}" + Environment.NewLine +
    "</script>";
        Page.RegisterStartupScript("habilitarBoton", strCode1);
        //ClientScript.RegisterStartupScript(this.GetType(),"habilitarBoton", strCode1);

        String strCode2 = "<script> " +
        "function deshabilitar() " +
        "{ document.getElementById('ctl00_cphTicket_btnSubmit').setAttribute('disabled',true);" + Environment.NewLine +
        " var t= setTimeout(\"habilitar()\", 1000); " +
        " } </script>";
       // ClientScript.RegisterStartupScript(this.GetType(), "deshabilitarBoton", strCode2);

            Page.RegisterStartupScript("deshabilitarBoton", strCode2);

    }
    private void loadScripts()
    {
        if (!ClientScript.IsClientScriptBlockRegistered("dlgBusqueda"))
        {

            string title = "Busqueda Cliente";
            string campoid = this.txtCodCliente.ClientID;
            string campodescrip = this.txtNomCliente.ClientID;
            string parameters = "&campoid=" + campoid + "&campodescrip=" + campodescrip;
            string height = "420";
            string width = "560";
            string page = "busquedaCliente.aspx";

            // use next line for direct with <base target="_self"> between <Head> and </HEAD> 
            string scrp = "<script>var rc = new Array(0,0);" +
               "function doIt(){rc=window.showModalDialog('" + page + "?Title=" + title + "&page=" + page +
               "&campoid=" + campoid + "&campodescrip=" + campodescrip + "','','dialogHeight:" + height +
               " px;dialogWidth:" + width + " px;');" + "\n";

            scrp += "if(rc!=undefined) {if(rc[0]!=null){document.getElementById('" + txtCodCliente.ClientID + "').innerText=rc[0];" +
            "document.getElementById('" + txtNomCliente.ClientID + "').innerText=rc[1];" +
            "__doPostBack('','');}}}</script>";

            ClientScript.RegisterStartupScript(this.GetType(), "dlgBusqueda", scrp);
            this.imgBuscar.Attributes.Add("onClick", "doIt();");
        }


        if (!ClientScript.IsClientScriptBlockRegistered("dlgBusquedaEquipo"))
        {
            string title = "Busqueda Equipo";
            string campoid = this.txtCodEquipo.ClientID;
            string campodescrip = this.txtEquipoId.ClientID;
            string parameters = "&campoid=" + campoid + "&campodescrip=" + campodescrip;
            string height = "420";
            string width = "560";
            string page = "busquedaEquipo.aspx";

            // use next line for direct with <base target="_self"> between <Head> and </HEAD> 
            string scrp = "<script>var rc = new Array(0,0);" +
                   "function doItEquipo(){rc=window.showModalDialog('" + page + "?Title=" + title + "&page=" + page +
                   "&campoid=" + campoid + "&campodescrip=" + campodescrip + "','','dialogHeight:" + height +
                   " px;dialogWidth:" + width + " px;');" + "\n";

            scrp += "if(rc!=undefined) {if(rc[0]!=null){document.getElementById('" + txtCodEquipo.ClientID + "').innerText=rc[0];" +
                "document.getElementById('" + txtEquipoId.ClientID + "').innerText=rc[1];" +
                "__doPostBack('','');}}}</script>";

            //Page.RegisterStartupScript("dlgBusqueda", scrp);
            ClientScript.RegisterStartupScript(this.GetType(), "dlgBusquedaEquipo", scrp);
            this.imgBuscarEquipo.Attributes.Add("onClick", "doItEquipo();");
        }

        //if (!ClientScript.IsClientScriptBlockRegistered("hacerencuesta")) // Agrega la funcion de modal la encuesta
        //{

        //    // use next line for direct with <base target="_self"> between <Head> and </HEAD> 
        //    string scrp = "<script> function hacerencuesta() {window.open('Encuesta.aspx?id=" + Request.QueryString["id"].ToString().Trim() + "');} </script>";

        //    //Page.RegisterStartupScript("dlgBusqueda", scrp);
        //    ClientScript.RegisterStartupScript(this.GetType(), "hacerencuesta", scrp);
        //    this.btn_encuesta.Attributes.Add("onClick", "hacerencuesta();");

        //}

        //if (!ClientScript.IsClientScriptBlockRegistered("editacliente"))
        //{
        //    string id = "editacliente";
        //    string script = "<script> function VerCliente() {window.open('vercliente.aspx?codcte= " + txtCodCliente.Text + "');} </script>";
        //    ClientScript.RegisterStartupScript(this.GetType(), id, script);

        //    this.lnvercliente.Attributes.Add("onClick", "VerCliente();");
        //}
    }
    private void cargaEspecialista()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        String tktId = Request.QueryString["id"];

        CMD.Connection = SQL;
        CMD.CommandText = "SELECT EspecialistaId, '[' + CAST(EspecialistaId As VARCHAR) + '] ' + Nombre As Nombre From Especialista (nolock) Where FechaBaja Is Null OR " +
            " especialistaId IN (SELECT especialistaId FROM ticket Where ticketId=" + tktId + ") ORDER BY EspecialistaId";
        RS = CMD.ExecuteReader();

        while (RS.Read())
        {
            //CARGAMOS LOS DATOS EN EL COMBOBOX
            ddEspecialista.Items.Add(new Telerik.Web.UI.RadComboBoxItem(RS[1].ToString(), RS[0].ToString()));
        }

        RS.Close();
        //DETERMINAMOS EL ESPECIALISTA ACTUAL
        CMD.CommandText = "SELECT especialistaid FROM ticket WHERE ticketid = " + Request.QueryString["id"].ToString();
        object SelValue = CMD.ExecuteScalar();
        ddEspecialista.SelectedValue = SelValue.ToString();
              
        SQL.Close();
    }
    private void cargaEmpresaPartner()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT EmpresaPartnerId,Nombre FROM empresapartner";
        RS = CMD.ExecuteReader();

        while (RS.Read())
        {
            //CARGAMOS LOS DATOS EN EL COMBOBOX
            ddEmpresaPartner.Items.Add(RS[1].ToString());
            ddEmpresaPartner.Items[ddEmpresaPartner.Items.Count - 1].Value = RS[0].ToString();
        }

        RS.Close();
        //DETERMINAMOS EL ESPECIALISTA ACTUAL
        CMD.CommandText = "SELECT empresapartnerid FROM ticket WHERE ticketid = " + Request.QueryString["id"].ToString();
        object SelValue = CMD.ExecuteScalar();
        ddEmpresaPartner.SelectedValue = SelValue.ToString();
        SQL.Close();
    }
    private void cargaTipoProblema()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;
        Usuario usr = (Usuario)Session["Usuario"];

        CMD.CommandText = "SELECT idtipoproblema,Nombre FROM dbo.tiposDeProblema(" + usr.Perfil + "," + Request.QueryString["id"] + ")";
        RS = CMD.ExecuteReader();

        while (RS.Read())
        {
            //CARGAMOS LOS DATOS EN EL COMBOBOX
            ddTipoProblema.Items.Add(RS[1].ToString());
            ddTipoProblema.Items[ddTipoProblema.Items.Count - 1].Value = RS[0].ToString();
        }

        RS.Close();
        //DETERMINAMOS EL ESPECIALISTA ACTUAL
        CMD.CommandText = "SELECT idtipoproblema FROM ticket WHERE ticketid = " + Request.QueryString["id"].ToString();
        object SelValue = CMD.ExecuteScalar();
        ddTipoProblema.SelectedValue = SelValue.ToString();
        SQL.Close();
    }
    private void cargarTecnicos()//Muestra los tecnicos o CAS correspondientes al area actual
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;

        CMD.CommandText = "SELECT id,Nombre,AreaId FROM tecnico WHERE (fechabaja is null and AreaId ='" + ddEmpresaPartner.SelectedValue + "')"+
            " OR id IN(select tecnicoId from ticketTecnico where ticketId=" + Request.QueryString["id"].ToString() +") order by Nombre";
        RS = CMD.ExecuteReader();
        
        ddTecnico.Items.Clear();
        ddTecnico.Items.Add(new Telerik.Web.UI.RadComboBoxItem("A�n no asignado", "0"));
        while (RS.Read())
        {
            //CARGAMOS LOS DATOS EN EL COMBOBOX
            ddTecnico.Items.Add(new Telerik.Web.UI.RadComboBoxItem(RS[1].ToString(), RS[0].ToString()));
        }
        ddTecnico.SelectedValue = "0";
        RS.Close();
        SQL.Close();
    }
    private void cargarTecnicoActual() //Selecciona el tecnico o CAS definido para el ticket actual
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT tt.TecnicoId, t.AreaId FROM ticketTecnico tt left join tecnico t on t.id=tt.tecnicoId WHERE ticketId='" + txtTicket.Text + "' AND t.AreaId='" + ddEmpresaPartner.SelectedValue + "'";

        RS = CMD.ExecuteReader();
        RS.Read();

        if (RS.HasRows && !RS.IsDBNull(0))
        {
                ddTecnico.SelectedValue = RS[0].ToString();
        }
        RS.Close();
        SQL.Close();
    }
    private void cargarHorarios()//Muestra opciones de franjas horarias predefinidas en la base de datos
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT id,descripcion FROM franjaHoraria";
        RS = CMD.ExecuteReader();
        lstHorarios.Items.Clear();
        while (RS.Read())
        {
            //CARGAMOS LOS DATOS EN EL COMBOBOX
            lstHorarios.Items.Add(RS[1].ToString());
            lstHorarios.Items[lstHorarios.Items.Count - 1].Value = RS[0].ToString();
        }
        RS.Close();
        SQL.Close();
    }
    private void cargarHorarioActual()//Selecciona la/s franja/s horaria/s definida/s para el ticket actual
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT horario1,horario2,horario3 FROM ticketTecnico WHERE ticketId='" + txtTicket.Text + "'";
        RS = CMD.ExecuteReader();
        RS.Read();
        if (RS.HasRows)
        {
            Boolean h1 = RS[0].ToString().CompareTo("1") == 0;
            Boolean h2 = RS[1].ToString().CompareTo("1") == 0;
            Boolean h3 = RS[2].ToString().CompareTo("1") == 0;
            if (h1) { lstHorarios.Items[0].Selected = true; }
            if (h2) { lstHorarios.Items[1].Selected = true; }
            if (h3) { lstHorarios.Items[2].Selected = true; }
        }
        RS.Close();
        SQL.Close();
    }
    private int loadContactos(string UbicacionId)
    {
        try
        {
            int ret = 0;
            System.Data.DataSet ds = Clientes.getContactosUbicacion(UbicacionId);
            if (ds == null)
            {
                lblError.Text = "No se recuperaron Contactos para la Ubicaci�n.";
                ret = -1;
            }
            else
            {

                ddContacto.Items.Clear();
                ret = ds.Tables[0].Rows.Count;
                foreach (System.Data.DataRow row in ds.Tables[0].Rows)
                {
                    System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem(row["Nombre"].ToString(), row["Id"].ToString());
                    ddContacto.Items.Add(li);
                }
            }
            return ret;
        }
        catch
        {
            return -1;
        }
    }
    private int loadUbicaciones(string ClienteID)
    {
        try
        {
            int ret = 0;
            System.Data.DataSet ds = Clientes.getUbicacionesCliente(ClienteID);

            if (ds == null)
            {
                lblError.Text = "No se recuperaros Ubicaciones para el cliente.";
                ret = -1;
            }
            else
            {

                ddUbicacion.Items.Clear();
                ret = ds.Tables[0].Rows.Count;
                foreach (System.Data.DataRow row in ds.Tables[0].Rows)
                {
                    System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem(row["Nombre"].ToString(), row["Id"].ToString());
                    ddUbicacion.Items.Add(li);
                }
            }
            return ret;
        }
        catch
        {
            return -1;
        }
    }
    private void SelectCurrentContacto()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;
        try
        {

            SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
            SQL.Open();

            CMD.Connection = SQL;

            CMD.CommandText = "select UBICON.ubicacioncontactoid,UBI.ubicacionid from ubicacioncontacto UBICON inner join ubicacion UBI on UBI.ubicacionid = UBICON.ubicacionid where ubicacioncontactoid = (select ubicacioncontactoid from ticket where ticketid = " + Request.QueryString["id"] + ")";
            RS = CMD.ExecuteReader();
            RS.Read();

            //YA TENEMOS AMBOS DATOS SELECCIONAMOS EL PRIMERO
            if (ddUbicacion.SelectedValue != RS[1].ToString())
            {
                ddUbicacion.SelectedValue = RS[1].ToString();
            }
            else
            {
                //SE SUPONE QUE YA ESTA SELECCIONADO LA UBICACION AHORA SELECCIONAMOS EL CONTACTO
                ddContacto.SelectedValue = RS[0].ToString();
            }
            RS.Close();
            SQL.Close();

        }
        catch (Exception ex)
        {
            if (SQL.State != ConnectionState.Closed) SQL.Close();
            lblError.Text = "Error cargando contacto";
        }
    }

    public void CargaMotivo ()
    {
        if (lblIdEstadoActual.Text == "162")
        {
            System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
            System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();


            try
            {
                string Motivo_Env_Tec_ID;
                string Motivo_Env_Tec_Descripcion;
                SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
                SQL.Open();

                CMD.Connection = SQL;

                CMD.CommandText = "SELECT ID_motivo FROM ticket_MOT_ENV_TEC WHERE TicketId= '" + Request.QueryString["id"] + "'";
                CMD.ExecuteScalar();
                if (null == CMD.ExecuteScalar()) return;
                Motivo_Env_Tec_ID = Convert.ToString(CMD.ExecuteScalar());

                CMD.CommandText = "SELECT Descripcion FROM motivoDerivacion WHERE ID_DerivadoTecnico= " + Motivo_Env_Tec_ID;
                CMD.ExecuteScalar();
                if (null == CMD.ExecuteScalar()) return;
                Motivo_Env_Tec_Descripcion = Convert.ToString(CMD.ExecuteScalar());
                Motivo_Env_Tec_Descripcion = Motivo_Env_Tec_Descripcion.ToUpper();

                SQL.Close();
                DropMotivo.SelectedValue = Motivo_Env_Tec_ID;
                lbl_Motivo_Tec_2.Text = Motivo_Env_Tec_Descripcion;
                lbl_Motivo_Tec_1.Visible = true;
                lbl_Motivo_Tec_2.Visible = true;


            }
            catch (Exception ex)
            {
                if (SQL.State != ConnectionState.Closed) SQL.Close();
                lblError.Text = "Error cargando Motivo de envio de tecnico";
                lblError.Text += "Tipo de ERROR:" + ex.Message.ToString();
            }
       }    else
        {

            lbl_Motivo_Tec_1.Visible = false;
            lbl_Motivo_Tec_2.Visible = false;
            DropMotivo.SelectedIndex = 0;

        }
    }
    #endregion

    #region ACCIONES DE LA PAGINA
    private void DeshabilitaFormulario()
    {
        //ESTA FUNCION CORROBORA SI UN TICKET SE ENCUENTRA CERRADO Y EN CASO DE SER ASI EVITA LAS MODIFICACIONES
        if (CequearEstado())
        {
            txtNroClaim.ReadOnly = true;
            ddEmpresaPartner.Enabled = false;
            txtCodCliente.Enabled = false;
            ddUbicacion.Enabled = false;
            ddContacto.Enabled = false;
            txtCodEquipo.ReadOnly = true;
            txtSerie.ReadOnly = true;
            ddEspecialista.Enabled = false;
           // ddTipoProblema.Enabled = false;
            ddPrioridad.Enabled = false;
            //txtDetalle.ReadOnly = true;
            ddlNuevoEstado.Enabled = false;
            txtInformeCliente.ReadOnly = true;
            txtFechaInicio.ReadOnly = true;
            mostrarCamposOnsiteSi("0", "0", "0", "0", "0", "0");
            RDTCarga.Enabled = false;
            RDTSolucion.Enabled = false;
            RDTCoordinada.Enabled = false;
            btn_encuesta.Enabled = false;
            btn_encuesta.Visible = false;
            //ddSistemaOperativo.Enabled = false;
            //chkSOModificado.Enabled = false;
            //arquitectura.Enabled = false;
            //servicePack.Enabled = false;
            dpFechaRecontactar.Enabled = false;
            ddMotivoRecontacto.Enabled = false;
            btviatico.Enabled = true;
        
        }
        else
        {
            txtNroClaim.ReadOnly = false;
            ddEmpresaPartner.Enabled = true;
            txtCodCliente.Enabled = true;
            ddUbicacion.Enabled = true;
            ddContacto.Enabled = true;
            txtCodEquipo.ReadOnly = false;
            txtSerie.ReadOnly = false;
            ddEspecialista.Enabled = true;
            //ddTipoProblema.Enabled = true;
            ddPrioridad.Enabled = true;
            //txtDetalle.ReadOnly = false;
            ddlNuevoEstado.Enabled = true;
            txtInformeCliente.ReadOnly = false;
            //ddSistemaOperativo.Enabled = true;
            //chkSOModificado.Enabled = true;
            //arquitectura.Enabled = true;
            //servicePack.Enabled = true;
            btviatico.Enabled = true;
           
          }
    }
    protected void corregirArea(String s)
    {
        if (s == "GBA")
        {
            if (ddlNuevoEstado.Items.FindByValue("135") == null)
            {
                ddlNuevoEstado.Items.Add("GES ONSITE CALLE");
                ddlNuevoEstado.Items[ddlNuevoEstado.Items.Count - 1].Value = "135";
            }
            ddlNuevoEstado.SelectedValue = "135";
        }
        else if (s == "NOGBA")
        {
            if (ddlNuevoEstado.Items.FindByValue("117") == null)
            {
                ddlNuevoEstado.Items.Add("GES ONSITE CAS");
                ddlNuevoEstado.Items[ddlNuevoEstado.Items.Count - 1].Value = "117";
               

            }
            ddlNuevoEstado.SelectedValue = "117";
        }
        sincronizarEstadoArea();
    }
    protected Boolean detectaryCorregirArea()
    {
        String area = getArea();
        if (area == "GBA" && ddEmpresaPartner.SelectedValue == "58") { corregirArea("GBA"); return true; }
        else if (area == "NOGBA" && ddEmpresaPartner.SelectedValue == "57") { corregirArea("NOGBA"); return true; }
        return false;
    }// detecta el area a la que corresponde la localidad y la corrige. Retorna true si el area fue corregida

    protected void sincronizarAreaEspecialista()
    {

        Usuario usr = (Usuario)Session["Usuario"];
        string PerfilID = usr.Perfil;
        string EmpresaPartnerID;
        SqlConnection SQL = new SqlConnection();
        SqlCommand CMD = new SqlCommand();
        SQL.ConnectionString = ConfigurationManager.ConnectionStrings["ticketDB"].ToString();
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT EmpresaPartnerId FROM NIVELES Where PerfilID =" + PerfilID;

        SQL.Open();
        if (null == CMD.ExecuteScalar()) return; // Si retorna null desde NIVELES el perfil es de Supervisor, no aplica modificacion
        EmpresaPartnerID = Convert.ToString(CMD.ExecuteScalar()); 
        SQL.Close();
        ddEmpresaPartner.SelectedValue = EmpresaPartnerID;// Verifica si es Operador y si lo es, Fuerza el Area segun perfil del usuario.
        sincronizarEspecialista();

    } // Si EXISTE PerfilID en tabla, Actualiza el Estado al perfil asociado

    protected void sincronizarEspecialista()
    {
        Usuario usr = (Usuario)Session["Usuario"];
        ddEspecialista.SelectedValue = usr.EspecialistaID;



    } // Actualiza Especialista al Especialista Actual
    protected bool VerificarEnvioTecnico() {
        if (ddlNuevoEstado.SelectedValue == "162" && DropMotivo.SelectedValue == "0")
        {

            return false;

        }
        if (ddlNuevoEstado.SelectedValue != "162" && ddlNuevoEstado.SelectedIndex != 0 )
        {

            DropMotivo.SelectedIndex = 0;
            return true;
        }
               
    return true;
    }
    protected void AbrirPopupEnvioTec()
    {
        if (ddlNuevoEstado.SelectedValue == "162") {


            string script = "function abrirPop() {$('#myModal').modal('show');}";
            //string script = "function abrirPop() {AbrirPopup('popupDerivadoTec.aspx', 'popup'); return false;}";
            Page.ClientScript.RegisterStartupScript(GetType(), "msgbox", script, true);
            //Page.RegisterStartupScript("AbrirPopUP", script);
            ClientScript.RegisterStartupScript(this.GetType(), "mostrarMensaje", "abrirPop();", true);
            //string script = "AbrirPopup('popupDeriv_Tecnico.aspx?id=" + _idTicket + "','popup'); return false;";
           //ScriptManager.RegisterStartupScript(this, GetType(), "popup", script, true) ;
        
        }
        else


        return;
    }

    protected void sincronizarEstadoArea()
    {
     


        Usuario usr = (Usuario)Session["Usuario"];
       

        if (ddlNuevoEstado.SelectedValue == "117") { ddEmpresaPartner.SelectedValue = "58"; return; }
        if (ddlNuevoEstado.SelectedValue == "135") { ddEmpresaPartner.SelectedValue = "57"; return; }
        List<string> estadosReclamo= new List<string> {"118","119","123","155","0","137"};
       
        if (!estadosReclamo.Contains(ddlNuevoEstado.SelectedValue.Trim()))
        {
//////////////////////-->     if (esOperador()) ddEmpresaPartner.SelectedValue = "54";  // Verifica si es Operador y si lo es, Fuerza el Area al Basico.
        }
        return;
    } // corrige Area en funcion del estado seleccionado
    protected void sincronizarAreaEstado()
    {
        if (ddEmpresaPartner.SelectedValue.CompareTo("58") == 0 && ddlNuevoEstado.Items.FindByValue("117") != null) ddlNuevoEstado.SelectedValue = "117";
        if (ddEmpresaPartner.SelectedValue.CompareTo("57") == 0 && ddlNuevoEstado.Items.FindByValue("135") != null) ddlNuevoEstado.SelectedValue = "135";
    } // corrige Estado en funcion del Area seleccionada
    private void ActualizarDatosHistorial()
    {
        SqlConnection SQL = new SqlConnection();
        SqlCommand CMD = new SqlCommand();
        SQL.ConnectionString = ConfigurationManager.ConnectionStrings["ticketDB"].ToString();
        CMD.Connection = SQL;
        SqlDataReader RS;
        SQL.Open();
        CMD.CommandText = "SELECT 1,'Historial de Casos [' + CAST (count(*) AS VARCHAR) + ']' as Cant FROM viewHistorial WHERE RTRIM(ClienteId) = '" + txtCodCliente.Text.TrimEnd() + "' UNION " +
                            "SELECT 2,'Visitas Realizadas en '+ CAST(YEAR(GETDATE()) AS VARCHAR) + ' ['+ CAST(count(*) AS VARCHAR)+']' as Cant FROM viewHistorial vh2 WHERE RTRIM(ClienteId) = '" + txtCodCliente.Text.TrimEnd() + "' AND YEAR(Fecha)=YEAR(GETDATE()) AND RTRIM(Estado)= 'VISITA REALIZADA' UNION " +
                            "SELECT 3,CAST(count(*) as VARCHAR) As Cant FROM viewHistorial vh3 WHERE RTRIM(ClienteId) = '" + txtCodCliente.Text.TrimEnd() + "' AND RTRIM(Estado)= 'ZONA INSEGURA'";
        RS = CMD.ExecuteReader();
        RS.Read();
        if (RS.IsDBNull(1) | RS[1].ToString() == "0") { SQL.Close(); return; }
        lnkHistorial.Text = RS[1].ToString();
        RS.Read();
        lnkOnsitesDelA�o.Text = RS[1].ToString();
        RS.Read();
        if (RS.IsDBNull(1) | RS[1].ToString() == "0") { SQL.Close(); return; }
        lnkZonaInsegura.Visible = RS[1].ToString() != "0";
        RS.Close();
        SQL.Close();
    

    }
    private TicketData getDataFromPantalla()
    
    {
        
        unTicket.idTicket = int.Parse(txtTicket.Text);

        unTicket.idEstado = int.Parse(ddlNuevoEstado.SelectedValue);

        unTicket.idCliente = txtCodCliente.Text;

        unTicket.CodigoEquipo = txtCodEquipo.Text;

        unTicket.idUbicacionContacto = int.Parse(ddContacto.SelectedValue);

        unTicket.idEspecialista = int.Parse(ddEspecialista.SelectedValue);

        unTicket.idTipoProblema = int.Parse(ddTipoProblema.SelectedValue);

        unTicket.idEmpresaPartner = int.Parse(ddEmpresaPartner.SelectedValue);

        unTicket.nroClaim = txtNroClaim.Text;

        unTicket.NroSerie = txtSerie.Text;

        unTicket.Prioridad = ddPrioridad.Text;

        unTicket.InformeCliente = txtInformeCliente.Text;

        unTicket.InformeInterno = txtAgregar.Text;

        unTicket.Problema = txtDetalle.Text;

        unTicket.idArticulo = int.Parse(txtEquipoId.Text);

        unTicket.popMotivo_Tec = int.Parse(DropMotivo.SelectedValue); //popMotivo_Tec
                

        if (unTicket.idEstado == 0)
            unTicket.idEstado = int.Parse(lblIdEstadoActual.Text);

        try
        {
            Usuario u = (Usuario)Session["Usuario"];
            unTicket.idUsuario = int.Parse(u.UsuarioId);
        }
        catch
        {
            unTicket.idUsuario = 0;
        }
        //if (ddSistemaOperativo.SelectedValue!="-1")
        //{
        //    unTicket.sistemaOperativoId = int.Parse(ddSistemaOperativo.SelectedValue);
        //    unTicket.arquitecturaId = int.Parse(arquitectura.SelectedValue);
        //    if (servicePack.SelectedValue != "") unTicket.servicePackId = int.Parse(servicePack.SelectedValue);
        //    unTicket.modificado = chkSOModificado.Checked;
        //}      
             
        unTicket.idMotivoRecontacto = int.Parse(ddMotivoRecontacto.SelectedValue);

        if (!dpFechaRecontactar.IsEmpty)
        {

            unTicket.recontactarDesde = (DateTime)dpFechaRecontactar.SelectedDate;
        }
        else {
            unTicket.recontactarDesde = DateTime.Now;
        }

        

        return unTicket;
    }
    private void putPanelMensaje(bool bOk)
    {
        if (bOk)
        {
            panelMensaje.Visible = true;
            panelTicket.Visible = false;
        }
        else
        {
            panelMensaje.Visible = false;
            panelTicket.Visible = true;
        }

    }
    private void pasoPantalla( App_Code.TicketsLib.TicketData tkt)
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        try
        {
            txtFecha.Text = tkt.FechaP;
            txtFechaInicio.Text = tkt.FechaInicioScreen;
            txtFechaFin.Text = tkt.FechaFinScreen;
            txtNroClaim.Text = tkt.nroClaim;
            txtTicket.Text = tkt.idTicket.ToString();
            lblNegocio.Text = tkt.Negocio;
            txtCodCliente.Text = tkt.CodigoCliente;
            txtNomCliente.Text = tkt.NombreCliente;
            lblUbicacion.Text = tkt.Ubicacion;
            lblContacto.Text = tkt.Contacto;
            txtCodEquipo.Text = tkt.CodigoEquipo;
            TxtNombreEquipo.Text = tkt.NombreEquipo;
            txtSerie.Text = tkt.NroSerie;

            lblEspecialista.Text = tkt.Especialista;
            lbltxtVendedor.Text = tkt.Vendedor;
            //lblTipoProblema.Text = tkt.TipoProblema;
            ddPrioridad.SelectedValue = tkt.Prioridad;
            lblEstado.Text = tkt.Estado;
            lblIdEstadoActual.Text = tkt.idEstado.ToString();
            txtDetalle.Text = tkt.Problema;
            txtInformeCliente.Text = tkt.InformeCliente;
            txtInformeInterno.Text = tkt.InformeInterno;
            txtAgregar.Text = "";
            //ddSistemaOperativo.SelectedValue = tkt.sistemaOperativoId.ToString();
            //if (tkt.arquitecturaId!=0) arquitectura.SelectedValue = tkt.arquitecturaId.ToString();
            //if (tkt.servicePackId!=0) servicePack.SelectedValue = tkt.servicePackId.ToString();
            //chkSOModificado.Checked = tkt.modificado;
            //RadComboBox1_SelectedIndexChanged(this, new Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs(ddSistemaOperativo.Text,ddSistemaOperativo.SelectedValue,"",""));
            if (tkt.idMotivoRecontacto != 0)
            {
                dpFechaRecontactar.SelectedDate = tkt.recontactarDesde;
                ddMotivoRecontacto.SelectedValue = tkt.idMotivoRecontacto.ToString();
            }            

            SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
            SQL.Open();
            CMD.Connection = SQL;
            CMD.CommandText = "SELECT Equipoid FROM equipo WHERE codigo = '" + txtCodEquipo.Text.TrimEnd() + "'";
            object EquipoId = CMD.ExecuteScalar();
            SQL.Close();
            txtEquipoId.Text = EquipoId.ToString();

            if (tkt.idTipoProblema == 5)
            {
                btnliq.Visible = true;
            }

            if (tkt.idEstado == 162)
            {
                int exedido = Convert.ToInt16(Funciones.getScalar("select count(*) from localidad where exedido = 1 and nombre in (select rtrim(localidad)  from viewcliente where clienteid = " + tkt.idCliente + "  ) "));
                if (exedido > 0) { lblError.Text = "El tkt esta exedido en km de viaticos"; }
            }
        }
        catch (Exception ex)
        {
            if (SQL.State != ConnectionState.Closed) SQL.Close();
            logger.Error("Error en pasoPantalla: " + ex.Message, ex);
            lblError.Text = "Error al pasar a Pantalla: " + ex.Message;
        }
    }
    private void mostrarCamposOnsite()
    {
        if (esAreaOnsite() && areaEstadoCompatible())
        {
            System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
            System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
            System.Data.SqlClient.SqlDataReader RS;
            Usuario usr = (Usuario)Session["Usuario"];

            //NOS CONECTAMOS AL SERVIDOR SQL
            SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
            SQL.Open();
            //PREPARAMOS LA CONSULTA 
            CMD.Connection = SQL;
            CMD.CommandText = "SELECT * FROM estadoInfoOnsite WHERE EstadoId='" + estado() + "' AND PerfilId=" + usr.Perfil.ToString();
            RS = CMD.ExecuteReader();
            RS.Read();
            if (RS.HasRows)
            {
                String state = RS[1].ToString() + " - " + RS[2].ToString();
                String flags = RS[4].ToString() + RS[5].ToString() + RS[6].ToString() + RS[7].ToString() + RS[8].ToString() + RS[9].ToString();
                mostrarCamposOnsiteSi(RS[4].ToString(), RS[5].ToString(), RS[6].ToString(), RS[7].ToString(), RS[8].ToString(), RS[9].ToString());
            }
            else
            {
                if (esAreaCalle()) mostrarCamposOnsiteSi("0", "0", "0", "0", "1", "1");
                else mostrarCamposOnsiteSi("0", "0", "0", "0", "0", "0");
            }
            RDTCarga.Visible = true;
            RDTSolucion.Visible = true;
            if (esPerfil("25") || esPerfil("1") || esPerfil("20") || esPerfil("17")) { RDTCarga.Visible = true; lblFCarga.Visible = true; RDTCoordinada.Visible = true; RDTSolucion.Visible = true; lblFSolucion.Visible = true; lblFCoordinada.Visible = true; }
            else { RDTCarga.Visible = false; lblFCarga.Visible = false; lblFCoordinada.Visible = false; RDTCoordinada.Visible = false; RDTSolucion.Visible = false; lblFSolucion.Visible = false; }
            RS.Close();
            SQL.Close();
        }
        else
        {
            lblHorarios.Visible = false;
            lstHorarios.Visible = false;
            lblTecnico.Visible = false;
            ddTecnico.Visible = false;
            RDTCarga.Visible = false;
            RDTSolucion.Visible = false;
            RDTCoordinada.Visible = false;
            lblFCarga.Visible = false;
            lblFSolucion.Visible = false;
            lblFCoordinada.Visible = false;
        }
        if (esAreaCAS()) { lblFCoordinada.Visible = false; RDTCoordinada.Visible = false; }

    }
    //Modifica permisos de lectura/escritura de los campos onsite de acuerdo a los flags
    private void mostrarCamposOnsiteSi(String vt, String mt, String vc, String mc, String vh, String mh)
    {
        //defino qu� flags usar para luego otorgar permisos del campo Tecnico/CAS
        String vToC = vt;
        String mToC = mt;
        if (esAreaCalle()) { lblTecnico.Text = "T�cnico"; vToC = vt; mToC = mt; }
        if (esAreaCAS()) { lblTecnico.Text = "CAS"; vToC = vc; mToC = mc; }

        //damos los permisos se�alados por los flags recibidos por parametro
        if (vToC.CompareTo("1") == 0) { lblTecnico.Visible = true; ddTecnico.Visible = true; }
        else { lblTecnico.Visible = false; ddTecnico.Visible = false; }

        //if (vh.CompareTo("1") == 0) { lblHorarios.Visible = true; lstHorarios.Visible = true; }
        //else { lblHorarios.Visible = false; lstHorarios.Visible = false; }

        if (mToC.CompareTo("1") == 0) { ddTecnico.Enabled = true; }
        else { ddTecnico.Enabled = false; }

        //if (mh.CompareTo("1") == 0) { lstHorarios.Enabled = true; }
        //else { lstHorarios.Enabled = false; }

        if (!esAreaCalle()) { lblHorarios.Visible = false; lstHorarios.Visible = false; }
    }
    #endregion

    #region ACCIONES DEL NEGOCIO
    private void generoTicket()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();

        /*try
        {*/
        //CORROBORAMOS SI EL ESTADO DEL TICKET NO ES UN ESTADO DE CIERRE

        string sEstado = lblEstado.Text;

        sEstado = sEstado.Trim();

        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        
        //if (validarCampoSO()) { return; }
        if (!validarCamposRecontacto()) { return; }
       App_Code.TicketsLib.TicketData unTicket = getDataFromPantalla();
       //((Hashtable)Session["InfoTickets"])[_idTicket] = unTicket;
#region Link con MSC_Servicios
        // inserto el tkt en servicios si es gestionCas
        if (unTicket.idEmpresaPartner ==  58 )
            {
            
            // verificar si existe un tkt creado en msc para este caso.
              if (!ticketLinkeado(unTicket.idTicket))
                {
                    string nuevotktserv = tktservicios.insertar_tkt(txtCodCliente.Text, unTicket.idTicket);

                    if (nuevotktserv.Contains("ERROR:"))
                    {
                        lblMensajePanel.Text = "No se pudo derivar el tkt a cas";
                    }
                    else
                    {
                        string query = "insert ticketslinkeados (ticketidspd,ticketidservicios) values (" + unTicket.idTicket + "," + nuevotktserv + ")";
                        App_Code.TicketsLibVb.Funciones.execSql(query);
                        unTicket.InformeInterno = unTicket.InformeInterno + Environment.NewLine + " Se ha generado automaticamente el tkt nro:" + nuevotktserv + " en MSC servicios para gestion CAS";
                    }
                }
                else 
                {
                //que hacer cuando el tkt existe y se agrega informacion.
                    tktservicios.ActualizarDatosMSCServicios(unTicket.idTicket, unTicket.idEstado, unTicket.InformeInterno);
                    
                }
             }
        
#endregion

        unTicket = App_Code.TicketsLib.Tickets.EditTicket(unTicket);
        //((Hashtable)Session["InfoTickets"])[_idTicket] = unTicket;
        if (unTicket.mensajeError.Trim().Length == 0 && esAreaCAS() && ("[118][119][123]".Contains("[" + ddlNuevoEstado.SelectedValue + "]")))
        {
            notificarReclamoOnsite(getEmails("reclamoOnsiteCAS"));
        }
        if (unTicket.mensajeError.Trim().Length == 0 && esAreaCalle() && ("[118][119][123]".Contains("[" + ddlNuevoEstado.SelectedValue + "]")))
        {
            notificarReclamoOnsite(getEmails("reclamoOnsiteCalle"));
        }
        if (unTicket.mensajeError.Trim().Length == 0)
        {
            if (esAreaOnsite())
            {
                if (ddlNuevoEstado.SelectedValue == "137") { notificarVisitaAnulada(unTicket); }
                guardarTicketOnsite(txtTicket.Text);
            }
            Response.Redirect("EditaTicket.aspx?id=" + unTicket.idTicket.ToString());
        }
        else
        {
            lblError.Text = "Ticket no generado: " + unTicket.mensajeError;
        }
        /* }
         catch (Exception ex)
         {
             logger.Error("Error en generoTicket: " + ex.Message, ex);
             lblError.Text = "Error en generoTicket: " + ex.Message;
         }*/
    }

    private bool ticketLinkeado(int ticketId)
    {
        return Convert.ToInt16(SqlHelper.ExecuteScalar(Funciones.getConnString(), CommandType.Text, "select count(*) from ticketslinkeados where ticketidspd = " + ticketId)) > 0;
    }
    private void guardarTicketOnsite(String id) //Inserta o actualiza el ticket onsite en la base de datos
    {

        int h1 = 0;
        int h2 = 0;
        int h3 = 0;
        //if (esAreaCalle())
        //{
        //    if (lstHorarios.Items[0].Selected) { h1 = 1; }
        //    if (lstHorarios.Items[1].Selected) { h2 = 1; }
        //    if (lstHorarios.Items[2].Selected) { h3 = 1; }
        //}

        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA
        CMD.Connection = SQL;
        String iCoordinada = ",NULL";
        String iCarga = ",NULL";
        String iSolucion = ",NULL";
        String uCoordinada = ",fechaCoordinada=NULL";
        String uCarga = ",fechaCarga=NULL";
        String uSolucion = ",fechaSolucion=NULL";

        if (!RDTCoordinada.IsEmpty) { iCoordinada = "," + RDTCoordinada.SelectedDate.Value.ToString("dd/MM/yyyy HH:mm"); uCoordinada = ",fechaCoordinada='" + RDTCoordinada.SelectedDate.Value.ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!RDTCarga.IsEmpty) { iCarga = "," + RDTCarga.SelectedDate.Value.ToString("dd/MM/yyyy HH:mm"); uCarga = ",fechaCarga='" + RDTCarga.SelectedDate.Value.ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (!RDTSolucion.IsEmpty) { iSolucion = "," + RDTSolucion.SelectedDate.Value.ToString("dd/MM/yyyy HH:mm"); uSolucion = ",fechaSolucion='" + RDTSolucion.SelectedDate.Value.ToString("dd/MM/yyyy HH:mm") + "'"; }
        if (tieneTicketOnsite(txtTicket.Text))
        {
            CMD.CommandText = "UPDATE ticketTecnico SET tecnicoId='" + ddTecnico.SelectedValue +
                "',horario1='" + h1 + "',horario2='" + h2 + "',horario3='" + h3 + "'" + uCoordinada + uCarga + uSolucion + " WHERE ticketId='" + id + "'";
        }
        else
        {
            CMD.CommandText = "INSERT INTO ticketTecnico(tecnicoId, ticketId, horario1, horario2, horario3,fechaCoordinada,fechaSolucion,fechaCarga) VALUES('" +
                ddTecnico.SelectedValue + "','" + id + "','" + h1 + "','" + h2 + "','" + h3 + "'" + iCoordinada + iSolucion + iCarga + ")";
        }
        try
        {
            Boolean res = CMD.ExecuteNonQuery() < 1;
            if (res)
            {
                lblError.Text = "Error en la asignaci�n del ticket al t�cnico.";
            }
        }
        catch (Exception ex)
        {
            if (SQL.State != ConnectionState.Closed) SQL.Close();
            lblError.Text = "Error: " + ex.Message;
        }
        SQL.Close();
    }
    private bool guardarInformeTecnicoYEstado()
    {
        if (txtAgregar.Text.Trim() == "" && ddlNuevoEstado.SelectedValue == "0") return false; 
        Usuario usr = (Usuario)Session["Usuario"];
        TicketData tkt = getDataFromPantalla();
        SqlConnection sql = new SqlConnection();
        SqlCommand cmd = new SqlCommand();
        sql.ConnectionString = Funciones.getConnString();
        cmd.Connection = sql;
        try
        {
            sql.Open();
            cmd.CommandText = "Select InformeInterno FROM ticket where TicketId=" + tkt.idTicket;
            tkt.InformeInterno = cmd.ExecuteScalar().ToString() + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " (" + usr.Nick.ToString().Trim() + ")" + Environment.NewLine;

            if (ddlNuevoEstado.SelectedValue != "0")
            {
                tkt.InformeInterno += "Estado: " + ddlNuevoEstado.SelectedValue.Trim() + " �� " + ddlNuevoEstado.SelectedItem.Text.ToUpper().Trim() + Environment.NewLine;

                cmd.CommandText = "Update ticket SET estadoId = '" + ddlNuevoEstado.SelectedValue + "' WHERE TicketId=" + tkt.idTicket;
                cmd.ExecuteNonQuery();
            }
            cmd.CommandText = "Update ticket SET InformeInterno = '" + (tkt.InformeInterno + txtAgregar.Text.Trim() + Environment.NewLine).Replace("'", "''") + "' WHERE TicketId=" + tkt.idTicket;
            cmd.ExecuteNonQuery();
            sql.Close();
            return true;
        }
        catch (Exception e)
        {
            if (sql.State != ConnectionState.Closed) sql.Close();
            return false;
        }

    }
    private void registrarEspecialistayNegocio()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;
        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT EspecialistaId, EmpresaPartnerId FROM ticket WHERE ticketId=" + txtTicket.Text;
        RS = CMD.ExecuteReader();
        RS.Read();
        String esp = ddEspecialista.SelectedItem.Text.Split(']')[1];
        String neg = RS[1].ToString();
        Boolean bCambioEspecialista = RS[0].ToString() != ddEspecialista.SelectedValue;
        Boolean bCambioNegocio = RS[1].ToString() != ddEmpresaPartner.SelectedValue;
        if (bCambioEspecialista) { txtAgregar.Text += "Especialista: " + ddEspecialista.SelectedValue + "��" + ddEspecialista.SelectedItem.Text.Split(']')[1].TrimEnd() + Environment.NewLine; }
        if (bCambioNegocio) { txtAgregar.Text += "Negocio: " + ddEmpresaPartner.Text + "��" + ddEmpresaPartner.SelectedItem.Text.TrimEnd() + Environment.NewLine; }
        SQL.Close();

    }
    private void registrarHorarios()
    {
        if (lstHorarios.Visible && lstHorarios.Enabled)
        {
            System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
            System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
            System.Data.SqlClient.SqlDataReader RS;
            //NOS CONECTAMOS AL SERVIDOR SQL
            SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
            SQL.Open();
            //PREPARAMOS LA CONSULTA 
            CMD.Connection = SQL;
            CMD.CommandText = "SELECT horario1, horario2, horario3 FROM ticketTecnico WHERE ticketId=" + txtTicket.Text;
            RS = CMD.ExecuteReader();
            RS.Read();
            String h1 = "0";
            String h2 = "0";
            String h3 = "0";
            if (lstHorarios.Items[0].Selected) h1 = "1";
            if (lstHorarios.Items[1].Selected) h2 = "1";
            if (lstHorarios.Items[2].Selected) h3 = "1";
            String strHor = "";
            foreach (ListItem itm in lstHorarios.Items)
            {
                if (itm.Selected)
                {
                    if (strHor == "") { strHor += itm.Text; }
                    else { strHor += ", " + itm.Text; }
                }
            }
            strHor += ".\r\n";
            String res = "000";
            if (RS.HasRows) { res = RS[0].ToString() + RS[1].ToString() + RS[2].ToString(); }
            Boolean huboCambio = res.CompareTo(h1 + h2 + h3) != 0;
            if (huboCambio)
            {
                txtAgregar.Text += "Disponibilidad horaria: " + strHor;
            }
            RS.Close();
            SQL.Close();
        }
    }
    private void registrarTecnicos()
    {
        if (ddTecnico.Visible && ddTecnico.Enabled)
        {
            System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
            System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
            System.Data.SqlClient.SqlDataReader RS;
            //NOS CONECTAMOS AL SERVIDOR SQL
            SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
            SQL.Open();
            //PREPARAMOS LA CONSULTA 
            CMD.Connection = SQL;
            CMD.CommandText = "SELECT tecnicoId FROM ticketTecnico WHERE ticketId=" + txtTicket.Text;
            RS = CMD.ExecuteReader();
            RS.Read();
            String strTec = ddTecnico.SelectedItem.Text;
            String res = "0";
            if (RS.HasRows) { RS[0].ToString(); }
            Boolean huboCambio = res != ddTecnico.SelectedValue;
            if (huboCambio)
            {
                txtAgregar.Text += lblTecnico.Text + " asignado: " + strTec + "\r\n";
            }
            RS.Close();
            SQL.Close();
        }
    }
    private void registrarFechas()
    {
        if (RDTCarga.Visible && RDTCarga.Enabled && RDTCoordinada.Enabled)
        {
            System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
            System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
            System.Data.SqlClient.SqlDataReader RS;
            //NOS CONECTAMOS AL SERVIDOR SQL
            SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
            SQL.Open();
            //PREPARAMOS LA CONSULTA 
            CMD.Connection = SQL;
            CMD.CommandText = "SELECT fechaCarga,fechaSolucion,fechaCoordinada FROM tickettecnico WHERE ticketId=" + txtTicket.Text;
            RS = CMD.ExecuteReader();
            RS.Read();
            if (RS.HasRows)
            {
                String strRes = "01/01/1980 00:00:00";
                if (RDTCarga.SelectedDate != null)
                { strRes = DateTime.Parse(RDTCarga.SelectedDate.ToString()).ToString("dd/MM/yyyy HH:mm"); }
                String strSol = "01/01/1980 00:00:00";
                if (RDTSolucion.SelectedDate != null)
                { strSol = DateTime.Parse(RDTSolucion.SelectedDate.ToString()).ToString("dd/MM/yyyy HH:mm"); }
                String strCoor = "01/01/1980 00:00:00";
                if (RDTCoordinada.SelectedDate != null)
                { strCoor = DateTime.Parse(RDTCoordinada.SelectedDate.ToString()).ToString("dd/MM/yyyy HH:mm"); }

                String RSRes = "01/01/1980 00:00:00";
                if (!RS.IsDBNull(0)) { RSRes = RS[0].ToString(); }
                String RSSol = "01/01/1980 00:00:00";
                if (!RS.IsDBNull(1)) { RSSol = RS[1].ToString(); }
                String RSCoor = "01/01/1980 00:00:00";
                if (!RS.IsDBNull(2)) { RSCoor = RS[2].ToString(); }

                Boolean huboCambioRes = RSRes != strRes;
                Boolean huboCambioSol = RSSol != strSol;
                Boolean huboCambioCoor = RSCoor != strCoor;
                if (huboCambioCoor) txtAgregar.Text += "Fecha Coordinada: " + strCoor + "\r\n";
                if (huboCambioSol) txtAgregar.Text += "Fecha de Soluci�n: " + strSol + "\r\n";
                if (huboCambioRes) txtAgregar.Text += "Fecha de Carga de datos: " + strRes + "\r\n";
            }
            RS.Close();
            SQL.Close();
        }
    }

    private Boolean validarDatosOnsite() {
        string query = "Select isNULL(domicilio,'') +'|'+ isNull(localidad,'') +'|'+ isnull(Telefono1,'') +'|'+ isNull(Telefono2,'') +'|'+ isnull(zip,'') +'|'+ isnull(provinciaId,'') " +
                       "FROM viewCliente WHERE clienteId='" + txtCodCliente.Text + "'";
        String arrVal = getScalar(query).ToString();
        Boolean domOk = arrVal.Split('|')[0].Trim() != "";
        Boolean locOk = arrVal.Split('|')[1].Trim() != "";
        Boolean telOk = arrVal.Split('|')[2].Trim() != "";
        Boolean tel2Ok = esTelefonoValido(arrVal.Split('|')[2].Trim());
        Boolean celOk = arrVal.Split('|')[3].Trim() != "";
        Boolean cpOk = arrVal.Split('|')[4].Trim() != "";
        Boolean provTelOk = esCaracteristicaTelValida(arrVal.Split('|')[2].Trim(), arrVal.Split('|')[5].Trim());
        Boolean provTelOk2 = esCaracteristicaTelValida2(arrVal.Split('|')[2].Trim(), arrVal.Split('|')[5].Trim());
        String msg="";
        if (!domOk) { msg += "El Campo 'Domicilio' est� vac�o.<br>"; }
        if (!locOk) { msg += "El Campo 'Localidad' est� vac�o.<br>"; }
        if (!telOk) { msg += "El Campo 'Tel�fono' est� vac�o.<br>"; }
        if (!tel2Ok) { msg += "El valor ingresado en el Campo 'Tel�fono' es inv�lido (debe ser Num�rico, de 10 d�gitos).<br>"; }
        if (!provTelOk) { msg += "La caracter�stica del n�mero telef�nico corresponde a Buenos Aires y Cap.Fed.(AMBA), verifique la provincia seleccionada.<br>"; }
        if (!provTelOk2) { msg += "La caracter�stica del n�mero telef�nico no corresponde a Capital Federal, verifique la provincia seleccionada.<br>"; }
        if (!celOk) { msg += "El Campo 'Celular' est� vac�o.<br>"; }
        if (!cpOk) { msg += "El Campo 'C�digo Postal' est� vac�o.<br>"; }
        if (msg == "") { return true; }
        msg = "Los siguientes datos del CLIENTE, presentan problemas de validaci�n: " + msg;
        lblError.Text = msg;
        return false;
    }
//    private Boolean validarCampoSO() {        
//        Boolean ret = ddSistemaOperativo.SelectedValue == "-1" | arquitectura.SelectedValue == "" | (servicePack.Items.Count > 0 && servicePack.SelectedValue=="");
//        if (ret) lblError.Text = "Debe ingresar todos los datos del Sistema Operativo.";
//        return ret;
//}
    private Boolean validarCamposRecontacto()
    {
        if (!ddMotivoRecontacto.Enabled) return true;
        Boolean error = ddMotivoRecontacto.SelectedValue == "0" | dpFechaRecontactar.IsEmpty | dpFechaRecontactar.SelectedDate.Value.Hour==0 ;
        if (error) lblError.Text = "Debe indicar Fecha, Hora y motivo de recontacto.";
        return !error;
    }


    #endregion

    #region NOTIFICACIONES
    public void notificarReclamoOnsite(string destinatarios)
    {
        String tkt, estado, ani, fechaCreacion, localidad, area, detalle, informeTecnico;
        tkt = txtTicket.Text.Trim();
        estado = WebUtility.HtmlEncode(ddlNuevoEstado.SelectedItem.Text.Trim());
        ani = txtNroClaim.Text.Trim();
        fechaCreacion = WebUtility.HtmlEncode(txtFecha.Text.Trim());
        localidad = WebUtility.HtmlEncode(obtenerLocalidad());
        area = WebUtility.HtmlEncode(ddEmpresaPartner.SelectedItem.Text.Trim());
        detalle = WebUtility.HtmlEncode(txtDetalle.Text.Trim());
        informeTecnico = WebUtility.HtmlEncode(txtInformeInterno.Text.Trim()).Replace(Environment.NewLine, "</br>");

        String subject = tkt + " - " + estado + " - " + DateTime.Now.ToString() + " - " + area;
        String cuerpo =
        "<html>" +
        "<body><table style=\" " +
            "font-family: verdana,arial,sans-serif;" +
            "font-size:14px;" +
            "color:black;" +
            "border-width: 1px;" +
            "border-color: black;" +
            "border-collapse: collapse;" +
        "\">" +
        "<tr><td style=\"border:1px solid black; padding:3px;\" ><b>TKT:</b> " + tkt + "</td><td style=\"border:1px solid black; padding:3px;\" ><b>Estado:</b> " + estado + "</b></td></tr>" +
        "<tr><td style=\"border:1px solid black; padding:3px;\" ><b>ANI:</b> " + ani + "</td><td style=\"border:1px solid black; padding:3px;\" ><b>Fecha de Creaci&oacute;n:</b> " + fechaCreacion + "</td></tr>" +
        "<tr><td style=\"border:1px solid black; padding:3px;\" ><b>Localidad:</b> " + localidad + "</td><td style=\"border:1px solid black; padding:3px;\" ><b>Area:</b> " + area.ToUpper() + "</td></tr>" +
        "<tr><td style=\"border:1px solid black; padding:3px;\"  colspan=2><b>Detalle:</b></br>" + detalle + "</td></tr>" +
        "<tr><td style=\"border:1px solid black; padding:3px;\"  colspan=2><b>Informe t&eacute;cnico:</b></br>" + informeTecnico + "</td></tr>" +
        "</table></body></html>";
        FUNCIONES_MOD.mailmega2.enviamail("megarobot@megatech.la", destinatarios, "megarobot@megatech.la", subject, cuerpo);
    }
    private void notificarVisitaAnulada(TicketData unTicket)
    {
        String infoCliente = getScalar(
            "select ' Raz�nSocial: ' + CAST(razonSocial AS VARCHAR) + '</br>' + "
                       + "' ' + CAST(ISNULL(comunicacion1,'') AS VARCHAR) +' '+ CAST(ISNULL(telefono1,'') AS VARCHAR) + '</br>' + "
                       + "' ' + CAST(ISNULL(comunicacion2,'') AS VARCHAR) +' '+ CAST(ISNULL(telefono2,'') AS VARCHAR) + '</br>' + "
                       + "' ' + CAST(ISNULL(comunicacion3,'') AS VARCHAR) +' '+ CAST(ISNULL(telefono3,'') AS VARCHAR) + '</br>' + "
                       + "' Domicilio: ' + CAST(ISNULL(domicilio,'') AS VARCHAR)+ '</br>' + "
                       + "' C�digo Postal: ' + CAST(ISNULL(zip,'') AS VARCHAR)+ '</br>' + "
                       + "' Localidad: ' + CAST(ISNULL(localidad,'') AS VARCHAR)+ '</br>' + "
                       + "' Provincia: ' + CAST(ISNULL(vp.nombre,'') AS VARCHAR)+ '</br>' "
                       + "from viewcliente inner join viewProvincia vp on viewCliente.provinciaId=vp.provinciaId where clienteId=" + unTicket.idCliente).ToString();

        String cuerpo = "El ticket Nro: " + unTicket.idTicket + " ha pasado al estado 'VISITA ANULADA'." + "</br></br>" +
            "A continuaci�n se detallan los datos del cliente:" + "</br>" +
            " Cliente Nro: " + unTicket.idCliente + "</br>" +
            infoCliente + "</br>" + " Ubicaci�n: " + ddUbicacion.SelectedItem.Text + "</br>" + " Contacto: " +
            ddContacto.SelectedItem.Text + "</br> </br>";
        string grupoDest = "";
        if (esAreaCalle()) { grupoDest = "visitaAnuladaCalle"; } else { grupoDest = "visitaAnuladaCAS"; }
        enviarMail("megarobot@megatech.la", getEmails(grupoDest), "VISITA ANULADA: " + unTicket.idTicket, cuerpo);
    }
    #endregion

    #region FUNCIONES AUXILIARES Y OTROS
    
    private void habilitarInfoRecontacto()
    { 
        Boolean mostrar = debeHabilitarCamposRecontacto();
        dpFechaRecontactar.Enabled = mostrar;
        ddMotivoRecontacto.Enabled = mostrar;
        lblFechaRecontactar.Enabled = mostrar;
        lblMotivoRecontacto.Enabled = mostrar;
    }
    private Boolean debeHabilitarCamposRecontacto() {
        string res = Funciones.getScalar("SELECT dbo.mostrarInfoRecontacto(" + lblIdEstadoActual.Text.Trim() + "," + ddlNuevoEstado.SelectedValue + ")").ToString();
        return res!="0";
    }
    private Boolean esTelefonoValido(String tel) {
        String telFiltrado = tel.Trim().Replace("(","").Replace(")","").Replace("-","").Replace(" ","");
        if (telFiltrado.Length < 10) { return false; }
        foreach (char c in telFiltrado) { if (!"1234567890".Contains(c.ToString())) { return false; } }
        return true;
    }
    private Boolean esCaracteristicaTelValida(String tel,String provid)
    {
        String telFiltrado = tel.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
        if ((telFiltrado.StartsWith("11") || telFiltrado.StartsWith("011")) && (!"902.901".Contains(provid.Trim()))){ return false; }
        return true;
    }
    private Boolean esCaracteristicaTelValida2(String tel, String provid)
    {
        String telFiltrado = tel.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
        if (!(telFiltrado.StartsWith("11") || telFiltrado.StartsWith("011")) && ("901"==provid.Trim())) { return false; }
        return true;
    }
    private Boolean esAreaOnsite()//Retorna true si el area seleccionada es un onsite
    {
        Boolean ret;
        ret = ddEmpresaPartner.SelectedValue == "57" || ddEmpresaPartner.SelectedValue == "58";
        return ret;
    }
    private Boolean esAreaCalle()//Retorna true si el area seleccionada es onsite CALLE
    {
        Boolean ret;
        ret = ddEmpresaPartner.SelectedValue == "57";
        return ret;
    }
    private Boolean esAreaCAS()//Retorna true si el area seleccionada es onsite CAS
    {
        Boolean ret;
        ret = ddEmpresaPartner.SelectedValue == "58";
        return ret;
    }
    private Boolean areaEstadoCompatible()
    {
        return !((esAreaCalle() && esEstadoCAS()) || (esAreaCAS() && esEstadoCalle()));
    }
    private Boolean esEstadoOnsite()//Retorna TRUE si el NuevoEstado seleccionado(si es "ninguno" usa el actual ) es CAS O CALLE
    {
       // Boolean b = estado() == "117" || estado() == "135";
        Boolean b = estado() == "162";
        //return estado() == "117" || estado() == "135";
        return estado() == "162";
    }
    private Boolean esEstadoCAS()//Retorna TRUE si el NuevoEstado seleccionado(si es "ninguno" usa el actual ) es CAS
    {
        return estado() == "117";
    }
    private Boolean esEstadoCalle()//Retorna TRUE si el NuevoEstado seleccionado(si es "ninguno" usa el actual ) es Calle
    {
        return estado() == "135";
    }
    private String estado()
    {
        if (ddlNuevoEstado.SelectedValue != "0") return ddlNuevoEstado.SelectedValue;
        else return lblIdEstadoActual.Text;
    }
    // Se agrega funcion en la que se verifica si el Operador es de Nivel Basico
    private Boolean esOperador()
    {
        return esPerfil("18") || esPerfil("26") || esPerfil("32"); 

    }

    private Boolean esPerfil(String p)
    {
        Usuario usr = (Usuario)Session["Usuario"];
        return usr.Perfil == p;
    }//Retorna TRUE si el user logueado es del perfil "p"
    private String getArea()//Retorna GBA o NOGBA de acuerdo a la ubicacion de la localidad del cliente.
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        String res = "";
        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;
        CMD.CommandText = "select area from localidad l inner join viewcliente c on ltrim(rtrim(l.nombre)) like ltrim(rtrim(c.localidad)) where c.clienteid ='" + txtCodCliente.Text + "' AND c.provinciaId = l.provinciaId";
        Object objRes = CMD.ExecuteScalar();
        if (objRes != null) res = objRes.ToString();
        SQL.Close();
        return res;
    }
    private bool CequearEstado()
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();

        string sEstado = lblEstado.Text;

        sEstado = sEstado.Trim();

        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT Ultimo FROM estado WHERE Nombre = '" + sEstado + "'";
        object estadoDeCierre = CMD.ExecuteScalar();

        if (estadoDeCierre.ToString() == "1")
        {
            //SIENDO QUE EL TICKET YA SE ENCUENTRA CERRADO EL USUARIO NO POSEE PRIVILEGIOS PARA EDITARLO
            lblError.Text = "El ticket se encuentra cerrado.";
            SQL.Close();
            return true;
        }
        SQL.Close();
        return false;
    }
    private bool Encuestaok() //Metodo de consulta de encuesta 
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlCommand res = new System.Data.SqlClient.SqlCommand();
        int lret;


        SQL.ConnectionString = ConfigurationManager.ConnectionStrings["TicketDB"].ConnectionString;
        SQL.Open();
        CMD.Connection = SQL;
        CMD.CommandText = "select Count(*) from encuesta_val where ltrim(rtrim(ticketid)) = " + Request.QueryString["id"].ToString().Trim() + "";


        lret = Convert.ToInt32(CMD.ExecuteScalar());

        if ((lret > 0))
        {

            SQL.Close();
            return true;
        }
        else
        {
            SQL.Close();
            return false;
        }

    }
    private Boolean tieneTicketOnsite(String id)//Retorna TRUE en caso de que ya exista un onsite para el ticket actual
    {
        System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
        System.Data.SqlClient.SqlCommand CMD = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataReader RS;

        //NOS CONECTAMOS AL SERVIDOR SQL
        SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
        SQL.Open();
        //PREPARAMOS LA CONSULTA 
        CMD.Connection = SQL;
        CMD.CommandText = "SELECT ticketId FROM ticketTecnico WHERE ticketId='" + id + "'";
        RS = CMD.ExecuteReader();
        Boolean ret = RS.HasRows;
        RS.Close();
        SQL.Close();
        return ret;
    }
    private bool esEstadoCierre()
    {
        return Funciones.getScalar("SELECT count(*) FROM Estado WHERE ultimo=1 and estadoId=" + ddlNuevoEstado.SelectedValue).ToString() == "1";
    }
    private object getScalar(string query)
    {
        SqlConnection SQL = new SqlConnection();
        SqlCommand CMD = new SqlCommand();
        object RES = null;
        try
        {
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings["TicketDB"].ToString();
            CMD.Connection = SQL;
            CMD.CommandText = query;
            SQL.Open();
            RES = CMD.ExecuteScalar();
            SQL.Close();
            return RES;
        }
        catch (Exception ex)
        {
            if (SQL.State != ConnectionState.Closed)
            {
                SQL.Close();
            }
            throw ex;
        }
    }
    public void enviarMail(string _from, string _to, string _subject, string _msg)
    {
        FUNCIONES_MOD.mailmega2.enviamail(_from, _to, _from, _subject, _msg);
    }
    private string obtenerLocalidad()
    {
        return getScalar("SELECT RTRIM(localidad) FROM viewCliente WHERE RTRIM(clienteId)='" + txtCodCliente.Text.Trim() + "'").ToString();
    }
    public string getEmails(String cod)
    {
        return getScalar("SELECT destinatarios FROM notificaciones WHERE id='" + cod + "'").ToString();
    }
    #endregion
    
    private bool puedeForzarEstados() {
        object cant = Funciones.getScalar("SELECT count(*) FROM permiso where pantallaId=140 and perfilId=" + Session["perfil"]);
        return int.Parse(cant.ToString()) > 0;
    }

    protected void RadComboBox1_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        //String arqs = " AND 1=2";
        //Boolean bSelected = ddSistemaOperativo.SelectedValue != "-1";
        //chkSOModificado.Visible = bSelected;
        //lblArquitecturaSO.Visible = bSelected;
        //arquitectura.Visible = bSelected;
        //servicePack.Visible = bSelected;
        //if (bSelected)
        //{

        //    String arqsVal = Funciones.getScalar("Select arquitecturas from sistemaOperativo where id=" + ddSistemaOperativo.SelectedValue).ToString();
        //    if (arqsVal.Trim() != "") { arqs = " AND arquitecturaid IN (" + arqsVal + ")"; }
        //}
        //dsArquitectura.SelectCommand = "Select arquitecturaid, nombre From arquitecturaSO where 1=1" + arqs;
        //String sp = " AND 1=2";

        //if (ddSistemaOperativo.SelectedValue != "-1")
        //{
        //    String spVal = Funciones.getScalar("Select servicePacks from sistemaOperativo where id=" + ddSistemaOperativo.SelectedValue).ToString();
        //    if (spVal.Trim() != "") { sp = " AND servicePackId IN (" + spVal + ")"; }
        //}

        //dsServicePack.SelectCommand = "Select servicePackId, nombre From servicePackSO where 1=1" + sp;

        //arquitectura.DataBind();
        //servicePack.DataBind();

        //if (arquitectura.Items.Count == 1) { arquitectura.SelectedIndex = 0; }
        //if (servicePack.Items.Count == 1) { servicePack.SelectedIndex = 0; }
        
    }
    protected void imgBuscar_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {

    }
    protected void btn_reIngreso_Click(object sender, EventArgs e)
    {
        if (CequearEstado()) 
        {
            if (ValidarHoraReIngreso())
            {
                System.Data.SqlClient.SqlConnection SQL = new System.Data.SqlClient.SqlConnection();
                SQL.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ticketdb"].ToString();
                string qry = "update ticket set EstadoId=115 where ticketid='" + Request.QueryString["id"] + "'";
                string tkt = Request.QueryString["id"];
                SqlCommand SQLC;
                SQLC = new SqlCommand(qry, SQL);
                try
                {
                    SQL.Open();
                    int res = (int)SQLC.ExecuteNonQuery();
                    SQL.Close();
                    if (res > 0)
                    {
                        Response.Redirect("EditaTicket.aspx?id=" + tkt + "");

                    }
                    else
                    {
                        lblError.Text = "Error al actualizar el estado";
                    }

                }

                catch (Exception ex)
                {
                    SQL.Close();

                }

            }
            else { lblError.Text = "El ticket  excede las 48 hs";}


        }

    }

    public bool ValidarHoraReIngreso()
    {
       
        DateTime FechaTkt = DateTime.Parse(txtFecha.Text);
        Boolean  excede48hs= DateTime.Now > FechaTkt.AddHours(48);
        if(excede48hs)
        {
        return false;
        } 
        else
        { 
         return true;
        }
        
    
    }
}
