using System;
using System.Data;
using System.Web.UI.WebControls;
using App_Code.TicketsLib;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using Popups;
public partial class Tickets : System.Web.UI.Page
{
    private TicketData unTicket;
    log4net.ILog logger = log4net.LogManager.GetLogger("File");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Form["__EVENTTARGET"] == "popUpCli")
        { }
        Usuario usr = (Usuario)Session["Usuario"];
        if (usr == null)
        {
            Response.Redirect("Default.aspx?url=" + Request.Url.OriginalString);
        }
        registrarScripts();
        if (retCliente.Value != "")
        {
            txtCodCliente.Text = PopupMgr.LeerDato(retCliente, "clienteId");
            txtNroClaim.Text = PopupMgr.LeerDato(retCliente, "Ani");
            txtSerie.Text = PopupMgr.LeerDato(retCliente, "Ani");
            txtNomCliente.Text = PopupMgr.LeerDato(retCliente, "razonSocial", true);
            txtCodCliente_TextChanged(this, new EventArgs());

        }
        if (retEquipo.Value != "")
        {
            txtCodEquipo.Text = PopupMgr.LeerDato(retEquipo, "Codigo");
            txtEquipoId.Text = PopupMgr.LeerDato(retEquipo, "EquipoId", true);
            txtCodEquipo_TextChanged(this, new EventArgs());

        }


    
        if (!IsPostBack)
        {
            panelMensaje.Visible = false;
            panelTicket.Visible = true;

            loadEquipo(usr);
            loadEspecialistas(usr);
            loadPrioridad();


            txtFecha.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            txtFechaInicio.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            RadDatePicker1.SelectedDate = DateTime.Now;

            txtFechaInicio.Attributes.Add("onblur", "this.value = isDate(this.value, true);");
            txtFechaFin.Attributes.Add("onblur", "this.value = isDate(this.value, true);");
            
        }
        //loadScripts();

        lbltxtVendedor.Visible = false;
       
    }

    private void checkearHistorial()
    {
        if (txtClienteID.Text.Trim() != "")
        {
            btHistorial.Visible = (int)Funciones.getScalar("SELECT count(*) FROM viewHistorial WHERE clienteId='" + txtCodCliente.Text + "'") > 0;
            string query = "SELECT CASE WHEN count(*)>0 THEN max(CAST(RTRIM(ticketId) AS VARCHAR)) ELSE 'NO' END FROM ticket WHERE clienteId='" + txtCodCliente.Text + "' " +
                                "and estadoId IN (SELECT estadoId from estado where ultimo=0 and fechabaja is null)";

            String tktAbierto = Funciones.getScalar(query).ToString();
            if (tktAbierto != "NO") { 
                pTicketAbierto.Visible = true; lnkTicketAbierto.PostBackUrl = "EditaTicket.aspx?id=" + tktAbierto;
                Usuario usr = (Usuario)Session["Usuario"];
                if (usr.Perfil != "1" && usr.Perfil != "20" && usr.UsuarioId != "541" && usr.UsuarioId != "850") { btnSubmit.Visible = false; }
            }
            else { pTicketAbierto.Visible=false; btnSubmit.Visible=true;}
        }
        else { btHistorial.Visible = false; }
    }
    private void registrarScripts(){
        String strVer = "<script>" +
                       "function verCliente(){ window.open('verCliente.aspx?codcte= " + txtCodCliente.Text + "', 'VerCliente', 'titlebar=0,resizable=1,scrollbars=1,left=100,top=300,height=600,width=800,location=0,menubar=0,toolbar=0'); }" +
                       "</script>";

        Page.RegisterStartupScript("verCli", strVer);

                            String strCode1 = "<script> " +
        "function habilitar() " +
        "{ if (document.getElementById('ctl00_cphTicket_validSummary').innerHTML != '') {document.getElementById('ctl00_cphTicket_btnSubmit').disabled = false;}}" + Environment.NewLine +
        "</script>";
        Page.RegisterStartupScript("habilitarBoton", strCode1);

        String strCode2 = "<script> " +
        "function deshabilitar() " +
        "{ document.getElementById('ctl00_cphTicket_btnSubmit').setAttribute('disabled',true);" + Environment.NewLine +
        " var t= setTimeout(\"habilitar()\", 1000); " +
        " } </script>";
        Page.RegisterStartupScript("deshabilitarBoton", strCode2);
    }

    protected void txtCodCliente_TextChanged(object sender, EventArgs e)
    {
        if (txtCodCliente.Text.Trim() != "")
        {
            
            lnvercliente.Attributes.Add("OnClick","window.open('verCliente.aspx?codcte= " + txtCodCliente.Text + "', 'VerCliente', 'titlebar=0,resizable=1,scrollbars=1,left=100,top=300,height=600,width=800,location=0,menubar=0,toolbar=0');");
            lnvercliente.Attributes.Add("href", "#");
        }
        else {
            lnvercliente.Attributes.Remove("OnClick");
            lnvercliente.Attributes.Add("href", "#");
        }
        
                try
        {
            FiltroCliente fc = new FiltroCliente();
            fc.Codigo=txtCodCliente.Text;
            FiltroCliente fco = Clientes.getUnCliente(fc);
            if (fco == null)
            {
                lblError.Text = "No se pudo recuperar datos de ese cliente.";
            }
            else
            {
                txtClienteID.Text = fco.ClienteId;
                txtNomCliente.Text = fco.RazonSocial;
                txtNroClaim.Text = fco.Telefono1;
                txtSerie.Text = fco.Telefono1;
                
                checkearHistorial();
                abrirHistorial();

                int cant = loadUbicaciones(txtClienteID.Text);
                if (cant > 0)
                {
                    string Id = ddlUbicacion.Items[0].Value;
                    int qContactos = loadContactos(Id);
                }
                else
                {
                    lblError.Text = "No se recuperaron Ubicaciones para el cliente.";
                }

            }

        }
        catch (Exception ex)
        {
            lblError.Text = "Error: " + ex.Message;
        }

    }
    private void abrirHistorial() {
        
        String strHistorial = "<script>" +
        "window.open('verHistorial.aspx?id= " + txtClienteID.Text + "', 'Historial', 'titlebar=0,resizable=1,scrollbars=1,left=100,top=300,height=600,width=800,location=0,menubar=0,toolbar=0');" +
        "</script>";
        
        Page.RegisterClientScriptBlock("test", strHistorial);
    }
    protected void txtCodEquipo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            FiltroEquipo fc = new FiltroEquipo();
            fc.Codigo = txtCodEquipo.Text;
            FiltroEquipo fco = Equipo.getUnEquipo(fc);
            if (fco == null)
            {
                lblError.Text = "No se pudo recuperar datos de ese Equipo.";
            }
            else
            {
                txtEquipoId.Text = fco.EquipoId;
                txtNombreEquipo.Text = fco.Nombre;
                
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "Error: " + ex.Message;
        }
    }
    

    protected void ddlUbicacion_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try 
        {
            string Id = ddlUbicacion.SelectedValue;
            int qContactos = loadContactos(Id);
        }
        catch (Exception ex)
        {
            lblError.Text = "Error: " + ex.Message;
        }

    }
    private int loadUbicaciones(string ClienteID)
    {
        try
        {
            int ret = 0;
            DataSet ds = Clientes.getUbicacionesCliente(ClienteID);
            if (ds == null)
            {
                lblError.Text = "No se recuperaron Ubicaciones para el cliente.";
                ret = -1;
            }
            else
            {

                ddlUbicacion.Items.Clear();
                ret = ds.Tables[0].Rows.Count;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ListItem li = new ListItem(row["Nombre"].ToString(), row["Id"].ToString());
                    ddlUbicacion.Items.Add(li);
                }
            }
            return ret;
        }
        catch
        {
            return -1;
        }
    }
    private int loadContactos(string UbicacionId)
    {
        try
        {
            int ret = 0;
            DataSet ds = Clientes.getContactosUbicacion(UbicacionId);
            if (ds == null)
            {
                lblError.Text = "No se recuperaron Contactos para la Ubicaci�n.";
                ret = -1;
            }
            else
            {

                ddlContacto.Items.Clear();
                ret = ds.Tables[0].Rows.Count;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ListItem li = new ListItem(row["Nombre"].ToString(), row["Id"].ToString());
                    ddlContacto.Items.Add(li);
                }
            }
            return ret;
        }
        catch
        {
            return -1;
        }
    }

    private void loadEquipo(Usuario usr) {
        if (usr.Equipo == "") return;
        else
        {

            try
            {
                FiltroEquipo fc = new FiltroEquipo();
                fc.Codigo = usr.Equipo;
                FiltroEquipo fco = Equipo.getUnEquipo(fc);
                if (fco == null)
                {
                    lblError.Text = "No se pudo recuperar datos de ese Equipo.";
                }
                else
                {
                    txtEquipoId.Text = fco.EquipoId;
                    txtNombreEquipo.Text = fco.Nombre;
                    txtCodEquipo.Text = usr.Equipo;
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error: " + ex.Message;
            }

        }
    } // Si el usuario Tiene un perfil con equipo, este lo carga, sino retorna.

    private int loadEspecialistas(Usuario usr)
    {
        try
        {
            int ret = 0;
            DataSet ds = TablasCombo.getComboEspecialista(usr);
            if (ds == null)
            {
                lblError.Text = "No se recuperaron Especialistas.";
                ret = -1;
            }
            else
            {

                ddlEspecialista.Items.Clear();
                ret = ds.Tables[0].Rows.Count;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ListItem li = new ListItem(row["Nombre"].ToString(), row["Codigo"].ToString());
                    ddlEspecialista.Items.Add(li);
                }
            }
            return ret;
        }
        catch
        {
            return -1;
        }
    }
    private int loadPrioridad()
    {
        try
        {

            ddlPrioridad.Items.Clear();
            ListItem l1 = new ListItem("Alta", "Alta");
            ddlPrioridad.Items.Add(l1);
            ListItem l2 = new ListItem("Media", "Media");
            ddlPrioridad.Items.Add(l2);
            ListItem l3 = new ListItem("Baja", "Baja");
            ddlPrioridad.Items.Add(l3);
            return 3;
        }
        catch (Exception ex)
        {
            lblError.Text = "Error: " + ex.Message;
            return -1;
        }
    }
    private void loadScripts()
    {
        if (!ClientScript.IsClientScriptBlockRegistered("dlgNuevoCliente"))
        {
            string title = "Busqueda Cliente";
            string campoid = this.txtCodCliente.ClientID;
            string campodescrip = this.txtNomCliente.ClientID;
            string parameters = "&campoid=" + campoid + "&campodescrip=" + campodescrip;
            string height = "520";
            string width = "650";
            string page = "ClientesAltaPop.aspx";

            // use next line for direct with <base target="_self"> between <Head> and </HEAD> 
            string scrp = "<script>var rc = new Array(0,0);" +
                   "function doIt2(){rc=window.showModalDialog('" + page + "?modal=1&Title=" + title + "&page=" + page +
                   "&campoid=" + campoid + "&campodescrip=" + campodescrip + "','','dialogHeight:" + height +
                   " px;dialogWidth:" + width + " px;');" + "\n";

            scrp += "if(rc!=undefined && rc!=null) {if(rc[0]!=null){document.getElementById('" + txtCodCliente.ClientID + "').innerText=rc[0];" +
                "document.getElementById('" + txtNomCliente.ClientID + "').innerText=rc[1];" +
                "__doPostBack('','');}}}</script>";

            
            
            
            
            //Page.RegisterStartupScript("dlgBusqueda", scrp);
            ClientScript.RegisterStartupScript(this.GetType(), "dlgNuevoCliente", scrp);
            this.nuevocliente.Attributes.Add("onClick", "doIt2();");
        }

        if (!ClientScript.IsClientScriptBlockRegistered("dlgBusqueda"))
        {
            string title = "Busqueda Cliente";
            string campoid = this.txtCodCliente.ClientID;
            string campodescrip = this.txtNomCliente.ClientID;
            string parameters = "&campoid=" + campoid + "&campodescrip=" + campodescrip;
            string height = "420";
            string width = "560";
            string page = "busquedaCliente.aspx";

            // use next line for direct with <base target="_self"> between <Head> and </HEAD> 
            string scrp = "<script>var rc = new Array(0,0);" +
                   "function doIt(){rc=window.showModalDialog('" + page + "?Title=" + title + "&page=" + page +
                   "&campoid=" + campoid + "&campodescrip=" + campodescrip + "','','dialogHeight:" + height +
                   " px;dialogWidth:" + width + " px;');" + "\n";

            scrp += "if(rc!=undefined) {if(rc[0]!=null){document.getElementById('" + txtCodCliente.ClientID + "').innerText=rc[0];" +
                "document.getElementById('" + txtNomCliente.ClientID + "').innerText=rc[1];" +
                "__doPostBack('','');}}}</script>";

            //Page.RegisterStartupScript("dlgBusqueda", scrp);
            ClientScript.RegisterStartupScript(this.GetType(), "dlgBusqueda", scrp);
            this.imgBuscar.Attributes.Add("onClick", "doIt();");
        }

        if (!ClientScript.IsClientScriptBlockRegistered("dlgBusquedaEquipo"))
        {
            string title = "Busqueda Equipo";
            string campoid = this.txtCodEquipo.ClientID;
            string campodescrip = this.txtNombreEquipo.ClientID;
            string parameters = "&campoid=" + campoid + "&campodescrip=" + campodescrip;
            string height = "420";
            string width = "560";
            string page = "busquedaEquipo.aspx";

            // use next line for direct with <base target="_self"> between <Head> and </HEAD> 
            string scrp = "<script>var rc = new Array(0,0);" +
                   "function doItEquipo(){rc=window.showModalDialog('" + page + "?Title=" + title + "&page=" + page +
                   "&campoid=" + campoid + "&campodescrip=" + campodescrip + "','','dialogHeight:" + height +
                   " px;dialogWidth:" + width + " px;');" + "\n";

            scrp += "if(rc!=undefined) {if(rc[0]!=null){document.getElementById('" + txtCodEquipo.ClientID + "').innerText=rc[0];" +
                "document.getElementById('" + txtNombreEquipo.ClientID + "').innerText=rc[1];" +
                "__doPostBack('','');}}}</script>";

            //Page.RegisterStartupScript("dlgBusqueda", scrp);
            ClientScript.RegisterStartupScript(this.GetType(), "dlgBusquedaEquipo", scrp);
            this.imgBuscarEquipo.Attributes.Add("onClick", "doItEquipo();");
        }

     /*  string scrp2 = " <Script> function doIt2(){ var rc=window.open('ClientesAlta.aspx', '', 'scrollbars=yes,height=700px,width=700px,resizable=yes' ); } </Script> ";


            if (!ClientScript.IsClientScriptBlockRegistered("doIt2"))
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "doIt2", scrp2);

            
            this.nuevocliente.Attributes.Add("onClick", "doIt2();");
        }
        */
    }

    public bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;

        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        return isNum;
    }

    private Boolean validarCliente()
    {
        SqlConnection SQL = new SqlConnection();
        SqlCommand CMD = new SqlCommand();
        SQL.ConnectionString = ConfigurationManager.ConnectionStrings["TicketDB"].ToString();
        CMD.Connection = SQL;
        SQL.Open();
        CMD.CommandText =
            "SELECT count(*) " +
            "FROM viewcliente " +
            "WHERE  Telefono1 IS NOT NULL AND " +
                    "Telefono2 IS NOT NULL AND " +
                    "ISNUMERIC(Telefono1)=1 AND " +
                    "LEN(Telefono1)>9 AND " +
                    "ISNUMERIC(Telefono2)=1 AND " +
                    "LEN(Telefono2)>9 AND " +
                    "RTRIM(RazonSocial) IS NOT NULL ";

        CMD.CommandText += "AND clienteId=" + txtCodCliente.Text.Trim();
        long res = Int64.Parse(CMD.ExecuteScalar().ToString());
        SQL.Close();
        return res > 0;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string msg ="";
        if (!validarCampos(ref msg)) { lblError.Text = msg; return; }
        if (!validarCliente()) { lblError.Text = "Verifique que los siguientes datos del CLIENTE sean v�lidos y no esten vac�os: Raz�n Social, Telefono y Celular.(en los campos 'celular' y 'telefono' debe ingresar un n�mero de al menos 10 d�gitos) "; return; } 
        try 
        {
            bool bOk = true;
            unTicket = getDataFromPanatalla();
            if (unTicket.nroClaim.Trim().Length == 0)
            {
                bOk = false;
                lblError.Text = "El campo Nro.Claim est� vacio.";
                return;
            }

            if (!Funciones.isDateTimeNull(unTicket.fechaFin))
            {
                if (unTicket.fechaFin < unTicket.fecha)
                {
                    bOk = false;
                    lblError.Text = "Fecha Fin debe ser mayor a Inicio.";
                }
            }
            if (bOk)
            {
                generoTicket();
            }
        }
        catch (Exception ex) 
        {
            logger.Error("Error en Ingresar_Click: Original: " + ex.Message, ex);
            lblError.Text = ex.Message;
        }
    }

    private bool validarCampos(ref string msg)
    {
        if (txtCodCliente.Text.Trim() == "") msg += "Debe seleccionar un cliente. <br>";
        if (txtCodEquipo.Text.Trim() == "") msg += "Debe seleccionar un Equipo. <br>";
        if (txtSerie.Text.Trim() == "") msg += "Debe ingresar el n�mero de serie. <br>";
        //if (infoSOInvalida()) msg += "Debe ingresar todos los datos del Sistema Operativo.";
        if (aniBloqueado(txtCodCliente.Text.Trim())) msg += "El numero telef�nico del cliente seleccionado est� bloqueado para la creaci�n de tickets.";
        return msg == "";
    }

    private bool aniBloqueado(string clienteId)
    {
        return (int)Funciones.getScalar("SELECT count(*) FROM aniBloqueado ab WHERE EXISTS(SELECT * FROM viewCliente vc WHERE ltrim(rtrim(vc.telefono1)) like ab.ani  and rtrim(vc.clienteId)=rtrim('" + clienteId.ToString() + "'))") > 0;
    }

    //private Boolean infoSOInvalida()
    //{
    //    return ddSistemaOperativo.SelectedValue == "-1" | arquitectura.SelectedValue == "" | (servicePack.Items.Count > 0 && servicePack.SelectedValue == "");
    //}

    private void generoTicket()
    {
        //try
        //{
            unTicket = getDataFromPanatalla();
            unTicket = App_Code.TicketsLib.Tickets.InsertTicket(unTicket);

            if (unTicket.mensajeError.Trim().Length == 0)
            {
                lblMensajePanel.Text = "Se ha generado el ticket: " + unTicket.idTicket.ToString();
                panelMensaje.Visible = true;
                panelTicket.Visible = false;
                Response.Redirect("EditaTicket.aspx?id=" + unTicket.idTicket.ToString());
            }
            else
            {
                lblError.Text = "Ticket no generado: " + unTicket.mensajeError;
            }
        //}
        //catch (Exception ex)
        //{
        //    logger.Error("Error en generoTicket: " + ex.Message, ex);
        //    lblError.Text = "Error en generoTicket: " + ex.Message;
        //}
    }
    private TicketData getDataFromPanatalla()
    {
        TicketData unTicket = new TicketData();
        //try
        //{
            
            unTicket.idTicket = 0;
            if (txtFechaFin.Text.Trim().Length>0)
                unTicket.fechaFin = Funciones.DateParse(txtFechaFin.Text);

            unTicket.fecha = Funciones.DateParse(txtFecha.Text);
            unTicket.fechaInicio = Funciones.DateParse(txtFechaInicio.Text);
            unTicket.idEmpresa = int.Parse(Config.getValue("EmpresaId"));
            unTicket.idCliente = txtClienteID.Text;
            unTicket.idEspecialista = int.Parse(ddlEspecialista.SelectedValue);
            unTicket.idArticulo = int.Parse(txtEquipoId.Text);
            unTicket.idEstado = int.Parse(ddlEstado.SelectedValue);
            unTicket.idEmpresaPartner = int.Parse(ddlNegocio.SelectedValue);
            unTicket.Prioridad = ddlPrioridad.SelectedValue;
            unTicket.NroSerie = txtSerie.Text;
            unTicket.Problema = txtDetalle.Text;
            unTicket.nroClaim = txtNroClaim.Text;
            if (txtFechaSol.Text.Trim().Length > 0)
            {
                unTicket.FechaEstimada = Funciones.DateParse(txtFechaSol.Text);
            }
            else
                unTicket.FechaEstimada = Funciones.dateNull();
            try
            {
                Usuario u = (Usuario)Session["Usuario"];
                unTicket.idUsuario = int.Parse(u.UsuarioId);
            }
            catch
            {
                unTicket.idUsuario = 0;
            }
            unTicket.idUbicacionContacto = int.Parse(ddlContacto.SelectedValue);
            unTicket.idTipoProblema = int.Parse(ddlTipoProblema.SelectedValue);
            int idVendedor=0;
            if (ddlVendedor.SelectedValue.Length == 0)
                idVendedor = 0;
            else
                idVendedor = int.Parse(ddlVendedor.SelectedValue);

            unTicket.idVendedor = idVendedor.ToString();
        /*}
        catch (Exception ex)
        {
            lblError.Text = "[Error de conversi�n] Original: " + ex.Message;
            logger.Error("Error al tomar de pantalla", ex);
            
        }*/
            //if (ddSistemaOperativo.SelectedValue != "-1")
            //{
            //    unTicket.sistemaOperativoId = int.Parse(ddSistemaOperativo.SelectedValue);
            //    unTicket.arquitecturaId = int.Parse(arquitectura.SelectedValue);
            //    if (servicePack.SelectedValue != "") unTicket.servicePackId = int.Parse(servicePack.SelectedValue);
            //    unTicket.modificado = chkSOModificado.Checked;
            //}
        return unTicket;
    }
    protected void odsEstado_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

    }
    protected void nuevocliente_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
       
    }


   protected void ddlNegocio_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
   protected void RadDatePicker1_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
   {
       txtFechaInicio.Text = RadDatePicker1.SelectedDate.ToString();
   }
   protected void RadDatePicker2_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
   {
       txtFechaFin.Text = RadDatePicker2.SelectedDate.ToString();
   }
   protected void RadDatePicker3_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
   {
       txtFechaSol.Text = RadDatePicker3.SelectedDate.ToString();
   }
    protected void btHistorial_Click(object sender, System.Web.UI.ImageClickEventArgs e)
   {
       txtCodCliente_TextChanged(this, new EventArgs());
   }
   protected void RadComboBox1_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
   {
   //    String arqs = " AND 1=2";
   //    Boolean bSelected = ddSistemaOperativo.SelectedValue != "-1";
   //     chkSOModificado.Visible= bSelected;
   //     lblArquitecturaSO.Visible=bSelected;
   //     arquitectura.Visible = bSelected;
   //     servicePack.Visible = bSelected;
   //    if (bSelected) { 

   //    String arqsVal = Funciones.getScalar("Select arquitecturas from sistemaOperativo where id=" + ddSistemaOperativo.SelectedValue).ToString();
   //    if (arqsVal.Trim() != "") { arqs = " AND arquitecturaid IN (" + arqsVal + ")"; }
   //    }
   //    dsArquitectura.SelectCommand = "Select arquitecturaid, nombre From arquitecturaSO where 1=1" + arqs;
   //    String sp = " AND 1=2";
       
   //    if (ddSistemaOperativo.SelectedValue != "-1")
   //    {
   //        String spVal = Funciones.getScalar("Select servicePacks from sistemaOperativo where id=" + ddSistemaOperativo.SelectedValue).ToString();
   //        if (spVal.Trim() != "") { sp = " AND servicePackId IN (" + spVal + ")"; }
   //    }
       
   //    dsServicePack.SelectCommand = "Select servicePackId, nombre From servicePackSO where 1=1" + sp;

   //    arquitectura.DataBind();
   //    servicePack.DataBind();

   //    if (arquitectura.Items.Count == 1) { arquitectura.SelectedIndex = 0; }
   //    if (servicePack.Items.Count == 1) { servicePack.SelectedIndex = 0; }
   }
}
