﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BuscarEquipo.aspx.vb" Inherits="BuscarEquipo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>BUSQUEDA DE EQUIPO</title>
        <link href="css/overlay.css" rel="stylesheet" />
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/overlay.js"></script>
    <script src="js/PopupMgr.js"></script>
</head>
<body>
       

     <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div style="min-width:700px;">
    <div style="height:60px;">
        <table style="margin:auto;"><tr>
            <td>
               <telerik:RadTextBox ID="txtBusqueda" runat="server" EmptyMessage="Buscar..." Font-Bold="False" Font-Size="14px" Height="24px" Width="250px" AutoPostBack="True" LabelWidth="81px" ClientIDMode="Static"></telerik:RadTextBox>
            </td>
            <td style="text-align:center;">
                <asp:ImageButton ID="imgBuscar" runat="server" ImageUrl="~/Images/search2.png" ClientIDMode="Static" />

            </td>
            <td style="text-align:center;"><asp:RadioButtonList ID="tipo" runat="server" RepeatDirection="Horizontal" Font-Bold="True" Font-Size="14px" ForeColor="#333333">
            <asp:ListItem Value="todo" Selected="True">Todo</asp:ListItem>
            <asp:ListItem Value="EquipoId">Código</asp:ListItem>
            <asp:ListItem Value="Nombre">Nombre</asp:ListItem>
        </asp:RadioButtonList>
            </td></tr></table>
    </div>
    <div style="overflow-y:auto;">
    
        <telerik:RadGrid ID="gridResultados" runat="server" CellSpacing="0" DataSourceID="dsEquipos" GridLines="None" Culture="es-ES">
            <ExportSettings>
                <Pdf PageWidth="" />
            </ExportSettings>
<MasterTableView AutoGenerateColumns="False" DataSourceID="dsEquipos">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="select" FilterControlAltText="Filter column column" Text="-" UniqueName="column" HeaderStyle-Width="40px">
<HeaderStyle Width="40px"></HeaderStyle>
        </telerik:GridButtonColumn>
        <telerik:GridBoundColumn DataField="EquipoId" FilterControlAltText="Filter EquipoId column" HeaderText="ID" SortExpression="EquipoId" UniqueName="EquipoId" HeaderStyle-Width="50px">
<HeaderStyle Width="80px"></HeaderStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Codigo" FilterControlAltText="Filter Codigo column" HeaderText="Código" SortExpression="Codigo" UniqueName="Codigo" HeaderStyle-Width="70px">
<HeaderStyle Width="160px"></HeaderStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter Nombre column" HeaderText="Descripción" SortExpression="Nombre" UniqueName="Nombre">
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
</MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsEquipos" runat="server" ConnectionString="<%$ ConnectionStrings:TicketDB %>" ></asp:SqlDataSource>
    </div>
    </div>
    </form>
</body>
</html>
