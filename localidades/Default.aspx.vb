﻿Imports Telerik.Web.UI

Partial Class _Default
    Inherits System.Web.UI.Page
    Private SQL As New Data.SqlClient.SqlConnection
    Private CMD As New Data.SqlClient.SqlCommand
    Private RS As Data.SqlClient.SqlDataReader

    Protected Sub txtBusqueda_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        actualizarGridView()
    End Sub

    Private Sub actualizarGridView()
        SQL.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDBLoc").ToString
        CMD.Connection = SQL
        SQL.Open()

        'verificamos la cantidad de partidos con el nombre especificado
        CMD.CommandText = "SELECT count(*) FROM partido WHERE nombre like '%" & txtBusqueda.Text & "%'"
        Dim cant = CMD.ExecuteScalar()

        CMD.CommandText = "SELECT count(*) FROM localidad WHERE nombre LIKE '%" & txtBusqueda.Text & "%'"
        Dim loccant As Long = CMD.ExecuteScalar

        Dim qSelectFrom As String = "select pr.Nombre Provincia, pa.nombre as Partido, l.Nombre as Localidad, " & _
            "CASE WHEN l.area='NOGBA' THEN 'CAS' ELSE 'CALLE' END as Area from localidad l LEFT JOIN partido pa ON l.partidoId=pa.id LEFT JOIN	viewProvincia pr ON pr.provinciaId=l.provinciaId WHERE 1=1 "
        Dim qWhere As String = ""

        dsGridView.ConnectionString = ConfigurationManager.ConnectionStrings("ticketDBLoc").ToString

        If cant > 0 Then
            LblAdicional.Text = "Seleccione la localidad dentro del partido de " & UCase(txtBusqueda.Text)
            LblAdicional.Font.Size = WebControls.FontUnit.Large
            LblAdicional.ForeColor = Drawing.Color.Red
            LblAdicional.Font.Bold = True
            LblAdicional.Visible = True
            If Not txtBusqueda.Text.Trim = "" Then qWhere = "AND (pa.nombre like '%" & txtBusqueda.Text & "%')"
        Else
            'AL NO HABER PARTIDOS VERIFICAMOS SI AL MENOS EXISTE UNA LOCALIDAD
            If loccant <= 0 Then
                'LA BUSQUEDA NO HA PRODUCIDO RESULTADOS
                LblAdicional.Text = "No se han producido resultados para la busqueda " & Chr(34) & UCase(txtBusqueda.Text) & Chr(34)
                LblAdicional.Font.Size = WebControls.FontUnit.Large
                LblAdicional.ForeColor = Drawing.Color.Red
                LblAdicional.Font.Bold = True
                LblAdicional.Visible = True
                Exit Sub
            End If
            LblAdicional.Text = "Solo hay una localidad para la busqueda " & Chr(34) & UCase(txtBusqueda.Text) & Chr(34)
            LblAdicional.Font.Size = WebControls.FontUnit.Large
            LblAdicional.ForeColor = Drawing.Color.LawnGreen
            LblAdicional.Font.Bold = True
            LblAdicional.Visible = True
            If Not txtBusqueda.Text.Trim = "" Then qWhere = "AND (l.nombre like '%" & txtBusqueda.Text & "%')"
        End If
        dsGridView.SelectCommand = qSelectFrom & qWhere & " ORDER BY pr.nombre"
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.DataBound
        If GridView1.Columns.Count > 2 Then
            GridView1.Columns(2).ItemStyle.Font.Bold = True
        End If

    End Sub
End Class
