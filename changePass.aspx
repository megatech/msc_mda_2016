﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="changePass.aspx.cs" Inherits="qlikview_changePass" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 128px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h3 style="width:100%; text-align:center;">Cambio de password - TASA</h3>
        <table align="center">
            <tr><td class="auto-style1">Password Actual: </td><td><asp:TextBox runat="server" ID="txtPass" TextMode="Password"></asp:TextBox></td></tr>
            <tr><td class="auto-style1">Nuevo Password: </td><td><asp:TextBox runat="server" ID="txtNewPass" TextMode="Password"></asp:TextBox></td></tr>
            <tr><td class="auto-style1">Confirmar Password: </td><td><asp:TextBox runat="server" ID="txtConf" TextMode="Password"></asp:TextBox></td></tr>
            <tr><td colspan="2" style="text-align: center"><asp:Label runat="server" ID="lblStatus" Font-Bold="True" ForeColor="#2B63A8" /></td></tr>
            <tr><td colspan="2" style="text-align: center"><asp:Button runat="server" ID="btCambiarPass" Text="Cambiar Password" OnClick="btCambiarPass_Click" /></td></tr>
        </table>
    
    </div>
    </form>
</body>
</html>
