﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class verCoachigUsuario
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "ver" Then
            Response.Redirect("vercoachNuevo.aspx?coachingid=" & GridView1.Rows(e.CommandArgument).Cells(0).Text)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("usuario") Is Nothing Then Response.Redirect("default.aspx")
        Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
        If Request.QueryString("id") = "" Then Response.Redirect("verCoaching.aspx")
        If Not permisoSobreElUsuario(Request.QueryString("id")) Then
            mostrarError("No posee permisos para ver coachings de este usuario.")
        End If

        If noTieneCoachings() Then mostrarError("No existen coachings registrados de este usuario.")
        If Not panelData.Visible Then Exit Sub
        lblUserName.Text = getUser(Request.QueryString("id"))
        If Not IsPostBack() Then actualizarDrop()
    End Sub

    Private Function permisoSobreElUsuario(ByVal id As String) As Boolean
        Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
        Return u.Perfil = "1" OrElse u.Perfil = "20" OrElse u.Perfil = "17" OrElse u.UsuarioId = Request.QueryString("id")
    End Function


    Private Sub mostrarError(ByVal msg As String)
        lblError.Text = msg
        lblError.Visible = True
        panelData.Visible = False
    End Sub


    Private Sub actualizarGrid()
        Dim id = Request.QueryString("id")
        SqlDataSource1.SelectCommand =
        "SELECT MIN(Month(fecha)) as nmes," & _
         "   MIN(Case Month(fecha) " & _
          "  WHEN 1 THEN 'Enero' " & _
           " WHEN 2 THEN 'Febrero' " & _
           " WHEN 3 THEN 'Marzo' " & _
           " WHEN 4 THEN 'Abril' " & _
           " WHEN 5 THEN 'Mayo' " & _
           " WHEN 6 THEN 'Junio' " & _
           " WHEN 7 THEN 'Julio' " & _
           " WHEN 8 THEN 'Agosto' " & _
           " WHEN 9 THEN 'Septiembre' " & _
           " WHEN 10 THEN 'Octubre' " & _
           " WHEN 11 THEN 'Noviembre' " & _
           " WHEN 12 THEN 'Diciembre' " & _
           " END) as Mes " & _
        "FROM coachingNuevo WHERE userId = " & id & _
        " AND YEAR(fecha) = " & ddAnio.SelectedValue() & " GROUP BY MONTH(fecha) ORDER BY nMes"
        GridView1.DataBind()
    End Sub

    Private Sub actualizarDrop()
        Dim id = Request.QueryString("id")
        SqlDataSource2.SelectCommand = _
"SELECT DISTINCT YEAR(fecha) as texto, CAST(YEAR(fecha) as int) as anio FROM coachingNuevo WHERE userId = " & id & " Order By Anio Desc"
        ddAnio.DataBind()
    End Sub

    Protected Sub ddAnio_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddAnio.DataBound
        actualizarGrid()
    End Sub

    Protected Sub ddAnio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddAnio.SelectedIndexChanged
        actualizarGrid()
    End Sub

    Private Function noTieneCoachings() As Boolean
        Dim id = Request.QueryString("id")
        Dim SQL As New SqlConnection(SqlDataSource1.ConnectionString)
        Dim CMD As New SqlCommand("Select count(*) from coachingNuevo where userId=" & id, SQL)
        SQL.Open()
        Dim res = CMD.ExecuteScalar() = 0
        SQL.Close()
        Return res
    End Function
    Private Function getUser(ByVal id As String) As String
        Dim SQL As New SqlConnection(SqlDataSource1.ConnectionString)
        Dim CMD As New SqlCommand("Select nombreCompleto from usuario where usuarioId=" & id, SQL)
        SQL.Open()
        Dim res = CMD.ExecuteScalar()
        SQL.Close()
        Return res
    End Function

    Public Sub GridView1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.DataBound
        Dim uid = Request.QueryString("id")
        Dim SQL As New SqlConnection(SqlDataSource1.ConnectionString)
        Dim CMD As New SqlCommand("", SQL)
        Dim RS As SqlDataReader
        SQL.Open()
        For Each r As GridViewRow In GridView1.Rows
            CMD.CommandText = "SELECT id,fecha, CASE WHEN id IN (SELECT id FROM viewCoachingsCriticos) THEN 1 ELSE 2 END AS ec FROM coachingNuevo " & _
                    "WHERE Month(fecha) = " & nroMes(r.Cells(0).Text) & " And userId = " & uid & " And Year(fecha)=" & ddAnio.SelectedValue
            RS = CMD.ExecuteReader()
            Dim cnt As Long = 0
            While RS.Read
                cnt += 1
                Dim bt = New Button
                bt.Text = RS("fecha").ToString.Substring(0, 5)
                bt.Font.Bold = True
                bt.ForeColor = IIf(RS("ec") = 2, Drawing.Color.DarkGreen, Drawing.Color.DarkRed)
                bt.PostBackUrl = "vercoachNuevo.aspx?coachingid=" & RS("id")
                r.Cells(RS("ec")).Controls.Add(bt)
            End While
            RS.Close()
        Next
        SQL.Close()
    End Sub

    Private Function nroMes(ByVal str As String) As Long
        If str = "Enero" Then Return 1
        If str = "Febrero" Then Return 2
        If str = "Marzo" Then Return 3
        If str = "Abril" Then Return 4
        If str = "Mayo" Then Return 5
        If str = "Junio" Then Return 6
        If str = "Julio" Then Return 7
        If str = "Agosto" Then Return 8
        If str = "Septiembre" Then Return 9
        If str = "Octubre" Then Return 10
        If str = "Noviembre" Then Return 11
        If str = "Diciembre" Then Return 12
        Return 0
    End Function




End Class
