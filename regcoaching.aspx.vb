﻿Imports App_Code.TicketsLib.Usuario
Imports System.Data.SqlClient
Imports System.Data
Imports Telerik.Web.UI

Partial Class regcoaching
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)

        If Not (u.Perfil = "17" Or u.Perfil = "20" Or u.Perfil = "1") Then Response.Redirect("Default2.aspx")

    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "1" Then
            Response.Redirect("cargacoachingNuevo.aspx?usuarioid=" & GridView1.Rows(e.CommandArgument).Cells(0).Text)
        End If
    End Sub

    Private Function getScalar(ByVal query As String) As Object
        Dim SQL As New SqlConnection
        Dim CMD As New SqlCommand
        Dim RES As Object
        Try
            SQL.ConnectionString = ConfigurationManager.ConnectionStrings("TicketDB").ToString
            CMD.Connection = SQL
            CMD.CommandText = query
            SQL.Open()
            RES = CMD.ExecuteScalar()
            SQL.Close()
            Return RES
        Catch ex As Exception
            If SQL.State <> ConnectionState.Closed Then
                SQL.Close()
            End If
            Throw ex
        End Try
    End Function

    Protected Sub GridView1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.DataBound
        For Each r As GridViewRow In GridView1.Rows
            Dim n As Long = r.Cells(2).Text
            If n > 2 Then
                Dim bt = TryCast(r.Cells(3).Controls(0), Button)
                'bt.Enabled = False
                r.ToolTip = "Este usuario posee demasiados coachings con errores críticos."
                r.ForeColor = Drawing.Color.DarkRed
                r.Cells(2).Font.Bold = True
                Label2.Visible = True
            End If
        Next
    End Sub

    Protected Sub ddLider_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddLider.SelectedIndexChanged
        GridView1.DataBind()
    End Sub

    Protected Sub ddLider_DataBound(sender As Object, e As EventArgs) Handles ddLider.DataBound
        Dim u As App_Code.TicketsLib.Usuario = TryCast(Session("usuario"), App_Code.TicketsLib.Usuario)
        If Not IsPostBack Then
            Dim itm = ddLider.FindItemByValue(u.UsuarioId)

            If Not itm Is Nothing Then
                ddLider.SelectedValue = u.UsuarioId
                GridView1.DataBind()
            End If
        End If
    End Sub
End Class
